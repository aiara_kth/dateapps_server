var cluster = require('cluster');
var numCPUs = require('os').cpus().length;

if (cluster.isMaster) {
  require('./lib/migrations').default().then(() => {
    // Fork workers. Don't fork in single core to save RAM
    for (var i = 0; i < numCPUs - 1; i++) {
      cluster.fork({
        SERVER_NO_FORK: true,
      });
    }

    cluster.on('exit', function(worker) {
      console.log('worker ' + worker.process.pid + ' died');
    });

    // Run worker
    require('./lib/worker').default();
    // Also, run to index to save RAM
    require('./lib');
  }).catch((e) => {
    console.error(e.stack);
    process.exit(1);
  });
} else {
  // If slave, simply link to index
  require('./lib');
}
