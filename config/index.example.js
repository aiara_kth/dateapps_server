'use strict';

const path = require('path');

module.exports = {
  network: {
    host: '0.0.0.0',
    port: 8000,
    // HTTPS is not enabled by default, however, you can enable it by
    // filling the data below. You may have to specify SSL chain other than
    // certificate and the key, however.
    // https: { cert: fs.readFileSync(...), key: fs.readFileSync(...) },
  },
  db: {
    host: 'localhost',
    dialect: 'sqlite',
    storage: './db.sqlite',
    uri: 'sqlite://localhost/database',
  },
  log: {
    stdout: 'dev',
    // By default the server will store HTTP logs. Delete this if that's not
    // required.
    access: {
      directory: 'logs',
      filename: 'access.log',
      format: 'combined',
    },
  },
  auth: {
    // Specify the session secret key to use.
    sessionSecret: 'pen_pineapple!authentication@protocol@@',
    // Specify bcrypt level to use.
    bcrypt: 10,
    deleteKey: '###',
  },
  upload: {
    directory: 'uploads',
    directoryPublic: '/uploads',
    url: 'https://api.estickerapp.com',
    temp: '/tmp/',
  },
  firebase: {
    credential: require('./firebase.json'),
    databaseURL: 'https://somewhere.com',
    // Firebase Cloud Messaging Key
    fcmKey: '...',
  },
  sms: {
    userId: '...',
    secureKey: '...',
    testFlag: 'Y',
    sendPhone: '010-0000-0000',
  },
  payment: path.resolve(__dirname, 'payment.json'),
  lounge: {
    average: 6,
    count: 3,
    time: 10 * 60 * 1000,
  },
  suggestion: {
    count: 5,
    expire: 4 * 24 * 60 * 60 * 1000,
    target: 7 * 24 * 60 * 60 * 1000,
  },
  iap: {
    googlePublicKeyPath: __dirname,
  },
  appVersion: {
    android: 'com.estickerapp.android',
  },
  s3: {
    enabled: true,
    bucket: '###',
    accessKeyId: '###',
    secretAccessKey: '###',
    signatureVersion: 'v4',
    region: 'ap-northeast-2',
  },
};

// To use in Docker environment, you can specify config folder in environment
// variable.
if (process.env.CONFIG_PATH != null) {
  module.exports = require(path.resolve(__dirname, process.env.CONFIG_PATH));
}
