# REST 설계 문서
REST API를 어떻게 설계할지 적어놓은 문서입니다.

## 사용자 인증

### 핸드폰 인증
`POST /session/phone`

### 회원가입
`POST /session/register`

### 로그인
`POST /session`

### 로그아웃
`DELETE /session`

### 비밀번호 변경
`PATCH /session/password`

### 비밀번호 찾기
`POST /session/find/password`

### 계정 찾기
`POST /session/find/email`

## 사용자

### 사용자 목록 조회 (관리자)
`GET /users`

### 사용자 검색 (라운지)
`GET /users`

### 내 프로필 조회
`GET /user`

#### 프로필 사진 업로드
`POST /user/photos`

#### 프로필 사진 전부 삭제
`DELETE /user/photos`

#### 프로필 사진 삭제
`DELETE /user/photos/:id`

#### 프로필 설정
`PATCH /user`

#### 계정 비활성화
`DELETE /user`

#### 나에게 관심 있는 유저 조회
`GET /user/followers`

#### 내가 관심 있는 유저 조회
`GET /user/following`

#### 가입한 스티커 조회
`GET /user/stickers`
`GET /users/:id/stickers`

#### 참가한 모임 조회
`GET /user/meetings`
`GET /users/:id/meetings`

### 다른 사람 프로필 조회
`GET /users/:id`

#### 관심 표현
`POST /users/:id/followers`

#### 관심 표현 취소
`DELETE /users/:id/followers`

#### 신고
`POST /users/:id/report`

#### 채팅 신청
`POST /users/:id/chat`

#### 별점
`POST /users/:id/rating`

## 알림

### 알림 조회
`GET /notifications`

### 알림 삭제
`DELETE /notifications`

## 공지사항 / 질문과 답변

### 공지사항 목록
`GET /notices`

### 공지사항 조회
`GET /notices/:id`

#### 공지사항 수정
`PATCH /notices/:id`

#### 공지사항 삭제
`DELETE /notices/:id`

### 공지사항 작성
`POST /notices`

## 질문과 답변

### 질문과 답변 목록
`GET /questions`

### 질문과 답변 조회
`GET /questions/:id`

#### 답변하기 (관리자)
`POST /questions/:id/answer`

### 질문과 답변 작성
`POST /questions`

## 스티커(API미존재)

### 스티커 검색
`GET /stickers`

### 스티커 랭킹
`GET /stickers`

### 스티커 생성
`POST /stickers`

### 스티커 조회
`GET /stickers/:id`

#### 스티커 정보 변경
`PATCH /stickers/:id`

#### 스티커 이미지 설정
`POST /stickers/:id/photo`

#### 스티커에 초대
`POST /stickers/:id/users`

#### 스티커 가입
`POST /stickers/:id/users`

#### 스티커 탈퇴
`DELETE /stickers/:id/users`

#### 스티커 회원 조회
`GET /stickers/:id/users`

##### 회원 등급 변경
`PATCH /stickers/:id/users/:uid`

##### 회원 가입 승인 / 거부
`PATCH /stickers/:id/users/:uid`

#### 스티커 폐쇄
`DELETE /stickers/:id`

#### 게시글 목록
`GET /stickers/:id/posts`

#### 게시글 조회
`GET /stickers/:id/posts/:postId`

##### 게시글 좋아요
`POST /stickers/:id/posts/:postId/likes`

##### 게시글 좋아요 취소
`DELETE /stickers/:id/posts/:postId/likes`

##### 게시글 삭제
`DELETE /stickers/:id/posts/:postId`

##### 게시글 수정
`PATCH /stickers/:id/posts/:postId`

##### 게시글 댓글 작성
`POST /stickers/:id/posts/:postId/comments`

##### 게시글 댓글 조회
`GET /stickers/:id/posts/:postId/comments`

###### 게시글 댓글 삭제
`DELETE /stickers/:id/posts/:postId/comments/:commentId`

#### 게시글 작성
`POST /stickers/:id/posts`

#### 번개 TODO

## 모임

### 모임 생성
`POST /meetings`

### 모임 검색
`GET /meetings`

### 모임 조회
`GET /meetings/:id`

#### 모임 수정
`PATCH /meetings/:id`

#### 모임 삭제
`DELETE /meetings/:id`

#### 모임 참가
`POST /meetings/:id/users`

#### 모임 참가 취소
`DELETE /meetings/:id/users`
