import { UserFollowing, User } from '../db';
import sendNotification, { REDUCE_NOTIFICATIONS }
  from '../api/util/notification';

export default function sendFollowingFeedback() {
  return setInterval(async () => {
    let followings = await UserFollowing.findAll({
      where: {
        createdAt: { $lte: new Date(Date.now()-(30*60*1000)) },
        feedback: false,
      },
      include: [{
        model: User,
        as: 'following',
      },{
        model: User,
        as: 'follower',
      }],
    });
    for(let follow of followings){
      await sendNotification(follow.follower, {
            type: 'userFeedback',
            title: '에스티커 알림',
            user: follow.following,
          });
      await follow.update({
        feedback: true,
      });
    }
  }, 60 * 1000);
}
