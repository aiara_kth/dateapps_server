import cron from 'node-cron';
import { sequelize, PaymentLog, User } from '../db';
import sendNotification, { REDUCE_NOTIFICATIONS }
  from '../api/util/notification';
import { doPayment, doPaymentFree, checkPayment, getPaymentConfig }
  from '../api/util/payment';

export default function bonusPromotion() {
  cron.schedule('0 0 10 * * *', async () => {
    let users = await User.findAll({
      where: {
        loggedAt: { $gte: new Date(Date.now() - (7*24*60*60*1000)) },
        isEnabled: true,
        approvedAt: { $not: null },
        diamonds: 0,
        gender: 0,
      },
    });
    for(let user of users){
      let paymentLogs = await user.countPaymentLogs({
        where: {
          type: 'charge',
        },
      });
      if(paymentLogs <= 0) continue;
      let bonusCharge = await user.countPaymentLogs({
        where: {
          createdAt: { $lte: new Date(Date.now()-(24*60*60*1000)) },
          type: 'bonusRechargePromotion',
        }
      });
      if(bonusCharge != 1) continue;
      let amount = getPaymentConfig('bonusRechargePromotion');
      if(0<amount){
        await sequelize.transaction(async transaction => {
          await user.reload({ transaction });
          await PaymentLog.create({
            type: 'bonusRechargePromotion',
            cost: -amount,
            diamondsAfter: user.diamonds + amount,
            userId: user.id,
          }, {transaction});
          await user.increment('diamonds',{ by: amount, transaction });
        });
      }
      await sendNotification(user, {
            type: 'bonusRechargePromotion',
            title: '에스티커 알림',
            message: '보너스 다이아가 '+amount+'개 지급되었습니다.',
            pushOnly: true,
      });
    }
  });
  return setInterval(async () => {
    let users = await User.findAll({
      where: {
        loggedAt: { $gte: new Date(Date.now() - (7*24*60*60*1000)) },
        isEnabled: true,
        approvedAt: { $not: null },
        diamonds: 0,
        gender: 0,
      },
    });
    for(let user of users){
      let paymentLogs = await user.countPaymentLogs({
        where: {
          type: 'charge',
        },
      });
      if(paymentLogs.length <= 0) continue;
      let bonusCharge = await user.countPaymentLogs({
        where: {
          type: 'bonusRechargePromotion',
        }
      });
      if(bonusCharge != 0) continue;
      let lastPayment = await user.countPaymentLogs({
        where: {
          cost: { $gt: 0 },
          createdAt: { $gte: new Date(Date.now() - (5 * 60 * 1000)) },
        },
        order: [['createdAt', 'DESC']],
        limit: 1,
      });
      if(0<lastPayment) continue;
      let amount = getPaymentConfig('bonusRechargePromotion');
      if(0<amount){
        await sequelize.transaction(async transaction => {
          await user.reload({ transaction });
          await PaymentLog.create({
            type: 'bonusRechargePromotion',
            cost: -amount,
            diamondsAfter: user.diamonds + amount,
            userId: user.id,
          }, {transaction});
          await user.increment('diamonds',{ by: amount, transaction });
        });
      }
      await sendNotification(user, {
            type: 'bonusRechargePromotion',
            title: '에스티커 알림',
            message: '보너스 다이아가 '+amount+'개 지급되었습니다.',
            pushOnly: true,
      });
    }
  }, 60 * 1000);
}
