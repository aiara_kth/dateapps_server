import { Meeting } from '../db';
import sendNotification from '../api/util/notification';

export default function installMeetingFailWorker() {
  return setInterval(async () => {
    let meetings = await Meeting.findAll({
      where: {
        date: {
          $lt: new Date(Date.now() + 3 * 60 * 60 * 1000),
        },
        failVerified: false,
        groupId: null,
      },
    });
    for (let meeting of meetings) {
      meeting.failVerified = true;
      meeting.failed = meeting.membersCount < meeting.minMembers;
      if (meeting.failed) {
        console.log('Failing meeting ' + meeting.id + ' ' + meeting.title);
        let users = await meeting.getMembers({
          where: { isEnabled: true },
        });
        for (let user of users) {
          await sendNotification(user, { type: 'meetingFail', meeting });
        }
      }else{
        let users = await meeting.getMembers({
			    where: { isEnabled: true },
        });
        for (let user of uesrs){
          let amount = 0;
          if(user.id === meeting.userId){
            amount = 100;
          }else{
            amount = 20;
          }
          await sequilize.transaction(async transaction => {
            await PaymentLog.create({
              type: 'event',
　　　　　　　cost: -amount,
　　　　　　　diamondsAfter: user.diamonds + amount,
　　　　　　　userId: user.id,
	　　}, { transaction });
　　　　　　await user.increments('diamonds', { by: amount, transaction });
　　　　　});
　　　　}
　　  }
      await meeting.save();
    }
  }, 10 * 1000);
}
