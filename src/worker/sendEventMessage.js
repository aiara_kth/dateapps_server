import cron from 'node-cron';

import sendNotification, { REDUCE_NOTIFICATIONS }
  from '../api/util/notification';
import { sequelize, User, Notification, EventMessage } from '../db';

export default function sendEventMessage() {
  cron.schedule('0 0 3 * * *', async () => {
    let users = await User.findAll({
      where: {
        isEnabled: true,
        gender: 0,
        approvedAt: { $not: null },
      },
    });
    for (let user of users) {
        await sendNotification(user, {
          type: 'loungeRefresh',
          title: '에스티커 메세지',
          message: '매일 낮 12시 라운지에서 수다 떨어요:)',
          pushOnly: true,
        });
    }
  });
  cron.schedule('0 0 10 * * 2-6', async () => {
    let users = await User.findAll({
      where: {
        isEnabled: true,
        gender: 0,
        approvedAt: { $not: null },
      },
    });
    for (let user of users) {
        await sendNotification(user, {
          type: 'loungeRefresh',
          title: '에스티커 메세지',
          message: '매일 저녁 7시에 라운지에서 퇴근한 직장인들이 이야기를 나누고 있어요.',
          pushOnly: true,
        });
    }
  });
  cron.schedule('0 0 10 * * 1,7', async () => {
    let users = await User.findAll({
      where: {
        isEnabled: true,
        gender: 0,
        approvedAt: { $not: null },
      },
    });
    for (let user of users) {
        await sendNotification(user, {
          type: 'loungeRefresh',
          title: '에스티커 메세지',
          message: '저녁 7시에 라운지에서 가장 많은 회원분들이 이야기를 나누고 있어요.',
          pushOnly: true,
        });
    }
  });
}

