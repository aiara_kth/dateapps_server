import { Group } from '../db';

export default function installGroupUnpaidDeleteWorker() {
  return setInterval(async () => {
    await Group.destroy({
      where: {
        subscriptionValidUntil: null,
        createdAt: {
          $lt: new Date(Date.now() - 60 * 60 * 1000),
        },
      },
    });
  }, 60 * 10 * 1000);
}
