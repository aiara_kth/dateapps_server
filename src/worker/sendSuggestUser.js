import cron from 'node-cron';

import sendNotification, { REDUCE_NOTIFICATIONS }
  from '../api/util/notification';
import Sequelize from 'sequelize';
import { sequelize, User, UserSuggestion, UserFollowing, UserView } from '../db';
import { suggestion as suggestionConfig } from '../../config';

export default function sendSuggestUser() {
  
  // Send weekly notification at every Sunday, 12PM KST.
  // That's Sunday, 3AM for UTC, and the server uses UTC timezone.
  cron.schedule('0 0 6,10,11 * * *', async () => {
  // cron.schedule('*/10 * * * * *', async () => {
    // Import all users with isWeeklySet, and has at least one notification
    // before logging in
    let date = new Date(Date.now() + 9*60*60*1000);
    let day = date.getDay();
    let dayTable = [
      {min:1,max:2},
      {min:2,max:2},
      {min:1,max:2},
      {min:2,max:2},
      {min:1,max:2},
      {min:2,max:2},
      {min:1,max:2},
    ];
    let daySet = dayTable[day];
    let users = await User.findAll({
      where: {
        loggedAt: {
          $gt: new Date(Date.now() - suggestionConfig.target),
        },
        diamonds: { $lte: 20 },
        isEnabled: true,
        approvedAt: { $not: null },
        gender: 0,
      },
    });
    let nowHour = new Date().getHours();
    let todayStart = new Date(Date.now() + (9*60*60*1000));
    todayStart = new Date(todayStart.setHours(0));
    todayStart = new Date(todayStart.setMinutes(0));
    for (let user of users) {
      setTimeout(async () => {
        let todayCount = await UserSuggestion.count({
          where: {
            createdAt: { $gte: todayStart },
            targetId: user.id,
          },
        });
        let maxLimit = (Date.now()-user.approvedAt.getTime())<(2 * 24 * 60 * 60 * 1000) ? 3 : 2;
        if(maxLimit<=todayCount){
          return;
        }
        if(await user.countGroups({
          through: { where: { type: { $in: ['admin'] } } },
          where: {
            deletedAt: { $or: [null, { $gte: new Date() }] },
          },
        }) > 0){
          return;
        }
        let suggestions = await UserSuggestion.findAll({
          where: {
            targetId: user.id,
          },
        });
        let ids = suggestions.map(suggestion => suggestion.userId);
        let viewIds = await UserView.findAll({
          where: {
            viewerId: user.id,
          },
        });
        viewIds = viewIds.map(v=>v.vieweeId);
        ids = viewIds.concat(ids);
        let where = {
            isSuggestion: true,
            isLoungeUsed: true,
            gender: 1,
        };
        let groups = await user.getGroups();
        for(let group of groups){
          let groupUsers = await group.getMembers({
            where: [sequelize.where(sequelize.literal('groupUser.type'), {
                $notIn: ['invited', 'banned'],
              }), {
                isEnabled: true,
              }],
          });
          let groupUserIds = groupUsers.map(groupUser => groupUser.id);
          ids.concat(groupUserIds);
        }
        let followingData = await UserFollowing.findAll({ where: {
          followerId: user.id,
        } });
        if(followingData!=null&&0<followingData.length){
          let followingIds = followingData.map(data => data.followingId);
          ids = ids.concat(followingIds);
        }
        let followerData = await UserFollowing.findAll({ where: {
          followingId: user.id,
        } });  
        if(followerData!=null&&0<followerData.length){
          let followerIds = followerData.map(data => data.followerId);
          ids = ids.concat(followerIds);
        }
        if(ids!=null&&0<ids.length){
          where.id = { $notIn: ids };
        }
        let suggestUsers = await User.findAll({
          where,
          order: [
            Sequelize.fn('RAND'),
          ],
          limit: suggestionConfig.count,
        });
        let count = 0;
        for(let suggest of suggestUsers){
          count += 1;
          let data = await UserSuggestion.create({
            userId: suggest.id,
            targetId: user.id,
          });
        }
        if(count!=0){
          todayCount += 1;
          let users;
          if(2==todayCount){
            users = await UserSuggestion.findAll({
              where: {
                createdAt: { $gte: todayStart },
                targetId: user.id,
              },
              include: [{
                model: User,
                as: 'user',
              },{
                model: User,
                as: 'target',
              }],
            });
            users = users.map(user => user.user);
          }
          await sendNotification(user, {
            type: 'userSuggest',
            title: '에스티커 알림',
            user: suggestUsers[0],
            users,
          });
        }
      }, Math.random() * 30 * 60 * 1000);
    }
  });
}
