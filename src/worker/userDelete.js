import { User, Group, Sticker } from '../db';

export default function installUserDeleteWorker() {
  return setInterval(async () => {
    let users = await User.findAll({
      where: {
        deletedAt: {
          $lt: new Date(Date.now() - 60 * 24 * 60 * 60 * 1000),
        },
        isEnabled: false,
      },
    });
    for (let user of users) {
      let groups = await user.getGroups();
      let meetings = await user.getHostedMeetings();
      await user.destroy();
      for (let group of groups) {
        await group.updateMembers();
        if (group.membersCount === 0 && group.deletedAt != null) {
          await group.update({ deletedAt: new Date() });
        }
      }
      for (let meeting of meetings) {
        await meeting.destroy();
      }
    }
    await Group.destroy({
      where: {
        deletedAt: {
          $lt: new Date(Date.now() - 60 * 24 * 60 * 60 * 1000),
        },
      },
    });
  }, 60 * 10 * 1000);
}
