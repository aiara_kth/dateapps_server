import cron from 'node-cron';

import { User } from '../db';

export default function installResetCounterWorker() {
  // Send weekly notification at every 12AM KST.
  // That's 15:00 for UTC, and the server uses UTC timezone.
  cron.schedule('0 0 15 * * *', async () => {
  // cron.schedule('*/10 * * * * *', async () => {
    await User.update({
      chargedCountToday: 0,
      profileCountToday: 0,
    }, {
      where: {},
    });
  });
}
