import cron from 'node-cron';

import sendNotification, { REDUCE_NOTIFICATIONS }
  from '../api/util/notification';
import { sequelize, User, Notification } from '../db';

export default function loungeRefreshNotification() {
  cron.schedule('0 0 3,8 * * *', async () => {
    let users = await User.findAll({
      where: {
        isEnabled: true,
        gender: 0,
        diamonds: 0,
      },
    });
    for (let user of users) {
      await sendNotification(user, {
        type: 'loungeRefresh',
        title: '에스티커 알림',
        message: '남은 시간 1시간! 지금 대화신청하세요. 같은 회원님과 다시 마주칠 가능성은 매우 낮습니다.',
        pushOnly: true,
      });
    }
  });
}
