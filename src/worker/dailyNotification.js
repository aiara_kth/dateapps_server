import cron from 'node-cron';

import sendNotification, { REDUCE_NOTIFICATIONS }
  from '../api/util/notification';
import { sequelize, User, Notification } from '../db';

export default function installDailyNotificationWorker() {
  // Send weekly notification at every Sunday, 12PM KST.
  // That's Sunday, 3AM for UTC, and the server uses UTC timezone.
  cron.schedule('0 0 0 * * *', async () => {
  // cron.schedule('*/10 * * * * *', async () => {
    // Import all users with isWeeklySet, and has at least one notification
    // before logging in
    let users = await User.findAll({
      where: { isDailySet: true },
      include: [{
        model: Notification,
        as: 'notifications',
        where: {
          type: { $in: REDUCE_NOTIFICATIONS },
          createdAt: { $gt: sequelize.literal('`user`.`loggedAt`') },
        },
        required: true,
      }],
    });
    for (let user of users) {
      // Send it! :)
      // First, count all the notifications...
      let followCount = user.notifications.filter(v => v.type === 'follow')
        .length;
      let chatCount = user.notifications.filter(v => v.type === 'chatInvite')
        .length;
      let output = [];
      if (followCount > 0) output.push(followCount + '건의 관심 표현');
      if (chatCount > 0) output.push(chatCount + '건의 채팅 신청');
      await sendNotification(user, {
        type: 'dailySend',
        title: '에스티커 알림',
        message: '마지막 로그인 한 뒤로 ' + output.join('과') + '을 받았습니다.',
        pushOnly: true,
      });
    }
  });
}
