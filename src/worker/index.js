import newbieWorker from './newbie';
import userDeleteWorker from './userDelete';
import dailyNotificationWorker from './dailyNotification';
import groupSubscriptionWorker from './groupSubscription';
import groupUnpaidDeleteWorker from './groupUnpaidDelete';
import resetCounterWorker from './resetCounter';
import meetingFailWorker from './meetingFail';
import sendSuggestUser from './sendSuggestUser'
import loungeRefreshNotification from './loungeRefreshNotification'
import sendEventMessage from './sendEventMessage'
import sendFollowingFeedback from './sendFollowingFeedback'
import bonusPromotion from './bonusPromotion'

export default function worker() {
  newbieWorker();
  userDeleteWorker();
  dailyNotificationWorker();
  groupSubscriptionWorker();
  groupUnpaidDeleteWorker();
  resetCounterWorker();
  meetingFailWorker();
  //sendSuggestUser();
  sendEventMessage();
  //sendFollowingFeedback();
  //bonusPromotion();
  //loungeRefreshNotification();
}
