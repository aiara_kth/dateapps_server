import { User, UserRatingHistory } from '../db';
import sendNotification from '../api/util/notification';
import getRatingData from '../api/util/getRatingData';
import { lounge as loungeConfig } from '../../config';

export default function installNewbieWorker() {
  return setInterval(async () => {
    // Load users with newbieCreatedAt after 10m
    let users = await User.findAll({
      where: {
        newbieStartedAt: {
          $lt: new Date(Date.now() - loungeConfig.time),
        },
        newbiePending: true,
      },
    });
    // Then, load the score factors
    for (let user of users) {
      // Retrieve all votes for the user.
      let ratingData = await getRatingData(user);
      let { average, count } = ratingData;
      // Store rating data history
      // Add UserRatingHistroy record
      await UserRatingHistory.create(Object.assign({
        userId: user.id,
        photo: user.photo,
        photoThumb: user.photoThumb,
      }, ratingData));
      user.newbiePending = false;
      if (user.isLoungeApproved) {
        await user.save();
        await sendNotification(user, {
          type: 'loungeComplete',
          hidden: true,
          rating: average,
        });
      } else {
        if (average >= loungeConfig.average && count >= loungeConfig.count) {
          // Success!
          user.isLoungeApproved = true;
          await user.save();
          // Send push notification to the user saying that (s)he is
          // allowed to use the lounge.
          await sendNotification(user,
            { type: 'loungeAccept', rating: average });
        } else {
          // Canceled
          await user.save();
          // Send push notification to the user saying that (s)he is
          // denied to use the lounge..
          await sendNotification(user,
            { type: 'loungeDeny', rating: average });
        }
      }
    }
  }, 30 * 1000);
}
