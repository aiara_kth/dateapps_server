import { Group } from '../db';
import iap from '../api/util/iap';
// import { BILLING_IDS } from '../api/util/payment';

export default function installGroupSubscriptionWorker() {
  return setInterval(async () => {
    let groups = await Group.findAll({
      where: {
        subscriptionValidUntil: {
          // Validation period is 48 hours
          $lt: new Date(Date.now() + 48 * 60 * 60 * 1000),
          // ...to 168 hours
          $gt: new Date(Date.now() - 168 * 60 * 60 * 1000),
        },
        subscriptionData: { $not: null },
      },
    });
    for (let group of groups) {
      // Lookup the payment id
      let data = group.subscriptionData;
      let signature = group.subscriptionSignature;
      // Run verificiation
      let receiptItem;
      try {
        receiptItem = await new Promise((resolve, reject) => {
          iap.validate(iap.GOOGLE, { data, signature }, (err, response) => {
            if (err) return reject(err);
            if (iap.isValidated(response)) {
              let purchaseList = iap.getPurchaseData(response);
              let item = purchaseList && purchaseList[0];
              if (item == null) return reject(new Error('Item is empty'));
              if (iap.isExpired(item)) reject(new Error('Item expired'));
              return resolve(item);
            } else {
              return reject(new Error('Unvalidated payment response'));
            }
          });
        });
      } catch (e) {
        console.log(e.stack);
        // Clear out subscriptionData
        if (group.cancelPending) {
          // Delete the group entirely
          await group.update({
            subscriptionData: null,
            deletedAt: new Date(),
          });
        } else {
          await group.update({
            subscriptionData: null,
          });
        }
        continue;
      }
      // What?
      // if (!BILLING_IDS.includes(receiptItem.productId)) continue;
      // Renew complete!
      await group.update({
        subscriptionValidUntil: new Date(receiptItem.expirationDate),
      });
    }
  }, 60 * 60 * 1000);
}
