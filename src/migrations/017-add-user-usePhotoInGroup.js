module.exports = {
  async up(queryInterface, Sequelize) {
    queryInterface.addColumn('users', 'usePhotoInGroup', {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    });
  },
  async down(queryInterface, Sequelize) {
  },
};
