import { User } from '../db';
import firebase from '../firebase';

module.exports = {
  async up(queryInterface, Sequelize) {
    const firebaseDB = firebase.database();
    // Publish fcm keys into the firebase.
    let users = await User.findAll();
    for (let user of users) {
      firebaseDB.ref('/updatePushes').child(user.id).set(user.updatePush);
    }
  },
  async down(queryInterface, Sequelize) {
  },
};
