module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.addColumn('users', 'isApproveDenied', {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    });
    await queryInterface.addColumn('users', 'hasApproveDeniedBefore', {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    });
  },
  async down(queryInterface, Sequelize) {
  },
};
