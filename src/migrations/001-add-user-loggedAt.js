module.exports = {
  async up(queryInterface, Sequelize) {
    queryInterface.addColumn('users', 'loggedAt', {
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW,
    });
  },
  async down(queryInterface, Sequelize) {
  },
};
