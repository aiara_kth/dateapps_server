module.exports = {
  async up(queryInterface, Sequelize) {
    queryInterface.addColumn('groups', 'deletedAt', {
      type: Sequelize.DATE,
    });
  },
  async down(queryInterface, Sequelize) {
  },
};
