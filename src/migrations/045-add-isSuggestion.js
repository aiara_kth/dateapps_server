module.exports = {
  async up(queryInterface, Sequelize) {
      queryInterface.addColumn('users','isSuggestion',{
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: false,
        });
    },
    async down(queryInterface, Sequelize) {
    },
};
