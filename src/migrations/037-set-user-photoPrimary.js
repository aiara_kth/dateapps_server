import { User } from '../db';

module.exports = {
  async up(queryInterface, Sequelize) {
    let users = await User.findAll();
    for (let user of users) {
      let { photoPrimary, photos } = user;
      // If photoPrimary is not 0, swap photo indices...
      if (photoPrimary !== 0 && photoPrimary != null) {
        let newPhotos = photos.slice();
        newPhotos[0] = photos[photoPrimary];
        newPhotos[photoPrimary] = photos[0];
        // Apply swapped indices
        console.log('Swapping photo for user ' + user.id + ': 0 <-> ' +
          photoPrimary);
        await user.update({ photos: newPhotos, photoPrimary: 0 });
      }
    }
  },
  async down(queryInterface, Sequelize) {
  },
};
