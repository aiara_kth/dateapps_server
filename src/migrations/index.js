import Umzug from 'umzug';
import Sequelize from 'sequelize';

import sequelize from '../db/init';
const queryInterface = sequelize.getQueryInterface();

const umzug = new Umzug({
  storage: 'sequelize',
  storageOptions: {
    sequelize: sequelize,
    tableName: 'sequelizeMeta',
    columnName: 'migration',
  },
  migrations: {
    path: __dirname,
    pattern: /^\d+[\w-]+\.js$/,
    params: [queryInterface, Sequelize],
  },
});

export default async function doMigration() {
  // Run migrations only if DB is not fresh
  console.log('Running migrations...');
  // That means if users table exists
  if ((await queryInterface.showAllTables()).indexOf('users') !== -1) {
    await umzug.up();
  } else {
    // Mark all migrations as complete
    console.log('Database fresh; skipping');
    let list = await umzug.pending();
    for (let migration of list) {
      await umzug.storage.logMigration(migration.file);
    }
  }
  console.log('Complete!');
}
