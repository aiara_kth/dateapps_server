import { Group, User } from '../db';

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.addColumn('groups', 'adminId', {
      type: Sequelize.INTEGER,
      references: {
        model: 'users',
        key: 'id',
      },
      onUpdate: 'cascade',
      onDelete: 'set null',
    });
    // Retrieve all groups and find admin.
    let groups = await Group.findAll({
      include: [{
        model: User,
        as: 'members',
        through: { where: { type: 'admin' } },
      }],
    });
    for (let group of groups) {
      if (group.members[0] != null) {
        await group.update({ adminId: group.members[0].id });
      }
    }
  },
  async down(queryInterface, Sequelize) {
  },
};
