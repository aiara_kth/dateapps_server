import { User } from '../db';

module.exports = {
  async up(queryInterface, Sequelize) {
    let users = await User.findAll();
    for (let user of users) {
      let { isSuggestion } = user;
      // If photoPrimary is not 0, swap photo indices...
      if (isSuggestion == null) {
        await user.update({ isSuggestion: false });
      }
    }
  },
  async down(queryInterface, Sequelize) {
  },
};
