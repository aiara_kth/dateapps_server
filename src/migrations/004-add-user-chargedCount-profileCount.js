module.exports = {
  async up(queryInterface, Sequelize) {
    queryInterface.addColumn('users', 'chargedCountToday', {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0,
    });
    queryInterface.addColumn('users', 'profileCountToday', {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0,
    });
  },
  async down(queryInterface, Sequelize) {
  },
};
