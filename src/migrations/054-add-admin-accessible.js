module.exports = {
  async up(queryInterface, Sequelize) {
        await queryInterface.addColumn('users','isAllAccessible',{
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false,
          });
    },
    async down(queryInterface, Sequelize) {

    },
};
