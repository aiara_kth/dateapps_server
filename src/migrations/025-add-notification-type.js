module.exports = {
  async up(queryInterface, Sequelize) {
    queryInterface.addColumn('notifications', 'type', {
      type: Sequelize.STRING,
    });
    queryInterface.addIndex('notifications', ['type']);
  },
  async down(queryInterface, Sequelize) {
  },
};
