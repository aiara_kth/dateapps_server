module.exports = {
  async up(queryInterface, Sequelize) {
    queryInterface.addColumn('userFollowing', 'denied', {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    });
  },
  async down(queryInterface, Sequelize) {
  },
};
