module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.addColumn('groups', 'subscriptionData',
      Sequelize.TEXT);
    await queryInterface.addColumn('groups', 'subscriptionSignature',
      Sequelize.TEXT);
    await queryInterface.addColumn('groups', 'subscriptionProvider',
      Sequelize.STRING);
    await queryInterface.addColumn('groups', 'subscriptionTransactionId',
      Sequelize.STRING);
    await queryInterface.addColumn('groups', 'subscriptionValidUntil',
      Sequelize.DATE);
    await queryInterface.addColumn('groups', 'cancelPending', {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    });
    await queryInterface.addIndex('groups', ['subscriptionTransactionId']);
    await queryInterface.addIndex('groups', ['subscriptionValidUntil']);
  },
  async down(queryInterface, Sequelize) {
  },
};
