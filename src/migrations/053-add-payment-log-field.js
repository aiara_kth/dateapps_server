module.exports = {
  async up(queryInterface, Sequelize) {
        await queryInterface.addColumn('paymentLogs','nickname',{
          type: Sequelize.STRING,
        });
        await queryInterface.addColumn('paymentLogs','name',{
          type: Sequelize.STRING,
        });
        await queryInterface.addColumn('paymentLogs','phone',{
          type: Sequelize.STRING,
        });
    },
    async down(queryInterface, Sequelize) {

    },
};
