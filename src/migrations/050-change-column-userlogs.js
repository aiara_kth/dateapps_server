module.exports = {
  async up(queryInterface, Sequelize) {
    queryInterface.changeColumn('userLogs', 'phone', {
        type: Sequelize.STRING(24),
        unique: false,
        allowNull: false,
      });
    queryInterface.changeColumn('userLogs', 'email', {
        type: Sequelize.STRING,
        unique: false,
        allowNull: false,
      });
  },
  async down(queryInterface, Sequelize) {
    queryInterface.changeColumn('userLogs', 'phone', {
        type: Sequelize.STRING(24),
        unique: false,
        allowNull: false,
      });
    queryInterface.changeColumn('userLogs', 'email', {
        type: Sequelize.STRING,
        unique: false,
        allowNull: false,
      });
  },
};
