import { UserView } from '../db';

module.exports = {
  async up(queryInterface, Sequelize) {
        await queryInterface.addColumn('userViews','ignore',{
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: false,
        });
    },
    async down(queryInterface, Sequelize) {

    },
};
