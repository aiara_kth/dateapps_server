module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.addColumn('groupPosts', 'isNotice', {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    });
  },
  async down(queryInterface, Sequelize) {
  },
};
