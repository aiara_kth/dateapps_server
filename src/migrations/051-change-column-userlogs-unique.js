module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.changeColumn('userLogs', 'phone', {
        type: Sequelize.STRING(24),
        unique: false,
        allowNull: false,
      });
    await queryInterface.changeColumn('userLogs', 'email', {
        type: Sequelize.STRING,
        unique: false,
        allowNull: false,
      });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.changeColumn('userLogs', 'phone', {
        type: Sequelize.STRING(24),
        unique: false,
        allowNull: false,
      });
    await queryInterface.changeColumn('userLogs', 'email', {
        type: Sequelize.STRING,
        unique: false,
        allowNull: false,
      });
  },
};
