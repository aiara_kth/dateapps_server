module.exports = {
  async up(queryInterface, Sequelize) {
        await queryInterface.addColumn('userFollowings','feedback',{
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false,
          });
    },
    async down(queryInterface, Sequelize) {

    },
};
