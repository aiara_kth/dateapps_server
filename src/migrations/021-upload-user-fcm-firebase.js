import { User } from '../db';
import firebase from '../firebase';

module.exports = {
  async up(queryInterface, Sequelize) {
    const firebaseDB = firebase.database();
    // Publish fcm keys into the firebase.
    let users = await User.findAll();
    for (let user of users) {
      if (user.fcmKey != null) {
        firebaseDB.ref('/fcmKeys').child(user.id).set(user.fcmKey);
      }
    }
  },
  async down(queryInterface, Sequelize) {
  },
};
