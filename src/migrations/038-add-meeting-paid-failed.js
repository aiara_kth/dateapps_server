module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.addColumn('meetings', 'paid', {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    });
    await queryInterface.addColumn('meetings', 'failed', {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    });
  },
  async down(queryInterface, Sequelize) {
  },
};
