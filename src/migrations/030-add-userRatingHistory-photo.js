module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.addColumn('userRatingHistories', 'photo', {
      type: Sequelize.STRING,
    });
    await queryInterface.addColumn('userRatingHistories', 'photoThumb', {
      type: Sequelize.STRING,
    });
  },
  async down(queryInterface, Sequelize) {
  },
};
