module.exports = {
  async up(queryInterface, Sequelize) {
    queryInterface.addColumn('users', 'isLoungeBest', {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
      allowNull: false,
    });
  },
  async down(queryInterface, Sequelize) {
  },
};
