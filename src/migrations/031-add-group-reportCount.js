module.exports = {
  async up(queryInterface, Sequelize) {
    queryInterface.addColumn('groups', 'reportCount', {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0,
    });
    queryInterface.addIndex('groups', ['reportCount']);
  },
  async down(queryInterface, Sequelize) {
  },
};
