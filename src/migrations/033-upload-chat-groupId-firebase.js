import { Group } from '../db';
import firebase from '../firebase';

module.exports = {
  async up(queryInterface, Sequelize) {
    const firebaseDB = firebase.database();
    // Publish fcm keys into the firebase.
    let groups = await Group.findAll({ where: {
      chatKey: { $not: null },
    } });
    for (let group of groups) {
      console.log('Updating', group.chatKey, group.id);
      firebaseDB.ref('/rooms').child(group.chatKey).update({
        groupId: group.id,
        isGroup: true,
      });
    }
  },
  async down(queryInterface, Sequelize) {
  },
};
