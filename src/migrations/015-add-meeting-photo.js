module.exports = {
  async up(queryInterface, Sequelize) {
    queryInterface.addColumn('meetings', 'photo', {
      type: Sequelize.STRING,
    });
  },
  async down(queryInterface, Sequelize) {
  },
};
