module.exports = {
  async up(queryInterface, Sequelize) {
    queryInterface.addColumn('userChats', 'expired', {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    });
  },
  async down(queryInterface, Sequelize) {
  },
};
