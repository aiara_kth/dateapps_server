module.exports = {
  async up(queryInterface, Sequelize) {
    queryInterface.addColumn('groupUsers', 'joinMessage', {
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: '',
    });
  },
  async down(queryInterface, Sequelize) {
  },
};
