module.exports = {
  async up(queryInterface, Sequelize) {
    queryInterface.addColumn('userFollowings', 'reply', {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    });
  },
  async down(queryInterface, Sequelize) {
  },
};
