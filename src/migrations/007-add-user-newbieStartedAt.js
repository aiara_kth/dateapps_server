module.exports = {
  async up(queryInterface, Sequelize) {
    queryInterface.addColumn('users', 'newbieStartedAt', {
      type: Sequelize.DATE,
    });
  },
  async down(queryInterface, Sequelize) {
  },
};
