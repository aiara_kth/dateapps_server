import path from 'path';

import { User } from '../db';
import s3Upload from '../api/util/s3';
import { upload as uploadConfig } from '../../config';

module.exports = {
  async up(queryInterface, Sequelize) {
    // This would take a lot of time, however, this is supposed to run
    // at very first stage of the service, so it'll be alright.
    let users = await User.findAll();
    for (let user of users) {
      // Process each file starting with /
      let photos = user.photos || [];
      let newPhotos = [];
      for (let photo of photos) {
        // Ignore already upload S3 files
        if (photo[0] !== '/') {
          newPhotos.push(photo);
          continue;
        }
        // Strip filename
        let filename = photo.slice(photo.lastIndexOf('/') + 1);
        let thumbname = filename.replace(/\.([a-z]+)$/i, '.thumb.$1');

        let filePath = path.resolve(uploadConfig.directory, filename);
        let publicPath = path.resolve(uploadConfig.directoryPublic, filename);

        let thumbFilePath = path.resolve(uploadConfig.directory, thumbname);
        let thumbPublicPath =
          path.resolve(uploadConfig.directoryPublic, thumbname);
        // ... Upload to S3
        let result = await Promise.all([
          s3Upload('images/' + filename, filePath,
            uploadConfig.url + publicPath),
          s3Upload('images/' + thumbname, thumbFilePath,
            uploadConfig.url + thumbPublicPath),
        ]);
        newPhotos.push(result[0]);
      }
      // Apply uploaded addresses
      await user.update({ photos: newPhotos });
    }
  },
  async down(queryInterface, Sequelize) {
  },
};
