module.exports = {
  async up(queryInterface, Sequelize) {
    queryInterface.changeColumn('meetings', 'price', {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0,
    });
  },
  async down(queryInterface, Sequelize) {
  },
};
