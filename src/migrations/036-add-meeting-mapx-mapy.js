module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.addColumn('meetings', 'mapx', Sequelize.INTEGER);
    await queryInterface.addColumn('meetings', 'mapy', Sequelize.INTEGER);
  },
  async down(queryInterface, Sequelize) {
  },
};
