module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.addColumn('groups', 'forceRanking', {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0,
    });
    await queryInterface.addIndex('groups', ['forceRanking']);
  },
  async down(queryInterface, Sequelize) {
  },
};
