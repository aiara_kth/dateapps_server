module.exports = {
  async up(queryInterface, Sequelize) {
    queryInterface.changeColumn('users', 'nickname', {
      type: Sequelize.STRING,
      unique: true,
    });
  },
  async down(queryInterface, Sequelize) {
  },
};
