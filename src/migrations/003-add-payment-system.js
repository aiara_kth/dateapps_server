module.exports = {
  async up(queryInterface, Sequelize) {
    queryInterface.addColumn('users', 'diamonds', {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0,
    });
  },
  async down(queryInterface, Sequelize) {
  },
};
