module.exports = {
  async up(queryInterface, Sequelize) {
    queryInterface.addColumn('users', 'deletedAt', {
      type: Sequelize.DATE,
    });
  },
  async down(queryInterface, Sequelize) {
  },
};
