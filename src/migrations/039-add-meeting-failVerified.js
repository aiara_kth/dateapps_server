module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.addColumn('meetings', 'failVerified', {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    });
  },
  async down(queryInterface, Sequelize) {
  },
};
