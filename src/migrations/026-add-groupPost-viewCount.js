module.exports = {
  async up(queryInterface, Sequelize) {
    queryInterface.addColumn('groupPosts', 'viewCount', {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0,
    });
    queryInterface.addIndex('groupPosts', ['viewCount']);
  },
  async down(queryInterface, Sequelize) {
  },
};
