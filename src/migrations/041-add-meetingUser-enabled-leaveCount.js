module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.renameTable('meetingUser', 'meetingUsers');
    await queryInterface.addColumn('meetingUsers', 'enabled', {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    });
    await queryInterface.addColumn('meetingUsers', 'leaveCount', {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0,
    });
  },
  async down(queryInterface, Sequelize) {
  },
};
