module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.addColumn('ongoingPhoneAuths', 'resetAt', {
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW,
    });
    await queryInterface.addColumn('ongoingPhoneAuths', 'count', {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0,
    });
  },
  async down(queryInterface, Sequelize) {
  },
};
