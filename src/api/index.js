import { Router } from 'express';

import middlewares from './middleware';
import errorHandler from './middleware/errorHandler';

import session from './session';
import user from './user';
import group from './group';
import meeting from './meeting';
import notification from './notification';
import notice from './notice';
import question from './question';
import tos from './tos';
import payment from './payment';
import appVersion from './appVersion';
import sharer from './sharer';

const router = new Router();
export default router;

router.use(middlewares);
router.use(session);
router.use(user);
router.use(group);
router.use(meeting);
router.use(notification);
router.use(notice);
router.use(question);
router.use(tos);
router.use(payment);
router.use(appVersion);
router.use(sharer);
router.use(errorHandler);
