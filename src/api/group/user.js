import { Router } from 'express';

import async from '../util/async';
import loginRequired from '../util/loginRequired';
import pick from '../util/pick';
import sendNotification from '../util/notification';
import { checkTypeHierarchy, checkTypeHierarchySet, checkType,
  checkPermission, setPermission, groupPermissionRequired }
  from '../util/groupAdminRequired';
import { addChatParticipant, removeChatParticipant, addChatData } from '../util/chat';
import { injectOtherUserInfo } from '../util/injectUserInfo';

import { validateSingle, sanitize } from '../../validation/validate';
import * as validationSchema from '../../validation/schema';

import { GroupUser, User, Group, sequelize } from '../../db';

export const userRouter = new Router();
export default userRouter;

userRouter.use(async(async (req, res, next) => {
  // Find userGroup
  req.selectedGroupUser = await GroupUser.findOne({ where: {
    userId: req.selectedUser.id,
    groupId: req.selectedGroup.id,
  } });
  next();
}));

/**
 * @api {get} /groups/:groupId/users/:userId 스티커 사용자 가져오기
 * @apiGroup GroupUser
 * @apiName GetGroupUser
 * @apiDescription 스티커의 사용자를 가져옵니다.
 * @apiUse GetUser
 * @apiUse GetGroup
 * @apiSuccess {User} _ 사용자
 * @apiSuccess {String} _.joinMessage 가입 인사
 * @apiSuccess {String[]} _.photos 사진 목록
 * @apiSuccess {String[]} _.photoThumbs 사진 썸네일 목록
 * @apiUse ReturnUser
 * @apiUse ReturnGroupUser
 */
userRouter.get('/', async(async (req, res) => {
  if (req.selectedGroupUser == null) throw 'UserNotFound';
  let data = await injectOtherUserInfo(req.selectedUser, req.user);
  data.groupUser = req.selectedGroupUser;
  data.joinMessage = req.selectedGroupUser.joinMessage;
  data.photos = req.selectedUser.getPhotos(req.user);
  data.photoThumbs = req.selectedUser.getPhotoThumbs(req.user);
  if (!data.usePhotoInGroup) {
    delete data.photo;
    delete data.photoThumb;
    delete data.photos;
    delete data.photoThumbs;
  }
  res.json(data);
}));

/**
 * @api {post} /groups/:groupId/users/:userId 스티커 초대하기
 * @apiGroup GroupUser
 * @apiName InviteGroup
 * @apiDescription 스티커에 초대합니다.
 * @apiUse GetGroup
 * @apiUse GetUser
 * @apiParam (Query) {Boolean} [force=false] 관리자라면 강제로 초대할지 결정합니다.
 * @apiError (Error 409) Conflict 이미 처리되었습니다.
 * @apiError (Error 400) GroupAdminRequired 해당 회원을 초대할 권한이 없습니다.
 * @apiSuccess {User} _ 사용자
 * @apiUse ReturnUser
 * @apiUse ReturnGroupUser
 */
userRouter.put('/', groupPermissionRequired('member'),
async(async (req, res) => {
  if (req.selectedGroupUser != null) throw 'Conflict';
  if (req.query.force && !req.user.isAdmin) throw 'AdminRequired';
  let type = 'invited';
  if (req.query.force) type = 'member';
  // Create GroupUser and we're good to go
  let groupUser = await GroupUser.create({
    userId: req.selectedUser.id, groupId: req.selectedGroup.id, type,
  });
  // Send notification
  await sendNotification(req.selectedUser, { type: 'groupInvite',
    group: req.selectedGroup,
    user: req.user });
  await req.selectedGroup.updateMembers();
  if (type !== 'invited' && req.selectedGroup.chatKey != null) {
    // Add user into chat participants
    await addChatParticipant(req.selectedGroup.chatKey, req.selectedUser);
  }
  res.json({ groupUser });
}));

/**
 * @api {patch} /groups/:groupId/users 스티커 알림 설정 변경하기
 * @apiGroup GroupUser
 * @apiName ChangeGroupNotification
 * @apiDescription 스티커의 알림 설정을 변경합니다.
 * @apiParam (Body) {Boolean} [updatePush=true] 업데이트 푸시 알림 수신 여부
 * @apiParam (Body) {Boolean} [chatPush=true] 채팅 푸시 알림 수신 여부
 * @apiUse GetGroup
 * @apiSuccess {User} _ 사용자
 * @apiUse ReturnUser
 * @apiUse ReturnGroupUser
 * @apiError (Error 404) UserNotFound 입력하신 사용자를 찾을 수 없습니다.
 */
/**
 * @api {patch} /groups/:groupId/users 스티커 초대 수락
 * @apiGroup GroupUser
 * @apiName GroupAcceptInvite
 * @apiDescription 스티커 초대를 수락합니다. `type`는 꼭 `member`로 지정해 주세요.
 * @apiParam (Body) {String} [type] 권한 이름 (member)
 * @apiError (Error 400) ValidationError 초대된 상태가 아닙니다.
 * @apiUse GetGroup
 * @apiSuccess {User} _ 사용자
 * @apiUse ReturnUser
 * @apiUse ReturnGroupUser
 * @apiError (Error 404) UserNotFound 입력하신 사용자를 찾을 수 없습니다.
 */
/**
 * @api {patch} /groups/:groupId/users/:userId 스티커 권한 바꾸기
 * @apiGroup GroupUser
 * @apiName ChangeGroupPermission
 * @apiDescription 해당 사용자의 스티커 권한을 변경합니다. 가입을 수락할 때에도
 *   이 명령이 사용됩니다.
 * @apiParam (Body) {String} type 권한 이름
 * @apiParam (Body) {Number} permission 권한 플래그
 * @apiError (Error 400) ValidationError 부시삽은 1명만 있을 수 있습니다.
 * @apiError (Error 400) ValidationError 스탭은 5명만 있을 수 있습니다.
 * @apiError (Error 400) GroupAdminRequired 해당 사용자를 바꿀 권한이 없습니다.
 * @apiUse GetUser
 * @apiUse GetGroup
 * @apiSuccess {User} _ 사용자
 * @apiUse ReturnUser
 * @apiUse ReturnGroupUser
 */
userRouter.patch('/', loginRequired, async(async (req, res) => {
  if (req.selectedGroupUser == null) throw 'UserNotFound';
  let data;
  if (req.user.id !== req.selectedUser.id && !req.user.isAdmin) {
    // Check permission to modify user first
    // Or totally ignore it if admin
    if (!req.user.isAdmin && !checkPermission(req.groupUser, 'member')) {
      throw 'GroupAdminRequired';
    }
    data = pick(req.body, ['type', 'permission']);
    // Check type....
    if (!checkTypeHierarchySet(
      req.groupUser.type,
      req.selectedGroupUser.type,
      data.type,
    )) {
      throw 'GroupAdminRequired';
    }
    // Only one subAdmin is allowed
    if (data.type === 'subAdmin') {
      let result = await req.selectedGroup.getMembers({
        where: { isEnabled: true },
        through: { where: { type: 'subAdmin' } },
        limit: 1,
      });
      if (result.length >= 1) throw 'ValidationError';
    }
    // Only five staff is allowed
    if (data.type === 'staff') {
      let result = await req.selectedGroup.getMembers({
        where: { isEnabled: true },
        through: { where: { type: 'staff' } },
        limit: 5,
      });
      if (result.length >= 5) throw 'ValidationError';
    }
  } else {
    if (req.user.isAdmin) {
      data = pick(req.body, ['updatePush', 'chatPush', 'type', 'permission']);
    } else {
      data = pick(req.body, ['updatePush', 'chatPush', 'type']);
    }
    // can't change type unless invited
    if (!req.user.isAdmin && data.type != null &&
      (data.type !== 'member' || req.groupUser.type !== 'invited')
    ) {
      throw 'ValidationError';
    }
  }
  // Run validation
  let validationError = validateSingle(data, validationSchema.GroupUser);
  if (validationError != null) {
    throw { name: 'ValidationError', data: validationError };
  }
  let sanitizedData = sanitize(data, validationSchema.GroupUser);
  sanitizedData.permission = setPermission(
    Object.assign({}, req.selectedGroupUser.toJSON(), sanitizedData));
  if (req.selectedGroupUser.type === 'pending' &&
    sanitizedData.type !== 'pending' && sanitizedData.type != null
  ) {
    // Send notification
    await sendNotification(req.selectedUser, { type: 'groupAccept',
      group: req.selectedGroup });
  } else if (sanitizedData.type === 'banned') {
    // Send notification
    if (req.user.id !== req.selectedUser.id) {
      await sendNotification(req.selectedUser, { type: 'groupBan',
        group: req.selectedGroup });
    }
  } else if ((req.selectedGroupUser.permission !== sanitizedData.permission) ||
    (req.selectedGroupUser.type !== sanitizedData.type)
  ) {
    // Send notification
    if (req.user.id !== req.selectedUser.id) {
      await sendNotification(req.selectedUser, { type: 'groupUserChange',
        group: req.selectedGroup });
    }
  }
  await req.selectedGroupUser.update(sanitizedData);
  await req.selectedGroup.updateMembers();
  if (checkType(req.selectedGroupUser.type, 'member')) {
    if (req.selectedGroup.chatKey != null) {
      // Add user into chat participants
      let user = {
        id: req.selectedUser.id,
        photoThumb: req.selectedUser.photoThumb,
        nickname: req.selectedUser.nickname,
        noChatPush: !req.selectedGroupUser.chatPush,
      };
      await addChatParticipant(req.selectedGroup.chatKey, user);
    }
  }
  let result = req.selectedUser.toJSON();
  result.groupUser = req.selectedGroupUser;
  result.joinMessage = req.selectedGroupUser.joinMessage;
  result.photos = req.selectedUser.getPhotos(req.user);
  result.photoThumbs = req.selectedUser.getPhotoThumbs(req.user);
  if (!result.usePhotoInGroup) {
    delete result.photo;
    delete result.photoThumb;
    delete result.photos;
    delete result.photoThumbs;
  }
  res.json(result);
}));

/**
 * @api {delete} /groups/:groupId/users 스티커 나가기
 * @apiGroup GroupUser
 * @apiName LeaveGroup
 * @apiDescription 스티커에서 나갑니다.
 * @apiUse GetGroup
 * @apiError (Error 404) UserNotFound 입력하신 사용자를 찾을 수 없습니다.
 */
/**
 * @api {delete} /groups/:groupId/users/:userId 스티커에서 강퇴하기
 * @apiGroup GroupUser
 * @apiName KickGroup
 * @apiDescription 스티커에서 강퇴합니다.
 * @apiError (Error 400) ValidationError 시삽은 강퇴될 수 없습니다.
 * @apiError (Error 400) GroupAdminRequired 해당 회원을 강퇴할 권한이 없습니다.
 * @apiUse GetUser
 * @apiUse GetGroup
 */
userRouter.delete('/', loginRequired, async(async (req, res) => {
  if (req.selectedGroupUser == null) throw 'UserNotFound';
  if (req.user.id !== req.selectedUser.id) {
    // Check permission to modify user first
    // Or totally ignore it if admin
    if (!req.user.isAdmin && !checkPermission(req.groupUser, 'member')) {
      throw 'GroupAdminRequired';
    }
    // Check type....
    if (!checkTypeHierarchy(req.groupUser.type, req.selectedGroupUser.type)) {
      throw 'GroupAdminRequired';
    }
  } else {
    // Banned user can't kick himself
    if (req.selectedGroupUser.type === 'banned') throw 'ValidationError';
  }
  // Obviously we can't kick admin
  if (!req.user.isAdmin && req.selectedGroupUser.type === 'admin') {
    throw 'ValidationError';
  }
  // Remove the user
  await req.selectedGroup.removeMember(req.selectedUser);
  await req.selectedGroup.updateMembers();
  // Remove user from chat participants
  if (req.selectedGroup.chatKey != null) {
    await removeChatParticipant(req.selectedGroup.chatKey, req.selectedUser);
		let receivers = await User.findAll({
		  where: { isEnabled: true },
			include: [{
			  model: GroupUser,
				where: {
				  groupId: req.selectedGroup.id,
					type: {
					  $in: ['admin', 'subAdmin'],
					},
				},
				attributes: [],
		  }],
	  });
		for(let admin of receivers){
		  sendNotification(admin, { type: 'groupLeave',
			  group: req.selectedGroup,
				user: req.selectedUser,
			});
		}
    receivers = receivers.map(user => user.nickname);
		await addChatData(req.selectedGroup.chatKey, req.selectedUser,"(관리자에게만) "+ req.selectedUser.nickname + "님께서 모임을 탈퇴 하셨습니다.",receivers);
  }
  // Send notification
  if (req.user.id !== req.selectedUser.id) {
    sendNotification(req.selectedUser, { type: 'groupKick',
      group: req.selectedGroup });
  }
  res.json({});
}));
