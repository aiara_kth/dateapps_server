import { Router } from 'express';

import iap from '../util/iap';
import async from '../util/async';
import loginRequired from '../util/loginRequired';
import adminRequired from '../util/adminRequired';
import { groupAdminRequired, groupPermissionRequired,
  checkPermission, groupMemberRequired }
  from '../util/groupAdminRequired';
import photoUpload, { upload, uploadHandler } from '../util/photoUpload';
import pick from '../util/pick';
import injectGroupUser from '../util/injectGroupUser';
import { injectOtherUserInfo } from '../util/injectUserInfo';
import { createMeetingQuery } from '../util/createQuery';
import sendNotification from '../util/notification';
import { createChat, addChatParticipant, addChatData } from '../util/chat';
import { doPayment, BILLING_IDS } from '../util/payment';
import isTrue from '../util/isTrue';
import processUserPhotoGroup from '../util/processUserPhotoGroup';

import { meetingRouter } from '../meeting';

import { validateSingle, sanitize } from '../../validation/validate';
import * as validationSchema from '../../validation/schema';

import { sequelize, User, Group, GroupUser, GroupPost, GroupReport, Meeting }
  from '../../db';

import userRouter from './user';
import postRouter from './post';

/**
 * @apiDefine GetGroup
 * @apiParam (Parameter) {Number} groupId 스티커 번호
 * @apiError (Error 404) GroupNotFound 입력하신 스티커을 찾을 수 없습니다.
 */

/**
 * @apiDefine GroupAdminRequired 스티커 권한 필요
 *   사용하려면 스티커의 수정 권한이 필요 합니다.
 * @apiError (Error 403) GroupAdminRequired 해당 자원을 수정할 스티커 권한이 없습니다.
 */

/**
 * @apiDefine GroupSubAdminRequired 스티커 부시삽 필요
 *   사용하려면 스티커의 부시삽 권한이 필요 합니다.
 * @apiError (Error 403) GroupSubAdminRequired 스티커의 시삽이어야 합니다.
 */

/**
 * @apiDefine GroupStaffRequired 스티커 운영진 필요
 *   사용하려면 스티커의 운영진이어야 합니다.
 * @apiError (Error 403) GroupStaffRequired 스티커의 운영진이어야 합니다.
 */

/**
 * @apiDefine GroupMemberRequired 스티커 멤버 필요
 *   사용하려면 스티커의 회원이어여야 합니다.
 * @apiError (Error 403) GroupMemberRequired 스티커의 회원이어야 합니다.
 */

/**
 * @apiDefine ReturnGroup
 * @apiSuccess {Group} _ 스티커 정보
 * @apiSuccess {Number} _.id 고유 번호
 * @apiSuccess {String} _.category 카테고리
 * @apiSuccess {String} _.name 이름
 * @apiSuccess {String} _.description 설명
 * @apiSuccess {Number} _.location 위치
 * @apiSuccess {Boolean} _.needApproval 가입시 수락 필요 여부
 * @apiSuccess {StringNullable} _.invitationKey 초대 키
 * @apiSuccess {StringNullable} _.profilePhoto 프로필 사진
 * @apiSuccess {StringNullable} _.profilePhotoThumb 프로필 사진 썸네일
 * @apiSuccess {StringNullable} _.mainPhoto 메인 사진
 * @apiSuccess {StringNullable} _.mainPhotoThumb 메인 사진 썸네일
 * @apiSuccess {Number} _.membersCount 멤버 수
 * @apiSuccess {Number} _.postsCount 게시글 수
 * @apiSuccess {Number} _.score 점수
 * @apiSuccess {Date} _.createdAt 그룹 생성 시각
 * @apiSuccess {Date} _.updatedAt 그룹 업데이트 시각
 * @apiSuccess {Boolean} _.isValid 그룹 결제 여부
 * @apiSuccess {Boolean} _.cancelPending 삭제 진행중 여부
 */

/**
 * @apiDefine ReturnGroupUser
 * @apiSuccess {GroupUser} _.groupUser 그룹에서의 사용자 상태
 * @apiSuccess {String=banned,invited,pending,member,staff,subAdmin,admin} _.groupUser.type 사용자 권한
 * @apiSuccess {Boolean} _.groupUser.updatePush 업데이트 푸시 알림 수신 여부
 * @apiSuccess {Boolean} _.groupUser.chatPush 채팅 푸시 알림 수신 여부
 * @apiSuccess {Date} _.groupUser.createdAt 가입 시각
 */

export const groupRouter = new Router();

/**
 * @api {get} /groups/:groupId/ ID에 속한 스티커 가져오기
 * @apiGroup Group
 * @apiName GetGroup
 * @apiDescription ID에 속하는 스티커을 가져옵니다.
 * @apiSuccess {String} _.subscriptionTransactionId 결제 ID (시삽, 관리자만)
 * @apiSuccess {String} _.subscriptionProvider 결제 PG사 유형 (시삽, 관리자만)
 * @apiSuccess {Date} _.subscriptionValidUntil 결제 유효기간 (시삽, 관리자만)
 * @apiUse ReturnGroup
 * @apiUse ReturnGroupUser
 * @apiUse GetGroup
 */
groupRouter.get('/', async(async (req, res) => {
  let data = req.selectedGroup.toJSON();
  data.groupUser = req.groupUser;
  if (req.groupUser != null && !(req.groupUser.type === 'invited' ||
    req.groupUser.type === 'pending' ||
    req.groupUser.type === 'banned') && req.user != null && req.user.isEnabled
  ) {
    if (req.selectedGroup.chatKey == null) {
      // Create chat
      let users = await req.selectedGroup.getMembers({
        where: sequelize.where(sequelize.literal('groupUser.type'), {
          $notIn: ['invited', 'pending'],
        }),
      });
      users = users.map(v => ({
        id: v.id,
        photoThumb: v.photoThumb,
        nickname: v.nickname,
        noChatPush: !v.groupUser.chatPush,
      }));
      let chatKey = await createChat(req.selectedGroup.name, users,
        req.selectedGroup, false);
      await req.selectedGroup.update({ chatKey });
      data.chatKey = chatKey;
    } else {
      data.chatKey = req.selectedGroup.chatKey;
    }
  }
  if ((req.groupUser != null && req.groupUser.type === 'admin') ||
    (req.user != null && req.user.isAdmin)
  ) {
    data.subscriptionTransactionId =
      req.selectedGroup.subscriptionTransactionId;
    data.subscriptionProvider =
      req.selectedGroup.subscriptionProvider;
    data.subscriptionValidUntil =
      req.selectedGroup.subscriptionValidUntil;
  }
  res.json(data);
}));

/**
 * @api {patch} /groups/:groupId/ 스티커 프로필 수정하기
 * @apiGroup Group
 * @apiName PatchGroupProfile
 * @apiDescription 스티커 프로필을 수정합니다.
 *
 *   사진 업로드를 하기 위해서는 `multipart/form-data`를 사용해 주세요.
 *
 * @apiParam (Body) {String} category 카테고리
 * @apiParam (Body) {String} name 이름
 * @apiParam (Body) {String} description 설명
 * @apiParam (Body) {Number} location 지역
 * @apiParam (Body) {Boolean} needApproval 가입 승인 필요 여부
 * @apiParam (Body) {File} [profilePhoto] 프로필 사진
 * @apiParam (Body) {File} [mainPhoto] 메인 사진
 * @apiUse ReturnGroup
 * @apiUse GetGroup
 * @apiPermission GroupAdminRequired
 * @apiUse GroupAdminRequired
 */
groupRouter.patch('/', groupAdminRequired, upload.fields([
  // { name: 'profilePhoto', maxCount: 1 },
  { name: 'mainPhoto', maxCount: 1 },
]), uploadHandler, async(async (req, res) => {
  // Validate data
  const groupData = pick(req.body, ['category', 'name', 'description',
    'location', 'needApproval']);
  /*
  if (req.photos && req.photos.profilePhoto != null) {
    groupData.profilePhoto = req.photos.profilePhoto[0];
  }
  */
  if (req.photos && req.photos.mainPhoto != null) {
    groupData.mainPhoto = req.photos.mainPhoto[0];
  }
  // Run validation
  let validationError = validateSingle(Object.assign({},
    req.selectedGroup.toJSON(), groupData), validationSchema.Group);
  if (validationError != null) {
    throw { name: 'ValidationError', data: validationError };
  }
  let sanitizedGroupData = sanitize(groupData, validationSchema.Group);
  if (req.user && req.user.isAdmin && req.body.subscriptionValidUntil != null) {
    sanitizedGroupData.subscriptionValidUntil =
      new Date(req.body.subscriptionValidUntil);
    if (req.selectedGroup.subscriptionProvider == null) {
      req.selectedGroup.subscriptionProvider = 'free';
    }
  }
  if (req.user && req.user.isAdmin && req.body.forceRanking != null) {
    let forceRanking = parseInt(req.body.forceRanking);
    if (!isNaN(forceRanking)) {
      sanitizedGroupData.forceRanking = forceRanking;
      // If the entry already exists on that number, set that to zero
      if (forceRanking > 0) {
        await Group.update({ forceRanking: 0 }, { where: { forceRanking } });
      }
    }
  }
  await req.selectedGroup.update(sanitizedGroupData);
  let data = req.selectedGroup.toJSON();
  data.groupUser = req.groupUser;
  if ((req.groupUser != null && req.groupUser.type === 'admin') ||
    (req.user != null && req.user.isAdmin)
  ) {
    data.subscriptionTransactionId =
      req.selectedGroup.subscriptionTransactionId;
    data.subscriptionProvider =
      req.selectedGroup.subscriptionProvider;
    data.subscriptionValidUntil =
      req.selectedGroup.subscriptionValidUntil;
  }
  // All done
  res.json(data);
}));

/**
 * @api {delete} /groups/:groupId/ 스티커 폐쇄하기
 * @apiGroup Group
 * @apiName DeleteGroup
 * @apiDescription 스티커를 폐쇄합니다. 결제가 이미 되어 있으면 결제 period가 끝날 때
 *   까지 계속 유효합니다.
 *
 *   **결제 플랫폼의 한계로 자동으로 서버에서 결제를 취소할 수가 없습니다.**
 *   **서버에서 주어지는 subscriptionTransactionId를 통해 클라이언트에서 결제를**
 *   **취소한 뒤 이 API를 호출해야 합니다.**
 * @apiUse GetGroup
 * @apiPermission GroupAdminRequired
 * @apiUse GroupAdminRequired
 */
groupRouter.delete('/', groupAdminRequired, async(async (req, res) => {
  if (req.query.force != null) {
    // TODO Call revoke API
    await req.selectedGroup.update({ deletedAt: new Date() });
    res.json({});
  } else {
    if (req.selectedGroup.isValid) {
      // TODO Call cancellation
      await req.selectedGroup.update({ cancelPending: true });
    } else {
      await req.selectedGroup.update({ deletedAt: new Date() });
    }
    res.json({});
  }
}));

/**
 * @api {post} /groups/:groupId/undelete 스티커 폐쇄 취소하기
 * @apiGroup Group
 * @apiName UndeleteGroup
 * @apiDescription 스티커를 폐쇄를 취소합니다.
 * @apiUse GetGroup
 * @apiPermission AdminRequired
 * @apiUse AdminRequired
 */
groupRouter.post('/undelete', adminRequired, async(async (req, res) => {
  await req.selectedGroup.update({ deletedAt: null });
  res.json({});
}));

/**
 * @api {post} /groups/:groupId/pay 스티커 결제하기
 * @apiGroup Group
 * @apiName PayGroup
 * @apiDescription 스티커를 결제합니다. 이미 결제가 된 항목이 있으면 서버에서는
 *   **원본 결제를 취소하지 않고** 덮어 씁니다. 시삽에게 주어지는
 *   subscriptionTransactionId를 사용하여 먼저 취소한 뒤 이 API를 호출해야 합니다.
 * @apiParam (Body) {String} id 품목 이름
 * @apiParam (Body) {String} provider PG 회사 종류 (google, apple)
 * @apiParam (Body) {String} data 결제 데이터
 * @apiParam (Body) {String} signature PG사 서명
 * @apiError (Error 404) NotFound 입력하신 품목을 찾을 수 없습니다.
 * @apiError (Error 403) Forbidden 거래 검증에 실패했습니다.
 * @apiUse GetGroup
 * @apiPermission GroupAdminRequired
 * @apiUse GroupAdminRequired
 */
groupRouter.post('/pay', groupAdminRequired, async(async (req, res) => {
  // Lookup the payment id
  let { id, data, signature } = req.body;
  if (!BILLING_IDS.includes(id)) throw 'NotFound';
  // Run verificiation
  let receiptItem = await new Promise((resolve, reject) => {
    iap.validate(iap.GOOGLE, { data, signature }, (err, response) => {
      if (err) return reject(err);
      if (iap.isValidated(response)) {
        let purchaseList = iap.getPurchaseData(response);
        let item = purchaseList && purchaseList[0];
        if (item == null) return reject('Forbidden');
        if (iap.isExpired(item)) reject('Forbidden');
        return resolve(item);
      } else {
        return reject('Forbidden');
      }
    });
  });
  if (receiptItem.productId !== id) {
    throw new Error('Receipt item and ID does not match.' +
      receiptItem.productId + ' - ' + id);
  }
  let otherGroup = await Group.findOne({ where: {
    subscriptionTransactionId: receiptItem.orderId || receiptItem.transactionId,
  } });
  if (otherGroup != null && otherGroup.id !== req.selectedGroup.id) {
    throw 'Forbidden';
  }
  // All done! :)
  await req.selectedGroup.update({
    subscriptionData: data,
    subscriptionSignature: signature,
    subscriptionProvider: 'google',
    subscriptionTransactionId: receiptItem.orderId || receiptItem.transactionId,
    subscriptionValidUntil: new Date(receiptItem.expirationDate),
  });
}));

/**
 * @api {post} /groups/:groupId/photo 스티커 사진 올리기
 * @apiGroup Group
 * @apiName PostGroupPhoto
 * @apiDescription 스티커 사진을 수정합니다.
 *
 *   사진 업로드를 하기 위해서는 `multipart/form-data`를 사용해 주세요.
 *
 * @apiParam (Body) {File} [profilePhoto] 프로필 사진
 * @apiParam (Body) {File} [mainPhoto] 메인 사진
 * @apiUse GetGroup
 * @apiUse ReturnGroup
 * @apiPermission GroupAdminRequired
 * @apiUse GroupAdminRequired
 */
groupRouter.post('/photo', groupAdminRequired, upload.fields([
  { name: 'profilePhoto', maxCount: 1 },
  { name: 'mainPhoto', maxCount: 1 },
]), uploadHandler, async(async (req, res) => {
  // Validate data
  const groupData = {};
  if (req.photos && req.photos.profilePhoto != null) {
    groupData.profilePhoto = req.photos.profilePhoto[0];
  }
  if (req.photos && req.photos.mainPhoto != null) {
    groupData.mainPhoto = req.photos.mainPhoto[0];
  }
  // Run validation
  let validationError = validateSingle(Object.assign({},
    req.selectedGroup.toJSON(), groupData), validationSchema.Group);
  if (validationError != null) {
    throw { name: 'ValidationError', data: validationError };
  }
  let sanitizedGroupData = sanitize(groupData, validationSchema.Group);
  req.selectedGroup.update(sanitizedGroupData);
  // All done
  res.json(req.selectedGroup);
}));

/**
 * @api {get} /groups/:id/reports 신고 내역 보기
 * @apiGroup Group
 * @apiName GetGroupReports
 * @apiDescription 스티커 신고 내역을 봅니다.
 * @apiParam (Query) {Number} [limit=20] 불러올 개수
 * @apiParam (Query) {Number} [page=0] 불러올 페이지 번호.
 * @apiUse GetGroup
 * @apiPermission AdminRequired
 * @apiUse AdminRequired
 */
groupRouter.get('/reports', adminRequired, async(async (req, res) => {
  let { limit = 20, page = 0 } = req.query;
  limit = parseInt(limit) || 0;
  page = parseInt(page) || 0;
  if (limit > 150) limit = 150;
  let result = await GroupReport.findAll({
    where: {
      reporteeId: req.selectedGroup.id,
    },
    offset: limit * page,
    limit,
    include: {
      model: User,
      as: 'reporter',
      attributes: ['id', 'name', 'nickname', 'photos', 'photoPrimary'],
    },
  });
  if (req.query.count != null) {
    let count = await req.selectedGroup.countReporters();
    res.json({ count, result });
  } else {
    res.json(result);
  }
}));

/**
 * @api {post} /groups/:id/reports 신고하기
 * @apiGroup Group
 * @apiName PostGroupReport
 * @apiDescription 스티커를 신고합니다.
 * @apiParam (Body) {String} [reason] 신고 사유
 * @apiError (Error 409) Conflict 이미 처리되었습니다.
 * @apiUse GetGroup
 * @apiPermission LoginRequired
 * @apiUse LoginRequired
 */
groupRouter.post('/reports', loginRequired, async(async (req, res) => {
  if (await req.selectedGroup.hasReporter(req.user)) throw 'Conflict';
  await req.selectedGroup.addReporter(req.user, { reason: req.body.reason });
  await req.selectedGroup.increment('reportCount');
  res.json({});
}));

/**
 * @api {delete} /groups/:id/reports 신고 내역 초기화
 * @apiGroup Group
 * @apiName DeleteGroupReports
 * @apiDescription 스티커 신고 내역을 초기화합니다.
 * @apiUse GetGroup
 * @apiPermission AdminRequired
 * @apiUse AdminRequired
 */
groupRouter.delete('/reports', adminRequired, async(async (req, res) => {
  await GroupReport.destroy({ where: {
    reporteeId: req.selectedGroup.id,
  }});
  await req.selectedGroup.update({ reportCount: 0 });
  res.json({});
}));

/**
 * @api {get} /groups/:groupId/users 스티커 사용자 목록 가져오기
 * @apiGroup GroupUser
 * @apiName GetGroupUsers
 * @apiDescription 스티커의 사용자 목록을 가져옵니다.
 * @apiUse GetGroup
 * @apiParam (Query) {Number} [limit=20] 불러올 개수입니다.
 * @apiParam (Query) {Number} [page=0] 불러올 페이지 번호입니다.
 * @apiSuccess {User[]} _ 사용자 목록
 * @apiSuccess {String} _.joinMessage 가입 인사
 * @apiUse ReturnUser
 * @apiUse ReturnGroupUser
 */
groupRouter.get('/users', async(async (req, res) => {
  let { limit = 20, page = 0 } = req.query;
  limit = parseInt(limit) || 0;
  page = parseInt(page) || 0;
  if (limit > 150) limit = 150;
  let where = [sequelize.where(sequelize.literal('groupUser.type'), {
    $notIn: ['invited', 'banned'],
  }), {
    isEnabled: true,
  }];
  let result = await req.selectedGroup.getMembers({
    where,
    order: [
		  [sequelize.literal('groupUser.type'), 'DESC'],
			[sequelize.literal('groupUser.createdAt'),'ASC'],
			],
    offset: limit * page,
    limit,
  });
  result = await Promise.all(result.map(async user => {
    let data = await injectOtherUserInfo(user, req.user);
    if (!data.usePhotoInGroup) {
      delete data.photo;
      delete data.photoThumb;
    }
    data.groupUser = user.groupUser;
    data.joinMessage = user.groupUser.joinMessage;
    return data;
  }));
  if (req.query.count != null) {
    let count = await req.selectedGroup.countMembers({
      where,
    });
    res.json({ count, result });
  } else {
    res.json(result);
  }
}));
/**
 * @api {post} /groups/:groupId/chatPush 스티커 채팅 알림
 * @apiGroup Group
 * @apiName ChatPush
 * @apiDescription 스티커에 채팅 알림을 전송합니다
 * @apiUse GetGroup
 * @apiParam (Body) {Number[]} tags 태그된 회원 목록, 비어있으면 그룹 전체에 알림을 전송합니다.
 * @apiParam (Body) {String} message 푸시로 전송할 채팅 메세지 입니다.
 * @apiPermission LoginRequired
 * @apiUse LoginRequired
 * @apiPermission GroupMemberRequired
 * @apiUse GroupMemberRequired
 */
groupRouter.post('/chatPush',groupMemberRequired, async(async (req,res) => {
  let { tags, message } = req.body;
  let ids = tags === '' ? [] : (tags || '').split(',').map(v => parseInt(v));
  let where = { isEnabled: true };
  if (0<ids.length){
    where.id = ids
  }
  let targetUsers = await User.findAll({
    where,
    include: [{
      model: GroupUser,
      where: {
        updatePush: true,
        groupId: req.selectedGroup.id,
        type: {
          $in: ['admin', 'subAdmin','staff','member'],
        },
      },
      attributes: [],
    }],
  });
  for(let user of targetUsers){
    await sendNotification(user, {
      type: 'groupChat',
      group: req.selectedGroup,
      user: req.user,
      text: message,
    });
  }
  res.json({});
}));
/**
 * @api {post} /groups/:groupId/users 스티커 가입하기
 * @apiGroup GroupUser
 * @apiName JoinGroup
 * @apiDescription 스티커에 가입합니다.
 * @apiUse GetGroup
 * @apiParam (Query) {Boolean} [force=false] 관리자라면 강제로 가입할지 결정합니다.
 * @apiParam (Body) {Boolean} [updatePush=true] 업데이트 푸시 알림 수신 여부
 * @apiParam (Body) {Boolean} [chatPush=true] 채팅 푸시 알림 수신 여부
 * @apiParam (Body) {String} joinMessage 가입 인사
 * @apiError (Error 409) Conflict 이미 처리되었습니다.
 * @apiError (Error 403) Forbidden 영구 탈퇴되어 다시 가입할 수 없습니다.
 * @apiError (Error 403) GroupThreshold 최대 가입 가능한 스티커 수를 초과했습니다.
 * @apiUse ReturnGroupUser
 * @apiPermission LoginRequired
 * @apiUse LoginRequired
 * @apiPermission AdminRequired
 * @apiUse AdminRequired
 */
groupRouter.post('/users', async(async (req, res) => {
  if (req.groupUser != null) {
    if (req.groupUser.type === 'banned') throw 'Forbidden';
    else if (req.groupUser.type === 'invited') {
      // Just continue;
    } else throw 'Conflict';
  }
  if (await req.user.countGroups({
    through: { where: { type: { $notIn: ['banned', 'invited'] } } },
    where: {
      deletedAt: { $or: [null, { $gte: new Date() }] },
    },
  }) >= 8) throw 'GroupThreshold';
  if (req.query.force && !req.user.isAdmin) throw 'AdminRequired';
  const data = pick(req.body, ['updatePush', 'chatPush', 'joinMessage']);
  data.type = 'pending';
  // Allow empty message for backward compatibility
  if (data.joinMessage == null) data.joinMessage = '';
  if (req.query.force || !req.selectedGroup.needApproval) data.type = 'member';
  if (req.groupUser != null && req.groupUser.type === 'invited') {
    data.type = 'member';
  }
  // Run validation
  let validationError = validateSingle(data, validationSchema.GroupUser);
  if (validationError != null) {
    throw { name: 'ValidationError', data: validationError };
  }
  let sanitizedData = sanitize(data, validationSchema.GroupUser);
  // Create GroupUser and we're good to go
  let groupUser = await GroupUser.create(Object.assign({
    userId: req.user.id, groupId: req.selectedGroup.id,
  }, sanitizedData));
  await req.selectedGroup.updateMembers();
  // Send notification to admins
  let users = await User.findAll({
    where: { isEnabled: true },
    include: [{
      model: GroupUser,
      where: {
        updatePush: true,
        groupId: req.selectedGroup.id,
        type: {
          $in: ['admin', 'subAdmin'],
        },
      },
      attributes: [],
    }],
  });
  for (let user of users) {
    await sendNotification(user, {
      type: 'groupJoin',
      group: req.selectedGroup,
      user: req.user,
    });
  }
  if (data.type !== 'pending' && req.selectedGroup.chatKey != null) {
    // Add user into chat participants
    await addChatParticipant(req.selectedGroup.chatKey, req.user);
		await addChatData(req.selectedGroup.chatKey,req.user,data.joinMessage);
    let targetUsers = await req.selectedGroup.getMembers({
      where: sequelize.where(sequelize.literal('groupUser.type'), {
        $notIn: ['invited', 'pending'],
      }),
    });
    for(let user of targetUsers){
      await sendNotification(user, {
        type: 'groupChat',
        group: req.selectedGroup,
        user: req.user,
        text: data.joinMessage,
      });
    }
  }
  res.json(groupUser);
}));

groupRouter.use('/users/:userId', async(async (req, res, next) => {
  // Pass to userRouter
  req.selectedUser = await User.findById(req.params.userId);
  if (req.selectedUser == null) throw 'UserNotFound';
  next();
}), userRouter);

groupRouter.use('/users', loginRequired, async(async (req, res, next) => {
  // Pass to userRouter
  req.selectedUser = req.user;
  next();
}), userRouter);

/**
 * @api {get} /groups/:groupId/notices 스티커 공지사항 목록 가져오기
 * @apiGroup GroupPost
 * @apiName GetGroupNotices
 * @apiDescription 스티커의 공지사항 목록을 가져옵니다.
 * @apiUse GetGroup
 * @apiSuccess {NullablePost[]} _ 게시글 목록. 자리가 비어 있으면 null이 출력됨
 * @apiSuccess {Boolean} _.liked 좋아요 했는지 여부
 * @apiUse ReturnPost
 */
groupRouter.get('/notices', async(async (req, res) => {
  const noticesList = req.selectedGroup.notices;
  let result = await req.selectedGroup.getPosts({
    where: {
      // Workaround empty array issue
      id: { $in: noticesList.concat([-999]) },
    },
    include: [{
      model: User,
      attributes: ['id', 'nickname', 'photos', 'photoPrimary',
        'usePhotoInGroup'],
      where: { isEnabled: true },
    }, {
      model: User,
      as: 'likeUsers',
      attributes: ['id'],
      required: false,
      where: { id: req.user && req.user.id },
    }],
  });
  let newResult = [];
  for (let i = 0; i < 8; ++i) {
    newResult[i] = null;
  }
  for (let post of result) {
    let data = post.toJSON();
    if (req.user != null) data.liked = await post.hasLikeUser(req.user);
    data.user = post.user && processUserPhotoGroup(post.user);
    delete data.likeUsers;
    data.liked = post.likeUsers.length > 0;
    data.noticeId = noticesList.indexOf(data.id);
    newResult[data.noticeId] = data;
  }
  res.json(newResult);
}));

/**
 * @api {put} /groups/:groupId/notices 스티커 공지사항 목록 덮어쓰기
 * @apiGroup GroupPost
 * @apiName PutGroupNotices
 * @apiDescription 스티커의 공지사항 목록을 덮어씁니다.
 * @apiUse GetGroup
 * @apiParam (Body) {Number[]} notices 공지사항 ID 목록. ,로 구분합니다.
 * @apiError (Error 404) NotFound 게시글을 찾을 수 없습니다.
 * @apiUse GroupAdminRequired
 */
groupRouter.put('/notices', groupPermissionRequired('post'),
async(async (req, res) => {
  let noticesList;
  if (req.body.notices === '') noticesList = [];
  else noticesList = req.body.notices.split(',').map(v => parseInt(v));
  let result = await req.selectedGroup.getPosts({
    where: {
      id: { $in: noticesList },
    },
    include: [{
      model: User,
      attributes: ['id', 'nickname', 'photos', 'photoPrimary',
        'usePhotoInGroup'],
      where: { isEnabled: true },
    }, {
      model: User,
      as: 'likeUsers',
      attributes: ['id'],
      required: false,
      where: { id: req.user && req.user.id },
    }],
  });
  if (result.length < noticesList.length) throw 'NotFound';
  // Write and we're good to go
  await req.selectedGroup.update({ notices: noticesList });
  res.json({});
}));

/**
 * @api {get} /groups/:groupId/posts 스티커 게시글 목록 가져오기
 * @apiGroup GroupPost
 * @apiName GetGroupPosts
 * @apiDescription 스티커의 게시글 목록을 가져옵니다.
 * @apiUse GetGroup
 * @apiParam (Query) {Number} [userId] 사용자 고유 번호 필터입니다.
 * @apiParam (Query) {String} [title] 제목 필터입니다.
 * @apiParam (Query) {String} [category] 카테고리 필터입니다. !로  시작하면 해당 항목만 제외
 * @apiParam (Query) {Boolean} [showNotice=false] 공지사항을 보여줄 지 여부입니다.
 * @apiParam (Query) {String=newest,oldest, newestUpdate, oldestUpdate} [sort=newest] 정렬 기준
 * @apiParam (Query) {Number} [limit=20] 불러올 개수입니다.
 * @apiParam (Query) {Number} [page=0] 불러올 페이지 번호입니다.
 * @apiSuccess {Post[]} _ 게시글 목록
 * @apiSuccess {Boolean} _.liked 좋아요 했는지 여부
 * @apiUse ReturnPost
 */
groupRouter.get('/posts', async(async (req, res) => {
  let { limit = 20, page = 0, sort, showNotice } = req.query;
  limit = parseInt(limit) || 0;
  page = parseInt(page) || 0;
  if (limit > 50) limit = 50;
  let where = {};
  if (!isTrue(showNotice)) {
    where.id = {
      // Workaround empty array issue
      $notIn: req.selectedGroup.notices.concat([-999]),
    };
  }
  if (req.query.userId != null) {
    where.userId = parseInt(req.query.userId);
  }
  if (req.query.title != null) {
    where.title = { $like: `%${req.query.title}%` };
  }
  if (req.query.category != null) {
    if (req.query.category.charAt(0) === '!') {
      where.category = { $not: req.query.category.slice(1) };
    } else {
      where.category = req.query.category;
    }
  }
  let order = ['createdAt', 'DESC'];
  if (sort === 'newest') order = ['createdAt', 'DESC'];
  if (sort === 'oldest') order = ['createdAt', 'ASC'];
  if (sort === 'newestUpdate') order = ['updatedAt', 'DESC'];
  if (sort === 'oldestUpdate') order = ['updatedAt', 'ASC'];
  let result = await req.selectedGroup.getPosts({
    where,
    order: [order],
    offset: limit * page,
    limit,
    include: [{
      model: User,
      attributes: ['id', 'nickname', 'photos', 'photoPrimary',
        'usePhotoInGroup'],
      where: { isEnabled: true },
    }, {
      model: User,
      as: 'likeUsers',
      attributes: ['id'],
      required: false,
      where: { id: req.user && req.user.id },
    }],
  });
  let newResult = [];
  const noticesList = req.selectedGroup.notices;
  for (let post of result) {
    let data = post.toJSON();
    if (req.user != null) data.liked = await post.hasLikeUser(req.user);
    data.user = post.user && processUserPhotoGroup(post.user);
    delete data.likeUsers;
    data.liked = post.likeUsers.length > 0;
    data.noticeId = noticesList.indexOf(data.id);
    newResult.push(data);
  }
  if (req.query.count != null) {
    let count = await req.selectedGroup.countPosts({ where });
    res.json({ count, result: newResult });
  } else {
    res.json(newResult);
  }
}));

/**
 * @api {post} /groups/:groupId/posts 스티커 게시글 작성하기
 * @apiGroup GroupPost
 * @apiName PostGroupPosts
 * @apiDescription 스티커에 게시글을 올립니다.
 *
 *   사진 업로드를 하기 위해서는 `multipart/form-data`를 사용해 주세요.
 *   `notice` 카테고리는 운영진 이상만 사용할 수 있습니다.
 *
 * @apiUse GetGroup
 * @apiParam (Body) {String} title 제목
 * @apiParam (Body) {String} category 카테고리.
 * @apiParam (Body) {Number} [noticeId] 공지 번호. -1 ~ 7
 * @apiParam (Body) {String} body 내용
 * @apiParam (Body) {File[]} photos 사진
 * @apiSuccess {Post} _ 게시글
 * @apiUse ReturnPost
 * @apiError (Error 403) PhotoThreshold 사진의 최대 개수를 초과했습니다.
 * @apiUse GroupAdminRequired
 * @apiPermission GroupMemberRequired
 * @apiUse GroupMemberRequired
 */
groupRouter.post('/posts', groupMemberRequired, photoUpload,
async(async (req, res) => {
  const data = pick(req.body, ['title', 'category', 'body', 'noticeId']);
  // Run validation
  let validationError = validateSingle(data, data.category === "gallery" ? validationSchema.GalleryPost:validationSchema.GroupPost);
  if (validationError != null) {
    throw { name: 'ValidationError', data: validationError };
  }
  let sanitizedData = sanitize(data, data.category === "gallery" ? validationSchema.GalleryPost:validationSchema.GroupPost);
  sanitizedData.photos = req.photos || [];
  if (sanitizedData.photos.length > 8) throw 'PhotoThreshold';
  if (sanitizedData.category === 'notice' ||
    (data.noticeId != null && data.noticeId !== '-1')
  ) {
    // Check permission, and reject if not staff
    if (!req.user.isAdmin && !checkPermission(req.groupUser, 'post')) {
      throw 'GroupAdminRequired';
    }
  }
  // Upload to group
  let post = await GroupPost.create(Object.assign({}, sanitizedData, {
    userId: req.user.id,
    groupId: req.selectedGroup.id,
  }));
  await req.selectedGroup.updatePosts();
  await req.selectedGroup.updateNotices(post.id, data.noticeId);
  res.json(Object.assign({}, post.toJSON(), {
    user: req.user,
    noticeId: req.selectedGroup.notices.indexOf(post.id),
  }));
}));

groupRouter.use('/posts/:postId', async(async (req, res, next) => {
  // Pass to postRouter
  req.selectedPost = await GroupPost.findOne({
    where: {
      groupId: req.selectedGroup.id,
      id: req.params.postId,
    },
    include: [ User ],
  });
  if (req.selectedPost == null) throw 'NotFound';
  next();
}), postRouter);

/**
 * @api {get} /groups/:groupId/meetings/ 모임 목록
 * @apiGroup Group
 * @apiName GetGroupMeetings
 * @apiUse GetGroup
 * @apiDescription 모임 목록을 가져옵니다.
 * @apiParam (Query) {String=likes,members,newest,oldest} [sort=newest] 정렬 기준
 * @apiParam (Query) {String} [title] 이름 필터
 * @apiParam (Query) {String} [category] 카테고리 필터
 * @apiParam (Query) {Number} [location] 지역 필터
 * @apiParam (Query) {Date} [dateFrom] 날짜 필터
 * @apiParam (Query) {Date} [dateTo] 날짜 필터
 * @apiParam (Query) {Number} [limit=20] 불러올 개수
 * @apiParam (Query) {Number} [page=0] 불러올 페이지 번호.
 * @apiSuccess {Meeting[]} _ 모임 목록
 * @apiUse ReturnMeeting
 */
groupRouter.get('/meetings', async(async (req, res) => {
  let query = createMeetingQuery(req);
  query.where.lightning = false;
  query.where.groupId = req.selectedGroup.id;
  query.include.push({
    model: User,
    as: 'likeUsers',
    attributes: ['id'],
    required: false,
    where: { id: req.user && req.user.id },
  });
  let result = await Meeting.findAll(query);
  let newResult = [];
  for (let meeting of result) {
    let data = meeting.toJSON();
    delete data.likeUsers;
    data.liked = meeting.likeUsers.length > 0;
    newResult.push(data);
  }
  if (req.query.count != null) {
    let count = await Meeting.count({ where: query.where });
    res.json({ count, result: newResult });
  } else {
    res.json(newResult);
  }
}));

/**
 * @api {post} /groups/:groupId/meetings 스티커 모임 생성하기
 * @apiGroup Group
 * @apiName PostGroupMeetings
 * @apiDescriptio 스티커 모임을 생성합니다.
 * @apiUse GetGroup
 * @apiParam (Body) {String} title 제목
 * @apiParam (Body) {String} body 내용
 * @apiParam (Body) {Number} location 지역
 * @apiParam (Body) {String} place 장소
 * @apiParam (Body) {Number} price 가격
 * @apiParam (Body) {Date} date 시간
 * @apiParam (Body) {Number} minMembers 최소 참가자 수
 * @apiParam (Body) {Number} maxMembers 최대 참가자 수
 * @apiParam (Body) {File} [photo] 사진
 * @apiUse GroupAdminRequired
 * @apiSuccess {Meeting} _ 모임
 * @apiUse ReturnMeeting
 */
groupRouter.post('/meetings', groupPermissionRequired('meeting'),
upload.array('photo', 1), uploadHandler, async(async (req, res) => {
  // Handles dedupe here
  // Try to get meeting
  let found = await Meeting.findAll({ where: {
    groupId: req.selectedGroup.id,
    lightning: false,
    date: { $gte: new Date() },
  } });
  if (found.length > 3) throw 'Conflict';
  // Run validation
  req.body.category = 'group';
  let validationError = validateSingle(req.body, validationSchema.Meeting);
  if (validationError != null) {
    throw { name: 'ValidationError', data: validationError };
  }
  let data = sanitize(req.body, validationSchema.Meeting);
  if (data.date.getTime() < Date.now()) throw 'ValidationError';
  data.userId = req.user.id;
  data.groupId = req.selectedGroup.id;
  data.lightning = false;
  if (req.photos && req.photos[0] != null) {
    data.photo = req.photos[0];
  }
  let meeting = await sequelize.transaction(async t => {
    // Create meeting
    let meeting = await Meeting.create(data, { transaction: t });
    await meeting.addMember(req.user, { transaction: t });
    return meeting;
  });
  await meeting.updateMembers();
  // All done
  res.json(meeting);
}));

/**
 * @api {post} /groups/:groupId/lightning 번개 생성하기
 * @apiGroup Group
 * @apiName PostGroupLightning
 * @apiDescription 번개를 생성합니다.
 * @apiUse GetGroup
 * @apiParam (Body) {String} title 제목
 * @apiParam (Body) {String} place 장소
 * @apiParam (Body) {Number} price 가격
 * @apiParam (Body) {Date} date 시간
 * @apiParam (Body) {Number} minMembers 최소 참가자 수
 * @apiParam (Body) {Number} maxMembers 최대 참가자 수
 * @apiParam (Body) {File} [photo] 사진
 * @apiError (Error 400) ValidationError 시간은 24시간 이내여야 합니다.
 * @apiError (Error 409) Conflict 이미 처리되었습니다.
 * @apiUse GroupAdminRequired
 * @apiSuccess {Meeting} _ 모임
 * @apiUse ReturnMeeting
 */
groupRouter.post('/lightning', groupMemberRequired, upload.array('photo', 1),
uploadHandler, async(async (req, res) => {
  // Try to get lightning
  let found = await Meeting.findOne({ where: {
    groupId: req.selectedGroup.id,
    lightning: true,
    date: { $gte: new Date() },
  } });
  if (found != null) throw 'Conflict';
  // Run validation
  let validationError = validateSingle(req.body,
    validationSchema.MeetingLightning);
  if (validationError != null) {
    throw { name: 'ValidationError', data: validationError };
  }
  let data = sanitize(req.body, validationSchema.MeetingLightning);
  if ((data.date.getTime() - Date.now()) > 24 * 60 * 60 * 1000) {
    throw 'ValidationError';
  }
  if (data.date.getTime() < Date.now()) throw 'ValidationError';
  data.userId = req.user.id;
  data.groupId = req.selectedGroup.id;
  data.lightning = true;
  data.category = 'lightning';
  data.body = '';
  data.location = req.selectedGroup.location;
  if (req.photos && req.photos[0] != null) {
    data.photo = req.photos[0];
  }
  let meeting = await sequelize.transaction(async t => {
    // Create meeting
    let meeting = await Meeting.create(data, { transaction: t });
    await meeting.addMember(req.user, { transaction: t });
    return meeting;
  });
  await meeting.updateMembers();
  // All done
  res.json(meeting);
}));

/**
 * @api {get} /groups/:groupId/lightning 번개 가져오기
 * @apiGroup Group
 * @apiName GetGroupLightning
 * @apiDescription 스티커 번개를 가져옵니다.
 *
 *   `/meetings/:meetingId` 안에 있는 API들을 여기로 호출해서 사용할 수도 있습니다.
 *   `/meetings/:meetingId/comments` -> `/groups/:groupId/lightning/comments`
 * @apiUse GetGroup
 * @apiError (Error 404) MeetingNotFound 입력하신 모임을 찾을 수 없습니다.
 * @apiSuccess {Meeting} _ 모임
 * @apiUse ReturnMeeting
 */
groupRouter.use('/lightning', async(async (req, res, next) => {
  let found = await Meeting.findOne({
    where: {
      groupId: req.selectedGroup.id,
      lightning: true,
      date: { $gte: new Date() },
    },
    include: [{ model: User, as: 'user' }],
  });
  if (found == null) throw 'MeetingNotFound';
  req.selectedMeeting = found;
  next();
}), meetingRouter);

/**
 * @api {get} /groups/:groupId/chat 채팅 가져오기
 * @apiGroup Group
 * @apiName GetGroupChat
 * @apiDescription 스티커 채팅을 가져옵니다.
 * @apiUse GetGroup
 * @apiSuccess {String} _ 채팅 키
 * @apiPermission GroupMemberRequired
 * @apiUse GroupMemberRequired
 */
groupRouter.get('/chat', groupMemberRequired, async(async (req, res) => {
  if (req.selectedGroup.chatKey == null) {
    // Create chat
    let users = await req.selectedGroup.getMembers({
      where: sequelize.where(sequelize.literal('groupUser.type'), {
        $notIn: ['invited', 'pending'],
      }),
    });
    users = users.map(v => ({
      id: v.id,
      photoThumb: v.photoThumb,
      nickname: v.nickname,
      noChatPush: !v.groupUser.chatPush,
    }));
    let chatKey = await createChat(req.selectedGroup.name, users,
      req.selectedGroup, false);
    await req.selectedGroup.update({ chatKey });
  }
  res.json(req.selectedGroup.chatKey);
}));

export const router = new Router();
export default router;

router.use('/groups/:groupId', async(async (req, res, next) => {
  // Pass to groupRouter
  req.selectedGroup = await Group.findById(req.params.groupId);
  if (req.selectedGroup == null) throw 'GroupNotFound';
  if (req.selectedGroup.deletedAt != null && (
    req.user == null || !req.user.isAdmin
  )) {
    throw 'GroupNotFound';
  }
  next();
}), injectGroupUser, groupRouter);

/**
 * @api {get} /groups/ 스티커 목록 가져오기
 * @apiGroup Group
 * @apiName GetGroups
 * @apiDescription 스티커 목록을 가져옵니다.
 * @apiParam (Query) {String=ranking,score,newest,oldest,scoreLeast,members,membersLeast,posts,postsLeast} [sort=score] 정렬 기준
 * @apiParam (Query) {String} [name] 이름 필터
 * @apiParam (Query) {String} [category] 카테고리 필터
 * @apiParam (Query) {Number} [location] 위치 필터
 * @apiParam (Query) {Boolean} [showDeleted=false] 삭제된 스티커 표출 여부
 * @apiParam (Query) {Boolean} [deleted=false] 삭제 여부
 * @apiParam (Query) {Number} [limit=20] 불러올 개수
 * @apiParam (Query) {Number} [page=0] 불러올 페이지 번호.
 * @apiSuccess {Group[]} _ 그룹 목록
 * @apiSuccess {User} _.admin 관리자 정보
 * @apiSuccess {Number} _.admin.id 고유 번호
 * @apiSuccess {StringNullable} _.admin.photo 대표 사진
 * @apiSuccess {StringNullable} _.admin.photoThumb 대표 사진 썸네일
 * @apiSuccess {NumberNullable} _.admin.photoPrimary 메인 사진 번호
 * @apiSuccess {StringNullable} _.admin.nickname 닉네임
 * @apiUse ReturnGroup
 */
router.get('/groups', async(async (req, res) => {
  // It should be able to sort by:
  // ranking, last updated date
  // It should be able to search by: name, category
  // Pagination is required, too
  let { limit = 20, page = 0, sort } = req.query;
  limit = parseInt(limit) || 0;
  page = parseInt(page) || 0;
  if (limit > 150) limit = 150;
  let where = {};
  if (isTrue(req.query.showValid) && req.user != null && req.user.isAdmin) {
    if (req.query.isValid != null) {
      if (isTrue(req.query.isValid)) {
        where.subscriptionValidUntil = { $gt: new Date() };
      } else {
        where.subscriptionValidUntil = { $or: [null, { $lte: new Date() }] };
      }
    }
  } else {
    where.subscriptionValidUntil = { $gt: new Date() };
  }
  if (req.query.name != null) {
    where.name = { $like: `%${req.query.name}%` };
  }
  if (req.query.category != null) {
    where.category = req.query.category;
  }
  if (req.query.location != null) {
    where.location = req.query.location;
  }
  if (isTrue(req.query.deleted)) {
    where.$or = [{ deletedAt: { $not: null } }, { cancelPending: true }];
  } else if (!isTrue(req.query.showDeleted)) {
    where.deletedAt = null;
    where.cancelPending = false;
  }
  where.membersCount = { $gt: 0 };
  let order = ['score', 'DESC'];
  if (sort === 'newest') order = ['createdAt', 'DESC'];
  if (sort === 'oldest') order = ['createdAt', 'ASC'];
  if (sort === 'scoreLeast') order = ['score', 'ASC'];
  if (sort === 'members') order = ['membersCount', 'DESC'];
  if (sort === 'membersLeast') order = ['membersCount', 'ASC'];
  if (sort === 'posts') order = ['postsCount', 'DESC'];
  if (sort === 'postsLeast') order = ['postsCount', 'DESC'];
  if (sort === 'report') order = ['reportCount', 'DESC'];
  let result;
  if (order[0] === 'score') {
    // Get groups with force ranking first.
    let forceRankings = await Group.findAll({
      where: Object.assign({}, where, { forceRanking: { $gt: 0 } }),
      order: [['forceRanking', 'ASC']],
      include: [{
        model: User,
        as: 'members',
        required: false,
        attributes: ['id', 'nickname', 'photos', 'photoPrimary',
          'usePhotoInGroup'],
        through: { where: { type: 'admin' } },
      }],
    });
    // We need to insert the forced groups into the results - before querying
    // actual groups, calculate what offsets and limits should be.
    // Let's assume that there are 4 forced rankings in page 1, and 3 in page 2,
    // and there are 10 items in one page.
    // Offset: 0, limit: 6 in page 1.
    // Offset: 6, limit: 7 in page 2.
    // As you can see, offset = limit * page - force ranking entries before
    // the original offset, limit = limit - force ranking entries in the
    // original range.
    // Thus, the following code would calculate them... I think?
    let rangeStart = limit * page;
    let rangeEnd = rangeStart + limit;
    let beforeCnt = 0;
    let currentCnt = 0;
    let beforeIdx = -1;
    let denyList = [-9999];
    for (let i = 0; i < forceRankings.length; ++i) {
      let entry = forceRankings[i];
      denyList.push(entry.id);
      if (entry.forceRanking - 1 < rangeStart) {
        beforeCnt += 1;
        beforeIdx = i;
      } else if (entry.forceRanking - 1 < rangeEnd) {
        currentCnt += 1;
      } else {
        break;
      }
    }
    // Tada!
    result = await Group.findAll({
      where: Object.assign({}, where, { id: { $notIn: denyList } }),
      order: [order],
      offset: limit * page - beforeCnt,
      limit: limit - currentCnt,
      include: [{
        model: User,
        as: 'members',
        required: false,
        attributes: ['id', 'nickname', 'photos', 'photoPrimary',
          'usePhotoInGroup'],
        through: { where: { type: 'admin' } },
      }],
    });
    // Then, splice the forced groups into the array.
    for (let i = beforeIdx + 1; i < forceRankings.length; ++i) {
      let entry = forceRankings[i];
      if (entry.forceRanking - 1 < rangeEnd) {
        result.splice(entry.forceRanking - 1 - rangeStart, 0, entry);
      }
    }
  } else {
    result = await Group.findAll({
      where,
      order: [order],
      offset: limit * page,
      limit,
      include: [{
        model: User,
        as: 'members',
        required: false,
        attributes: ['id', 'nickname', 'photos', 'photoPrimary',
          'usePhotoInGroup'],
        through: { where: { type: 'admin' } },
      }],
    });
  }
  result = result.map(v => Object.assign({}, v.toJSON(), {
    members: null,
    admin: v.members[0] && processUserPhotoGroup(v.members[0]),
  }));
  if (req.query.count != null) {
    let count = await Group.count({ where });
    res.json({ count, result });
  } else {
    res.json(result);
  }
}));

/**
 * @api {post} /groups/ 스티커 생성하기
 * @apiGroup Group
 * @apiName CreateGroup
 * @apiDescription 새로운 스티커을 생성합니다. 다이아가 부과됩니다.
 *
 *   사진 업로드를 하기 위해서는 `multipart/form-data`를 사용해 주세요.
 *
 * @apiParam (Body) {String} category 카테고리
 * @apiParam (Body) {String} name 이름
 * @apiParam (Body) {String} description 설명
 * @apiParam (Body) {Number} location 지역
 * @apiParam (Body) {Boolean} needApproval 가입 승인 필요 여부
 * @apiParam (Body) {File} [profilePhoto] 프로필 사진
 * @apiParam (Body) {File} [mainPhoto] 메인 사진
 * @apiUse ReturnGroup
 * @apiError (Error 403) GroupThreshold 최대 가입 가능한 스티커 수를 초과했습니다.
 * @apiError (Error 403) NoDiamonds 다이아가 부족합니다.
 */
router.post('/groups', upload.fields([
  // { name: 'profilePhoto', maxCount: 1 },
  { name: 'mainPhoto', maxCount: 1 },
]), uploadHandler, async(async (req, res) => {
  // TODO Check billing information?
  if (await req.user.countGroups({
    through: { where: { type: { $notIn: ['banned', 'invited'] } } },
    where: {
      deletedAt: { $or: [null, { $gte: new Date() }] },
    },
  }) >= 8) throw 'GroupThreshold';
	if (!req.user.isAdmin&&await req.user.countGroups({
	  through: { where: { type: 'admin' } },
		where: {
		  deletedAt: { $or: [null, { $gte: new Date() }] },
		},
	}) > 0) throw 'GroupThreshold';
  // Validate data
  const groupData = pick(req.body, ['category', 'name', 'description',
    'location', 'needApproval']);
  /*
  if (req.photos && req.photos.profilePhoto != null) {
    groupData.profilePhoto = req.photos.profilePhoto[0];
  }
  */
  if (req.photos && req.photos.mainPhoto != null) {
    groupData.mainPhoto = req.photos.mainPhoto[0];
  }
  // Run validation
  let validationError = validateSingle(groupData, validationSchema.Group);
  if (validationError != null) {
    throw { name: 'ValidationError', data: validationError };
  }
  let sanitizedGroupData = sanitize(groupData, validationSchema.Group);
  sanitizedGroupData.adminId = req.user.id;
  sanitizedGroupData.subscriptionValidUntil = new Date(Date.now() + (6 * 31 * 24 * 60 * 60 * 1000));
  sanitizedGroupData.subscriptionProvider = "free";
  // Run payment
  await doPayment(req.user, 'groupCreate');
  let group = await sequelize.transaction(async t => {
    // Create group
    if (!req.user.isAdmin&&await req.user.countGroups({
      through: { where: { type: 'admin' } },
      where: {
        deletedAt: { $or: [null, { $gte: new Date() }] },
      },
    }, {transaction: t}) > 0) throw 'GroupThreshold';
    let group = await Group.create(sanitizedGroupData, { transaction: t });
    await group.addMember(req.user, {
      type: 'admin',
      permission: 7,
      transaction: t,
    });
    return group;
  });
  await group.updateMembers();
  // All done
  let result = group.toJSON();
  // Since GroupUser result is completely deterministic, just forge one
  result.groupUser = {
    type: 'admin',
    permission: 7,
    updatePush: true,
    chatPush: true,
    createdAt: group.createdAt,
    updatedAt: group.updatedAt,
  };
  result.diamonds = req.user.diamonds;
  res.json(result);
}));
