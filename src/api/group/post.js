import { Router } from 'express';

import async from '../util/async';
import photoUpload from '../util/photoUpload';
import pick from '../util/pick';
import loginRequired from '../util/loginRequired';
import { groupMemberRequired, checkPermission }
from '../util/groupAdminRequired';
import sendNotification from '../util/notification';
import processUserPhotoGroup from '../util/processUserPhotoGroup';

import { validateSingle, sanitize } from '../../validation/validate';
import * as validationSchema from '../../validation/schema';

import { sequelize, User, GroupPostComment, GroupUser } from '../../db';

export const postRouter = new Router();
export default postRouter;

function checkModifiable(req, res, next) {
  if (req.user == null) return next('LoginRequired');
  if (req.user.isAdmin) return next();
  if (req.user.id !== req.selectedPost.userId) return next('AdminRequired');
  return next();
}

function checkCommentModifiable(req, res, next) {
  if (req.user == null) return next('LoginRequired');
  if (req.user.isAdmin) return next();
  if (req.user.id !== req.selectedComment.userId) return next('AdminRequired');
  return next();
}

function isDeletable(req, res, next) {
  if (req.user == null) return next('LoginRequired');
  if (req.user.isAdmin) return next();
  if (req.user.id === req.selectedPost.userId) return next();
  if (req.groupUser == null) return next('GroupAdminRequired');
  if (!checkPermission(req.groupUser, 'post')) {
    return next('GroupAdminRequired');
  }
  return next();
}

function isCommentDeletable(req, res, next) {
  if (req.user == null) return next('LoginRequired');
  if (req.user.isAdmin) return next();
  if (req.user.id === req.selectedComment.userId) return next();
  if (req.groupUser == null) return next('GroupAdminRequired');
  if (!checkPermission(req.groupUser, 'post')) {
    return next('GroupAdminRequired');
  }
  return next();
}

/**
 * @apiDefine ReturnPost
 * @apiSuccess {Number} _.id 고유 번호
 * @apiSuccess {String=notice,free,greeting,gallery} _.category 카테고리
 * @apiSuccess {Number} _.noticeId 공지 ID (-1 ~ 7)
 * @apiSuccess {String} _.title 제목
 * @apiSuccess {String} _.body 내용
 * @apiSuccess {Number} _.likesCount 좋아요 수
 * @apiSuccess {Number} _.commentsCount 댓글 수
 * @apiSuccess {Number} _.viewCount 조회 수
 * @apiSuccess {String[]} _.photos 사진 목록
 * @apiSuccess {String[]} _.photoThumbs 사진 썸네일 목록
 * @apiSuccess {User} _.user 작성자
 * @apiSuccess {Number} _.user.id 고유 번호
 * @apiSuccess {String[]} _.user.photo 대표 사진
 * @apiSuccess {String[]} _.user.photoThumb 대표 사진 썸네일
 * @apiSuccess {NumberNullable} _.user.photoPrimary 메인 사진 번호
 * @apiSuccess {StringNullable} _.user.nickname 닉네임
 * @apiSuccess {Date} _.createdAt 그룹 생성 시각
 * @apiSuccess {Date} _.updatedAt 그룹 업데이트 시각
 */

/**
 * @apiDefine GetPost
 * @apiParam (Parameter) {Number} postId 게시글 번호
 * @apiError (Error 404) NotFound 지정된 객체를 찾을 수 없습니다.
 */

/**
 * @apiDefine Modifiable 소유자 필요
 *   해당 객체의 소유자이거나 관리자 권한이 필요합니다.
 * @apiError (Error 403) AdminRequired 수정할 수 있는 권한이 없습니다.
 */

/**
 * @api {get} /groups/:groupId/posts/:postId 스티커 게시글 가져오기
 * @apiGroup GroupPost
 * @apiName GetGroupPost
 * @apiDescription 고유 번호에 해당하는 게시글을 가져옵니다.
 * @apiUse GetGroup
 * @apiUse GetPost
 * @apiSuccess {Post} _ 게시글
 * @apiSuccess {Boolean} _.liked 좋아요 했는지 여부
 * @apiUse ReturnPost
 */
postRouter.get('/', async(async (req, res) => {
  await req.selectedPost.increment('viewCount', { silent: true });
  let data = req.selectedPost.toJSON();
  data.noticeId = req.selectedGroup.notices.indexOf(req.selectedPost.id);
  if (req.user != null) {
    data.liked = await req.selectedPost.hasLikeUser(req.user);
  }
  if (req.selectedPost.user) {
    data.user = req.selectedPost.user.toJSON();
    data.user.photos = req.selectedPost.user.getPhotos(req.user);
    data.user.photoThumbs = req.selectedPost.user.getPhotoThumbs(req.user);
    if (!data.user.usePhotoInGroup) {
      delete data.user.photo;
      delete data.user.photoThumb;
      delete data.user.photos;
      delete data.user.photoThumbs;
    }
  }
  res.json(data);
}));

/**
 * @api {patch} /groups/:groupId/posts/:postId 스티커 게시글 수정하기
 * @apiGroup GroupPost
 * @apiName PatchGroupPost
 * @apiDescription 게시글을 수정합니다.
 *
 *   사진 업로드를 하기 위해서는 `multipart/form-data`를 사용해 주세요. 사진이 여기를
 *   사용해서 업로드된 경우 이미 있던 사진들이 전부 삭제됩니다. 이를 원하지 않는다면
 *   `/groups/:groupId/posts/:postId/photos` 엔드포인트를 사용해 주세요.
 *
 *   `notice` 카테고리는 운영진 이상만 사용할 수 있습니다.
 *
 * @apiUse GetGroup
 * @apiUse GetPost
 * @apiParam (Body) {String} title 제목
 * @apiParam (Body) {String} category 카테고리
 * @apiParam (Body) {Number} [noticeId] 공지 번호. -1 ~ 7
 * @apiParam (Body) {String} body 내용
 * @apiParam (Body) {File[]} photos 사진
 * @apiSuccess {Post} _ 게시글
 * @apiUse ReturnPost
 * @apiError (Error 403) PhotoThreshold 사진의 최대 개수를 초과했습니다.
 * @apiUse GroupAdminRequired
 * @apiPermission Modifiable
 * @apiUse Modifiable
 */
postRouter.patch('/', checkModifiable, photoUpload,
async(async (req, res) => {
  const data = pick(req.body, ['title', 'category', 'noticeId', 'body']);
  // Run validation
  let validationError = validateSingle(
    Object.assign({}, req.selectedPost.toJSON(), data),
    validationSchema.GroupPost);
  if (validationError != null) {
    throw { name: 'ValidationError', data: validationError };
  }
  let sanitizedData = sanitize(data, validationSchema.GroupPost);
  if (req.photos != null && req.photos.length > 0) {
    sanitizedData.photos = req.photos || [];
    if (sanitizedData.photos.length > 8) throw 'PhotoThreshold';
  }
  if (sanitizedData.category === 'notice' ||
    (data.noticeId != null && data.noticeId !== '-1')
  ) {
    // Check permission, and reject if not staff
    if (!req.user.isAdmin && !checkPermission(req.groupUser, 'post')) {
      throw 'GroupAdminRequired';
    }
  }
  // Upload to group
  await req.selectedPost.update(sanitizedData);
  await req.selectedGroup.updateNotices(req.selectedPost.id, data.noticeId);
  let result = req.selectedPost.toJSON();
  result.noticeId = req.selectedGroup.notices.indexOf(req.selectedPost.id);
  if (req.user != null) {
    result.liked = await req.selectedPost.hasLikeUser(req.user);
  }
  res.json(result);
}));

/**
 * @api {delete} /groups/:groupId/posts/:postId 스티커 게시글 삭제하기
 * @apiGroup GroupPost
 * @apiName DeleteGroupPost
 * @apiDescription 게시글을 삭제합니다.
 * @apiUse GetGroup
 * @apiUse GetPost
 * @apiUse GroupAdminRequired
 * @apiPermission Modifiable
 * @apiUse Modifiable
 */
postRouter.delete('/', isDeletable, async(async (req, res) => {
  // TODO Delete the post's comments and likes
  await req.selectedPost.destroy();
  await req.selectedGroup.updateNotices(req.selectedPost.id, -1);
  await req.selectedGroup.updatePosts();
  res.json({});
}));

/**
 * @api {get} /groups/:groupId/posts/:postId/photos 스티커 게시글 사진 가져오기
 * @apiGroup GroupPost
 * @apiName GetGroupPostPhotos
 * @apiDescription 게시글의 사진 목록을 가져옵니다.
 * @apiUse GetGroup
 * @apiUse GetPost
 * @apiSuccess {String[]} photos 사진 배열
 * @apiSuccess {String[]} photoThumbs 사진 썸네일 배열
 */
postRouter.get('/photos', async(async (req, res) => {
  const { photos, photoThumbs } = req.selectedPost;
  res.json({ photos, photoThumbs });
}));

/**
 * @api {post} /groups/:groupId/posts/:postId/photos 스티커 게시글 사진 업로드하기
 * @apiGroup GroupPost
 * @apiName PostGroupPostPhotos
 * @apiDescription 사진을 추가로 업로드합니다.
 * @apiParam (Body) {File[]} photos 업로드 할 파일 배열
 * @apiUse GetGroup
 * @apiUse GetPost
 * @apiSuccess {String[]} photos 사진 배열
 * @apiSuccess {String[]} photoThumbs 사진 썸네일 배열
 * @apiError (Error 400) PhotoInvalid 올바른 사진 파일이 아닙니다.
 * @apiError (Error 403) PhotoThreshold 사진의 최대 개수를 초과했습니다.
 * @apiPermission Modifiable
 * @apiUse Modifiable
 */
postRouter.post('/photos', checkModifiable, photoUpload,
async(async (req, res) => {
  let post = req.selectedPost;
  let photos = post.photos.concat(req.photos || []);
  if (photos.length > 8) throw 'PhotoThreshold';
  await post.update({ photos });
  const { photoThumbs } = post;
  res.json({ photos, photoThumbs });
}));

/**
 * @api {delete} /groups/:groupId/posts/:postId/photos 스티커 게시글 사진 전부 삭제하기
 * @apiGroup GroupPost
 * @apiName DeleteGroupPostPhotos
 * @apiDescription 사진을 전부 삭제합니다.
 * @apiSuccess {String[]} photos 사진 배열
 * @apiSuccess {String[]} photoThumbs 사진 썸네일 배열
 * @apiUse GetGroup
 * @apiUse GetPost
 * @apiPermission Modifiable
 * @apiUse Modifiable
 */
postRouter.delete('/photos', checkModifiable, async(async (req, res) => {
  let post = req.selectedPost;
  await post.update({ photos: [] });
  const { photos, photoThumbs } = post;
  res.json({ photos, photoThumbs });
}));

/**
 * @api {delete} /groups/:groupId/posts/:postId/photos/:photoId 스티커 게시글 사진 삭제하기
 * @apiGroup GroupPost
 * @apiName DeleteGroupPostPhoto
 * @apiDescription n번째 사진을 삭제합니다.
 * @apiParam (Parameter) {Number} photoId 삭제할 사진 번호
 * @apiSuccess {String[]} photos 사진 배열
 * @apiSuccess {String[]} photoThumbs 사진 썸네일 배열
 * @apiUse GetGroup
 * @apiUse GetPost
 * @apiPermission Modifiable
 * @apiUse Modifiable
 */
postRouter.delete('/photos/:photoId', checkModifiable,
async(async (req, res) => {
  let index = parseInt(req.params.photoId);
  if (isNaN(index)) throw 'ValidationError';
  let post = req.selectedPost;
  await post.update({
    photos: post.photos.filter((_, i) => i !== index),
  });
  const { photos, photoThumbs } = post;
  res.json({ photos, photoThumbs });
}));

/**
 * @api {get} /groups/:groupId/posts/:postId/likes 스티커 게시글 좋아요 목록
 * @apiGroup GroupPost
 * @apiName GetGroupPostLike
 * @apiDescription 게시글을 좋아요한 사용자들을 가져옵니다.
 * @apiUse GetGroup
 * @apiUse GetPost
 * @apiParam (Query) {Number} [limit=20] 불러올 개수입니다.
 * @apiParam (Query) {Number} [page=0] 불러올 페이지 번호입니다.
 * @apiSuccess {User[]} _ 사용자 목록
 * @apiUse ReturnUserSimple
 */
postRouter.get('/likes', async(async (req, res) => {
  let { limit = 20, page = 0 } = req.query;
  limit = parseInt(limit) || 0;
  page = parseInt(page) || 0;
  if (limit > 50) limit = 50;
  let result = await req.selectedPost.getLikeUsers({
    order: [['createdAt', 'DESC']],
    offset: limit * page,
    limit,
    attributes: ['id', 'nickname', 'photos', 'photoPrimary', 'usePhotoInGroup'],
  });
  result = result.map(processUserPhotoGroup);
  if (req.query.count != null) {
    let count = await req.selectedPost.countLikeUsers();
    res.json({ count, result });
  } else {
    res.json(result);
  }
}));

/**
 * @api {post} /groups/:groupId/posts/:postId/likes 스티커 게시글 좋아요
 * @apiGroup GroupPost
 * @apiName PostGroupPostLike
 * @apiDescription 게시글을 좋아요합니다.
 * @apiPermission GroupMemberRequired
 * @apiUse GroupMemberRequired
 * @apiUse GetGroup
 * @apiUse GetPost
 * @apiError (Error 409) Conflict 이미 처리되었습니다.
 * @apiSuccess {Number} likesCount 좋아요 개수 목록
 */
postRouter.post('/likes', groupMemberRequired, async(async (req, res) => {
  if (await req.selectedPost.hasLikeUser(req.user)) throw 'Conflict';
  await req.selectedPost.addLikeUser(req.user);
  await req.selectedPost.updateLikes();
  // Send notification to OP
  let groupUser = await GroupUser.findOne({ where: {
    userId: req.selectedPost.user.id,
    groupId: req.selectedGroup.id,
  } });
  if (groupUser != null && groupUser.updatePush) {
    await sendNotification(req.selectedPost.user, {
      type: 'like',
      post: req.selectedPost,
      group: req.selectedGroup,
      user: req.user,
    });
  }
  res.json({ likesCount: req.selectedPost.likesCount });
}));

/**
 * @api {delete} /groups/:groupId/posts/:postId/likes 스티커 게시글 좋아요 취소
 * @apiGroup GroupPost
 * @apiName DeleteGroupPostLike
 * @apiDescription 게시글을 좋아요 취소합니다.
 * @apiPermission LoginRequired
 * @apiUse LoginRequired
 * @apiUse GetGroup
 * @apiUse GetPost
 * @apiError (Error 409) Conflict 이미 처리되었습니다.
 * @apiSuccess {Number} likesCount 좋아요 개수 목록
 */
postRouter.delete('/likes', loginRequired, async(async (req, res) => {
  if (!(await req.selectedPost.hasLikeUser(req.user))) throw 'Conflict';
  await req.selectedPost.removeLikeUser(req.user);
  await req.selectedPost.updateLikes();
  res.json({ likesCount: req.selectedPost.likesCount });
}));

/**
 * @apiDefine GetComment
 * @apiParam (Parameter) {Number} commentId 댓글 번호
 * @apiError (Error 404) NotFound 지정된 객체를 찾을 수 없습니다.
 */

/**
 * @apiDefine ReturnComment
 * @apiSuccess {Number} _.id 고유 번호
 * @apiSuccess {String} _.body 내용
 * @apiSuccess {User} _.user 작성자
 * @apiSuccess {Number} _.user.id 고유 번호
 * @apiSuccess {String[]} _.user.photos 사진 목록
 * @apiSuccess {String[]} _.user.photoThumbs 사진 썸네일 목록
 * @apiSuccess {NumberNullable} _.user.photoPrimary 메인 사진 번호
 * @apiSuccess {StringNullable} _.user.nickname 닉네임
 * @apiSuccess {Date} _.createdAt 그룹 생성 시각
 * @apiSuccess {Date} _.updatedAt 그룹 업데이트 시각
 */


/**
 * @api {get} /groups/:groupId/posts/:postId/comments 스티커 댓글 목록
 * @apiGroup GroupPost
 * @apiName GetGroupComments
 * @apiDescription 게시글 댓글을 가져옵니다.
 * @apiUse GetGroup
 * @apiUse GetPost
 * @apiParam (Query) {Number} [limit=20] 불러올 개수입니다.
 * @apiParam (Query) {Number} [page=0] 불러올 페이지 번호입니다.
 * @apiSuccess {Comment[]} _ 댓글 목록
 * @apiUse ReturnComment
 */
postRouter.get('/comments', async(async (req, res) => {
  let { limit = 20, page = 0 } = req.query;
  limit = parseInt(limit) || 0;
  page = parseInt(page) || 0;
  if (limit > 50) limit = 50;
  let result = await req.selectedPost.getComments({
    order: [['createdAt', 'DESC']],
    offset: limit * page,
    limit,
    include: [{
      model: User,
      attributes: ['id', 'nickname', 'photos', 'photoPrimary',
        'usePhotoInGroup'],
      where: { isEnabled: true },
    }],
  });
  result = result.map(v => Object.assign({}, v.toJSON(), {
    user: v.user && processUserPhotoGroup(v.user),
  }));
  if (req.query.count != null) {
    let count = await req.selectedPost.countComments();
    res.json({ count, result });
  } else {
    res.json(result);
  }
}));

/**
 * @api {post} /groups/:groupId/posts/:postId/comments 스티커 댓글 올리기
 * @apiGroup GroupPost
 * @apiName PostGroupComment
 * @apiDescription 게시글 댓글을 올립니다.
 * @apiUse GetGroup
 * @apiUse GetPost
 * @apiPermission GroupMemberRequired
 * @apiUse GroupMemberRequired
 * @apiParam (Body) {String} body 내용
 * @apiSuccess {Comment} _ 댓글
 * @apiUse ReturnComment
 */
postRouter.post('/comments', groupMemberRequired, async(async (req, res) => {
  const data = pick(req.body, ['body']);
  // Run validation
  let validationError = validateSingle(data, validationSchema.GroupPostComment);
  if (validationError != null) {
    throw { name: 'ValidationError', data: validationError };
  }
  let sanitizedData = sanitize(data, validationSchema.GroupPostComment);
  let comment = await GroupPostComment.create(
    Object.assign({}, sanitizedData, {
      userId: req.user.id,
      postId: req.selectedPost.id,
    })
  );
  await req.selectedPost.updateComments();
  // Fetch user IDs
  let userIds = await GroupPostComment.findAll({
    where: { postId: req.selectedPost.id },
    attributes: [
      [sequelize.fn('DISTINCT', sequelize.col('groupPostComment.userId')),
        'userId'],
    ],
  }).map(v => v.userId);
  let opId = req.selectedPost.user.id;
  if (userIds.indexOf(opId) === -1) userIds.push(opId);
  // Fetch users
  let users = await User.findAll({
    where: { id: { $in: userIds } },
    include: [{
      model: GroupUser,
      where: { updatePush: true, groupId: req.selectedGroup.id },
      attributes: [],
    }],
  });
  for (let user of users) {
    await sendNotification(user, {
      type: 'comment',
      post: req.selectedPost,
      group: req.selectedGroup,
      user: req.user,
    });
  }
  // TODO Send push notification to previous comment users.
  res.json(Object.assign({}, comment.toJSON(), { user: req.user }));
}));

export const commentRouter = new Router();

/**
 * @api {get} /groups/:groupId/posts/:postId/comments/:commentId 댓글 가져오기
 * @apiGroup GroupPost
 * @apiName GetGroupComment
 * @apiDescription 댓글을 가져옵니다.
 * @apiUse GetGroup
 * @apiUse GetPost
 * @apiUse GetComment
 * @apiSuccess {Comment} _ 댓글
 * @apiUse ReturnComment
 */
commentRouter.get('/', async(async (req, res) => {
  res.json(req.selectedComment);
}));

/**
 * @api {patch} /groups/:groupId/posts/:postId/comments/:commentId 댓글 수정하기
 * @apiGroup GroupPost
 * @apiName PatchGroupComment
 * @apiDescription 댓글을 수정합니다.
 * @apiUse GetGroup
 * @apiUse GetPost
 * @apiUse GetComment
 * @apiParam (Body) {String} body 내용
 * @apiSuccess {Comment} _ 댓글
 * @apiUse ReturnComment
 * @apiPermission Modifiable
 * @apiUse Modifiable
 */
commentRouter.patch('/', checkCommentModifiable, async(async (req, res) => {
  const data = pick(req.body, ['body']);
  // Run validation
  let validationError = validateSingle(data, validationSchema.GroupPostComment);
  if (validationError != null) {
    throw { name: 'ValidationError', data: validationError };
  }
  let sanitizedData = sanitize(data, validationSchema.GroupPostComment);
  await req.selectedComment.update(sanitizedData);
  res.json(req.selectedComment);
}));

/**
 * @api {delete} /groups/:groupId/posts/:postId/comments/:commentId 댓글 삭제하기
 * @apiGroup GroupPost
 * @apiName DeleteGroupComment
 * @apiDescription 댓글을 삭제합니다.
 * @apiUse GetGroup
 * @apiUse GetPost
 * @apiUse GetComment
 * @apiUse GroupAdminRequired
 * @apiPermission Modifiable
 * @apiUse Modifiable
 */
commentRouter.delete('/', isCommentDeletable, async(async (req, res) => {
  await req.selectedComment.destroy();
  await req.selectedPost.updateComments();
  res.json({});
}));

postRouter.use('/comments/:commentId', async(async (req, res, next) => {
  // Pass to commentRouter
  req.selectedComment = await GroupPostComment.findOne({
    where: {
      postId: req.selectedPost.id,
      id: req.params.commentId,
    },
    include: [ User ],
  });
  if (req.selectedComment == null) throw 'NotFound';
  next();
}), commentRouter);
