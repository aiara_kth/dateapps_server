import { Router } from 'express';
import randToken from 'rand-token';

import async from './util/async';
import loginRequired from './util/loginRequired';

import { ShareEntry } from '../db';

export const router = new Router();
export default router;

router.get('/sharer/:sharerId', async(async (req, res, next) => {
  let entry = await ShareEntry.findById(req.params.sharerId);
  if (entry == null) {
    return res.status(404).send('입력하신 주소가 존재하지 않습니다.');
  }
  await entry.increment('viewCount');
  res.render('sharer', { metadata: Object.assign({}, entry.toJSON(), {
    url: entry.url || 'estickerapp://',
  }) });
}));

/**
 * @api {get} /sharer 일회용 공유 링크 가져오기
 * @apiGroup Sharer
 * @apiName GetSharer
 * @apiDescription 공유 링크를 가져옵니다. 가능하면 POST /sharer를 사용해 주세요.
   (하위 호환용 API)
 * @apiParam (Query) {String} title 이름
 * @apiParam (Query) {String} description 내용
 * @apiParam (Query) {String} image 사진
 * @apiParam (Query) {String} url URL
 * @apiSuccess {HTML} html 렌더된 페이지
 */
router.get('/sharer', (req, res) => {
  res.render('sharer', { metadata: Object.assign({}, req.query, {
    url: req.query.url || 'estickerapp://',
  }) });
});

/**
 * @api {post} /sharer 공유 링크 만들기
 * @apiGroup Sharer
 * @apiName PostSharer
 * @apiDescription 공유 링크를 만듭니다.
 * @apiParam (Body) {String} title 이름
 * @apiParam (Body) {String} description 내용
 * @apiParam (Body) {String} image 사진
 * @apiParam (Body) {String} url URL
 * @apiUse LoginRequired
 * @apiSuccess {String} code 공유 링크 ID. http://api.estickerapp.com/sharer/:code같이 접근할 수 있습니다.
 */
router.post('/sharer', loginRequired, async(async (req, res, next) => {
  let data = Object.assign({}, req.body);
  data.code = randToken.generate(10, '0123456789abcdefghijkmnopqrstuvwxyz');
  data.userId = req.user.id;
  let entry = await ShareEntry.create(data);
  res.json({ code: entry.code });
}));
