import { Router } from 'express';

import async from './util/async';
import loginRequired from './util/loginRequired';
import { checkPermission, checkType } from './util/groupAdminRequired';
import { validateSingle, sanitize } from '../validation/validate';
import * as validationSchema from '../validation/schema';
import injectGroupUser from './util/injectGroupUser';
import { injectOtherUserInfo } from './util/injectUserInfo';
import { createMeetingQuery } from './util/createQuery';
import sendNotification from './util/notification';
import { doPayment, refundPayment } from './util/payment';
import { upload, uploadHandler } from './util/photoUpload';
import processUserPhotoGroup from './util/processUserPhotoGroup';

import { sequelize, Meeting, MeetingUser, Group, User, GroupUser, PaymentLog,
    MeetingComment } from '../db';

function processUser(meeting, user) {
    if (meeting.groupId == null) return user.toJSON();
    return processUserPhotoGroup(user);
}

/**
 * @apiDefine GetMeeting
 * @apiParam (Parameter) {Number} meetingId 모임 번호
 * @apiError (Error 404) MeetingNotFound 입력하신 모임을 찾을 수 없습니다.
 */

/**
 * @apiDefine MeetingMemberRequired 모임 참여자 필요
 *   사용하려면 모임의 참여자여야 합니다.
 * @apiError (Error 403) MeetingMemberRequired 모임의 참여자여야 합니다.
 */

/**
 * @apiDefine MeetingHostRequired 미팅 작성자 필요
 *   사용하려면 모임의 작성자여야 합니다.
 * @apiError (Error 403) MeetingHostRequired 모임의 작성자여야 합니다.
 */

/**
 * @apiDefine ReturnMeeting
 * @apiSuccess {Number} _.id 고유 번호
 * @apiSuccess {String} _.category 카테고리
 * @apiSuccess {String} _.title 제목
 * @apiSuccess {String} _.body 내용
 * @apiSuccess {Number} _.location 지역
 * @apiSuccess {String} _.place 장소
 * @apiSuccess {Number} _.mapx 좌표 X
 * @apiSuccess {Number} _.mapy 좌표 Y
 * @apiSuccess {Number} _.price 가격
 * @apiSuccess {String} _.photo 사진 주소
 * @apiSuccess {String} _.photoThumb 사진 썸네일 주소
 * @apiSuccess {Date} _.date 시각소
 * @apiSuccess {NumberNullable} _.groupId 소유 그룹 고유 번호
 * @apiSuccess {NumberNullable} _.userId 주최자 고유 번호
 * @apiSuccess {Boolean} _.lightning 번개 여부
 * @apiSuccess {Number} _.minMembers 최소 멤버 수
 * @apiSuccess {Number} _.maxMembers 최대 멤버 수
 * @apiSuccess {Number} _.membersCount 멤버 수
 * @apiSuccess {Number} _.likesCount 좋아요 수
 * @apiSuccess {Number} _.commentsCount 댓글 수
 * @apiSuccess {Boolean} _.expired 끝남 여부
 * @apiSuccess {Boolean} _.paid 결제 여부
 * @apiSuccess {Boolean} _.failed 멤버 모집 실패 여부
 * @apiSuccess {Date} _.createdAt 그룹 생성 시각
 * @apiSuccess {Date} _.updatedAt 그룹 업데이트 시각
 */

export const meetingRouter = new Router();

function checkModifiable(req, res, next) {
    if (req.user == null) return next('LoginRequired');
    if (!req.user.isEnabled) return next('UserDisabled');
    if (req.user.isAdmin) return next();
    if (req.user.id === req.selectedMeeting.userId) return next();
    // If the meeting belongs to a group, their admins should be able to
    // modify the meeting.
    if (req.selectedMeeting.groupId != null && req.groupUser != null &&
        checkPermission(req.groupUser, 'meeting')
    ) {
        return next();
    }
    if (req.selectedMeeting.groupId != null) return next('GroupAdminRequired');
    return next('AdminRequired');
}

function checkCommentModifiable(req, res, next) {
    if (req.user == null) return next('LoginRequired');
    if (!req.user.isEnabled) return next('UserDisabled');
    if (req.user.isAdmin) return next();
    if (req.user.id === req.selectedComment.userId) return next();
    return next('AdminRequired');
}

function checkCommentDeletable(req, res, next) {
    if (req.user == null) return next('LoginRequired');
    if (!req.user.isEnabled) return next('UserDisabled');
    if (req.user.isAdmin) return next();
    if (req.user.id === req.selectedComment.userId) return next();
    if (req.user.id === req.selectedMeeting.userId) return next();
    // If the meeting belongs to a group, their admins should be able to
    // modify the meeting.
    if (req.selectedMeeting.groupId != null && req.groupUser != null &&
        checkPermission(req.groupUser, 'meeting')
    ) {
        return next();
    }
    if (req.selectedMeeting.groupId != null) return next('GroupAdminRequired');
    return next('AdminRequired');
}

function checkJoinable(req, res, next) {
    if (req.user == null) return next('LoginRequired');
    if (!req.user.isEnabled) return next('UserDisabled');
    if (req.user.isAdmin) return next();
    if (req.user.id === req.selectedMeeting.userId) return next();
    if (req.selectedMeeting.groupId == null) {
        return next();
    }
    if (req.groupUser == null) return next('GroupMemberRequired');
    if (!checkType(req.groupUser.type, 'member')) {
        return next('GroupMemberRequired');
    }
    return next();
}

/**
 * @api {get} /meetings/:meetingId/ 모임 가져오기
 * @apiGroup Meeting
 * @apiName GetMeeting
 * @apiDescription 모임을 가져옵니다.
 * @apiUse GetMeeting
 * @apiSuccess {Meeting} _ 모임
 * @apiUse ReturnMeeting
 */
meetingRouter.get('/', async(async (req, res) => {
    let data = req.selectedMeeting.toJSON();
    delete data.user;
    if (req.user != null) {
        data.liked = await req.selectedMeeting.hasLikeUser(req.user);
    }
    res.json(data);
}));

/**
 * @api {patch} /meetings/:meetingId/ 모임 수정하기
 * @apiGroup Meeting
 * @apiName PatchMeeting
 * @apiDescription 모임을 수정합니다.
 *
 *   사진 업로드를 하기 위해서는 `multipart/form-data`를 사용해 주세요.
 *
 * @apiParam (Body) {String} category 카테고리
 * @apiParam (Body) {String} title 제목
 * @apiParam (Body) {String} body 내용
 * @apiParam (Body) {Number} location 지역
 * @apiParam (Body) {String} place 장소
 * @apiParam (Body) {Number} price 가격
 * @apiParam (Body) {Date} date 시간
 * @apiParam (Body) {Number} minMembers 최소 참가자 수
 * @apiParam (Body) {Number} maxMembers 최대 참가자 수
 * @apiParam (Body) {File} [photo] 사진
 * @apiError (Error 400) ValidationError 번개의 경우 번개 생성 시각부터 24시간 이내여야 합니다.
 * @apiUse GetMeeting
 * @apiSuccess {Meeting} _ 모임
 * @apiUse ReturnMeeting
 * @apiPermission Modifiable
 * @apiUse GroupAdminRequired
 * @apiUse Modifiable
 */
meetingRouter.patch('/', checkModifiable, upload.array('photo', 1),
    uploadHandler, async(async (req, res) => {
        // Run validation
        let validationError = validateSingle(Object.assign({},
            req.selectedMeeting.toJSON(), req.body), validationSchema.Meeting);
        if (validationError != null) {
            throw { name: 'ValidationError', data: validationError };
        }
        let data = sanitize(req.body, validationSchema.Meeting);
        if (data.date != null) {
            if (req.selectedMeeting.lightning &&
                (data.date.getTime() - req.selectedMeeting.createdAt) >
                24 * 60 * 60 * 1000
            ) {
                throw 'ValidationError';
            }
            if (data.date.getTime() < Date.now()) throw 'ValidationError';
        }
        if (req.photos && req.photos[0] != null) {
            data.photo = req.photos[0];
        }
        req.selectedMeeting.update(data);
        // All done
        let result = req.selectedMeeting.toJSON();
        delete result.user;
        res.json(result);
    }));

/**
 * @api {delete} /meetings/:meetingId/ 모임 삭제하기
 * @apiGroup Meeting
 * @apiName DeleteMeeting
 * @apiDescription 모임을 삭제합니다.
 * @apiUse GetMeeting
 * @apiError (Error 400) ValidationError 모집에 실패하지 않은 모임은 모임 시작 6시간 전부터 삭제할 수 없습니다.
 * @apiPermission Modifiable
 * @apiUse GroupAdminRequired
 * @apiUse Modifiable
 */
meetingRouter.delete('/', checkModifiable, async(async (req, res) => {
    if (!req.selectedMeeting.lightning&&
        !req.selectedMeeting.failed &&
        req.selectedMeeting.date.getTime() < Date.now() + 6 * 60 * 60 * 1000
    ) {
        throw 'ValidationError';
    }
    // Refund if available
    if (req.selectedMeeting.paid) {
        // Find the user who paid the meeting
        let log = await PaymentLog.findOne({
            where: {
                type: 'meetingCreate',
                targetId: req.selectedMeeting.id,
            },
            include: [ User ],
        });
        if (log != null && log.user != null) {
            await refundPayment(log.user, 'meetingCreate', req.selectedMeeting.id);
        }
    }
    // Send notification
    let users = await req.selectedMeeting.getMembers({
        where: { isEnabled: true },
    });
    for (let user of users) {
        if (user.id === req.user.id) continue;
        await sendNotification(user, {
            type: 'meetingCancel',
            meeting: req.selectedMeeting,
        });
    }
    await req.selectedMeeting.destroy();
    res.json({});
}));

/**
 * @api {post} /meetings/:meetingId/photo 모임 사진 올리기
 * @apiGroup Meeting
 * @apiName PostMeetingPhoto
 * @apiDescription 모임 사진을 수정합니다.
 *
 *   사진 업로드를 하기 위해서는 `multipart/form-data`를 사용해 주세요.
 *
 * @apiParam (Body) {File} [photo] 사진
 * @apiUse GetMeeting
 * @apiUse ReturnMeeting
 * @apiPermission Modifiable
 * @apiUse GroupAdminRequired
 * @apiUse Modifiable
 */
meetingRouter.post('/photo', checkModifiable, upload.array('photo', 1),
    uploadHandler, async(async (req, res) => {
        // Validate data
        const meetingData = {};
        if (req.photos && req.photos[0] != null) {
            meetingData.photo = req.photos[0];
        }
        req.selectedMeeting.update(meetingData);
        // All done
        let result = req.selectedMeeting.toJSON();
        delete result.user;
        res.json(result);
    }));

/**
 * @api {post} /meetings/:meetingId/pay 모임 결제하기
 * @apiGroup Meeting
 * @apiName PayMeeting
 * @apiDescription 모임을 결제합니다. 결제 이후에는 연락처 정보를 볼 수 있습니다.
 * @apiError (Error 409) Conflict 이미 결제되었습니다.
 * @apiError (Error 403) NoDiamonds 잔여 다이아가 부족합니다.
 * @apiError (Error 400) ValidationError 최소 인원 수에 도달하지 못했습니다.
 * @apiError (Error 400) ValidationError 스티커 모임 / 번개는 결제할 수 없습니다.
 * @apiSuccess {Number} _ 잔여 다이아 개수
 * @apiUse GetMeeting
 */
meetingRouter.post('/pay', checkModifiable, async(async (req, res) => {
    if (req.selectedMeeting.paid) throw 'Conflict';
    if (req.selectedMeeting.groupId != null) throw 'ValidationError';
    // Run payment
    await doPayment(req.user, 'meetingCreate', req.selectedMeeting.id);
    await req.selectedMeeting.update({ paid: true });
    res.json({ diamonds: req.user.diamonds });
}));

/**
 * @api {get} /meetings/:meetingId/users/:userId 모임 참가자 가져오기
 * @apiGroup Meeting
 * @apiName GetMeetingUser
 * @apiDescription 모임 참가자를 가져옵니다.
 * @apiUse GetUser
 * @apiUse GetMeeting
 * @apiSuccess {User} _ 사용자 목록
 * @apiSuccess {String[]} _.photos 사진 목록
 * @apiSuccess {String[]} _.photoThumbs 사진 썸네일 목록
 * @apiSuccess {Boolean} _.isHost 호스트 여부
 * @apiUse ReturnUser
 */
meetingRouter.get('/users/:id', async(async (req, res) => {
    let users = await req.selectedMeeting.getMembers({
        through: { where: { enabled: true } },
        where: {
            id: req.params.id,
        },
    });
    let user = users[0];
    if (user == null) throw 'UserNotFound';
    let data = await injectOtherUserInfo(user, req.user);
    // If meetings starts in less than 24h, reveal cellphone number
    if (req.selectedMeeting.paid && req.user != null &&
        req.selectedMeeting.userId === req.user.id
        && req.selectedMeeting.date.getTime()-(24*60*60*1000)<new Date().getTime()
    ) {
        data.phone = user.phone;
    }else{
        delete data.phone;
    }
    data.isHost = req.selectedMeeting.userId === user.id;
    if (req.selectedMeeting.paid && data.isHost && req.user != null &&
        req.meetingUser != null && req.meetingUser.enabled
        && req.selectedMeeting.date.getTime()-(24*60*60*1000)<new Date().getTime()
    ) {
        data.phone = user.phone;
    }else{
        delete data.phone;
    }
    if (req.selectedMeeting.groupId != null && !user.usePhotoInGroup) {
        delete data.photo;
        delete data.photoThumb;
    } else {
        data.photos = user.getPhotos(req.user);
        data.photoThumbs = user.getPhotoThumbs(req.user);
    }
    res.json(data);
}));

/**
 * @api {get} /meetings/:meetingId/users/ 모임 참가자 목록 가져오기
 * @apiGroup Meeting
 * @apiName GetMeetingUsers
 * @apiDescription 모임 참가자 목록을 가져옵니다.
 * @apiUse GetMeeting
 * @apiParam (Query) {Number} [limit=20] 불러올 개수입니다.
 * @apiParam (Query) {Number} [page=0] 불러올 페이지 번호입니다.
 * @apiSuccess {User[]} _ 사용자 목록
 * @apiSuccess {Boolean} _.isHost 호스트 여부
 * @apiUse ReturnUser
 */
meetingRouter.get('/users', async(async (req, res) => {
    let { limit = 20, page = 0 } = req.query;
    limit = parseInt(limit) || 0;
    page = parseInt(page) || 0;
    if (limit > 50) limit = 50;
    let users = await req.selectedMeeting.getMembers({
        through: { where: { enabled: true } },
        where: { isEnabled: true },
        offset: limit * page,
        limit,
        order: [[sequelize.literal('meetingUser.updatedAt'), 'ASC']],
    });
    let revealPhone = req.selectedMeeting.paid && req.user != null
        && req.selectedMeeting.userId === req.user.id && req.selectedMeeting.date.getTime()-(24*60*60*1000)<new Date().getTime();
    users = await Promise.all(users.map(async user => {
        let data = await injectOtherUserInfo(user, req.user);
        // If meetings starts in less than 24h, reveal cellphone number
        if (revealPhone) data.phone = user.phone;
        if (req.selectedMeeting.groupId != null && !user.usePhotoInGroup) {
            delete data.photo;
            delete data.photoThumb;
        }
        data.isHost = req.selectedMeeting.userId === user.id;
        if (req.selectedMeeting.paid && data.isHost && req.user != null &&
            req.meetingUser != null && req.meetingUser.enabled
            && req.selectedMeeting.date.getTime()-(24*60*60*1000)<new Date().getTime()
        ) {
            data.phone = user.phone;
        }else{
            delete data.phone;
        }
        return data;
    }));
    if (req.query.count != null) {
        let count = await req.selectedMeeting.countMembers({
            where: { isEnabled: true },
        });
        res.json({ count, result: users });
    } else {
        res.json(users);
    }
}));

/**
 * @api {post} /meetings/:meetingId/users/ 모임 참가하기
 * @apiGroup Meeting
 * @apiName PostMeetingUsers
 * @apiDescription 모임에 참가합니다.
 * @apiUse GetMeeting
 * @apiPermission GroupMemberRequired
 * @apiUse GroupMemberRequired
 * @apiError (Error 400) MemberThreshold 정원을 초과했습니다.
 * @apiError (Error 403) Forbidden 이미 참가 / 취소를 3번 반복해서 다시 참가할 수 없습니다.
 * @apiError (Error 409) Conflict 이미 처리되었습니다.
 * @apiError (Error 409) DateConflict 다른 모임의 6시간 범위 밖의 모임만 참가할 수 있습니다.
 */
meetingRouter.post('/users', checkJoinable, async(async (req, res) => {
    if (req.selectedMeeting.membersCount + 1 > req.selectedMeeting.maxMembers) {
        throw 'MemberThreshold';
    }
    if (req.meetingUser != null && req.meetingUser.enabled) throw 'Conflict';
    if (req.meetingUser != null && req.meetingUser.leaveCount >= 3) {
        throw 'Forbidden';
    }
    // Check if the schedule conflicts
    let conflictMeetings = await req.user.getMeetings({
        where: {
            date: {
                $lt: new Date(new Date(req.selectedMeeting.date).getTime() +
                    6 * 60 * 60 * 1000),
                $gt: new Date(new Date(req.selectedMeeting.date).getTime() -
                    6 * 60 * 60 * 1000),
            },
        },
        through: { where: { enabled: true } },
    });
    if (conflictMeetings.length > 0) throw 'DateConflict';
    if (req.meetingUser == null) {
        await req.selectedMeeting.addMember(req.user);
    } else {
        await req.meetingUser.update({ enabled: true });
    }
    await req.selectedMeeting.updateMembers();
    // Send notification to OP
    let doSend = true;
    if (req.selectedGroup != null) {
        let groupUser = await GroupUser.findOne({ where: {
                userId: req.selectedMeeting.userId,
                groupId: req.selectedGroup.id,
            } });
        doSend = groupUser != null && groupUser.updatePush;
    }
    if (doSend) {
        await sendNotification(req.selectedMeeting.user, {
            type: 'meetingJoin',
            meeting: req.selectedMeeting,
            group: req.selectedGroup,
            user: req.user,
        });
    }
    res.json({});
}));

/**
 * @api {delete} /meetings/:meetingId/users/ 모임 참가 취소하기
 * @apiGroup Meeting
 * @apiName DeleteMeetingUsers
 * @apiDescription 모임 참가를 취소합니다.
 * @apiUse GetMeeting
 * @apiError (Error 400) ValidationError 주최자는 참가를 취소할 수 없습니다.
 * @apiError (Error 400) ValidationError 모임 6시간 전에는 참가를 취소할 수 없습니다.
 * @apiError (Error 409) Conflict 이미 처리되었습니다.
 */
meetingRouter.delete('/users', loginRequired, async(async (req, res) => {
    if (req.user.id === req.selectedMeeting.userId) throw 'ValidationError';
    if (req.meetingUser == null || !req.meetingUser.enabled) throw 'Conflict';
    if (req.selectedMeeting.lightning === false &&
        Date.now() > req.selectedMeeting.date.getTime() - (6 * 60 * 60 * 1000)) {
        // Can't leave.
        throw 'ValidationError';
    }
    await req.meetingUser.update({
        enabled: false,
        leaveCount: req.meetingUser.leaveCount + 1,
    });
    await req.selectedMeeting.updateMembers();
    // Send notification to OP
    let doSend = true;
    if (req.selectedGroup != null) {
        let groupUser = await GroupUser.findOne({ where: {
                userId: req.selectedMeeting.userId,
                groupId: req.selectedGroup.id,
            } });
        doSend = groupUser != null && groupUser.updatePush;
    }
    if (doSend) {
        await sendNotification(req.selectedMeeting.user, {
            type: 'meetingLeave',
            meeting: req.selectedMeeting,
            group: req.selectedGroup,
            user: req.user,
        });
    }
    res.json({});
}));

/**
 * @api {delete} /meetings/:meetingId/users/:userId 모임 강퇴하기
 * @apiGroup Meeting
 * @apiName DeleteMeetingUser
 * @apiDescription 모임에서 강퇴합니다.
 * @apiUse GetMeeting
 * @apiUse GetUser
 * @apiError (Error 400) ValidationError 주최자는 참가를 취소할 수 없습니다.
 * @apiError (Error 409) Conflict 이미 처리되었습니다.
 */
meetingRouter.delete('/users/:userId', checkModifiable,
    async(async (req, res) => {
        let userId = parseInt(req.params.userId);
        if (isNaN(userId)) throw 'ValidationError';
        if (userId === req.selectedMeeting.userId) throw 'ValidationError';
        let meetingUser = await MeetingUser.findOne({ where: {
                meetingId: req.selectedMeeting.id,
                userId: userId,
            } });
        await meetingUser.update({
            enabled: false,
            leaveCount: 100,
        });
        await req.selectedMeeting.updateMembers();
        res.json({});
    }));

/**
 * @api {get} /meetings/:meetingId/likes 좋아요 목록 가져오기
 * @apiGroup Meeting
 * @apiName GetMeetingLikes
 * @apiDescription 모임을 좋아요한 사용자들을 가져옵니다.
 * @apiUse GetMeeting
 * @apiParam (Query) {Number} [limit=20] 불러올 개수입니다.
 * @apiParam (Query) {Number} [page=0] 불러올 페이지 번호입니다.
 * @apiSuccess {User[]} _ 사용자 목록
 * @apiUse ReturnUserSimple
 */
meetingRouter.get('/likes', async(async (req, res) => {
    let { limit = 20, page = 0 } = req.query;
    limit = parseInt(limit) || 0;
    page = parseInt(page) || 0;
    if (limit > 50) limit = 50;
    let result = await req.selectedMeeting.getLikeUsers({
        order: [['createdAt', 'DESC']],
        offset: limit * page,
        limit,
        attributes: ['id', 'nickname', 'photos', 'photoPrimary',
            'usePhotoInGroup'],
    });
    result = result.map(processUser.bind(null, req.selectedMeeting));
    if (req.query.count != null) {
        let count = await req.selectedMeeting.countLikeUsers();
        res.json({ count, result });
    } else {
        res.json(result);
    }
}));

/**
 * @api {post} /meetings/:meetingId/likes 좋아요 하기
 * @apiGroup Meeting
 * @apiName PostMeetingLike
 * @apiDescription 모임을 좋아요합니다.
 * @apiPermission GroupMemberRequired
 * @apiUse GroupMemberRequired
 * @apiUse GetMeeting
 * @apiError (Error 409) Conflict 이미 처리되었습니다.
 * @apiSuccess {Number} likesCount 좋아요 개수 목록
 */
meetingRouter.post('/likes', checkJoinable, async(async (req, res) => {
    if (await req.selectedMeeting.hasLikeUser(req.user)) throw 'Conflict';
    await req.selectedMeeting.addLikeUser(req.user);
    await req.selectedMeeting.updateLikes();
    // Send notification to OP
    let doSend = true;
    if (req.selectedGroup != null) {
        let groupUser = await GroupUser.findOne({ where: {
                userId: req.selectedMeeting.userId,
                groupId: req.selectedGroup.id,
            } });
        doSend = groupUser != null && groupUser.updatePush;
    }
    if (doSend) {
        await sendNotification(req.selectedMeeting.user, {
            type: 'meetingLike',
            meeting: req.selectedMeeting,
            group: req.selectedGroup,
            user: req.user,
        });
    }
    res.json({ likesCount: req.selectedMeeting.likesCount });
}));

/**
 * @api {delete} /meetings/:meetingId/likes 좋아요 취소
 * @apiGroup Meeting
 * @apiName DeleteMeetingLike
 * @apiDescription 모임을 좋아요 취소합니다.
 * @apiPermission LoginRequired
 * @apiUse LoginRequired
 * @apiUse GetMeeting
 * @apiError (Error 409) Conflict 이미 처리되었습니다.
 * @apiSuccess {Number} likesCount 좋아요 개수 목록
 */
meetingRouter.delete('/likes', loginRequired, async(async (req, res) => {
    if (!(await req.selectedMeeting.hasLikeUser(req.user))) throw 'Conflict';
    await req.selectedMeeting.removeLikeUser(req.user);
    await req.selectedMeeting.updateLikes();
    res.json({ likesCount: req.selectedMeeting.likesCount });
}));

/**
 * @api {get} /meetings/:meetingId/comments 댓글 목록
 * @apiGroup Meeting
 * @apiName GetMeetingComments
 * @apiDescription 모임 댓글을 가져옵니다.
 * @apiUse GetMeeting
 * @apiParam (Query) {Number} [limit=20] 불러올 개수입니다.
 * @apiParam (Query) {Number} [page=0] 불러올 페이지 번호입니다.
 * @apiSuccess {Comment[]} _ 댓글 목록
 * @apiUse ReturnComment
 */
meetingRouter.get('/comments', async(async (req, res) => {
    let { limit = 20, page = 0 } = req.query;
    limit = parseInt(limit) || 0;
    page = parseInt(page) || 0;
    if (limit > 50) limit = 50;
    let result = await req.selectedMeeting.getComments({
        order: [['createdAt', 'DESC']],
        offset: limit * page,
        limit,
        include: [{
            model: User,
            attributes: ['id', 'nickname', 'photos', 'photoPrimary',
                'usePhotoInGroup'],
        }],
    });
    result = result.map(v => Object.assign({}, v.toJSON(), {
        user: v.user && processUser(req.selectedMeeting, v.user),
    }));
    if (req.query.count != null) {
        let count = await req.selectedMeeting.countComments();
        res.json({ count, result });
    } else {
        res.json(result);
    }
}));

/**
 * @api {post} /meetings/:meetingId/comments 댓글 올리기
 * @apiGroup Meeting
 * @apiName PostMeetingComment
 * @apiDescription 모임 댓글을 올립니다.
 * @apiUse GetMeeting
 * @apiPermission GroupMemberRequired
 * @apiUse GroupMemberRequired
 * @apiParam (Body) {String} body 내용
 * @apiSuccess {Comment} _ 댓글
 * @apiUse ReturnComment
 */
meetingRouter.post('/comments', checkJoinable, async(async (req, res) => {
    // Run validation
    let validationError = validateSingle(req.body,
        validationSchema.MeetingComment);
    if (validationError != null) {
        throw { name: 'ValidationError', data: validationError };
    }
    let sanitizedData = sanitize(req.body, validationSchema.MeetingComment);
    let comment = await MeetingComment.create(
        Object.assign({}, sanitizedData, {
            userId: req.user.id,
            meetingId: req.selectedMeeting.id,
        })
    );
    await req.selectedMeeting.updateComments();
    // Send notification to all participants.
    let userQuery = {};
    if (req.selectedGroup != null) {
        userQuery = {
            include: [{
                model: GroupUser,
                where: { updatePush: true, groupId: req.selectedGroup.id },
                attributes: [],
            }],
        };
    }
    let users = await req.selectedMeeting.getMembers(userQuery);
    for (let user of users) {
        await sendNotification(user, {
            type: 'meetingComment',
            meeting: req.selectedMeeting,
            group: req.selectedGroup,
            user: req.user,
        });
    }
    res.json(Object.assign({}, comment.toJSON(), { user: req.user }));
}));

export const commentRouter = new Router();

/**
 * @api {get} /meetings/:meetingId/comments/:commentId 댓글 가져오기
 * @apiGroup Meeting
 * @apiName GetMeetingComment
 * @apiDescription 모임 댓글을 가져옵니다.
 * @apiUse GetMeeting
 * @apiUse GetComment
 * @apiSuccess {Comment} _ 댓글
 * @apiUse ReturnComment
 */
commentRouter.get('/', async(async (req, res) => {
    res.json(req.selectedComment);
}));

/**
 * @api {patch} /meetings/:meetingId/comments/:commentId 댓글 수정하기
 * @apiGroup Meeting
 * @apiName PatchMeetingComment
 * @apiDescription 모임댓글을 수정합니다.
 * @apiUse GetMeeting
 * @apiUse GetComment
 * @apiParam (Body) {String} body 내용
 * @apiSuccess {Comment} _ 댓글
 * @apiUse ReturnComment
 * @apiPermission Modifiable
 * @apiUse Modifiable
 */
commentRouter.patch('/', checkCommentModifiable, async(async (req, res) => {
    // Run validation
    let validationError = validateSingle(req.body,
        validationSchema.MeetingComment);
    if (validationError != null) {
        throw { name: 'ValidationError', data: validationError };
    }
    let sanitizedData = sanitize(req.body, validationSchema.MeetingComment);
    await req.selectedComment.update(sanitizedData);
    res.json(req.selectedComment);
}));

/**
 * @api {delete} /meetings/:meetingId/comments/:commentId 댓글 삭제하기
 * @apiGroup Meeting
 * @apiName DeleteMeetingComment
 * @apiDescription 댓글을 삭제합니다.
 * @apiUse GetMeeting
 * @apiUse GetComment
 * @apiPermission MeetingHostRequired
 * @apiUse MeetingHostRequired
 * @apiUse GroupAdminRequired
 * @apiPermission Modifiable
 * @apiUse Modifiable
 */
commentRouter.delete('/', checkCommentDeletable, async(async (req, res) => {
    await req.selectedComment.destroy();
    await req.selectedMeeting.updateComments();
    res.json({});
}));

meetingRouter.use('/comments/:commentId', async(async (req, res, next) => {
    // Pass to commentRouter
    req.selectedComment = await MeetingComment.findOne({
        where: {
            meetingId: req.selectedMeeting.id,
            id: req.params.commentId,
        },
        include: [ User ],
    });
    if (req.selectedComment == null) throw 'NotFound';
    next();
}), commentRouter);

export const router = new Router();
export default router;

router.use('/meetings/:meetingId', async(async (req, res, next) => {
    // Pass to meetingRouter
    req.selectedMeeting = await Meeting.findById(req.params.meetingId, {
        include: [{
            model: User,
            as: 'user',
        }],
    });
    if (req.selectedMeeting == null) throw 'MeetingNotFound';
    // If this is linked with a group, we need to find that group / groupUser
    // association too
    if (req.selectedMeeting.groupId != null) {
        req.selectedGroup = await Group.findById(req.selectedMeeting.groupId);
        // GroupUser will be injected by injectGroupUser
    }
    let meetingUser = await MeetingUser.findOne({ where: {
            meetingId: req.selectedMeeting.id,
            userId: req.user.id,
        } });
    req.meetingUser = meetingUser;
    next();
}), injectGroupUser, meetingRouter);

/**
 * @api {get} /meetings/ 모임 목록
 * @apiGroup Meeting
 * @apiName GetMeetings
 * @apiDescription 모임 목록을 가져옵니다.
 * @apiParam (Query) {String=likes,likesLeast,members,membersLeast,newest,oldest} [sort=newest] 정렬 기준
 * @apiParam (Query) {String} [title] 이름 필터
 * @apiParam (Query) {String} [category] 카테고리 필터
 * @apiParam (Query) {Number} [location] 지역 필터
 * @apiParam (Query) {Date} [dateFrom] 날짜 필터
 * @apiParam (Query) {Date} [dateTo] 날짜 필터
 * @apiParam (Query) {Number} [dateLoop] 날짜 필터
 * @apiParam (Query) {Number} [dateLoopCount=10] 날짜 필터
 * @apiParam (Query) {Date} [dateLoopCapFrom] 날짜 필터
 * @apiParam (Query) {Date} [dateLoopCapTo] 날짜 필터
 * @apiParam (Query) {Number} [limit=20] 불러올 개수
 * @apiParam (Query) {Number} [page=0] 불러올 페이지 번호.
 * @apiSuccess {Meeting[]} _ 모임 목록
 * @apiUse ReturnMeeting
 */
router.get('/meetings', async(async (req, res) => {
    req.notExpired = true;
    let query = createMeetingQuery(req);
    query.include.push({
        model: User,
        as: 'likeUsers',
        attributes: ['id'],
        required: false,
        where: { id: req.user && req.user.id },
    });
    query.include.push({
        model: User,
        as: 'user',
        attributes: ['id', 'nickname', 'photos', 'photoPrimary',
            'usePhotoInGroup'],
        required: false,
    });
    let result = await Meeting.findAll(query);
    let newResult = [];
    for (let meeting of result) {
        let data = meeting.toJSON();
        data.user = meeting.user && processUserPhotoGroup(meeting.user);
        delete data.likeUsers;
        data.liked = meeting.likeUsers.length > 0;
        newResult.push(data);
    }
    if (req.query.count != null) {
        let count = await Meeting.count({ where: query.where });
        res.json({ count, result: newResult });
    } else {
        res.json(newResult);
    }
}));

/**
 * @api {post} /meetings/ 모임 생성하기
 * @apiGroup Meeting
 * @apiName CreateMeeting
 * @apiDescription 새로운 모임을 생성합니다.
 * @apiParam (Body) {String} category 카테고리
 * @apiParam (Body) {String} title 제목
 * @apiParam (Body) {String} body 내용
 * @apiParam (Body) {Number} location 지역
 * @apiParam (Body) {String} place 장소
 * @apiParam (Body) {Number} [mapx] 좌표 X
 * @apiParam (Body) {Number} [mapy] 좌표 Y
 * @apiParam (Body) {Number} price 가격
 * @apiParam (Body) {Date} date 시간
 * @apiParam (Body) {Number} minMembers 최소 참가자 수
 * @apiParam (Body) {Number} maxMembers 최대 참가자 수
 * @apiParam (Body) {File} [photo] 사진
 * @apiSuccess {Meeting} _ 모임
 * @apiUse ReturnMeeting
 */
router.post('/meetings', upload.array('photo', 1),
    uploadHandler, async(async (req, res) => {
        // Run validation
        let validationError = validateSingle(req.body, validationSchema.Meeting);
        if (validationError != null) {
            throw { name: 'ValidationError', data: validationError };
        }
        let data = sanitize(req.body, validationSchema.Meeting);
        data.userId = req.user.id;
        data.groupId = null;
        data.lightning = false;
        if (data.date.getTime() < Date.now()) throw 'ValidationError';
        if ((Date.now()+(60*24*60*60*1000))<data.date.getTime()) throw 'ValidationError';
        if (req.photos && req.photos[0] != null) {
            data.photo = req.photos[0];
        }
        /*
        // Run payment
        await doPayment(req.user, 'meetingCreate');
        */
        let meeting = await sequelize.transaction(async t => {
            // Create meeting
            let meeting = await Meeting.create(data, { transaction: t });
            await meeting.addMember(req.user, { transaction: t });
            return meeting;
        });
        await meeting.updateMembers();
        // All done
        let result = meeting.toJSON();
        result.diamonds = req.user.diamonds;
        res.json(result);
    }));
