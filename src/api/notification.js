import { Router } from 'express';

import async from './util/async';
import loginRequired from './util/loginRequired';
import { Notification } from '../db';

export const router = new Router();
export default router;

/**
 * @apiDefine ReturnNotifications
 * @apiSuccess {Number} _.id 고유 번호
 * @apiSuccess {Date} _.createdAt 생성 시각
 * @apiSuccess {Date} _.updatedAt 업데이트 시각
 * @apiSuccess {Object} _.data 알림 데이터
 * @apiSuccess {String} _.data.type 알림 유형
 * @apiSuccess {String} _.data.title 알림 제목
 * @apiSuccess {String} _.data.message 알림 내용
 * @apiSuccess {StringNullable} _.data.photoThumb 알림 사진 썸네일
 * @apiSuccess {ObjectNullable} _.data.user 해당 행동을 한 사용자
 * @apiSuccess {Number} _.data.user.id 사용자 고유 번호
 * @apiSuccess {String} _.data.user.nickname 사용자 닉네임
 * @apiSuccess {ObjectNullable} _.data.group 해당 행동이 속한 스티커
 * @apiSuccess {Number} _.data.group.id 스티커 고유 번호
 * @apiSuccess {String} _.data.group.name 스티커 이름
 * @apiSuccess {ObjectNullable} _.data.post 해당 행동이 속한 게시글
 * @apiSuccess {Number} _.data.post.id 게시글 고유 번호
 * @apiSuccess {String} _.data.post.title 게시글 제목
 * @apiSuccess {ObjectNullable} _.data.meeting 해당 행동이 속한 모임
 * @apiSuccess {Number} _.data.meeting.id 모임 고유 번호
 * @apiSuccess {String} _.data.meeting.title 모임 제목
 * @apiSuccess {ObjectNullable} _.data.question 해당 행동이 속한 질문
 * @apiSuccess {Number} _.data.question.id 질문 고유 번호
 * @apiSuccess {String} _.data.question.title 질문 제목
 */

/**
 * @api {get} /notifications/ 알림 목록 가져오기
 * @apiGroup Notification
 * @apiName GetNotifications
 * @apiDescription 사용자의 알림 목록을 가져옵니다.
 *
 *   사용자가 로그인하지 않은 경우 401을 반환합니다.
 *
 * @apiParam (Query) {Number} [limit=20] 불러올 개수입니다.
 * @apiParam (Query) {Number} [page=0] 불러올 페이지 번호입니다.
 * @apiSuccess {Notification[]} _ 알림 목록
 * @apiUse ReturnNotifications
 * @apiPermission LoginRequired
 * @apiUse LoginRequired
 */
router.get('/notifications', loginRequired, async(async (req, res) => {
  let { limit = 20, page = 0 } = req.query;
  limit = parseInt(limit) || 0;
  page = parseInt(page) || 0;
  if (limit > 50) limit = 50;
  let result = await req.user.getNotifications({
    where: {
      createdAt: {
        $gt: new Date(Date.now() - 8 * 24 * 60 * 60 * 1000),
      },
    },
    order: [['createdAt', 'DESC']],
    offset: limit * page,
    limit,
  });
  if (req.query.count != null) {
    let count = await req.user.countNotifications();
    res.json({ count, result });
  } else {
    res.json(result);
  }
}));

/**
 * @api {delete} /notifications/ 알림 목록 비우기
 * @apiGroup Notification
 * @apiName RemoveNotifications
 * @apiDescription 사용자의 알림을 전부 삭제합니다.
 *
 *   사용자가 로그인하지 않은 경우 401을 반환합니다.
 *
 * @apiSuccess {Notification[]} _ 삭제된 알림 목록
 * @apiUse ReturnNotifications
 * @apiPermission LoginRequired
 * @apiUse LoginRequired
 */
router.delete('/notifications', loginRequired, async(async (req, res) => {
  let list = await req.user.getNotifications({
    where: {
      createdAt: {
        $gt: new Date(Date.now() - 8 * 24 * 60 * 60 * 1000),
      },
    },
    order: [['createdAt', 'DESC']],
  });
  await req.user.removeNotifications();
  res.json(list);
}));

/**
 * @api {delete} /notifications/:id 알림 삭제하기
 * @apiGroup Notification
 * @apiName RemoveNotification
 * @apiDescription 지정된 알림을 삭제합니다.
 * @apiError (Error 404) NotFound 지정된 객체를 찾을 수 없습니다.
 * @apiSuccess {Notification} _ 삭제된 알림
 * @apiUse ReturnNotifications
 * @apiPermission LoginRequired
 * @apiUse LoginRequired
 */
router.delete('/notifications/:id', loginRequired, async(async (req, res) => {
  const { id } = req.params;
  let notification = await Notification.findOne({
    where: { id, userId: req.user.id },
    order: [['createdAt', 'DESC']],
  });
  if (notification == null) throw 'NotFound';
  await notification.destroy();
  res.json(notification);
}));
