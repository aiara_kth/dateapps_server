import { Router } from 'express';

import iap from './util/iap';
import async from './util/async';
import loginRequired from './util/loginRequired';
import adminRequired from './util/adminRequired';
import pick from './util/pick';
import { getPaymentConfig, getPaymentConfigAll,
  savePaymentConfig } from './util/payment';
import { sequelize, PaymentLog, User } from '../db';

export const router = new Router();
export default router;

/**
 * @api {get} /paymentTable 가격 테이블 가져오기
 * @apiGroup Payment
 * @apiName GetPaymentTable
 * @apiDescription 가격 테이블을 가져옵니다.
 * @apiSuccess {Object} _ 가격 테이블
 */
router.get('/paymentTable', async(async (req, res) => {
  res.json(getPaymentConfigAll());
}));

/**
 * @api {patch} /paymentTable 가격 테이블 수정하기
 * @apiGroup Payment
 * @apiName PatchPaymentTable
 * @apiDescription 가격 테이블을 수정합니다.
 * @apiParam (Body) {JSON} table 가격 테이블 정보
 * @apiSuccess {Object} _ 가격 테이블
 * @apiPermission AdminRequired
 * @apiUse AdminRequired
 */
router.patch('/paymentTable', adminRequired, async(async (req, res) => {
  let data = JSON.parse(req.body.table);
  savePaymentConfig(data);
  res.json(getPaymentConfigAll());
}));

/**
 * @api {get} /payments 결제 목록 가져오기 (관리자)
 * @apiGroup Payment
 * @apiName GetPayments
 * @apiDescription 결제했던 내역을 가져옵니다.
 * @apiParam (Query) {Number} [limit=20] 불러올 개수
 * @apiParam (Query) {Number} [page=0] 불러올 페이지 번호.
 * @apiParam (Query) {String} [type] 결제 타입
 * @apiParam (Query) {String} [userId] 사용자 고유번호
 * @apiParam (Query) {String} [targetId] 타겟 번호
 * @apiSuccess {PaymentLog[]} _ 결제 기록 테이블
 * @apiPermission AdminRequired
 * @apiUse AdminRequired
 */
router.get('/payments', adminRequired, async(async (req, res) => {
  let { limit = 20, page = 0 } = req.query;
  limit = parseInt(limit) || 0;
  page = parseInt(page) || 0;
  if (limit > 50) limit = 50;
  let where = pick(req.query, ['type', 'userId', 'targetId']);
  if (req.query.createdFrom != null) {
    where.createdAt = where.createdAt || {};
    where.createdAt.$gte = new Date(req.query.createdFrom);
  }
  if (req.query.createdTo != null) {
    where.createdAt = where.createdAt || {};
    where.createdAt.$lte = new Date(req.query.createdTo);
  }
  const { sort = 'createdAt', sortOrder = 'DESC' } = req.query;
  let order = [[sort, sortOrder]];
  let paymentLogs = await PaymentLog.findAll({
    where,
    order,
    offset: limit * page,
    limit,
    include: [{
      model: User,
      attributes: ['id', 'name', 'nickname', 'photos', 'photoPrimary'],
    }],
  });
  if (req.query.count != null) {
    let count = await PaymentLog.count({ where });
    res.json({ count, result: paymentLogs });
  } else {
    res.json(paymentLogs);
  }
}));

/**
 * @api {post} /payments 다이아 충전하기
 * @apiGroup Payment
 * @apiName PostPayment
 * @apiDescription 다이아를 충전합니다.
 * @apiParam (Body) {String} id 품목 이름 (결제 테이블의 charge 객체의 키)
 * @apiParam (Body) {String} provider PG 회사 종류 (google, apple)
 * @apiParam (Body) {String} data 결제 데이터
 * @apiParam (Body) {String} signature PG사 서명
 * @apiError (Error 404) NotFound 입력하신 품목을 찾을 수 없습니다.
 * @apiError (Error 403) Forbidden 거래 검증에 실패했습니다.
 * @apiPermission LoginRequired
 * @apiUse LoginRequired
 */
router.post('/payments', loginRequired, async(async (req, res) => {
  // Lookup the payment id
  let user = req.user;
  let { id, provider, data, signature } = req.body;
  let paymentItem = getPaymentConfig('charge')[id];
  if (paymentItem == null) throw 'NotFound';
  if(paymentItem.limit!=null){
    let where = {
      type : 'charge',
      userId : user.id,
    };
    let limit = paymentItem.limit;
    if(!id.endsWith("event")){
      where.cost = -paymentItem.value;
    }
    let count = await PaymentLog.count({
      where
    });
    if(limit<=count){
      throw "Conflict";
    }
  }
  // Run verificiation
  let receiptItem = await new Promise((resolve, reject) => {
    if(provider === "google"){
      iap.validate(iap.GOOGLE, { data, signature }, (err, response) => {
        if (err) return reject(err);
        if (iap.isValidated(response)) {
          let purchaseList = iap.getPurchaseData(response);
          let item = purchaseList && purchaseList[0];
          if (item == null) return reject('Forbidden');
          return resolve(item);
        } else {
          return reject('Forbidden');
        }
      });
    }else if(provider === "apple"){
      var s = Buffer.from(data, 'base64').toString();
      if (s.indexOf('Sandbox') !== -1 && user.isAdmin){
        return resolve({productId:id,orderId:id});
      }
      iap.validate(iap.APPLE, data, (err,response) => {
        if(err) return reject(err);
        if(iap.isValidated(response)) {
          let purchaseList = iap.getPurchaseData(response);
          let item = purchaseList && purchaseList[0];
          if (item == null) return reject('Forbidden');
          return resolve(item);
        }else{
          return reject('Forbidden');
        }
      });
    }
  });
  if(provider === "apple"){
    receiptItem.productId = receiptItem.productId.replace('com.esticker.ios.','');
  }
  if (receiptItem.productId !== id) {
    throw new Error('Receipt item and ID does not match.' +
      receiptItem.productId + ' - ' + id);
  }
  console.log('Adding funds: ' + user.email + ' ' + paymentItem.value);
  // Add funds to the user
  await sequelize.transaction(async transaction => {
    if (user.diamonds + (getPaymentConfig('bias') || 0) < 0) {
      await user.update({ diamonds: -(getPaymentConfig('bias') || 0) },
        { transaction });
    }
    await user.reload({ transaction });
    // Create payment log
    await PaymentLog.create({
      type: 'charge',
      cost: -paymentItem.value,
      diamondsAfter: user.diamonds + paymentItem.value +
        (getPaymentConfig('bias') || 0),
      nickname: user.nickname,
      name: user.name,
      phone: user.phone,
      userId: user.id,
      data: receiptItem.orderId,
    }, { transaction });
    await user.increment('diamonds', { by: paymentItem.value, transaction });
  });
  res.json({ diamonds: user.diamonds + paymentItem.value });
}));
