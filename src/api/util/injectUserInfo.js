import { checkPayment, getPaymentConfig } from './payment';
import { User, UserChat, UserRating, UserFollowing, UserSuggestion,PaymentLog } from '../../db';
import { suggestion as suggestionConfig } from '../../../config';

export default async function injectUserInfo(user, dataOrig) {
  let data;
  if (dataOrig != null) data = dataOrig;
  else data = user.toJSON();
  data.name = user.name;
  data.isLoungeApproved = user.isLoungeApproved || user.approvedAt != null;
  data.isApproved = data.isLoungeApproved;
  data.diamonds = Math.max(0, user.diamonds +
    (getPaymentConfig('bias') || 0));
  data.photos = user.getPhotos(user);
  data.photoThumbs = user.getPhotoThumbs(user);
  data.ratingHistoriesCount = await user.countRatingHistories();
  data.stickersCount = await user.countGroups({
    through: { where: { type: { $notIn: ['pending','banned', 'invited'] } } },
    where: {
      deletedAt: { $or: [null, { $gte: new Date() }] },
    },
  });
  data.invitedStickersCount = await user.countGroups({
    through: { where: { type: 'invited' } },
    where: {
      deletedAt: { $or: [null, { $gte: new Date() }] },
    },
  });
  data.followingsCount = await user.countFollowings({
    where: {
      isEnabled: true,
    },
  });
  let suggestions = await UserSuggestion.findAll({
    where: {
      createdAt: { $gte: new Date(Date.now() - suggestionConfig.expire) },
      targetId: user.id ,
    },
    include: [{
      model: User,
      as: 'user',
    },{
      model: User,
      as: 'target',
    }],
  });
  for(let suggest of suggestions){
    let following = await user.hasFollowing(suggest.user);
    let follower = await user.hasFollower(suggest.user);
    if(following&&!follower){
      data.followingsCount -= 1;
    }
  }
  data.followersCount = await user.countFollowers({
    through: { where: { denied: false } },
    where: {
      isEnabled: true,
    },
  });
  data.followersCount += suggestions.length;
  data.meetingsCount = await user.countMeetings({
    through: { where: { enabled: true } },
  });
  data.chatsCount = await UserChat.count({
    where: {
      $or: [
        { senderId: user.id },
        { receiverId: user.id },
      ],
      $not: {
        expired: true,
        chatKey: null,
      },
    },
  }) + await UserFollowing.count({
    where: {
      followingId: user.id,
      reply: true,
      '$`follower.receiverChats`.`senderId`$': null,
      '$`follower.senderChats`.`senderId`$': null,
    },
    order: [['createdAt', 'DESC']],
    /* offset: Math.max(0, limit * page - count),
    limit: Math.max(0, page - result.length), */
    include: [{
      model: User,
      attributes: ['id', 'nickname', 'photos', 'photoPrimary'],
      as: 'follower',
      /* where: {
        $and: [
          sequelize.literal('follower.senderChats.id is NULL'),
          sequelize.literal('follower.receiverChats.id is NULL'),
        ],
      }, */
      include: [{
        model: UserFollowing,
        attributes: [],
        as: 'followerFollowings',
        where: {
          followerId: user.id,
        },
      }, {
        model: UserChat,
        as: 'senderChats',
        where: {
          receiverId: user.id,
        },
        required: false,
      }, {
        model: UserChat,
        as: 'receiverChats',
        where: {
          senderId: user.id,
        },
        required: false,
      }],
    }],
  });
  data.loungeGenderPay = await checkPayment(user, 'loungeGender');
  data.chargeCount = await PaymentLog.count({ where : { type: 'charge', userId: user.id } });
  let paymentLogs = await PaymentLog.findAll({
   where : { userId: user.id },
   order : [['createdAt', 'DESC']],
   limit : 1,
  });
  if(paymentLogs!=null){
    data.lastPaidDate = paymentLogs.createdAt;
  }
  return data;
}

export async function injectOtherUserInfo(selectedUser, user, dataOrig) {
  let data;
  if (dataOrig != null) data = dataOrig;
  else data = selectedUser.toJSON();
  if (user == null) return data;
  let votedWhere = {
    voterId: user.id,
    voteeId: selectedUser.id,
  };
	let votedCountWhere = {
	  voteeId: selectedUser.id,
	};
  if (selectedUser.newbieStartedAt != null) {
    votedWhere.updatedAt = {
      $gt: selectedUser.newbieStartedAt,
    };
		votedCountWhere.updatedAt = {
		  $gt: selectedUser.newbieStartedAt,
		};
  }
  let votedReport = await UserRating.findOne({ where: votedWhere });
	let votedCount = await UserRating.count({ where: votedCountWhere });
  data.hasVoted = votedReport != null;
	data.votedCount = votedCount;
  data.votedScore = votedReport && votedReport.score;
  let followingData = await UserFollowing.findOne({ where: {
    followingId: selectedUser.id,
    followerId: user.id,
  } });
  let followerData = await UserFollowing.findOne({ where: {
    followingId: user.id,
    followerId: selectedUser.id,
  } });
  let chatData = await UserChat.findOne({ where: {
    senderId: user.id,
    receiverId: selectedUser.id,
  } });
  data.following = followingData != null;
  data.followingDenied = followingData != null && followingData.denied;
  data.follower = followerData != null;
  data.followerDenied = followerData != null && followerData.denied;
  data.canChat = followerData != null && followerData.reply;
  data.canChatForce = !data.following && !data.follower;
  data.canFollow = !data.follower || chatData == null;
  {
    let where = {
      $or: [
        { senderId: selectedUser.id, receiverId: user.id },
        { receiverId: selectedUser.id, senderId: user.id },
      ],
    };
    data.chat = await UserChat.findOne({ where });
  }
  return data;
}
