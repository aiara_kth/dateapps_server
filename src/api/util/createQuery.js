import isTrue from './isTrue';
import { Group, User } from '../../db';

export function createMeetingQuery(req) {
  let { limit = 20, page = 0, sort } = req.query;
  limit = parseInt(limit) || 0;
  page = parseInt(page) || 0;
  if (limit > 50) limit = 50;
  let where = { groupId: null, date: { $gte: new Date() }, failed: false };
  let include = [];
  if (req.query.all != null && isTrue(req.query.all)) {
    if(req.notExpired != null && req.notExpired){
      where = {date: { $gte: new Date() }, failed: false};
    }else{
      where = {};
    }
    include.push({
      model: Group,
      attributes: ['id', 'category', 'name', 'profilePhoto', 'mainPhoto',
        'profilePhotoThumb', 'mainPhotoThumb', 'location'],
    });
  }
  if (req.query.title != null) {
    where.title = { $like: `%${req.query.title}%` };
  }
  if (req.query.category != null) {
    where.category = req.query.category;
  }
  if (req.query.location != null) {
    where.location = req.query.location;
  }
  if (req.query.failed != null) {
    where.failed = isTrue(req.query.failed);
  }
  if (req.query.dateLoop != null) {
    let loop = parseInt(req.query.dateLoop);
    let loopCount = parseInt(req.query.dateLoopCount) || 10;
    if (loopCount > 50) loopCount = 50;
    let from = new Date(req.query.dateFrom).getTime();
    let to = new Date(req.query.dateTo).getTime();
    let or = [];
    for (let i = 0; i < loopCount; ++i) {
      or.push({ date: {
        $gte: new Date(from + loop * i),
        $lte: new Date(to + loop * i),
      } });
    }
    where.$or = or;
    if (req.query.dateLoopCapFrom != null) {
      where.date.$gte = new Date(req.query.dateLoopCapFrom);
    }
    if (req.query.dateLoopCapTo != null) {
      where.date.$lte = new Date(req.query.dateLoopCapTo);
    }
  } else {
    if (req.query.dateFrom != null) {
      where.date.$gte = new Date(req.query.dateFrom);
    }
    if (req.query.dateTo != null) {
      where.date.$lte = new Date(req.query.dateTo);
    }
  }
  let order = ['createdAt', 'DESC'];
  if (sort === 'members') order = ['membersCount', 'DESC'];
  if (sort === 'membersLeast') order = ['membersCount', 'ASC'];
  if (sort === 'likes') order = ['likesCount', 'DESC'];
  if (sort === 'likesLeast') order = ['likesCount', 'ASC'];
  if (sort === 'newest') order = ['createdAt', 'DESC'];
  if (sort === 'oldest') order = ['createdAt', 'ASC'];
  if (req.query.sortLocation !== 'false' && req.user != null) {
    /* order = [[sequelize.literal(
      `meeting.location='${parseInt(req.user.location)}'`
    ), 'DESC'], order]; */
    if (order[0] === 'createdAt') {
      order = [order];
    } else {
      order = [order, ['createdAt', 'DESC']];
    }
  } else {
    order = [order];
  }
  return {
    where,
    order: order,
    offset: limit * page,
    limit,
    include,
  };
}
