import { sequelize, PaymentLog, PaymentTable } from '../../db';

let paymentEntry, paymentTable;

export const BILLING_IDS = [
  'premium_1month',
  'premium_3month',
  'premium_1years',
];

async function refreshPaymentTable() {
  let entry = await PaymentTable.findOrCreate({
    where: {},
    defaults: {
      data: require('../../../config/payment.example.json'),
    },
  });
  paymentEntry = entry[0];
  paymentTable = paymentEntry.data;
}

refreshPaymentTable();
setInterval(refreshPaymentTable, 1000 * 30);

export function getPaymentConfig(field) {
  return paymentTable[field];
}

export function getPaymentConfigAll() {
  return paymentTable;
}

export async function savePaymentConfig(data) {
  paymentTable = data;
  await paymentEntry.update({ data });
}

export async function checkPayment(user, type, targetId, ignoreExpire) {
  let paymentType = getPaymentConfig(type);
  let where = { type };
  if (targetId != null) where.targetId = targetId;
  if (paymentType.expire != null && !ignoreExpire) {
    where.createdAt = {
      $gt: new Date(Date.now() - paymentType.expire),
    };
  }
  let list = await user.getPaymentLogs({ where });
  return list.length > 0;
}

export async function doPayment(user, type, targetId) {
  // Check if the payment is really necessary
  let paymentType = getPaymentConfig(type);
  let where = { type };
  if (paymentType.expire != null) {
    where.createdAt = {
      $gt: new Date(Date.now() - paymentType.expire),
    };
  }
  if (targetId != null) where.targetId = targetId;
  let alreadyPaid = false;
  if (paymentType.expire != null || targetId != null) {
    let list = await user.getPaymentLogs({ where });
    alreadyPaid = list.length > 0;
  }
  if (alreadyPaid) throw 'Conflict';
  console.log('Making payment: ' + user.email + ' ' + type + ' ' + targetId);
  // Check balance
  await sequelize.transaction(async transaction => {
    await user.reload({ transaction });
    if ((user.diamonds + (getPaymentConfig('bias') || 0)) < paymentType.cost) {
      throw 'NoDiamonds';
    }
    // Create payment log
    await PaymentLog.create({
      type,
      targetId,
      cost: paymentType.cost,
      diamondsAfter: user.diamonds - paymentType.cost +
        (getPaymentConfig('bias') || 0),
      nickname: user.nickname,
      name: user.name,
      phone: user.phone,
      userId: user.id,
    }, { transaction });
    await user.decrement('diamonds', { by: paymentType.cost, transaction });
  });
  user.diamonds -= paymentType.cost;
  // All done
  return true;
}

export async function refundPayment(user, type, targetId) {
  let paymentType = getPaymentConfig(type);
  console.log('Making refund: ' + user.email + ' ' + type + ' ' + targetId);
  // Check balance
  await sequelize.transaction(async transaction => {
    await user.reload({ transaction });
    // Create payment log
    await PaymentLog.create({
      type: type + 'Refund',
      targetId,
      cost: -paymentType.cost,
      nickname: user.nickname,
      name: user.name,
      phone: user.phone,
      diamondsAfter: user.diamonds + paymentType.cost +
        (getPaymentConfig('bias') || 0),
      userId: user.id,
    }, { transaction });
    await user.increment('diamonds', { by: paymentType.cost, transaction });
  });
  user.diamonds += paymentType.cost;
  // All done
  return true;
}

export async function doPaymentFree(user, type, targetId) {
  // Check if the payment is really necessary
  let paymentType = getPaymentConfig(type);
  let where = { type };
  if (paymentType.expire != null) {
    where.createdAt = {
      $gt: new Date(Date.now() - paymentType.expire),
    };
  }
  if (targetId != null) where.targetId = targetId;
  let alreadyPaid = false;
  if (paymentType.expire != null || targetId != null) {
    let list = await user.getPaymentLogs({ where });
    alreadyPaid = list.length > 0;
  }
  if (alreadyPaid) throw 'Conflict';
  console.log('Making free payment: ' + user.email + ' ' + type + ' ' +
    targetId);
  // Create payment log
  await PaymentLog.create({
    type,
    targetId,
    cost: 0,
    nickname: user.nickname,
    name: user.name,
    phone: user.phone,
    diamondsAfter: user.diamonds + (getPaymentConfig('bias') || 0),
    userId: user.id,
  });
  // All done
  return true;
}
