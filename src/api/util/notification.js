import pick from './pick';
import { Notification } from '../../db';
import FCM from 'fcm-node';
import flatten from 'flat';
import { firebase as firebaseConfig } from '../../../config';

const fcm = new FCM(firebaseConfig.fcmKey);

const NOTIFICATION_TABLE = {
  notice: ['에스티커 메세지', '공지사항'],
  joinAccept: ['에스티커 메세지', '관리자의 승인을 받으셨습니다. 라운지에서 좋은 인연 만드시길 바랍니다.'],
  joinDeny: ['에스티커 메세지', '에스티커 승인이 거절되었습니다.'],
  loungeAccept: ['에스티커 메세지', '관리자의 승인을 받으셨습니다. 라운지에서 좋은 인연 만드시길 바랍니다.'],
  loungeDeny: ['에스티커 메세지', '투표한 사람이 적어 라운지 승인에 실패했습니다.'],
  joinCancel: ['에스티커 메세지', '에스티커 라운지 승인이 취소되었습니다.'],
  loungeCancel: ['에스티커 메세지', '라운지 승인이 취소되었습니다.'],
  follow: ['에스티커 메세지', '%u님에게 관심카드를 받으셨어요 :)', 'user'],
  followDeny: ['에스티커 메세지', '%u님이 회원님의 관심 표현을 거절했습니다.', 'user'],
  groupInvite: ['에스티커 메세지', '%u님이 %g 스티커에 초대했습니다.', 'group'],
  groupInviteAccept: ['에스티커 메세지', '%u님이 %g 스티커 초대를 수락했습니다.',
    'group'],
  groupJoin: ['에스티커 메세지', '%u님이 %g 스티커의 가입을 요청했습니다.', 'user'],
  groupAccept: ['에스티커 메세지', '%g 스티커의 가입이 승인되었습니다.', 'group'],
  groupKick: ['에스티커 메세지', '%g 스티커에서 강퇴되었습니다.'],
  groupBan: ['에스티커 메세지', '%g 스티커에서 영구 추방되었습니다.'],
  groupUserChange: ['에스티커 메세지', '%g 스티커에서의 권한이 변경되었습니다.'],
	groupLeave: ['에스티커 메세지','%u님이 %g 스티커를 탈퇴하셨습니다.'],
  comment: ['에스티커 메세지', '%p 게시글에 %u님이 댓글을 남겼습니다.', 'user'],
  like: ['에스티커 메세지', '%p 게시글에 %u님이 좋아요를 눌렀습니다.', 'user'],
  newPost: ['에스티커 메세지', '%g 스티커에 %p 게시글이 올라왔습니다.', 'group'],
  meetingLike: ['에스티커 메세지', '%m 모임에 %u님이 좋아요를 눌렀습니다.', 'user'],
  meetingJoin: ['에스티커 메세지', '%m 모임에 %u님이 참가했습니다.', 'user'],
  meetingLeave: ['에스티커 메세지', '%m 모임에 %u님이 참가 취소했습니다.', 'user'],
  meetingComment: ['에스티커 메세지', '%m 모임에 %u님이 댓글을 남겼습니다.', 'user'],
  meetingFail: ['에스티커 메세지', '%m 모임의 참가자가 최소 인원보다 적어 모임이 ' +
    '취소되었습니다.'],
  meetingCancel: ['에스티커 메세지', '%m 모임이 취소되었습니다.'],
  questionAnswer: ['에스티커 메세지', '질문하신 문의에 답변이 달렸습니다.'],
  chatInvite: ['에스티커 메세지', '%u님이 대화 신청을 하셨습니다. %u님과 대화하시겠습니까?', 'user'],
  chatAccept: ['에스티커 메세지', '%u님이 대화 신청을 수락하였습니다. 즐거운 대화 나누세요. ^^', 'user'],
  chatDeny: ['에스티커 메세지', '%u님이 대화 신청을 거절하였습니다. 아쉽네요. 조금 더 좋은 분과 만나실 수 있을 거예요.', 'user'],
  groupChat: ['에스티커 메세지','%t'],
  personalChat: ['에스티커 메세지','%t'],
  userSuggest: ['에스티커 메세지','%u님에게 관심카드를 받으셨어요 :)'],
  userFeedback: ['에스티커 메세지','%u회원님이 관심표현을 확인하였습니다. 지금 대화 신청을 하시면 대화를 수락할 가능성이 높습니다.'],
  followView: ['에스티커 메세지','%u회원님이 관심표현을 확인하였습니다. 지금 대화 신청을 하시면 대화를 수락할 가능성이 높습니다.'],
  /* dailySend: ['에스티커 알림', '1명이 회원님에게 관심 표현을 했고, 2명이 채팅 신청을 했습니다.'] */
};

export const REDUCE_NOTIFICATIONS = ['follow', 'chatInvite'];
const REDUCE_NOTIFICATIONS_COUNT = 4;

function runTemplate(template, data) {
  let returned = template;
  returned = returned.replace('%u', () => data.user.nickname);
  returned = returned.replace('%u', () => data.user.nickname);
  returned = returned.replace('%g', () => data.group.name);
  returned = returned.replace('%p', () => data.post.title);
  returned = returned.replace('%m', () => data.meeting.title);
  returned = returned.replace('%t', () => data.text);
  return returned;
}

export default async function sendNotification(target, data) {
  // Construct title / message from data, if not available
  if (NOTIFICATION_TABLE[data.type] != null) {
    const [titleTemplate, messageTemplate, photo] =
      NOTIFICATION_TABLE[data.type];
    if (data.type === "groupChat") data.pushOnly = true;
    if (data.title == null) data.title = runTemplate(titleTemplate, data);
    if (data.message == null) data.message = runTemplate(messageTemplate, data);
    if (photo === 'user') data.photoThumb = data.user.photoThumb;
    else if (photo === 'group') data.photoThumb = data.group.profilePhotoThumb;
    else data.photoThumb = null;
    if(data.hidden == null){
		  if (target.deviceType === "android") data.hidden = true;
		  else data.hidden = false;
    }
  }
  if(data.type === "userSuggest"){
    if(data.users != null){
      data.message = data.users[0].nickname + "님과 " + data.users[1].nickname + "님이 당신을 애타게 기다려요.";
      delete data.users;
    }
  }
  // Sanitize fields
  if (data.user != null) {
    let field = ['id', 'nickname','photo'];
    data.user = pick(data.user.toJSON(), field);
    // If acting user is same as recipicent, stop
    if (target.id === data.user.id) return;
  }
  if (data.group != null) {
    data.group = pick(data.group.toJSON(), ['id', 'name']);
  }
  if (data.post != null) {
    data.post = pick(data.post.toJSON(), ['id', 'title']);
  }
  if (data.meeting != null) {
    data.meeting = pick(data.meeting.toJSON(), ['id', 'title']);
  }
  if (data.question != null) {
    data.question = pick(data.question.toJSON(), ['id', 'title']);
  }
  if (Array.isArray(target)) {
    if (!data.pushOnly) {
      // Do bulk create
      await Notification.bulkCreate(target.map(user => ({
        userId: user.id, data, type: data.type,
      })));
    }
    // Then send notifications...
    let pushKeys = [];
    target.forEach(user => {
      if (user.isEnabled && user.updatePush && user.fcmKey != null) {
        pushKeys.push(user.fcmKey);
      }
    });
    if (pushKeys.length > 0) {
      for (let i = 0; i < pushKeys.length; i += 1000) {
        let message = {
          registration_ids: pushKeys.slice(i, i + 1000),
          collapse_key: data.type,
          notification: data.hidden ? undefined : {
            title: data.title,
            body: data.message,
						sound: "default"
          },
          data: flatten(data),
					priority: data.hidden ? "normal" : "high",
        };
        // Don't care about FCM success or fail
        fcm.send(message, (err, res) => {
          // TODO remove this
          if (err) {
            console.log(err);
          } else {
            console.log(res);
          }
        });
      }
    }
  } else {
    if (!data.pushOnly) {
      // Create notification entry
      await Notification.create({ userId: target.id, data, type: data.type });
    }
    console.log('Notification to ' + target.email + ': ' + data.message);
    // If the target is single user and the notification is one of
    // 'reduce' notifications, don't send FCM if the user already has
    // 'isDailySet' set, or count notifications for past 24 hours and
    // detect whether if we should set the flag.
    // if (REDUCE_NOTIFICATIONS.includes(data.type)) {
    //   if (target.isDailySet) {
    //     // Stop sending FCM
    //     return;
    //   } else {
    //     let count = await Notification.count({ where: {
    //       userId: target.id,
    //       type: { $in: REDUCE_NOTIFICATIONS },
    //       createdAt: {
    //         $gt: new Date(Date.now() - 24 * 60 * 60 * 1000),
    //       },
    //     } });
    //     if (count > REDUCE_NOTIFICATIONS_COUNT) {
    //       // Send dailySet notification.
    //       // await sendNotification(target, { type: 'dailySet' });
    //       // await target.update({ isDailySet: true });
    //       // // And stop sending FCM.
    //       // return;
    //     }
    //   }
    // }
    if (target.isEnabled && target.updatePush && target.fcmKey != null) {
      // TODO Collapse key
      let message = {
        to: target.fcmKey,
        collapse_key: data.type,
        notification: data.hidden ? undefined : {
          title: data.title,
          body: data.message,
					sound: "default",
        },
        data: flatten(data),
				priority: "high",
      };
      // Don't care about FCM success or fail
      fcm.send(message, (err, res) => {
        // TODO remove this
        if (err) {
          console.log(err);
        } else {
          console.log(res);
        }
      });
    }
  }
}
