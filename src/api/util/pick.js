// Clears out undefined / null, and extracts fields
export default function pick(data, fields) {
  let newData = {};
  fields.map(key => {
    if (data[key] == null) return;
    newData[key] = data[key];
  });
  return newData;
}
