import iap from 'in-app-purchase';
import path from 'path';

import { iap as iapConfig } from '../../../config';

const updatedIapConfig = Object.assign({}, iapConfig, {
  googlePublicKeyPath: path.resolve(iapConfig.googlePublicKeyPath) + '/',
  applePassword: iapConfig.applePassword,
});

iap.config(updatedIapConfig);
iap.setup(err => {
  if (err) throw err;
});

export default iap;
