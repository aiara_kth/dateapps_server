export default function isTrue(value) {
  return value === 'true' || value === 'yes' || value === '1' ||
    value === true || value === 1;
}
