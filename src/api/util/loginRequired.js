export function loginRequiredHandler(req, res, next,
  approve = false, profile = false, lounge = false, loungeProfile = false,
) {
  if (req.user == null) {
    next('LoginRequired');
  } else if (!req.user.isEnabled) {
    next('UserDisabled');
  } else if (req.user.isAdmin) {
    next();
  } else if (approve && !req.user.isApproved && req.user.approvedAt == null) {
    next('ApproveRequired');
	} else if (profile && (req.user.name == null || req.user.birth == null || req.user.location == null || req.user.nickname == null ) ){
	  next('ProfileRequired');
  } else if (loungeProfile && !req.user.hasProfile && req.user.approvedAt == null) {
    next('LoungeProfileRequired');
  } else if (lounge && !req.user.isLoungeApproved && req.user.approvedAt == null) {
    next('LoungeRequired');
  } else {
    next();
  }
}

export default (req, res, next) =>
  loginRequiredHandler(req, res, next, false, false, false,false);

export const loungeRequired = (req, res, next) =>
  loginRequiredHandler(req, res, next, true, true, true,true);

export const approveRequired = (req, res, next) =>
  loginRequiredHandler(req, res, next, true, true, false,false);

export const profileRequired = (req, res, next) =>
  loginRequiredHandler(req, res, next, false, true, false,false);

export const loungeProfileRequired = (req, res, next) =>
  loginRequiredHandler(req,res,next,false,true,false,true);
