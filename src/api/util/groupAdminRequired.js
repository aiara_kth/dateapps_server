export const TYPE_TABLE = {
  banned: -100,
  pending: -1,
  invited: -1,
  member: 0,
  staff: 1,
  subAdmin: 2,
  admin: 3,
};

export const DEFAULT_PERMISSION_TABLE = {
  staff: 4,
  subAdmin: 7,
  admin: 7,
};

export const PERMISSION_TABLE = {
  member: 1,
  post: 2,
  meeting: 4,
};

export function checkTypeHierarchySet(myType, userType, postType) {
  let userLevel = Math.max(TYPE_TABLE[userType], TYPE_TABLE[postType]);
  let myLevel = TYPE_TABLE[myType];
  return userLevel < myLevel;
}

export function checkTypeHierarchy(myType, userType) {
  let userLevel = TYPE_TABLE[userType];
  let myLevel = TYPE_TABLE[myType];
  return userLevel < myLevel;
}

export function checkType(myType, targetType) {
  return TYPE_TABLE[targetType] <= TYPE_TABLE[myType];
}

export function checkPermission(groupUser, targetType) {
  if (groupUser == null) return false;
  let permission = (DEFAULT_PERMISSION_TABLE[groupUser.type] || 0) |
    groupUser.permission;
  return (permission & PERMISSION_TABLE[targetType]) !== 0;
}

export function setPermission({ type, permission }) {
  let output = permission;
  output |= DEFAULT_PERMISSION_TABLE[type];
  if (TYPE_TABLE[type] <= 0) output = 0;
  return output;
}

export function groupPermissionRequired(targetType) {
  return (req, res, next) => {
    if (req.user == null) return next('LoginRequired');
    if (req.user.isAdmin) return next();
    if (!req.user.isEnabled) return next('UserDisabled');
    if (!checkPermission(req.groupUser, targetType)) {
      return next('GroupAdminRequired');
    }
    next();
  };
}

export function groupAdminRequired(req, res, next) {
  if (req.user == null) return next('LoginRequired');
  if (req.user.isAdmin) return next();
  if (!req.user.isEnabled) return next('UserDisabled');
  if (req.groupUser == null) return next('GroupMemberRequired');
  if (req.groupUser.type !== 'admin') return next('GroupAdminRequired');
  next();
}

export function groupMemberRequired(req, res, next) {
  if (req.user == null) return next('LoginRequired');
  if (req.user.isAdmin) return next();
  if (!req.user.isEnabled) return next('UserDisabled');
  if (req.groupUser == null) return next('GroupMemberRequired');
  if (!checkType(req.groupUser.type, 'member')) {
    return next('GroupMemberRequired');
  }
  next();
}
