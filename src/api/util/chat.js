import firebase from '../../firebase';

const firebaseDB = firebase.database();
const rooms = firebaseDB.ref('/rooms');
const messages = firebaseDB.ref('/messages');

const TYPE_TEXT = 0;
const TYPE_PHOTO = 1;

export async function createChat(name, participantsList, group, isForce) {
  // Convert participants to object
  let participants = {};
  for (let i = 0; i < participantsList.length; ++i) {
    let user = participantsList[i];
    // TODO We may change this to something else
    participants[user.id] = {
      photo: user.photoThumb,
      nickname: user.nickname,
      name: name ||
        (participantsList[Math.max(1 - i, 0)].nickname + '님과의 대화'),
      noChatPush: user.noChatPush || false,
    };
  }
  let roomRef = rooms.push();
  roomRef.set({
    name: name ||
      (participantsList.map(v => v.nickname + '님').join(', ') + '과의 대화'),
    groupId: group ? group.id : null,
    isGroup: !!group,
    isForce,
    participants,
    createdAt: new Date(),
    expired: false,
  });
  let roomId = roomRef.key;
  let messagesRef = messages.child(roomId);
  messagesRef.set({});
  return roomId;
}

export async function lockChat(chatKey) {
  let roomRef = rooms.child(chatKey);
  roomRef.update({
    expired: true,
  });
}

export async function addChatParticipant(chatKey, participant) {
  let roomRef = rooms.child(chatKey);
  let participantRef = roomRef.child('participants').child(participant.id);
  console.log('Adding chat participant ' + participant.id + ' to ' + chatKey);
  roomRef.once('value', snapshot => {
    let room = snapshot.val();
    participantRef.set({
      photo: participant.photoThumb,
      nickname: participant.nickname,
      name: room.name,
      noChatPush: participant.noChatPush || false,
    });
  });
}

export async function addChatData(chatKey, user, body, receiver = null){
  let messageRef = messages.child(chatKey);
	let newMessage = messageRef.push();
	newMessage.set({
	  messageType: TYPE_TEXT,
		sender: user.nickname,
		body : body,
		receiver: receiver,
		photoUrl: "NOPHOTO",
		profileUrl: user.photo,
		emoticonNumber: -1,
		timeStamp: firebase.database.ServerValue.TIMESTAMP,
		isAdmin: false,
	});
}

export async function removeChatParticipant(chatKey, participant) {
  let roomRef = rooms.child(chatKey);
  let participantRef = roomRef.child('participants').child(participant.id);
  participantRef.set(null);
}
