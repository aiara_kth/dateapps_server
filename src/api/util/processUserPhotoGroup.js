export default function processUserPhotoGroup(v) {
  let user = v.toJSON();
  if (!user.usePhotoInGroup) {
    delete user.photo;
    delete user.photoThumb;
  }
  delete user.photos;
  delete user.photoThumbs;
  return user;
}
