import { GroupUser } from '../../db';

import async from '../util/async';

export default async(async (req, res, next) => {
  if (req.user == null) return next();
  if (req.selectedGroup == null) return next();
  let groupUser = await GroupUser.findOne({ where: {
    groupId: req.selectedGroup.id,
    userId: req.user.id,
  } });
  req.groupUser = groupUser;
  next();
});
