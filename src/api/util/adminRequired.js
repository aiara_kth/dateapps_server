export default function adminRequired(req, res, next) {
  if (req.user == null) {
    next('LoginRequired');
  } else if (!req.user.isAdmin) {
    next('AdminRequired');
  } else {
    next();
  }
}
