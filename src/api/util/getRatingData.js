import { sequelize, UserRating } from '../../db';

export default async function getRatingData(user) {
  let where = {
    voteeId: user.id,
  };
  if (user.newbieStartedAt != null) {
    where.updatedAt = {
      $gt: user.newbieStartedAt,
    };
  }
  let sums = await UserRating.findAll({
    attributes: ['score', [sequelize.fn('count', sequelize.col('voterId')),
      'countVal']],
    where,
    group: ['score'],
  });
  let scores = [];
  let votes = 0;
  let sum = 0;
  for (let i = 0; i <= 10; ++i) {
    scores[i] = 0;
  }
  sums.forEach(v => {
    sum += v.score * v.get('countVal');
    votes += v.get('countVal');
    scores[v.score] = v.get('countVal');
  });
  let average = sum / votes;
  if (isNaN(average)) average = 5;
  return { average, votes, scores };
}
