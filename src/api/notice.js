import { Router } from 'express';

import async from './util/async';
import adminRequired from './util/adminRequired';
import photoUpload from './util/photoUpload';
import { validateSingle, sanitize } from '../validation/validate';
import * as validationSchema from '../validation/schema';

import { Notice } from '../db';

export const router = new Router();
export default router;

/**
 * @apiDefine ReturnNotice
 * @apiSuccess {Number} _.id 고유 번호
 * @apiSuccess {Date} _.createdAt 생성 시각
 * @apiSuccess {Date} _.updatedAt 업데이트 시각
 * @apiSuccess {String} _.category 카테고리
 * @apiSuccess {String} _.title 제목
 * @apiSuccess {String} _.body 내용
 * @apiSuccess {String[]} _.photos 사진
 * @apiSuccess {String[]} _.photoThumbs 사진 썸네일
 */

/**
 * @apiDefine GetNotice
 * @apiParam (Parameter) {Number} noticeId 공지사항 번호
 * @apiError (Error 404) NotFound 지정된 객체를 찾을 수 없습니다.
 */

export const noticeRouter = new Router();

/**
 * @api {get} /notices/:noticeId/ 공지사항 가져오기
 * @apiGroup Notice
 * @apiName GetNotice
 * @apiDescription 공지사항을 가져옵니다.
 * @apiUse GetNotice
 * @apiSuccess {Notice} _ 공지사항
 * @apiUse ReturnNotice
 */
noticeRouter.get('/', async(async (req, res) => {
  await req.selectedNotice.increment('viewCount');
  res.json(req.selectedNotice);
}));

/**
 * @api {patch} /notices/:noticeId/ 공지사항 수정하기
 * @apiGroup Notice
 * @apiName PatchNotice
 * @apiDescription 공지사항을 수정합니다.
 *
 *   사진 업로드를 하기 위해서는 `multipart/form-data`를 사용해 주세요. 사진이 여기를
 *   사용해서 업로드된 경우 이미 있던 사진들이 전부 삭제됩니다. 이를 원하지 않는다면
 *   `/notices/:noticeId/photos` 엔드포인트를 사용해 주세요.
 *
 * @apiUse GetNotice
 * @apiParam (Body) {String} title 제목
 * @apiParam (Body) {String} category 카테고리
 * @apiParam (Body) {String} body 내용
 * @apiParam (Body) {File[]} photos 사진
 * @apiSuccess {Notice} _ 공지사항
 * @apiUse ReturnNotice
 * @apiPermission AdminRequired
 * @apiUse AdminRequired
 */
noticeRouter.patch('/', adminRequired, photoUpload, async(async (req, res) => {
  // Run validation
  let validationError = validateSingle(
    Object.assign({}, req.selectedNotice.toJSON(), req.body),
    validationSchema.Notice);
  if (validationError != null) {
    throw { name: 'ValidationError', data: validationError };
  }
  let sanitizedData = sanitize(req.body, validationSchema.Notice);
  if (req.photos != null && req.photos.length > 0) {
    sanitizedData.photos = req.photos || [];
    if (sanitizedData.photos.length > 8) throw 'PhotoThreshold';
  }
  // Update
  await req.selectedNotice.update(sanitizedData);
  res.json(req.selectedNotice);
}));

/**
 * @api {delete} /notices/:noticeId/ 공지사항 삭제하기
 * @apiGroup Notice
 * @apiName DeleteNotice
 * @apiDescription 공지사항을 삭제합니다.
 * @apiUse GetNotice
 * @apiPermission AdminRequired
 * @apiUse AdminRequired
 */
noticeRouter.delete('/', adminRequired, async(async (req, res) => {
  await req.selectedNotice.destroy();
  res.json({});
}));

/**
 * @api {get} /notices/:noticeId/photos 공지사항 사진 가져오기
 * @apiGroup Notice
 * @apiName GetNoticePhotos
 * @apiDescription 공지의 사진 목록을 가져옵니다.
 * @apiUse GetNotice
 * @apiSuccess {String[]} photos 사진 배열
 * @apiSuccess {String[]} photoThumbs 사진 썸네일 배열
 */
noticeRouter.get('/photos', async(async (req, res) => {
  const { photos, photoThumbs } = req.selectedNotice;
  res.json({ photos, photoThumbs });
}));

/**
 * @api {post} /notices/:noticeId/photos 공지사항 사진 올리기
 * @apiGroup Notice
 * @apiName PostNoticePhotos
 * @apiDescription 공지의 사진을 추가로 업로드합니다.
 * @apiUse GetNotice
 * @apiSuccess {String[]} photos 사진 배열
 * @apiSuccess {String[]} photoThumbs 사진 썸네일 배열
 * @apiError (Error 400) PhotoInvalid 올바른 사진 파일이 아닙니다.
 * @apiError (Error 403) PhotoThreshold 사진의 최대 개수를 초과했습니다.
 * @apiPermission AdminRequired
 * @apiUse AdminRequired
 */
noticeRouter.post('/photos', adminRequired, photoUpload,
async(async (req, res) => {
  let notice = req.selectedNotice;
  let photos = notice.photos.concat(req.photos || []);
  if (photos.length > 8) throw 'PhotoThreshold';
  await notice.update({ photos });
  const { photoThumbs } = notice;
  res.json({ photos, photoThumbs });
}));

/**
 * @api {delete} /notices/:noticeId/photos 공지사항 사진 전부 삭제하기
 * @apiGroup Notice
 * @apiName DeleteNoticePhotos
 * @apiDescription 공지의 사진을 전부 삭제합니다.
 * @apiUse GetNotice
 * @apiSuccess {String[]} photos 사진 배열
 * @apiSuccess {String[]} photoThumbs 사진 썸네일 배열
 * @apiPermission AdminRequired
 * @apiUse AdminRequired
 */
noticeRouter.delete('/photos', adminRequired, async(async (req, res) => {
  let post = req.selectedNotice;
  await post.update({ photos: [] });
  const { photos, photoThumbs } = post;
  res.json({ photos, photoThumbs });
}));

/**
 * @api {post} /notices/:noticeId/photos/:photoId 공지사항 사진 삭제하기
 * @apiGroup Notice
 * @apiName DeleteNoticePhoto
 * @apiDescription n번째 사진을 삭제합니다.
 * @apiParam (Parameter) {Number} photoId 삭제할 사진 번호
 * @apiSuccess {String[]} photos 사진 배열
 * @apiSuccess {String[]} photoThumbs 사진 썸네일 배열
 * @apiUse GetNotice
 * @apiPermission AdminRequired
 * @apiUse AdminRequired
 */
noticeRouter.delete('/photos/:photoId', adminRequired,
async(async (req, res) => {
  let index = parseInt(req.params.photoId);
  if (isNaN(index)) throw 'ValidationError';
  let post = req.selectedNotice;
  await post.update({
    photos: post.photos.filter((_, i) => i !== index),
  });
  const { photos, photoThumbs } = post;
  res.json({ photos, photoThumbs });
}));

router.use('/notices/:noticeId', async(async (req, res, next) => {
  let notice = await Notice.findById(req.params.noticeId);
  if (notice == null) throw 'NotFound';
  req.selectedNotice = notice;
  next();
}), noticeRouter);

/**
 * @api {get} /notices/ 공지사항 목록 가져오기
 * @apiGroup Notice
 * @apiName GetNotices
 * @apiDescription 공지사항 목록을 가져옵니다.
 * @apiParam (Query) {String} [category] 카테고리 필터입니다.
 * @apiParam (Query) {Number} [limit=20] 불러올 개수입니다.
 * @apiParam (Query) {Number} [page=0] 불러올 페이지 번호입니다.
 * @apiSuccess {Notice[]} _ 공지사항 목록
 * @apiUse ReturnNotice
 */
router.get('/notices', async(async (req, res) => {
  let { limit = 20, page = 0 } = req.query;
  limit = parseInt(limit) || 0;
  page = parseInt(page) || 0;
  if (limit > 50) limit = 50;
  let where = {};
  if (req.query.title != null) {
    where.title = { $like: `%${req.query.title}%` };
  }
  if (req.query.category != null) {
    where.category = req.query.category;
  }
  let result = await Notice.findAll({
    where,
    attributes: { exclude: ['body'] },
    order: [['createdAt', 'DESC']],
    offset: limit * page,
    limit,
  });
  if (req.query.count != null) {
    let count = await Notice.count({ where });
    res.json({ count, result });
  } else {
    res.json(result);
  }
}));

/**
 * @api {post} /notices/ 공지사항 올리기
 * @apiGroup Notice
 * @apiName PostNotices
 * @apiDescription 공지사항을 올립니다.
 *
 *   사진 업로드를 하기 위해서는 `multipart/form-data`를 사용해 주세요.
 *
 * @apiParam (Body) {String} title 제목
 * @apiParam (Body) {String} category 카테고리
 * @apiParam (Body) {String} body 내용
 * @apiParam (Body) {File[]} photos 사진
 * @apiSuccess {Notice} _ 공지사항
 * @apiUse ReturnNotice
 * @apiPermission AdminRequired
 * @apiUse AdminRequired
 */
router.post('/notices', adminRequired, photoUpload, async(async (req, res) => {
  // Run validation
  let validationError = validateSingle(req.body, validationSchema.Notice);
  if (validationError != null) {
    throw { name: 'ValidationError', data: validationError };
  }
  let sanitizedData = sanitize(req.body, validationSchema.Notice);
  if (req.photos != null && req.photos.length > 0) {
    sanitizedData.photos = req.photos || [];
    if (sanitizedData.photos.length > 8) throw 'PhotoThreshold';
  }
  // Create
  let notice = await Notice.create(sanitizedData);
  res.json(notice);
}));
