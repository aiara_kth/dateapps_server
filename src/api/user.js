import { Router } from 'express';

import async from './util/async';
import bcrypt from './util/bcrypt';
import loginRequired, { approveRequired, loungeRequired } from './util/loginRequired';
import adminRequired from './util/adminRequired';
import pick from './util/pick';
import photoUpload from './util/photoUpload';
import sendNotification from './util/notification';
import { createMeetingQuery } from './util/createQuery';
import { createChat, lockChat, removeChatParticipant } from './util/chat';
import injectUserInfo, { injectOtherUserInfo } from './util/injectUserInfo';

import { validateSingle, sanitize } from '../validation/validate';
import * as validationSchema from '../validation/schema';

import { sequelize, Passport, User, UserRating, UserReport, UserChat, ProfileHistory,
  Group, UserView, PaymentLog, UserFollowing, Question, UserSuggestion, UserLog } from '../db';
import { doPayment, doPaymentFree, checkPayment, getPaymentConfig }
  from './util/payment';
import firebase from '../firebase';
import Sequelize from 'sequelize';

import { lounge as loungeConfig, auth as authConfig, suggestion as suggestionConfig } from '../../config';

function checkModifiable(req, res, next) {
  if (req.user == null || req.selectedUser.id !== req.user.id ||
      !req.user.isEnabled
      ) {
      adminRequired(req, res, next);
  } else {
    next();
  }
}

let timeOutList = [];

async function getRatingData(user) {
  let where = {
    voteeId: user.id,
  };
  if (user.newbieStartedAt != null) {
    where.updatedAt = {
      $gt: user.newbieStartedAt,
    };
  }
  let sums = await UserRating.findAll({
    attributes: ['score', [sequelize.fn('count', sequelize.col('voterId')),
    'countVal']],
    where,
    group: ['score'],
  });
  let scores = [];
  let votes = 0;
  let sum = 0;
  for (let i = 0; i <= 10; ++i) {
    scores[i] = 0;
  }
  sums.forEach(v => {
    sum += v.score * v.get('countVal');
    votes += v.get('countVal');
    scores[v.score] = v.get('countVal');
  });
  let average = sum / votes;
  if (isNaN(average)) average = 5;
  return { average, votes, scores };
}

const firebaseDB = firebase.database();

export const userRouter = new Router();

async function getUser(req, res) {
  let hasPaid = true;
  if (!req.user.isAdmin && req.user.id !== req.selectedUser.id) {
    let oldProfile = await req.selectedUser.getProfileHistories({
      limit: 1,
    });
    if(0<oldProfile.length){
      oldProfile = oldProfile[0];
      req.selectedUser.applyOldProfile(oldProfile);
    }
    // Check if we've previously bought profile revealer.
    //hasPaid = await checkPayment(req.user, 'profileView', req.selectedUser.id);
    hasPaid = await checkPayment(req.user, 'profileView', req.selectedUser.id) 
    let followingData = await UserFollowing.findOne({ where: {
      followingId: req.selectedUser.id,
      followerId: req.user.id,
    } });
    let followerData = await UserFollowing.findOne({ where: {
      followingId: req.user.id,
      followerId: req.selectedUser.id,
    } });  
    hasPaid = (hasPaid && followerData != null) || followerData == null || followingData != null || req.user.gender === req.selectedUser.gender;
    if (!hasPaid) {
      // Or, if the chat between two users exist, just enable it for
      // indeterminate time.
      let chatData = await UserChat.findOne({ where: {
        $or: [
        { senderId: req.user.id, receiverId: req.selectedUser.id },
        { receiverId: req.user.id, senderId: req.selectedUser.id },
        ],
      } });
      if (chatData != null) hasPaid = true;
    }
  }
  let data;
  if (hasPaid) {
    if (req.user != null && req.user.isAdmin) {
      data = req.selectedUser.get({ plain: true });
      data.diamonds = Math.max(0, req.selectedUser.diamonds +
        (getPaymentConfig('bias') || 0));
    } else {
      data = req.selectedUser.toJSON();
    }
    if (req.user != null && req.user.id === req.selectedUser.id) {
      data.name = req.selectedUser.name;
      data.diamonds = Math.max(0, req.selectedUser.diamonds +
        (getPaymentConfig('bias') || 0));
    }
    data.photos = req.selectedUser.getPhotos(req.user);
    data.photoThumbs = req.selectedUser.getPhotoThumbs(req.user);
  } else {
    // :P
    data = {
      id: req.selectedUser.id,
      nickname: req.selectedUser.nickname,
      photo: req.selectedUser.photo,
      photos: req.selectedUser.getPhotos(req.user),
      photoThumb: req.selectedUser.photoThumb,
      photoThumbs: req.selectedUser.getPhotoThumbs(req.user),
    };
  }
  if (req.user != null) {
    if (req.selectedUser.id === req.user.id) {
      data = await injectUserInfo(req.selectedUser, data);
    } else {
      data = await injectOtherUserInfo(req.selectedUser, req.user, data);
    }
    if(req.user.isAdmin){
      let oldProfile = await req.selectedUser.getProfileHistories({
        limit: 1,
      });
      if(0<oldProfile.length){
        oldProfile = oldProfile[0];
      }
      data.oldProfile = oldProfile;
    }
  }
  return data;
}


function getFullIntroduction(type,d1,d2,d3){
    switch (type){
        case 1:
        case "1":
            return "나는 " + d1 + "사랑하고 나는 " + d2 + " 싫어해요. \n" + d3 + '사람들은 나를 좋아할거에요.'; break;
        case 2:
        case '2':
            return "나는 " + d1 + "성격이고 나는 " + d2 + "매력포인트에요.\n이런 나는 " + d3 + '사람에게 끌려요.'; break;
        case 3:
        case "3 ":
            return "나는 " + d1 + "사랑을 해보고 싶고 애인에게는 " + d2 + "해줄꺼에요.\n저게" + d3 + '사람이 되주세요'; break;
        default:
            return "!ERROR: 잘못된 주관식 타입"; break;
    }
}

userRouter.get('/', loginRequired, async(async (req, res) => {
  let data = await getUser(req, res);
  res.json(data);
}));

/**
 * @api {post} /users/:id/pay 사용자 프로필 보기 위해 결제하기
 * @apiGroup User
 * @apiName PayUserProfile
 * @apiDescription ID에 속하는 사용자의 프로필을 보기 위해 결제합니다. 결제 이후에는
 * 사용자 정보를 볼 수 있습니다.
 * @apiError (Error 409) Conflict 이미 결제되었습니다.
 * @apiError (Error 403) NoDiamonds 잔여 다이아가 부족합니다.
 * @apiSuccess {Number} _ 잔여 다이아 개수
 * @apiSuccess {User[]} _ 사용자 목록
 * @apiUse ReturnUser
 * @apiUse GetUser
 */
 userRouter.post('/pay', loginRequired, async(async (req, res) => {
  if (req.user.profileCountToday < getPaymentConfig('maxProfilesPerDay')) {
    await doPaymentFree(req.user, 'profileView', req.selectedUser.id);
    await req.user.increment('profileCountToday');
  } else {
    await doPayment(req.user, 'profileView', req.selectedUser.id);
  }
  let data = await getUser(req, res);
  data.diamonds = req.user.diamonds;
  res.json(data);
}));

/**
 * @api {get} /user/payments 결제 정보 가져오기
 * @apiGroup User
 * @apiName GetMyUserPayments
 * @apiDescription 자신의 결제했던 내역을 가져옵니다.
 * @apiParam (Query) {Number} [limit=20] 불러올 개수
 * @apiParam (Query) {Number} [page=0] 불러올 페이지 번호.
 * @apiSuccess {PaymentLog[]} _ 결제 기록 테이블
 */
 userRouter.get('/payments', checkModifiable, async(async (req, res) => {
  let { limit = 20, page = 0 } = req.query;
  limit = parseInt(limit) || 0;
  page = parseInt(page) || 0;
  if (limit > 150) limit = 150;
  let paymentLogs = await req.selectedUser.getPaymentLogs({
    order: [['createdAt', 'DESC']],
    offset: limit * page,
    limit,
  });
  if (req.query.count != null) {
    let count = await req.selectedUser.countPaymentLogs();
    res.json({ count, result: paymentLogs });
  } else {
    res.json(paymentLogs);
  }
}));

/**
 * @apiDefine GetUser
 * @apiParam (Parameter) {Number} id 사용자 번호
 * @apiError (Error 404) UserNotFound 입력하신 사용자를 찾을 수 없습니다.
 */

/**
 * @apiDefine ReturnChat
 * @apiSuccess {User} _.sender 채팅을 먼저 건 쪽입니다.
 * @apiSuccess {Number} _.sender.id 고유 번호
 * @apiSuccess {String[]} _.sender.photo 대표 사진
 * @apiSuccess {String[]} _.sender.photoThumb 대표 사진 썸네일
 * @apiSuccess {NumberNullable} _.sender.photoPrimary 메인 사진 번호
 * @apiSuccess {StringNullable} _.sender.nickname 닉네임
 * @apiSuccess {User} _.receiver 채팅을 나중에 건 쪽입니다.
 * @apiSuccess {Number} _.receiver.id 고유 번호
 * @apiSuccess {String[]} _.receiver.photo 대표 사진
 * @apiSuccess {String[]} _.receiver.photoThumb 대표 사진 썸네일
 * @apiSuccess {NumberNullable} _.receiver.photoPrimary 메인 사진 번호
 * @apiSuccess {StringNullable} _.receiver.nickname 닉네임
 * @apiSuccess {StringNullable} _.chatKey 채팅 키입니다. null인 경우 채팅 수신자가 아직 수락을 하지 않았음을 의미합니다.
 */

/**
 * @apiDefine AdminRequired 관리자 필요
 *   사용하려면 관리자 권한이 필요 합니다.
 * @apiError (Error 403) AdminRequired 수정할 수 있는 권한이 없습니다.
 */

 export async function updateInfo(req, user) {
  const profile = pick(req.body, ['photoPrimary', 'name', 'nickname',
    'location', 'birth', 'job', 'school', 'alcohol', 'cigarette', 'personality',
    'bloodtype', 'bodytype', 'height', 'religion', 'introduction',
    'isLoungeUsed', 'usePhotoInGroup', 'updatePush', 'isDailySet']);
  const flags = pick(req.body, ['isEnabled', 'isApproved','isAllAccessible',
    'isLoungeApproved', 'isApproveDenied', 'isAdmin', 'hasProfile','isSuggestion','ignoreLounge']);
  // Run validation
  let validationError = validateSingle(profile, validationSchema.UserProfile);
  if (validationError != null) {
    throw { name: 'ValidationError', data: validationError };
  }
  let sanitizedProfile = sanitize(profile, validationSchema.UserProfile);
  // Check birth
  let allowedMaxBirth = new Date();
  allowedMaxBirth.setFullYear(new Date().getFullYear() - 18);
  allowedMaxBirth.setMonth(0);
  allowedMaxBirth.setDate(1);
  if (sanitizedProfile.birth != null &&
    new Date(sanitizedProfile.birth).getTime() >
    allowedMaxBirth.getTime()
    ) {
    throw 'ValidationError';
}
  // If every fields are available, tick this field.
  if (['name', 'nickname',
    'location', 'birth','job','school','alcohol','cigarette','personality',
    'bloodtype','bodytype','height','religion'].every(v =>
      user[v] != null || sanitizedProfile[v] != null
      ) && (user.photos != null && 0 < user.photos.length)) {
    sanitizedProfile.hasProfile = true;
  if(user.id === req.user.id){
    sanitizedProfile.isApproveDenied = false;
    sanitizedProfile.isApproved = false;
    sanitizedProfile.isLoungeApproved = false;
    sanitizedProfile.isSuggestion = false;
    if(user.isLoungeApproved){
      await ProfileHistory.create({
        photos: user.photos,
        photoPrimary: user.photoPrimary,
        name: user.name,
        nickname: user.nickname,
        location: user.location,
        birth: user.birth,
        job: user.job,
        school: user.school,
        alcohol: user.alcohol,
        cigarette: user.cigarette,
        personality: user.personality,
        bloodtype: user.bloodtype,
        bodytype: user.bodytype,
        height: user.height,
        religion: user.religion,
        introduction: user.introduction,
        userId: user.id,
      });
    }
  }
}
if(user.id === req.user.id) sanitizedProfile.profileUpdatedAt = new Date();
  // Check if the photo index is valid
  if (profile.photoPrimary != null) {
    let photoId = parseInt(profile.photoPrimary);
    if (user.photos[photoId] == null) throw 'PhotoIndexInvalid';
    let newPhotos = user.photos.slice();
    newPhotos[photoId] = user.photos[0];
    newPhotos[0] = user.photos[photoId];
    sanitizedProfile.photoPrimary = 0;
    sanitizedProfile.photos = newPhotos;
  }
  // If admin, put flags too
  if (req.user && req.user.isAdmin) {
    for (let key in flags) {
      let value = flags[key];
      flags[key] = value === 'true' || value === 'yes' || value === '1' ||
      value === true || value === 1;
    }
    if (flags.isEnabled === false) {
      flags.deletedAt = new Date();
      await sendNotification(user, { type: 'userDisabled', hidden: true });
    }
    if (req.user.id === user.id && user.isAdmin && !flags.isAdmin) {
      flags.isAdmin = true;
    }
    if (flags.isLoungeApproved != null) {
      flags.newbiePending = false;
    }
    if (!user.isApproveDenied && flags.isApproveDenied === true) {
      flags.hasProfile = false;
      flags.hasApproveDeniedBefore = true;
      if (!flags.isApproved) {
        await sendNotification(user, { type: 'joinDeny' });
        for(var i=0;i<timeOutList.length;i++){
          if(timeOutList[i].id===user.id){
            clearTimeout(timeOutList[i].timeOut);
          }
        }
      }
    }else if (!user.isLoungeApproved && flags.isLoungeApproved) {
      await sendNotification(user, { type: 'loungeAccept', message: user.approvedAt!=null?'수정하신 프로필이 변경되었습니다.':null });
      sanitizedProfile.approvedAt = new Date();
      if(user.gender == 0){
        var timeOut = setTimeout(async () => {
          return;
          // let profile = await User.findById(user.id);
          // if(profile==null||!profile.isLoungeApproved){
          //   return;
          // }
          // if(await user.countGroups({
          //   through: { where: { type: { $in: ['admin'] } } },
          //   where: {
          //     deletedAt: { $or: [null, { $gte: new Date() }] },
          //   },
          // }) > 0){
          //   return;
          // }
          // let suggestions = await UserSuggestion.findAll({
          //   where: {
          //     targetId: user.id,
          //   },
          //   include: [{
          //     model: User,
          //     as: 'user',
          //   },{
          //     model: User,
          //     as: 'target'
          //   }],
          // });
          // let ids = suggestions.map(suggestion => suggestion.user.id);
          // let where = {
          //   isSuggestion: true,
          //   isLoungeUsed: true,
          //   gender: 1,
          // };
          // let groups = await user.getGroups({
          //   where: { deletedAt: null },
          //   through: { $not: 'banned' },
          // });
          // for(let group of groups){
          //   let groupUsers = await group.getMembers({
          //     where: [sequelize.where(sequelize.literal('groupUser.type'), {
          //       $notIn: ['invited', 'banned'],
          //     }), {
          //       isEnabled: true,
          //     }],
          //   });
          //   let groupUserIds = groupUsers.map(groupUser => groupUser.id);
          //   ids = ids.concat(groupUserIds);
          // }
          // let followingData = await UserFollowing.findAll({ where: {
          //   followerId: user.id,
          // } });
          // if(followingData!=null&&0<followingData.length){
          //   let followingIds = followingData.map(data => data.followingId);
          //   ids = ids.concat(followingIds);
          // }
          // let followerData = await UserFollowing.findAll({ where: {
          //   followingId: user.id,
          // } });  
          // if(followerData!=null&&0<followerData.length){
          //   let followerIds = followerData.map(data => data.followerId);
          //   ids = ids.concat(followerIds);
          // }
          // if(ids!=null&&0<ids.length){
          //   where.id = { $notIn: ids };
          // }
          // let suggestUsers = await User.findAll({
          //   where,
          //   order: [
          //   Sequelize.fn('RAND'),
          //   ],
          //   limit: 1,
          // });
          // let count = 0;
          // for(let suggest of suggestUsers){
          //   count += 1;
          //   let data = await UserSuggestion.create({
          //     userId: suggest.id,
          //     targetId: user.id,
          //   });
          // }
          // if(count!=0){
          //   let messages = [suggestUsers[0].nickname+'님이 당신의 프로필을 3회 이상 조회하셨네요. 대화 신청 수락 가능성이 매우 높습니다.','내게 관심 있는 카드는 24시간(00분)이내 삭제됩니다. 마음에 드시는 분이 있다면 지금 바로 관심 표현해보세요.'];
          //   let index = Math.round(Math.random());
          //   if(user.approvedAt!=null&&user.approvedAt.getTime()<2 * 24 * 60 * 60 * 1000){
          //     index = 0;
          //   }
          //   let message = message[index];
          //   await sendNotification(user, {
          //     type: 'userSuggest',
          //     title: '에스티커 알림',
          //     user: suggestUsers[0],
          //     message
          //   });
          // }
        },(5+Math.floor((Math.random()*3)+1))*60*1000);
        timeOutList.push({id:user.id,timeOut:timeOut});
      }
      // If user's balance is 0 and don't have payment log, give promotion
      // balance
      let freeBalance = user.gender == 0 ? getPaymentConfig('manPromotion') : getPaymentConfig('joinPromotion');
      if (freeBalance > 0) {
        let hasPaidBefore = await PaymentLog.count({
          where: { userId: user.id },
        }) !== 0;
        if (user.diamonds === 0 && !hasPaidBefore) {
          console.log('Adding funds: ' + user.email + ' ' + freeBalance);
          // Give free balance
          await sequelize.transaction(async transaction => {
            await user.reload({ transaction });
            // Create payment log
            await PaymentLog.create({
              type: 'chargePromotion',
              cost: -freeBalance,
              diamondsAfter: user.diamonds + freeBalance,
              userId: user.id,
            }, { transaction });
            await user.increment('diamonds', { by: freeBalance, transaction });
          });
        }
      }
    } else if (!user.isApproved && flags.isApproved) {
      await sendNotification(user, { type: 'joinAccept' });

    } else if (user.isApproved && flags.isApproved === false) {
      await sendNotification(user, { type: 'joinCancel' });
      for(var i=0;i<timeOutList.length;i++){
        if(timeOutList[i].id===user.id){
          clearTimeout(timeOutList[i].timeOut);
        }
      }
    } else if (user.isLoungeApproved && flags.isLoungeApproved === false) {
      await sendNotification(user, { type: 'loungeCancel' });
      for(var i=0;i<timeOutList.length;i++){
        if(timeOutList[i].id===user.id){
          clearTimeout(timeOutList[i].timeOut);
        }
      }
    }
    if (user.isAdmin) {
      flags.isEnabled = true;
    }
    Object.assign(sanitizedProfile, flags);
    const additional = pick(req.body, ['phone', 'gender',
      'deviceType']);
    let additionalProfile = sanitize(additional, validationSchema.User);
    Object.assign(sanitizedProfile, additionalProfile, flags);
    if (req.body.diamonds != null) {
      sanitizedProfile.diamonds = parseInt(req.body.diamonds) -
      (getPaymentConfig('bias') || 0);
    }
    if(req.body.rating!=null){
      sanitizedProfile.rating = req.body.rating;
    }
    // If email is changed, we have to change the passport too
    if (req.body.email != null) {
      await sequelize.transaction(async t => {
        await Passport.update({ identifier: req.body.email }, {
          where: {
            userId: user.id,
            type: 'local',
          },
          transaction: t,
        });
        await user.update({ email: req.body.email }, { transaction: t });
      });
    }
    // If password is changed, change passport.
    if (req.body.password != null) {
      let salt = await bcrypt.genSalt(authConfig.bcrypt);
      let hash = await bcrypt.hash(req.body.password, salt);
      await Passport.update({ data: hash }, {
        where: {
          userId: user.id,
          type: 'local',
        },
      });
    }
  }
  // Perform update
  await user.update(sanitizedProfile);
  if (sanitizedProfile.isEnabled != null) {
    let groups = await user.getGroups();
    let meetings = await user.getHostedMeetings();
    for (let group of groups) {
      await group.updateMembers();
      if (group.membersCount === 0 && group.deletedAt != null) {
        await group.update({ deletedAt: new Date() });
      }
    }
    for (let meeting of meetings) {
      await meeting.destroy();
    }
  }
  let data;
  if (req.user != null && req.user.isAdmin) {
    data = user.get({ plain: true });
  } else {
    data = user.toJSON();
  }
  data.name = user.name;
  data.photos = user.getPhotos(req.user);
  data.photoThumbs = user.getPhotoThumbs(req.user);
  firebaseDB.ref('/updatePushes').child(user.id).set(user.updatePush);
  return data;
}



/**
 * @api {patch} /user/ 프로필 정보 수정하기
 * @apiGroup User
 * @apiName SetUserProfile
 * @apiDescription 사용자의 프로필을 수정합니다.
 * @apiSuccess {User} _ 사용자 정보
 * @apiSuccess {String[]} _.photos 사진 목록
 * @apiSuccess {String[]} _.photoThumbs 사진 썸네일 목록
 * @apiUse ReturnUser
 * @apiParam (Body) {Number} [photoPrimary] 대표 사진 인덱스
 * @apiParam (Body) {String} [name] 이름
 * @apiParam (Body) {String} [nickname] 닉네임
 * @apiParam (Body) {Number} [location] 지역
 * @apiParam (Body) {Date} [birth] 생년월일
 * @apiParam (Body) {String} [job] 직업
 * @apiParam (Body) {String} [school] 학교
 * @apiParam (Body) {Number} [alcohol] 음주
 * @apiParam (Body) {Number} [cigarette] 담배
 * @apiParam (Body) {Number} [personality] 성격
 * @apiParam (Body) {Number} [bloodtype] 혈액형
 * @apiParam (Body) {Number} [bodytype] 체형
 * @apiParam (Body) {Number} [height] 키
 * @apiParam (Body) {Number} [religion] 종교
 * @apiParam (Body) {String} [introduction] 자기소개
 * @apiParam (Body) {Boolean} [isLoungeUsed] 라운지 사용 여부
 * @apiParam (Body) {Boolean} [usePhotoInGroup] 그룹에서 사진 사용 여부
 * @apiParam (Body) {Boolean} [updatePush] 푸시 알림 수신 여부
 * @apiParam (Body) {Boolean} [isEnabled] 계정 활성화 여부 (관리자만)
 * @apiParam (Body) {Boolean} [isApproved] 계정 승인 여부 (관리자만)
 * @apiParam (Body) {Boolean} [isApproveDenied] 계정 승인 거절 여부 (관리자만)
 * @apiParam (Body) {Boolean} [isLoungeApproved] 라운지 승인 여부 (관리자만)
 * @apiParam (Body) {Boolean} [isAdmin] 관리자 여부 (관리자만)
 * @apiError (Error 400) PhotoIndexInvalid 대표 사진 인덱스가 잘못되었습니다.
 * @apiError (Error 409) NicknameConflict 닉네임이 충돌합니다.
 * @apiPermission LoginRequired
 * @apiUse LoginRequired
 */
 userRouter.patch('/', checkModifiable, async(async (req, res) => {
  let user = await updateInfo(req, req.selectedUser);
  res.json(user);
}));


/**
 * @api {delete} /user/ 회원 탈퇴하기
 * @apiGroup User
 * @apiName DeleteUser
 * @apiDescription 회원 탈퇴를 합니다.
 */
 userRouter.delete('/', checkModifiable, async(async (req, res) => {
  if (req.selectedUser.isAdmin) throw 'Forbidden';
  if (req.query.force != null) {
    if (!req.user.isAdmin) throw 'AdminRequired';
    if (!(await bcrypt.compare(req.body.code, authConfig.deleteKey))) {
      throw 'PasswordInvalid';
    }
    let user = req.selectedUser;
    let groups = await user.getGroups();
    let meetings = await user.getHostedMeetings();
    await user.destroy();
    for (let group of groups) {
      await group.updateMembers();
      if (group.membersCount === 0 && group.deletedAt != null) {
        await group.update({ deletedAt: new Date() });
      }
    }
    for (let meeting of meetings) {
      await meeting.destroy();
    }
    res.json({});
    return;
  }
  let user = req.selectedUser;
  let groups = await user.getGroups();
  let meetings = await user.getHostedMeetings();
  if(!req.user.isAdmin||req.user.id===user.id){
    await UserLog.create({
      email:user.email,
      gender: user.gender,
      phone: user.phone,
      photos: user.photos,
      photoThumbs: user.photoThumbs,
      photoPrimary: user.photoPrimary,
      name: user.name,
      nickname: user.nickname,
    });
  }
  await user.destroy();
  for (let group of groups) {
    await group.updateMembers();
    if (group.membersCount === 0 && group.deletedAt != null) {
      await group.update({ deletedAt: new Date() });
    }
  }
  for (let meeting of meetings) {
    await meeting.destroy();
  }
  res.json({});
}));

 userRouter.get('/fcm', adminRequired, (req, res) => {
  res.json({ fcmKey: req.selectedUser.fcmKey });
});

 userRouter.post('/notifications', adminRequired, async(async (req, res) => {
  const { title, message } = req.body;
  const data = { title, message, type: 'notice' };
  // Tada
  await sendNotification(req.selectedUser, data);
  res.json({});
}));

/*
 * @api_ {get} /user/photos 자신의 사진 정보 가져오기
 * @apiGroup User
 * @apiName GetMyUserPhotos
 * @apiDescription 자신의 사진 정보를 가져옵니다.
 * @apiSuccess {String[]} photos 사진 배열
 * @apiSuccess {String[]} photoThumbs 사진 썸네일 배열
 * @apiPermission LoginRequired
 * @apiUse LoginRequired
 */
/*
 * @api_ {get} /users/:id/photos 사진 정보 가져오기
 * @apiGroup User
 * @apiName GetUserPhotos
 * @apiDescription 사용자의 사진 정보를 가져옵니다.
 * @apiSuccess {String[]} photos 사진 배열
 * @apiSuccess {String[]} photoThumbs 사진 썸네일 배열
 * @apiUse GetUser
 *
userRouter.get('/photos', (req, res) => {
  const { photos, photoThumbs } = req.selectedUser;
  res.json({ photos, photoThumbs });
});
*/

/**
 * @api {post} /user/photos 사진 업로드하기
 * @apiGroup User
 * @apiName PostMyUserPhotos
 * @apiDescription 사진을 추가로 업로드합니다.
 * @apiParam (Body) {File[]} photos 업로드 할 파일 배열
 * @apiParam (Body) {Number} photoPrimary 주 사진 번호 (0~6)
 * @apiSuccess {String[]} photos 사진 배열
 * @apiSuccess {String[]} photoThumbs 사진 썸네일 배열
 * @apiSuccess {Number} photoPrimary 사진 번호
 * @apiError (Error 400) PhotoInvalid 올바른 사진 파일이 아닙니다.
 * @apiError (Error 403) PhotoThreshold 사진의 최대 개수를 초과했습니다.
 * @apiPermission LoginRequired
 * @apiUse LoginRequired
 */
 userRouter.post('/photos', checkModifiable, photoUpload,
  async(async (req, res) => {
    let user = req.selectedUser;
    let photos = user.photos.concat(req.photos || []);
    if (photos.length > 5) throw 'PhotoThreshold';
    let photoPrimary = user.photoPrimary;
    if (photos.length > 0 && photoPrimary == null) photoPrimary = 0;
    let primaryInput = parseInt(req.body.photoPrimary);
    if (primaryInput >= 0 && primaryInput < photos.length) {
      photoPrimary = primaryInput;
    }
    let newProfile = { photos, photoPrimary, isApproved: false, isLoungeApproved: false, isSuggestion: false };
    if (['name', 'nickname',
      'location', 'birth','job','school','alcohol','cigarette','personality',
      'bloodtype','bodytype','height','religion'].every(v =>
        user[v] != null) && (user.photos != null && 0 < user.photos.length)) {
      newProfile.hasProfile = true;
  }
  if(req.user.id === user.id) newProfile.photoUpdatedAt = new Date();
  if(user.isLoungeApproved&&newProfile.isLoungeApproved!=null&&!newProfile.isLoungeApproved){
    await ProfileHistory.create({
      photos: user.photos,
      photoPrimary: user.photoPrimary,
      name: user.name,
      nickname: user.nickname,
      location: user.location,
      birth: user.birth,
      job: user.job,
      school: user.school,
      alcohol: user.alcohol,
      cigarette: user.cigarette,
      personality: user.personality,
      bloodtype: user.bloodtype,
      bodytype: user.bodytype,
      height: user.height,
      religion: user.religion,
      introduction: user.introduction,
      userId: user.id,
    });
  }
  await user.update(newProfile);
  const { photoThumbs } = user;
  res.json({ photos, photoThumbs, photoPrimary });
}));

/**
 * @api {delete} /user/photos 사진 전부 삭제하기
 * @apiGroup User
 * @apiName DeleteMyUserPhotos
 * @apiDescription 사진을 전부 삭제합니다.
 * @apiSuccess {String[]} photos 사진 배열
 * @apiSuccess {String[]} photoThumbs 사진 썸네일 배열
 * @apiSuccess {Number} photoPrimary 대표 사진 인덱스
 * @apiPermission LoginRequired
 * @apiUse LoginRequired
 */
 userRouter.delete('/photos', checkModifiable, async(async (req, res) => {
  let user = req.selectedUser;
  await user.update({ photos: [], photoPrimary: null });
  const { photos, photoThumbs, photoPrimary } = user;
  res.json({ photos, photoThumbs, photoPrimary });
}));

/**
 * @api {put} /user/photos/:photoId 사진 덮어 쓰기
 * @apiGroup User
 * @apiName PutMyUserPhoto
 * @apiDescription 사진을 해당 위치에 덮어 씁니다.
 * @apiParam (Body) {File} photos 업로드 할 파일
 * @apiParam (Body) {Number} photoPrimary 주 사진 번호 (0~6)
 * @apiSuccess {String[]} photos 사진 배열
 * @apiSuccess {String[]} photoThumbs 사진 썸네일 배열
 * @apiSuccess {Number} photoPrimary 사진 번호
 * @apiError (Error 400) PhotoInvalid 올바른 사진 파일이 아닙니다.
 * @apiError (Error 403) PhotoThreshold 사진의 최대 개수를 초과했습니다.
 * @apiPermission LoginRequired
 * @apiUse LoginRequired
 */
 userRouter.put('/photos/:photoId', checkModifiable, photoUpload,
  async(async (req, res) => {
    let index = parseInt(req.params.photoId);
    if (isNaN(index)) throw 'ValidationError';
    let user = req.selectedUser;
    let photos = user.photos.slice();
    if (photos[index] == null) throw 'ValidationError';
    let photo = (req.photos || [])[0];
    if (photo == null) throw 'ValidationError';
    photos[index] = photo;
    let photoPrimary = user.photoPrimary;
    let primaryInput = parseInt(req.body.photoPrimary);
    if (primaryInput >= 0 && primaryInput < photos.length) {
      photoPrimary = primaryInput;
    }
    let newProfile = { photos, photoThumbs, isApproved: false, isLoungeApproved: false, isSuggestion: false };
    if (['name', 'nickname',
      'location', 'birth','job','school','alcohol','cigarette','personality',
      'bloodtype','bodytype','height','religion'].every(v =>
        user[v] != null) && (user.photos != null && 0 < user.photos.length)) {
      newProfile.hasProfile = true;
  }
  if(req.user.id === user.id) newProfile.photoUpdatedAt = new Date();
  if(user.isLoungeApproved&&newProfile.isLoungeApproved!=null&&!newProfile.isLoungeApproved){
    await ProfileHistory.create({
      photos: user.photos,
      photoPrimary: user.photoPrimary,
      name: user.name,
      nickname: user.nickname,
      location: user.location,
      birth: user.birth,
      job: user.job,
      school: user.school,
      alcohol: user.alcohol,
      cigarette: user.cigarette,
      personality: user.personality,
      bloodtype: user.bloodtype,
      bodytype: user.bodytype,
      height: user.height,
      religion: user.religion,
      introduction: user.introduction,
      userId: user.id,
    });
  }
  await user.update(newProfile);
  const { photoThumbs } = user;
  res.json({ photos, photoThumbs, photoPrimary });
}));

/**
 * @api {delete} /user/photos/:photoId 특정 사진 삭제하기
 * @apiGroup User
 * @apiName DeleteMyUserPhoto
 * @apiDescription n번째 사진을 삭제합니다.
 * @apiParam (Parameter) {Number} photoId 삭제할 사진 번호
 * @apiSuccess {String[]} photos 사진 배열
 * @apiSuccess {String[]} photoThumbs 사진 썸네일 배열
 * @apiSuccess {Number} photoPrimary 대표 사진 인덱스
 * @apiPermission LoginRequired
 * @apiUse LoginRequired
 */
 userRouter.delete('/photos/:photoId', checkModifiable,
  async(async (req, res) => {
    let index = parseInt(req.params.photoId);
    if (isNaN(index)) throw 'ValidationError';
    let user = req.selectedUser;
    let photoPrimary = user.photoPrimary;
    if (photoPrimary && index <= photoPrimary) photoPrimary -= 1;
    if (photoPrimary < 0) photoPrimary = null;
    await user.update({
      photos: user.photos.filter((_, i) => i !== index), photoPrimary,
    });
    if(req.user.id!=user.id){
      await sendNotification(user,
        { type: 'photoDeleted', hidden: true });
    }
    const { photos, photoThumbs } = user;
    res.json({ photos, photoThumbs, photoPrimary });
  }));
 /**
 * @api {delete} /users/:id/profileHistories 라운지 이력 완전 삭제하기
 * @apiGroup User
 * @apiName DeleteLoungeHistories
 * @apiPermission LoginRequired
 * @apiPermission AdminRequired
 * @apiUse LoginRequired
 */
 userRouter.delete('/profileHistories', adminRequired,
  async(async (req, res) => {
    await ProfileHistory.destroy({
      where:{
        userId: req.selectedUser.id,
      },
    });
    await req.selectedUser.update({
      approvedAt: null,
    });
    await sendNotification(req.selectedUser,
        { type: 'loungeDeleted', hidden: true });
    res.json({});
  }));
/**
 * @api {post} /users/:id/chatPush 개인 채팅 알림
 * @apiGroup User
 * @apiName ChatPush
 * @apiDescription 해당 유저에게 채팅 알림을 전송합니다
 * @apiUse GetUser
 * @apiParam (Body) {String} message 푸시로 전송할 채팅 메세지 입니다.
 * @apiPermission LoginRequired
 * @apiUse LoginRequired
 * @apiUse ApproveRequired
 */
 userRouter.post('/chatPush',approveRequired, async(async (req,res) => {
  let { message } = req.body;
  await sendNotification(req.selectedUser, {
    type: 'personalChat',
    user: req.user,
    text: message,
  });
  res.json({});
}));
/**
 * @api {get} /user/ratingHistories 심사받았던 별점 목록 가져오기
 * @apiGroup User
 * @apiName GetUserRatingHistories
 * @apiDescription 심사받았던 별점 목록을 가져옵니다.
 * @apiSuccess {RatingHistory[]} _ 심사 기록
 * @apiSuccess {Number[]} _.scores 각 별점 개수 별로 카운트된 투표 개수입니다.
 * @apiSuccess {Number} _.votes 투표 개수
 * @apiSuccess {Number} _.average 평균
 * @apiSuccess {String} _.photo 사진
 * @apiSuccess {String} _.photoThumb 사진 썸네일
 * @apiSuccess {Date} _.createdAt 생성 시각 (null이면 현재를 의미함)
 * @apiPermission ApproveRequired
 * @apiUse ApproveRequired
 * @apiUse GetUser
 */
 userRouter.get('/ratingHistories', approveRequired, checkModifiable,
  async(async (req, res) => {
    let records = await req.selectedUser.getRatingHistories({
      order: [['createdAt', 'DESC']],
    });
    let data = await getRatingData(req.selectedUser);
    records.unshift(Object.assign({
      createdAt: null,
      photo: req.selectedUser.photo,
      photoThumb: req.selectedUser.photoThumb,
    }, data));
    res.json(records);
  }));

/**
 * @api {get} /users/:id/rating 별점 목록 가져오기
 * @apiGroup User
 * @apiName GetUserRatings
 * @apiDescription 별점 목록을 가져옵니다.
 * @apiSuccess {Number[]} _ 각 별점 개수 별로 카운트된 투표 개수입니다.
 * @apiPermission ApproveRequired
 * @apiUse ApproveRequired
 * @apiUse GetUser
 */
 userRouter.get('/rating', approveRequired, checkModifiable,
  async(async (req, res) => {
    let data = await getRatingData(req.selectedUser);
    res.json(data.scores);
  }));

/**
 * @api {post} /users/:id/rating 별점 투표하기
 * @apiGroup User
 * @apiName VoteUserRating
 * @apiDescription 사용자의 별점을 투표합니다.
 * @apiParam (Body) {Number{0-10}} score 투표할 점수입니다.
 * @apiError (Error 400) ValidationError 같은 성별의 사용자는 투표할 수 없습니다.
 * @apiError (Error 400) ValidationError 필드의 형식이 잘못되었습니다.
 * @apiError (Error 409) Conflict 이미 처리되었습니다.
 * @apiError (Error 403) Throttle 잠시 후 다시 시도하세요.
 * @apiSuccess {Number} rating 점수
 * @apiPermission LoungeRequired
 * @apiUse LoungeRequired
 * @apiUse GetUser
 */
 userRouter.post('/rating', loungeRequired, async(async (req, res) => {
  if (req.selectedUser.gender === req.user.gender) throw 'ValidationError';
  if (await req.selectedUser.hasVoter(req.user, {
    where: {
      updatedAt: { $gte: req.user.newbieStartedAt },
    },
  })) {
    throw 'Conflict';
  }
  let score = parseInt(req.body.score);
  if (score < 0 || score > 10 || isNaN(score)) throw 'ValidationError';
  // Count and determine if there are more than 6 1-score votes in a minute
  // in 5 minutes.
  let beforeVotes = await UserRating.findAll({
    where: {
      voterId: req.user.id,
      score: { $lte: 1 },
      updatedAt: { $gte: new Date(Date.now() - 5 * 60 * 1000) },
    },
    order: [['updatedAt', 'ASC']],
  });
  // Check if we should stop
  let lastVoteTime = 0;
  let denyCount = 0;
  for (let vote in beforeVotes) {
    if ((vote.updatedAt.getTime() - lastVoteTime) > 60 * 1000) {
      denyCount = 0;
    }
    denyCount++;
    lastVoteTime = vote.updatedAt.getTime();
    if (denyCount >= 5) {
      throw 'Throttle';
    }
  }
  await UserRating.upsert({
    voterId: req.user.id,
    voteeId: req.selectedUser.id,
    score,
  });
  await req.selectedUser.updateRating();
  await sendNotification(req.selectedUser,
    { type: 'loungePending', rating: req.selectedUser.rating, hidden: true });
  // If the user has rated more than 8 and target user is not approved yet,
  // follow the user automatically
 /* if (score >= 8 && !req.selectedUser.loungeApproved &&
    !await req.selectedUser.hasFollower(req.user)
  ) {
    await req.selectedUser.addFollower(req.user);
    await sendNotification(req.selectedUser,
      { type: 'follow', user: req.user });
    }*/
  // Increment votedCount
  // Check if we've previously bought profile revealer.
  let hasPaid =
  await checkPayment(req.user, 'profileView', req.selectedUser.id, true);
  if (score === 10 && !hasPaid) {
    await sequelize.transaction(async transaction => {
      let currentDiamonds = Math.max(0, req.user.diamonds +
        (getPaymentConfig('bias') || 0));
      if (currentDiamonds >= getPaymentConfig('voteDiamondsCap')) return;
      // Get first expired date, if voteDiamondsExpire is set.
      if (getPaymentConfig('voteDiamondsExpire')) {
        let firstDate = await PaymentLog.findOne({
          where: {
            type: 'chargeVote',
            userId: req.user.id,
            createdAt: {
              $lt: new Date(Date.now() -
                getPaymentConfig('voteDiamondsExpire')),
            },
          },
          transaction,
        });
        if (firstDate != null) return;
      }
      // Check if the payment limit has reached
      if (req.user.chargedCountToday < getPaymentConfig('maxDiamondsPerDay')) {
        // Create payment log
        await PaymentLog.create({
          type: 'chargeVote',
          cost: -getPaymentConfig('diamondsPerVote'),
          diamondsAfter:
          currentDiamonds + getPaymentConfig('diamondsPerVote'),
          userId: req.user.id,
        }, { transaction });
        await req.user.increment('diamonds', {
          by: getPaymentConfig('diamondsPerVote'),
          transaction,
        });
        await req.user.increment('chargedCountToday', { transaction });
      }
    });
  }
  res.json({ rating: req.selectedUser.rating });
}));


/**
 * @api {get} /users/:id/onequestion
 * @apiGroup User
 * @apiName GetUserOneQuestion
 * @apiDescription 해당 사용자의 핵심 질문을 가져옵니다.
 * @apiError (Error 400) NoOneQuestion 핵신질문값이 없습니다.
 * @apiUser GetUser
 */
 userRouter.get('/onequestion', async(async (req, res) => {
        let data = await getUser(req,res);
        // console.log(data);
        if(data.onequestion != null && data.onequestion != '') {
            res.json(data.onequestion);
        }else{
             throw 'NoOneQuestion';
        }
    }));




/**
 * @api {get} /users/:id/introduction
 * @apiGroup User
 * @apiName GetUserIntroduction
 * @apiDescription 해당 사용자의 주관식 가지소개을 가져옵니다.
 * @apiError (Error 400) NoIntroduction 주관식 가지소개의 ID값이 없습니다. 관리자한테 문의해주세요.
 * @apiUser GetUser
 */
userRouter.get('/introduction', async(async (req, res) => {
    let id  = 3;
    let data = await getUser(req,res);
    if(data.introduction != null) {
        let introduction_type = data.introduction;
        let introduction_all  = getFullIntroduction(introduction_type,data.introduct_word1,data.introduct_word2,data.introduct_word3);
        let return_array = Array();
        res.json({type: introduction_type, word1 : data.introduct_word1,word2 : data.introduct_word3, word3 : data.introduct_word3, getall : introduction_all});
    }else {
        throw 'NoIntroduction';
    }
    // console.log(data);
}));


/**
 * @api {get} /users/:id/profileHistories
 * @apiGroup User
 * @apiName GetUserProfileHisoties
 * @apiDescription 프로필 변경 기록을 조회합니다.
 * @apiUser GetUser
 */
 userRouter.get('/profileHistories', async(async(req,res)=>{
  let result = await req.selectedUser.getProfileHistories();
  if (req.query.count != null) {
    let count = await req.selectedUser.countProfileHistories();
    res.json({ count, result });
  } else {
    res.json(result);
  }
}));
 /**
 * @api {get} /users/:id/profileHistories
 * @apiGroup User
 * @apiName GetUserProfileHisoties
 * @apiDescription 프로필 변경 기록을 조회합니다.
 * @apiUser GetUser
 */
 userRouter.get('/profileHistories/:historyId', async(async(req,res)=>{
  let result = await req.selectedUser.getProfileHistories({
    where: {
      id: req.params.historyId,
    }
  });
  res.json(result);
}));
/**
 * @api {delete} /users/:id/rating 재심사 요청하기
 * @apiGroup User
 * @apiName RevoteUserRating
 * @apiDescription 심사를 다시 요청합니다. 이미 승인이 된 이후 요청하면 과금됩니다.
 * @apiError (Error 409) Conflict 이미 심사중에 있습니다.
 * @apiError (Error 403) NoDiamonds 다이아가 부족합니다.
 * @apiPermission ApproveRequired
 * @apiUse ApproveRequired
 * @apiUse GetUser
 */
 userRouter.delete('/rating', approveRequired, checkModifiable,
  async(async (req, res) => {
    if (req.selectedUser.newbiePending) {
      throw 'Conflict';
    }
    if (req.selectedUser.isLoungeApproved) {
      await doPayment(req.user, 'loungeRevote');
    }
    await UserView.destroy({ where: { vieweeId: req.selectedUser.id } });
    await req.selectedUser.update({
      newbieStartedAt: new Date(),
      newbiePending: true,
      rating: 5,
    });
    res.json({});
  }));

/**
 * @api {get} /user/followers 자기 자신의 팔로워 정보 가져오기
 * @apiGroup User
 * @apiName GetMyUserFollowers
 * @apiParam (Query) {Number} [limit=20] 불러올 개수
 * @apiParam (Query) {Number} [page=0] 불러올 페이지 번호.
 * @apiDescription 자기 자신의 팔로워 정보를 가져옵니다.
 * @apiSuccess {User[]} _ 사용자 목록
 * @apiPermission Modifiable
 * @apiUse Modifiable
 * @apiUse ReturnUser
 */
 userRouter.get('/followers', checkModifiable, async(async (req, res) => {
  let { limit = 20, page = 0 } = req.query;
  limit = parseInt(limit) || 0;
  page = parseInt(page) || 0;
  if (limit > 150) limit = 150;
  let suggestions = await UserSuggestion.findAll({
    where: {
      createdAt: { $gte: new Date(Date.now() - suggestionConfig.expire) },
      targetId: req.selectedUser.id ,
    },
    include: [{
      model: User,
      as: 'user',
    },{
      model: User,
      as: 'target',
    }],
    limit: limit,
    offset: limit * page,
  });
  suggestions = suggestions.map(suggestion => {
    let result = suggestion.user;
    result.suggestedAt = suggestion.createdAt;
    return result;
  });
  if(suggestions == null){
    suggestions = [];
  }
  let users = [];
  let userPage = 0;
  if(suggestions.length<limit){
    let diff = limit - suggestions.length;
    let bias = 0;
    if(diff == limit){
      let suggestionCount = await UserSuggestion.count({
        where: {
          createdAt: { $gte: new Date(Date.now() - suggestionConfig.expire) },
          targetId: req.selectedUser.id ,
        },
      });
      userPage = suggestionCount<limit?0:Math.floor(suggestionCount / limit)-1;
      if(suggestionCount%limit != 0){
        userPage += 1;
        bias = limit - (suggestionCount % limit);
      }
    }
    users = await await UserFollowing.findAll({
      where: {
        denied: false,
        followingId: req.selectedUser.id,
      },
      include: [{
        model: User,
        as: 'follower',
      }],
    });
    users = users.map(user=>{
      let result = user.follower;
      result.suggestedAt = user.createdAt;
      return result;
    });
    users = suggestions.concat(users);
  }else{
    users = suggestions;
  }
  let result = [];
  for (let user of users) {
    let following = await user.hasFollower(req.user);
    let follower = await req.user.hasFollower(user);
    let hasPaid = await checkPayment(req.user, 'profileView', user.id);
    if (hasPaid || following || follower){
      let data = user.toJSON();
      data.photos = user.getPhotos(req.user);
      data.photoThumbs = user.getPhotoThumbs(req.user);
      data.suggestedAt = user.suggestedAt;
      data.following = following;
      data.follower = follower;
      result.push(data);
    }else{
      let data = {
        id: user.id,
        nickname: user.nickname,
        photo: user.photo,
        photos: user.getPhotos(req.user),
        photoThumb: user.photoThumb,
        photoThumbs: user.getPhotoThumbs(req.user),
      };
      data.suggestedAt = user.suggestedAt;
      data.following = following;
      data.follower = follower;
      result.push(data);
    }
  }
  if (req.query.count != null) {
    let count = await req.selectedUser.countFollowers();
    res.json({ count, result });
  } else {
    res.json(result);
  }
}));

/**
 * @api {post} /users/:id/followers 팔로우하기
 * @apiGroup User
 * @apiName FollowUser
 * @apiDescription 해당 사용자를 팔로우합니다. 맞 팔로우는 다이아가 들지 않습니다.
 * @apiError (Error 400) ValidationError 자기 자신은 팔로우할 수 없습니다.
 * @apiError (Error 400) ValidationError 이미 채팅을 걸었으면 팔로우할 수 없습니다.
 * @apiError (Error 409) Conflict 이미 처리되었습니다.
 * @apiError (Error 403) NoDiamonds 잔여 다이아가 부족합니다.
 * @apiUse GetUser
 */
 userRouter.post('/followers', loungeRequired, async(async (req, res) => {
  if (req.selectedUser.id === req.user.id) throw 'ValidationError';
  if (await req.selectedUser.hasFollower(req.user)) throw 'Conflict';
  // Check if other user follows me, then check payment
  let paid = false;
  if (!await req.user.hasFollower(req.selectedUser)) {
    try {
      if(req.selectedUser.gender!=req.user.gender) await doPayment(req.user, 'profileFollow', req.selectedUser.id);
      paid = true;
    } catch (e) {
      if (e !== 'Conflict') throw e;
    }
    // Done
    await req.selectedUser.addFollower(req.user);
  } else {
    await UserFollowing.create({
      followingId: req.selectedUser.id,
      followerId: req.user.id,
      reply: true,
    });
  }
  let suggestions = await UserSuggestion.findOne({
    where: {
      userId: req.selectedUser.id,
      targetId: req.user.id,
    },
  });
  let follower = await req.selectedUser.hasFollowing(req.user);
  if((suggestions==null||follower)&&req.selectedUser.gender == 0){
    await sendNotification(req.selectedUser, { type: 'follow', user: req.user });
  }
  res.json({ diamonds: req.user.diamonds, paid });
}));

/**
 * @api {delete} /users/:id/followers 팔로우 취소하기
 * @apiGroup User
 * @apiName UnfollowUser
 * @apiDescription 해당 사용자를 팔로우 취소합니다.
 * @apiError (Error 409) Conflict 이미 처리되었습니다.
 * @apiUse GetUser
 */
 userRouter.delete('/followers', approveRequired, async(async (req, res) => {
  if (!await req.selectedUser.hasFollower(req.user)) throw 'Conflict';
  await req.selectedUser.removeFollower(req.user);
  // If chat is present, delete it.
  await UserChat.destroy({ where: {
    $or: [{
      senderId: req.selectedUser.id,
      receiverId: req.user.id,
    }, {
      senderId: req.user.id,
      receiverId: req.selectedUser.id,
    }],
  } });
  res.json({});
}));

/**
 * @api {delete} /user/followingBulk 팔로우 단체로 취소하기
 * @apiGroup User
 * @apiName UnfollowUsers
 * @apiParam (Body) {Number[]} ids 사용자 번호 목록
 * @apiDescription 해당 사용자들을 팔로우 취소합니다.
 * @apiError (Error 409) Conflict 이미 처리되었습니다.
 */
 userRouter.delete('/followingBulk', checkModifiable, async(async (req, res) => {
  let ids = (req.body.ids || '').split(',').map(v => parseInt(v));
  await UserFollowing.destroy({ where: {
    followerId: req.selectedUser.id,
    followingId: ids,
  } });
  // If chat is present, delete it.
  await UserChat.destroy({ where: {
    $or: [{
      senderId: req.selectedUser.id,
      receiverId: ids,
    }, {
      senderId: ids,
      receiverId: req.selectedUser.id,
    }],
  } });
  res.json({});
}));

/**
 * @api {get} /user/following 자기 자신의 팔로잉 정보 가져오기
 * @apiGroup User
 * @apiName GetMyUserFollowing
 * @apiDescription 자기 자신의 팔로잉 정보를 가져옵니다.
 *
 *   다른 사용자를 언팔로우할 때에는 `DELETE /users/:id/followers`를 사용해 주세요.
 * @apiParam (Query) {Number} [limit=20] 불러올 개수
 * @apiParam (Query) {Number} [page=0] 불러올 페이지 번호.
 * @apiSuccess {User[]} _ 사용자 목록
 * @apiUse ReturnUser
 * @apiPermission LoginRequired
 * @apiUse LoginRequired
 */
 userRouter.get('/following', checkModifiable, async(async (req, res) => {
  let { limit = 20, page = 0 } = req.query;
  limit = parseInt(limit) || 0;
  page = parseInt(page) || 0;
  if (limit > 150) limit = 150;
  let users = await req.selectedUser.getFollowings({
    order: [[sequelize.literal('userFollowing.createdAt'), 'DESC']],
    joinTableAttributes: [],
    offset: limit * page,
    limit,
    where: { isEnabled: true },
  });
  let result = [];
  for (let user of users) {
    let suggest = await UserSuggestion.findOne({where:{userId:user.id,targetId:req.selectedUser.id}});
    let follower = await req.selectedUser.hasFollower(user);
    if(suggest!=null&&!follower){
      continue;
    }
    let data = user.toJSON();
    data.photos = user.getPhotos(req.user);
    data.photoThumbs = user.getPhotoThumbs(req.user);
    result.push(data);
  }
  if (req.query.count != null) {
    let count = await req.selectedUser.countFollowings();
    res.json({ count, result });
  } else {
    res.json(result);
  }
}));

 userRouter.post('/followView',approveRequired, async(async (req, res) => {
  sendNotification(req.selectedUser,{ type: 'followView', user: req.user });
  res.json({});
 }));

/**
 * @api {delete} /users/:id/following 팔로워 삭제하기
 * @apiGroup User
 * @apiName BlockFollowerUser
 * @apiDescription 해당 사용자를 팔로워 목록에서 삭제합니다.
 * @apiError (Error 409) Conflict 이미 처리되었습니다.
 * @apiUse GetUser
 */
 userRouter.delete('/following', approveRequired, async(async (req, res) => {
  if (!await req.selectedUser.hasFollowing(req.user)) throw 'Conflict';
  let [rowCount] = await UserFollowing.update({ denied: true }, { where: {
    denied: false,
    followerId: req.selectedUser.id,
    followingId: req.user.id,
  } });
  if (rowCount > 0) {
    // If chat is present, delete it.
    await UserChat.destroy({ where: {
      $or: [{
        senderId: req.selectedUser.id,
        receiverId: req.user.id,
      }, {
        senderId: req.user.id,
        receiverId: req.selectedUser.id,
      }],
    } });
    await UserSuggestion.destroy({
      where: {
        userId: req.selectedUser.id,
        targetId: req.user.id,
      },
    });
    await sendNotification(req.selectedUser,
      { type: 'followDeny', user: req.user });
  }
  res.json({});
}));

/**
 * @api {delete} /user/followersBulk 팔로워 단체로 삭제하기
 * @apiGroup User
 * @apiName BlockFollowerUsers
 * @apiParam (Body) {Number[]} ids 사용자 번호 목록
 * @apiDescription 해당 사용자들을 팔로워 목록에서 삭제합니다.
 * @apiError (Error 409) Conflict 이미 처리되었습니다.
 */
 userRouter.delete('/followersBulk', checkModifiable, async(async (req, res) => {
  let ids = (req.body.ids || '').split(',').map(v => parseInt(v));
  let followings = await UserFollowing.findAll({
    where: {
      denied: false,
      followerId: ids,
      followingId: req.selectedUser.id,
    },
    include: [{
      model: User,
      as: 'follower',
    }],
  });
  for (let following of followings) {
    if (following.follower != null) {
      await sendNotification(following.follower,
        { type: 'followDeny', user: req.user });
    }
  }
  // If chat is present, delete it.
  await UserChat.destroy({ where: {
    $or: [{
      senderId: ids,
      receiverId: req.user.id,
    }, {
      senderId: req.user.id,
      receiverId: ids,
    }],
  } });
  await UserFollowing.update({ denied: true }, { where: {
    followerId: ids,
    followingId: req.user.id,
  } });
  await UserSuggestion.destroy({
    where: {
      userId: ids,
      targetId: req.user.id,
    },
  });
  res.json({});
}));

/**
 * @api {get} /users/:id/reports 신고 내역 보기
 * @apiGroup User
 * @apiName GetUserReports
 * @apiDescription 사용자 신고 내역을 봅니다.
 * @apiParam (Query) {Number} [limit=20] 불러올 개수
 * @apiParam (Query) {Number} [page=0] 불러올 페이지 번호.
 * @apiUse GetUser
 * @apiPermission AdminRequired
 * @apiUse AdminRequired
 */
 userRouter.get('/reports', adminRequired, async(async (req, res) => {
  let { limit = 20, page = 0 } = req.query;
  limit = parseInt(limit) || 0;
  page = parseInt(page) || 0;
  if (limit > 150) limit = 150;
  let result = await UserReport.findAll({
    where: {
      reporteeId: req.selectedUser.id,
    },
    offset: limit * page,
    limit,
    include: {
      model: User,
      as: 'reporter',
      attributes: ['id', 'name', 'nickname', 'photos', 'photoPrimary'],
    },
  });
  if (req.query.count != null) {
    let count = await req.selectedUser.countReporters();
    res.json({ count, result });
  } else {
    res.json(result);
  }
}));

/**
 * @api {post} /users/:id/reports 신고하기
 * @apiGroup User
 * @apiName PostUserReport
 * @apiDescription 사용자를 신고합니다.
 * @apiParam (Body) {String} [reason] 신고 사유
 * @apiError (Error 409) Conflict 이미 처리되었습니다.
 * @apiUse GetUser
 * @apiPermission LoginRequired
 * @apiUse LoginRequired
 */
 userRouter.post('/reports', loginRequired, async(async (req, res) => {
  if (await req.selectedUser.hasReporter(req.user)) throw 'Conflict';
  await req.selectedUser.addReporter(req.user, { reason: req.body.reason });
  await req.selectedUser.increment('reportCount');
  res.json({});
}));

/**
 * @api {delete} /users/:id/reports 신고 내역 초기화
 * @apiGroup User
 * @apiName DeleteUserReports
 * @apiDescription 사용자 신고 내역을 초기화합니다.
 * @apiUse GetUser
 * @apiPermission AdminRequired
 * @apiUse AdminRequired
 */
 userRouter.delete('/reports', adminRequired, async(async (req, res) => {
  await UserReport.destroy({ where: {
    reporteeId: req.selectedUser.id,
  }});
  await req.selectedUser.update({ reportCount: 0 });
  res.json({});
}));

/**
 * @api {get} /user/groups 자기 자신의 그룹 목록 가져오기
 * @apiGroup User
 * @apiName GetMyUserGroups
 * @apiParam (Query) {String} [type] 그룹 타입. !로 시작하면 해당 타입을 제외합니다.
 * @apiDescription 자기 자신의 그룹 목록을 가져옵니다.
 * @apiSuccess {Group[]} _ 그룹 목록
 * @apiUse ReturnGroup
 * @apiUse ReturnGroupUser
 * @apiPermission LoginRequired
 * @apiUse LoginRequired
 */
/**
 * @api {get} /users/:id/groups 그룹 목록 가져오기
 * @apiGroup User
 * @apiName GetUserGroups
 * @apiParam (Query) {String} [type] 그룹 타입. !로 시작하면 해당 타입을 제외합니다.
 * @apiDescription 사용자의 그룹 목록을 가져옵니다.
 * @apiSuccess {Group[]} _ 그룹 목록
 * @apiUse ReturnGroup
 * @apiUse ReturnGroupUser
 * @apiUse GetUser
 */
 userRouter.get('/groups', async(async (req, res) => {
  let { type } = req.query;
  let where = {};
  if (type != null && type[0] === '!') {
    where.type = { $notIn: ['banned', type.slice(1)] };
  } else if (type != null) {
    where.type = type;
  } else {
    where.type = { $not: 'banned' };
  }
  let groups = await req.selectedUser.getGroups({
    where: { deletedAt: null },
    through: { where },
  });
  res.json(groups);
}));
/**
 * @api {delete} /user/groupsBulk 스티커 단체로 삭제하기
 * @apiGroup User
 * @apiName DeleteUserGroupsBulk
 * @apiParam (Body) {Number[]} ids 스티커 번호 목록
 * @apiDescription 해당 스티커들을 삭제합니다.
 * @apiError (Error 409) Conflict 이미 처리되었습니다.
 */
 userRouter.delete('/groupsBulk', checkModifiable, async(async (req, res) => {
  let ids = (req.body.ids || '').split(',').map(v => parseInt(v));
  let list = await Group.findAll({ where: {
    id: ids,
  } });
  for (let group of list) {
      // Remove the user
      await group.removeMember(req.user);
      await group.updateMembers();
    // Remove user from chat participants
    if (group.chatKey != null) {
      removeChatParticipant(group.chatKey, req.user);
    }
  }
  res.json({});
}));

/**
 * @api {get} /user/meetings/ 자기 자신의 모임 목록 가져오기
 * @apiGroup User
 * @apiName GetMyUserMeetings
 * @apiDescription 모임 목록을 가져옵니다.
 * @apiParam (Query) {String=likes,members,newest,oldest} [sort=newest] 정렬 기준
 * @apiParam (Query) {String} [title] 이름 필터
 * @apiParam (Query) {String} [category] 카테고리 필터
 * @apiParam (Query) {Number} [location] 지역 필터
 * @apiParam (Query) {Boolean} [all] 모두 가져오기
 * @apiParam (Query) {Date} [dateFrom] 날짜 필터
 * @apiParam (Query) {Date} [dateTo] 날짜 필터
 * @apiParam (Query) {Number} [limit=20] 불러올 개수
 * @apiParam (Query) {Number} [page=0] 불러올 페이지 번호.
 * @apiSuccess {Meeting[]} _ 모임 목록
 * @apiUse ReturnMeeting
 */
/**
 * @api {get} /users/:id/meetings/ 모임 목록 가져오기
 * @apiGroup User
 * @apiName GetUserMeetings
 * @apiDescription 모임 목록을 가져옵니다.
 * @apiParam (Query) {String=likes,members,newest,oldest} [sort=newest] 정렬 기준
 * @apiParam (Query) {String} [title] 이름 필터
 * @apiParam (Query) {String} [category] 카테고리 필터
 * @apiParam (Query) {Number} [location] 지역 필터
 * @apiParam (Query) {Boolean} [all] 모두 가져오기
 * @apiParam (Query) {Date} [dateFrom] 날짜 필터
 * @apiParam (Query) {Date} [dateTo] 날짜 필터
 * @apiParam (Query) {Number} [limit=20] 불러올 개수
 * @apiParam (Query) {Number} [page=0] 불러올 페이지 번호.
 * @apiSuccess {Meeting[]} _ 모임 목록
 * @apiUse ReturnMeeting
 * @apiUse GetUser
 */
 userRouter.get('/meetings', async(async (req, res) => {
  let query = createMeetingQuery(req);
  query.through = { where: { enabled: true } };
  let result = await req.selectedUser.getMeetings(query);
  if (req.query.count != null) {
    let count = await req.selectedUser.countMeetings({
      where: query.where,
      through: query.through,
    });
    res.json({ count, result });
  } else {
    res.json(result);
  }
}));
 userRouter.post('/suggestions',adminRequired,async(async(req,res)=>{
  let user = req.selectedUser;
  let todayStart = new Date(Date.now()+(9*60*60*1000));
  todayStart = new Date(todayStart.setHours(0));
  todayStart = new Date(todayStart.setMinutes(0));
  let todayCount = await UserSuggestion.count({
    where: {
      createdAt: { $gte: todayStart },
      targetId: user.id,
    },
  });
  let suggestions = await UserSuggestion.findAll({
    where: {
      targetId: user.id,
    },
  });
  let ids = suggestions.map(suggestion => suggestion.userId);
  let viewIds = await UserView.findAll({
    where: {
      viewerId: user.id,
    },
  });
  viewIds = viewIds.map(v=>v.vieweeId);
  ids = viewIds.concat(ids);
  let where = {
    isSuggestion: true,
    isLoungeUsed: true,
    gender: 1,
  };
  let groups = await user.getGroups();
  for(let group of groups){
    let groupUsers = await group.getMembers({
      where: [sequelize.where(sequelize.literal('groupUser.type'), {
        $notIn: ['invited', 'banned'],
      }), {
        isEnabled: true,
      }],
    });
    let groupUserIds = groupUsers.map(groupUser => groupUser.id);
    ids.concat(groupUserIds);
  }
  let followingData = await UserFollowing.findAll({ where: {
    followerId: user.id,
  } });
  if(followingData!=null&&0<followingData.length){
    let followingIds = followingData.map(data => data.followingId);
    ids = ids.concat(followingIds);
  }
  let followerData = await UserFollowing.findAll({ where: {
    followingId: user.id,
  } });  
  if(followerData!=null&&0<followerData.length){
    let followerIds = followerData.map(data => data.followerId);
    ids = ids.concat(followerIds);
  }
  if(ids!=null&&0<ids.length){
    where.id = { $notIn: ids };
  }
  let suggestUsers = await User.findAll({
    where,
    order: [
    Sequelize.fn('RAND'),
    ],
    limit: suggestionConfig.count,
  });
  let count = 0;
  for(let suggest of suggestUsers){
    count += 1;
    let data = await UserSuggestion.create({
      userId: suggest.id,
      targetId: user.id,
    });
  }
  if(count!=0){
    todayCount += 1;
    let users;
    if(2==todayCount){
      users = await UserSuggestion.findAll({
        where: {
          createdAt: { $gte: todayStart },
          targetId: user.id,
        },
        include: [{
          model: User,
          as: 'user',
        },{
          model: User,
          as: 'target',
        }],
      });
      users = users.map(user => user.user);
    }
    await sendNotification(user, {
      type: 'userSuggest',
      title: '에스티커 알림',
      user: suggestUsers[0],
      users,
    });
  }
  res.json(suggestUsers);
}));
/**
 * @api {get} /user/chats/:userId 자기 자신과 상대방과의 채팅 정보 가져오기
 * @apiGroup User
 * @apiName GetMyUserChatWithUser
 * @apiDescription 자기 자신과 상대방의 채팅 정보를 가져옵니다. 없으면 null이 반환됩니다.
 * @apiSuccess {Chat} _ 채팅
 * @apiUse ReturnChat
 * @apiPermission Modifiable
 * @apiUse Modifiable
 */
 userRouter.get('/chats/:targetId', checkModifiable, async(async (req, res) => {
  let { targetId } = req.params;
  targetId = parseInt(targetId);
  let where = {
    $or: [
    { senderId: req.selectedUser.id, receiverId: targetId },
    { receiverId: req.selectedUser.id, senderId: targetId },
    ],
  };
  let result = await UserChat.findOne({
    where,
    include: [{
      model: User,
      attributes: ['id', 'nickname', 'photos', 'photoPrimary', 'isEnabled','location','birth','job','height','bodytype'],
      as: 'sender',
      where: { isEnabled: true },
    }, {
      model: User,
      attributes: ['id', 'nickname', 'photos', 'photoPrimary', 'isEnabled','location','birth','job','height','bodytype'],
      as: 'receiver',
      where: { isEnabled: true },
    }],
  });
  res.json(result);
}));

/**
 * @api {post} /user/chats/:userId/lock 채팅 잠그기
 * @apiGroup User
 * @apiName LockChatUser
 * @apiDescription 상대방과의 채팅을 잠급니다.
 * @apiSuccess {Chat} _ 채팅
 * @apiUse ReturnChat
 * @apiPermission Modifiable
 * @apiUse Modifiable
 */
 userRouter.post('/chats/:targetId/lock',
  checkModifiable, async(async (req, res) => {
    let { targetId } = req.params;
    targetId = parseInt(targetId);
    let where = {
      $or: [
      { senderId: req.selectedUser.id, receiverId: targetId },
      { receiverId: req.selectedUser.id, senderId: targetId },
      ],
    };
    let result = await UserChat.findOne({ where });
    if (result == null || result.chatKey == null) throw 'NotFound';
    if (result.expired) throw 'Conflict';
    if ((Date.now() - result.updatedAt.getTime()) < 210 * 60 * 1000) {
      throw 'ValidationError';
    }
  // Mark the chat as expired
  await result.update({ expired: true });
  await lockChat(result.chatKey);
  res.json({});
}));

/**
 * @api {get} /user/chats/ 자기 자신의 채팅 정보 가져오기
 * @apiGroup User
 * @apiName GetMyUserChats
 * @apiDescription 자기 자신의 채팅 정보를 가져옵니다.
 * @apiParam (Query) {Number} [limit=20] 불러올 개수
 * @apiParam (Query) {Number} [page=0] 불러올 페이지 번호.
 * @apiSuccess {Chat[]} _ 채팅 목록
 * @apiUse ReturnChat
 * @apiPermission Modifiable
 * @apiUse Modifiable
 */
 userRouter.get('/chats', checkModifiable, async(async (req, res) => {
  let { limit = 20, page = 0 } = req.query;
  limit = parseInt(limit) || 0;
  page = parseInt(page) || 0;
  if (limit > 150) limit = 150;
  let where = {
    $or: [
    { senderId: req.selectedUser.id },
    { receiverId: req.selectedUser.id },
    ],
    $not: {
      expired: true,
      chatKey: null,
    },
  };
  let result = await UserChat.findAll({
    where,
    order: [['updatedAt', 'DESC']],
    offset: limit * page,
    limit,
    include: [{
      model: User,
      attributes: ['id', 'nickname', 'photos', 'photoPrimary'],
      as: 'sender',
    }, {
      model: User,
      attributes: ['id', 'nickname', 'photos', 'photoPrimary'],
      as: 'receiver',
    }],
  });
  let userFollowingQuery = {
    where: {
      followingId: req.selectedUser.id,
      reply: true,
      '$`follower.receiverChats`.`senderId`$': null,
      '$`follower.senderChats`.`senderId`$': null,
    },
    order: [['createdAt', 'DESC']],
    /* offset: Math.max(0, limit * page - count),
    limit: Math.max(0, page - result.length), */
    include: [{
      model: User,
      attributes: ['id', 'nickname', 'photos', 'photoPrimary'],
      as: 'follower',
      /* where: {
        $and: [
          sequelize.literal('follower.senderChats.id is NULL'),
          sequelize.literal('follower.receiverChats.id is NULL'),
        ],
      }, */
      include: [{
        model: UserFollowing,
        attributes: [],
        as: 'followerFollowings',
        where: {
          followerId: req.selectedUser.id,
        },
      }, {
        model: UserChat,
        as: 'senderChats',
        where: {
          receiverId: req.selectedUser.id,
        },
        required: false,
      }, {
        model: UserChat,
        as: 'receiverChats',
        where: {
          senderId: req.selectedUser.id,
        },
        required: false,
      }],
    }],
  };
  let count = await UserChat.count({ where }) +
  await UserFollowing.count(userFollowingQuery);
  result = result.map(v => Object.assign({}, v.toJSON(), {
    sender: v.sender && v.sender.toJSON(),
    receiver: v.receiver && v.receiver.toJSON(),
  }));
  // Append 'dummy' chats if result is not full..
  if (result.length < limit) {
    let resultDummies = await UserFollowing.findAll(userFollowingQuery);
    resultDummies = resultDummies.map(v => ({
      id: null,
      chatKey: null,
      expired: false,
      dummy: true,
      receiverId: req.selectedUser.id,
      senderId: v.follower.id,
      receiver: req.selectedUser.toJSON(),
      sender: v.follower.toJSON(),
    }));
    result = result.concat(resultDummies);
  }
  if (req.query.count != null) {
    res.json({ count, result });
  } else {
    res.json(result);
  }
}));

/**
 * @api {post} /users/:id/chats/ 채팅 신청/수락하기
 * @apiGroup User
 * @apiName PostUserChat
 * @apiDescription 해당 사용자와의 채팅을 신청하거나 수락합니다.
 * @apiParam (Body) {Boolean} [force=false] 강제로 신청할지 여부입니다. 강제로 신청하면 상대방의 관심 표현이 필요 없고 그냥 채팅 신청할 수 있습니다.
 * @apiError (Error 400) ValidationError 상대방이 나를 관심 표현해야 합니다. (강제 신청시 예외)
 * @apiError (Error 409) Conflict 이미 처리되었습니다.
 * @apiError (Error 403) NoDiamonds 잔여 다이아가 부족합니다.
 * @apiSuccess {Chat} _ 채팅
 * @apiUse ReturnChat
 * @apiUse GetUser
 */
 userRouter.post('/chats', approveRequired, async(async (req, res) => {
  if (req.user.id === req.selectedUser.id) throw 'ValidationError';
  // Get already existing chat
  let chat = await UserChat.findOne({
    where: {
      $or: [
      { senderId: req.selectedUser.id, receiverId: req.user.id },
      { receiverId: req.selectedUser.id, senderId: req.user.id },
      ],
    },
    include: [{
      model: User,
      attributes: ['id', 'nickname', 'photos', 'photoPrimary'],
      as: 'sender',
    }, {
      model: User,
      attributes: ['id', 'nickname', 'photos', 'photoPrimary'],
      as: 'receiver',
    }],
  });
  if (chat == null) {
    // Check if the selected user follows the user
    let followsMe = await UserFollowing.findOne({ where: {
      followingId: req.user.id,
      followerId: req.selectedUser.id,
    } });
    let force = req.body.force;
    force = force === 'true' || force === 'yes' || force === '1' ||
    force === true || force === 1;
    let forceOpen = false;
    let paid = false;
    try {
      if(req.user.gender!=req.selectedUser.gender) await doPayment(req.user, 'profileChat', req.selectedUser.id);
      paid = true;
      forceOpen = force;
    } catch (e) {
      if (e !== 'Conflict') throw e;
    }
    // Create invitiation
    chat = await UserChat.create({
      senderId: req.user.id,
      receiverId: req.selectedUser.id,
    });
    if(req.selectedUser.gender == 0){
      sendNotification(req.selectedUser, {
        type: 'chatInvite',
        user: req.user,
      });
    }
    if (forceOpen) {
      let chatKey = await createChat(undefined, [req.user, req.selectedUser],
        null, false);
      await chat.update({ chatKey });
    }
    let data = chat.toJSON();
    if (paid) data.diamonds = req.user.diamonds;
    data.receiver = chat.receiver || req.selectedUser.toJSON();
    data.sender = chat.sender || req.user.toJSON();
    return res.json(data);
  }
  if (chat.chatKey == null && chat.receiverId === req.user.id) {
    let chatKey = await createChat(undefined, [req.user, req.selectedUser],
      null, false);
    sendNotification(req.selectedUser, {
        type: 'chatAccept',
        user: req.user,
      });
    await chat.update({ chatKey });
    return res.json(chat);
  }
  throw 'Conflict';
}));

/**
 * @api {delete} /users/:id/chats/ 채팅 삭제/거부하기
 * @apiGroup User
 * @apiName DeleteUserChat
 * @apiDescription 해당 사용자와의 채팅을 삭제하거나 거부합니다.
 * @apiError (Error 409) Conflict 이미 처리되었습니다.
 * @apiUse GetUser
 */
 userRouter.delete('/chats', approveRequired, async(async (req, res) => {
  if (req.user.id === req.selectedUser.id) throw 'ValidationError';
  // Get already existing chat
  let chat = await UserChat.findOne({
    where: {
      $or: [
      { senderId: req.selectedUser.id, receiverId: req.user.id },
      { receiverId: req.selectedUser.id, senderId: req.user.id },
      ],
    },
  });
  if (chat == null) throw 'Conflict';
  if (chat.expired) throw 'Conflict';
  // Send notification if chatKey doesn't exist and is receiving
  if (chat.chatKey == null && req.user.id === chat.receiverId) {
    sendNotification(req.selectedUser, {
      type: 'chatDeny',
      user: req.user,
    });
  }
  await chat.update({ expired: true });
  // await chat.destroy();
  res.json({});
}));

/**
 * @api {get} /users/:id/contacts 채팅중 연락처 받기
 * @apiGroup User
 * @apiName GetContacts
 * @apiDescription 해당 사용자의 연락처를 받습니다.
 * @apiError (Error 403) PurchaseRequired 구매해야 합니다.
 * @apiError (Error 404) NotFound 채팅중 상태가 아닙니다.
 * @apiSuccess {String} phone 휴대전화 번호
 * @apiSuccess {String} name 실명
 * @apiUse GetUser
 */
 userRouter.get('/contacts', approveRequired, async(async (req, res) => {
  if (req.user.id === req.selectedUser.id) throw 'ValidationError';
  // Get already existing chat
  if (!await checkPayment(req.user, 'chatContact', req.selectedUser.id)) {
    throw 'PurchaseRequired';
  }
  res.json({
    phone: req.selectedUser.phone,
    name: req.selectedUser.name,
  });
}));

/**
 * @api {post} /users/:id/contacts 채팅중 연락처 구매하기
 * @apiGroup User
 * @apiName PayContacts
 * @apiDescription 해당 사용자의 연락처를 구매합니다.
 * @apiError (Error 409) Conflict 이미 처리되었습니다.
 * @apiError (Error 404) NotFound 채팅중 상태가 아닙니다.
 * @apiUse GetUser
 */
 userRouter.post('/contacts', approveRequired, async(async (req, res) => {
  if (req.user.id === req.selectedUser.id) throw 'ValidationError';
  // Get already existing chat
  await doPayment(req.user, 'chatContact', req.selectedUser.id);
  res.json({});
}));

/**
 * @api {get} /user/questions/ 질문 목록 가져오기
 * @apiGroup User
 * @apiName GetQuestions
 * @apiDescription 사용자의 질문 목록을 가져옵니다.
 * @apiParam (Query) {String} [title] 제목 필터입니다.
 * @apiParam (Query) {String} [category] 카테고리 필터입니다.
 * @apiParam (Query) {Boolean} [answered] 답변 여부입니다.
 * @apiParam (Query) {Number} [limit=20] 불러올 개수입니다.
 * @apiParam (Query) {Number} [page=0] 불러올 페이지 번호입니다.
 * @apiSuccess {Question[]} _ 질문 목록
 * @apiUse ReturnQuestion
 * @apiPermission Modifiable
 * @apiUse Modifiable
 */
 userRouter.get('/questions', checkModifiable, async(async (req, res) => {
  let { limit = 20, page = 0 } = req.query;
  limit = parseInt(limit) || 0;
  page = parseInt(page) || 0;
  if (limit > 150) limit = 150;
  let where = {};
  if (req.query.title != null) {
    where.title = { $like: `%${req.query.title}%` };
  }
  if (req.query.category != null) {
    where.category = req.query.category;
  }
  if (req.query.answered != null) {
    where.answeredAt = req.query.answered === '1' ? { $not: null } : null;
  }
  where.userId = req.selectedUser.id;
  let result = await Question.findAll({
    where,
    order: [['createdAt', 'DESC']],
    offset: limit * page,
    limit,
  });
  if (req.query.count != null) {
    let count = await Question.count({ where });
    res.json({ count, result });
  } else {
    res.json(result);
  }
}));

 export const router = new Router();
 export default router;


/**
 * @api {get} /user/ 자기 자신 사용자 가져오기
 * @apiGroup User
 * @apiName GetCurrentUser
 * @apiDescription 현재 사용자 객체를 가져옵니다.
 *
 *   사용자가 로그인하지 않은 경우 401을 반환합니다.
 *
 * @apiSuccess {User} _ 사용자 정보
 * @apiSuccess {String[]} _.photos 사진 목록
 * @apiSuccess {String[]} _.photoThumbs 사진 썸네일 목록
 * @apiSuccess {Number} _.stickersCount 스티커 개수
 * @apiSuccess {Number} _.invitedStickersCount 초대받은 스티커 개수
 * @apiSuccess {Number} _.followingsCount 팔로잉 개수
 * @apiSuccess {Number} _.followersCount 팔로워 개수
 * @apiSuccess {Number} _.chatsCount 채팅 개수
 * @apiSuccess {Number} _.meetingsCount 모임 개수
 * @apiSuccess {Number} _.ratingHistoriesCount 재심사 횟수
 * @apiSuccess {Boolean} _.loungeGenderPay 라운지 고급 검색 결제 여부
 * @apiUse ReturnUser
 * @apiPermission LoginRequired
 * @apiUse LoginRequired
 */
 router.use('/user', loginRequired, (req, res, next) => {
  req.selectedUser = req.user;
  next();
}, userRouter);
/**
 * @api {patch} /users/bulk 프로필 정보 벌크로 수정하기
 * @apiGroup User
 * @apiName SetUserProfileBulk
 * @apiDescription 사용자의 프로필을 벌크로 수정합니다.
 * @apiSuccess {User[]} _ 사용자 정보들
 * @apiSuccess {String[]} _.photos 사진 목록
 * @apiSuccess {String[]} _.photoThumbs 사진 썸네일 목록
 * @apiUse ReturnUser
 * @apiParam (Body) {Number[]} ids 아이디 배열
 * @apiParam (Body) {Number} [photoPrimary] 대표 사진 인덱스
 * @apiParam (Body) {String} [name] 이름
 * @apiParam (Body) {String} [nickname] 닉네임
 * @apiParam (Body) {Number} [location] 지역
 * @apiParam (Body) {Date} [birth] 생년월일
 * @apiParam (Body) {String} [job] 직업
 * @apiParam (Body) {String} [school] 학교
 * @apiParam (Body) {Number} [alcohol] 음주
 * @apiParam (Body) {Number} [cigarette] 담배
 * @apiParam (Body) {Number} [personality] 성격
 * @apiParam (Body) {Number} [bloodtype] 혈액형
 * @apiParam (Body) {Number} [bodytype] 체형
 * @apiParam (Body) {Number} [height] 키
 * @apiParam (Body) {Number} [religion] 종교
 * @apiParam (Body) {String} [introduction] 자기소개
 * @apiParam (Body) {Boolean} [isLoungeUsed] 라운지 사용 여부
 * @apiParam (Body) {Boolean} [updatePush] 푸시 알림 수신 여부
 * @apiParam (Body) {Boolean} [usePhotoInGroup] 그룹에서 사진 사용 여부
 * @apiParam (Body) {Boolean} [isEnabled] 계정 활성화 여부 (관리자만)
 * @apiParam (Body) {Boolean} [isApproved] 계정 승인 여부 (관리자만)
 * @apiParam (Body) {Boolean} [isLoungeApproved] 라운지 승인 여부 (관리자만)
 * @apiParam (Body) {Boolean} [isAdmin] 관리자 여부 (관리자만)
 * @apiError (Error 400) PhotoIndexInvalid 대표 사진 인덱스가 잘못되었습니다.
 * @apiError (Error 409) NicknameConflict 닉네임이 충돌합니다.
 * @apiPermission AdminRequired
 * @apiUse AdminRequired
 */
 router.patch('/users/bulk', adminRequired, async(async (req, res) => {
  let ids = (req.body.ids || '').split(',').map(v => parseInt(v));
  let users = await User.findAll({ where: { id: ids } });
  let output = await Promise.all(users.map(user => updateInfo(req, user)));
  res.json(output);
}));
/**
 * @api {delete} /users/bulk 사용자 단체로 삭제하기
 * @apiGroup User
 * @apiName DeleteUserBulk
 * @apiDescription 사용자를 벌크로 영구적으로 강제 삭제합니다.
 * @apiParam (Body) {Number[]} ids 아이디 배열
 * @apiParam (Body) {String} code 삭제용 비밀번호; 설정 파일에 bcrypt로 암호화 처리되어 저장되어 있습니다
 * @apiPermission AdminRequired
 * @apiUse AdminRequired
 */
 router.delete('/users/bulk', adminRequired, async(async (req, res) => {
  if (!(await bcrypt.compare(req.body.code, authConfig.deleteKey))) {
    throw 'PasswordInvalid';
  }
  let ids = (req.body.ids || '').split(',').map(v => parseInt(v));
  let users = await User.findAll({ where: { id: ids } });
  for (let user of users) {
    if (user.isAdmin) continue;
    let groups = await user.getGroups();
    let meetings = await user.getHostedMeetings();
    await user.destroy();
    for (let group of groups) {
      await group.updateMembers();
      if (group.membersCount === 0 && group.deletedAt != null) {
        await group.update({ deletedAt: new Date() });
      }
    }
    for (let meeting of meetings) {
      await meeting.destroy();
    }
  }
  res.json({});
}));

 router.post('/users/bulk/notifications', adminRequired,
  async(async (req, res) => {
    let ids = (req.body.ids || '').split(',').map(v => parseInt(v));
    let users = await User.findAll({ where: { id: ids } });
    const { title, message } = req.body;
    const data = { title, message, type: 'notice' };
  // Tada
  await sendNotification(users, data);
  res.json({});
}));

 router.post('/users/all/notifications', adminRequired,
  async(async (req, res) => {
  // We need to perform pagination - do loop while there are something in the
  // results.
  let lastCount = 0;
  let offset = 0;
  do {
    let users = await User.findAll({ limit: 1000, offset });
    lastCount = users.length;
    offset += 1000;
    if (users.length > 0) {
      const { title, message } = req.body;
      const data = { title, message, type: 'notice' };
      // Tada
      await sendNotification(users, data);
    }
  } while (lastCount > 0);
  res.json({});
}));

/**
 * @api {get} /users/genderPay 라운지 성별 이성으로 보이는지 확인
 * @apiGroup User
 * @apiName GetPayUserGender
 * @apiDescription 라운지의 성별을 이성으로 보기 위해 결제했는지 여부를 확인합니다.
 * @apiSuccess {Boolean} _ 결제 여부
 * @apiPermission LoginRequired
 * @apiUse LoginRequired
 */
 router.get('/users/genderPay', loginRequired, async(async (req, res) => {
  let paid = await checkPayment(req.user, 'loungeGender');
  res.json(paid);
}));
/**
 * @api {post} /users/genderPay 라운지 성별 이성으로 보기 위해 결제하기
 * @apiGroup User
 * @apiName PayUserGender
 * @apiDescription 라운지의 성별을 이성으로 보기 위해 결제합니다.
 * @apiError (Error 409) Conflict 이미 결제되었습니다.
 * @apiError (Error 403) NoDiamonds 잔여 다이아가 부족합니다.
 * @apiPermission LoginRequired
 * @apiUse LoginRequired
 */
 router.post('/users/genderPay', loginRequired, async(async (req, res) => {
  await doPayment(req.user, 'loungeGender');
  res.json({ diamonds: req.user.diamonds });
}));

/**
 * @api {get} /users/:id/ ID에 속한 사용자 가져오기
 * @apiGroup User
 * @apiName GetUser
 * @apiDescription ID에 속하는 사용자를 가져옵니다. 결제하지 않았으면 고유번호와 닉네임만
 * 표시됩니다.
 * @apiSuccess {Boolean} hasVoted 사용자를 투표했는지 여부
 * @apiSuccess {Number} votedCount 해당 사용자의 투표 수
 * @apiSuccess {Number} votedScore 사용자 투표 점수
 * @apiSuccess {Boolean} following 자신이 사용자를 팔로우하는지 여부
 * @apiSuccess {Boolean} followingDenied 사용자가 자신을 거절했는지 여부
 * @apiSuccess {Boolean} follower 사용자가 자신을 팔로우하는지 여부
 * @apiSuccess {Boolean} followerDenied 자신이 사용자를 거절했는지 여부
 * @apiSuccess {Boolean} canChat 채팅 신청 가능한지 여부
 * @apiSuccess {Boolean} canChatForce 채팅 신청 강제로 가능한지 여부
 * @apiSuccess {Boolean} canFollow 팔로우 가능한지 여부
 * @apiSuccess {Chat} chat 사용자와 자신의 채팅
 * @apiSuccess {User} _ 사용자 정보
 * @apiSuccess {String[]} _.photos 사진 목록
 * @apiSuccess {String[]} _.photoThumbs 사진 썸네일 목록
 * @apiUse ReturnUser
 * @apiUse GetUser
 */
 router.use('/users/:id', async(async (req, res, next) => {
  req.selectedUser = await User.findById(req.params.id);
  if (req.selectedUser == null) throw 'UserNotFound';
  if (req.user == null ||
    (!req.user.isAdmin && !req.selectedUser.isEnabled)
    ) {
    throw 'UserNotFound';
}
next();
}), userRouter);


router.use('/users_n/:id', async(async (req, res, next) => {
    req.selectedUser = await User.findById(req.params.id);
    if (req.selectedUser == null) throw 'UserNotFound';
    if (req.user == null) {
        throw 'UserNotFound';
    }
    next();
}), userRouter);



/**
 * @api {get} /users/?mode=lounge 유저 목록 (라운지) 가져오기
 * @apiGroup User
 * @apiName GetUserLounge
 * @apiDescription 사용자 목록을 가져옵니다.
 * @apiParam (Query) {String} mode 정렬 타입. lounge로 고정해 주세요
 * @apiParam (Query) {Number} [gender] 성별 필터 (결제시 사용 가능)
 * @apiParam (Query) {Number} [ageMin] 나이 필터 (결제시 사용 가능)
 * @apiParam (Query) {Number} [ageMax] 나이 필터 (결제시 사용 가능)
 * @apiParam (Query) {Number} [genderRatio] 성비 필터 (남자 1 기준 상대적 여성 혼합 비율)
 * @apiParam (Query) {Boolean} [duplicate=false] 과거에 프로필을 봤던 사용자도 불러올지 여부
 * @apiParam (Query) {Number} [limit=20] 불러올 개수
 * @apiParam (Query) {Number} [page=0] 불러올 페이지 번호.
 * @apiSuccess {User[]} _ 사용자 목록
 * @apiError (Error 403) PurchaseRequired 구매가 필요합니다.
 * @apiUse ReturnUser
 * @apiPermission LoungeRequired
 * @apiUse LoungeRequired
 */
/**
 * @api {get} /users/?mode=newbie 유저 목록 (신입 심사) 가져오기
 * @apiGroup User
 * @apiName GetUserNewbie
 * @apiDescription 사용자 목록을 가져옵니다.
 * @apiParam (Query) {String} mode 정렬 타입. newbie로 고정해 주세요
 * @apiParam (Query) {Number} [limit=20] 불러올 개수
 * @apiSuccess {User[]} _ 사용자 목록
 * @apiUse ReturnUser
 * @apiPermission LoungeRequired
 * @apiUse LoungeRequired
 */
/**
 * @api {get} /users/?mode=admin 유저 목록 (관리자) 가져오기
 * @apiGroup User
 * @apiName GetUserAdmin
 * @apiDescription 사용자 목록을 가져옵니다.
 * @apiParam (Query) {String} mode 정렬 타입. admin으로 고정해 주세요
 * @apiParam (Query) {Number} [limit=20] 불러올 개수
 * @apiParam (Query) {Number} [page=0] 불러올 페이지 번호.
 * @apiParam (Query) {String=name, nickname,rating,createdAt} [sort=createdAt] 정렬 기준
 * @apiParam (Query) {String=asc,desc} [sortOrder=desc] 정렬 방향
 * @apiParam (Query) {String} [nickname] 닉네임 필터
 * @apiParam (Query) {Number} [gender] 성별 필터
 * @apiParam (Query) {Number} [ageMin] 나이 필터
 * @apiParam (Query) {Number} [ageMax] 나이 필터
 * @apiParam (Query) {String} [phone] 전화번호 필터
 * @apiParam (Query) {String} [email] 이메일 필터
 * @apiParam (Query) {Boolean} [isLoungeApproved] 라운지 허용 여부
 * @apiParam (Query) {Boolean} [isAdmin] 관리자 여부
 * @apiParam (Query) {Boolean} [isApproved] 승인 여부
 * @apiParam (Query) {Boolean} [isEnabled] 활성화 여부
 * @apiSuccess {User[]} _ 사용자 목록
 * @apiUse ReturnUser
 * @apiPermission AdminRequired
 * @apiUse AdminRequired
 */
 router.get('/users', loungeRequired, async(async (req, res) => {
  let { mode, limit = 20, page = 0, duplicate } = req.query;
  limit = parseInt(limit) || 0;
  page = parseInt(page) || 0;
  if (limit > 150) limit = 150;
  let where = {};
  let include = [];
  let attributes = [];
  let order = [['createdAt', 'DESC']];
  if (mode === 'lounge') {
    let ids = await PaymentLog.findAll({ where: {
      userId: req.user.id,
      type: 'profileView',
      createdAt: {
        $lt: new Date(Date.now() - getPaymentConfig('profileView').expire),
      },
    } });
    ids = ids.map(v => v.targetId);
    ids.push(req.user.id);
    let followers = await req.user.getFollowers({
      order: [[sequelize.literal('userFollowing.createdAt'), 'DESC']],
      joinTableAttributes: [],
      through: { where: { denied: false } },
      where: { isEnabled: true },
    });
    duplicate = 'true';
    if (['Y', 'true', 'yes', '1'].includes(duplicate)) ids = [req.user.id];
    let followerIds = followers.map(v => v.id);
    ids = ids.concat(followerIds);
    where = {
      approvedAt: { $not: null},
      isLoungeUsed: true,
      isEnabled: true,
      id: { $notIn: ids },
    };
    if(req.user.gender == 1){
      where.isSuggestion = false;
    }
    // attributes = [[sequelize.literal('(((id * 1001347) % 1000679) + ((' +
    //   req.user.id + '* 1000303) % 1001017) % 1000921)'), 'randomToken']];
    // order = [[sequelize.literal('randomToken'), 'ASC']];
    order = [['loggedAt', 'DESC']];
    // Has the user paid for gender thingy?
    if (await checkPayment(req.user, 'loungeGender')) {
      if (req.query.gender != null) where.gender = req.query.gender;
      if (req.query.ageMin != null) {
        if (where.birth == null) where.birth = {};
        where.birth.$gte = new Date(new Date().getFullYear() -
          parseInt(req.query.ageMin) + 1, 0, 1);
      }
      if (req.query.ageMax != null) {
        if (where.birth == null) where.birth = {};
        where.birth.$lte = new Date(new Date().getFullYear() -
          parseInt(req.query.ageMax) + 2, 0, 1);
      }
    } else {
      // If any of these fields exist, throw an error.
      if (['gender', 'ageMin', 'ageMax'].some(v => req.query[v] != null)) {
        throw 'PurchaseRequired';
      }
    }
  } else if (mode === 'newbie') {
    let ids = await UserView.findAll({ where: { viewerId: req.user.id } });
    ids = ids.map(v => v.vieweeId);
    // A simple workaround to avoid NULL
    if (ids.length === 0) ids = [-65535];
    where = {
      hasProfile: true,
      isApproved: true,
      isLoungeUsed: true,
      isEnabled: true,
      gender: 1 - req.user.gender,
      newbieStartedAt: {
        $gte: new Date(Date.now() - loungeConfig.time),
      },
      id: {
        // This is actually bad, however, there is no workaround for
        // this - Sequelize doesn't permit using both LEFT OUTER JOIN and
        // LIMIT query. Until that's fixed, we'll stick to this -
        // besides, the ID table won't be that large anyway.
        $notIn: ids,
      },
    };
    order = [['newbieStartedAt', 'DESC']];
  } else if (mode === 'admin') {
    if (!req.user.isAdmin) throw 'AdminRequired';
    const { sort = 'createdAt', sortOrder = 'DESC' } = req.query;
    order = [[sort, sortOrder]];
    where = pick(req.query, ['name', 'nickname', 'gender', 'phone', 'email',
      'location', 'bloodtype', 'job', 'school', 'alcohol', 'cigarette',
      'personality', 'bodytype', 'religion', 'hasProfile', 'isApproveDenied',
      'hasApproveDeniedBefore', 'isLoungeApproved', 'isAdmin', 'isApproved',
      'isEnabled', 'deviceType','isSuggestion','ignoreLounge']);
    if (where.name != null) {
      where.name = { $like: `%${where.name}%` };
    }
    if (where.nickname != null) {
      where.nickname = { $like: `%${where.nickname}%` };
    }
    if (where.email != null) {
      where.email = { $like: `%${where.email}%` };
    }
    if (req.query.createdFrom != null) {
      where.createdAt = where.createdAt || {};
      where.createdAt.$gte = new Date(req.query.createdFrom);
    }
    if (req.query.createdTo != null) {
      where.createdAt = where.createdAt || {};
      where.createdAt.$lte = new Date(req.query.createdTo);
    }
    if (req.query.ageMin != null) {
      if (where.birth == null) where.birth = {};
      where.birth.$gte = new Date(new Date().getFullYear() -
        parseInt(req.query.ageMin) + 1, 0, 1);
    }
    if (req.query.ageMax != null) {
      if (where.birth == null) where.birth = {};
      where.birth.$lte = new Date(new Date().getFullYear() -
        parseInt(req.query.ageMax) + 2, 0, 1);
    }
    if (!req.user.isAllAccessible){
      where.phone = { $like: "010-%", };
    }
    // Location should include 'or'
    if (where.location != null && where.location.indexOf(',') !== -1) {
      where.location = {
        $in: where.location.split(',').map(v => parseInt(v)),
      };
    }
  } else {
    throw 'ValidationError';
  }
  let result = [];
  if (req.query.genderRatio != null){
    where.gender = 0;
    let males = await User.findAll({
      where,
      include,
      order,
      offset: (limit/2) * page,
      limit: (limit/2),
      attributes: { include: attributes },
    });
    where.gender = 1;
    let females = await User.findAll({
      where,
      include,
      order,
      offset: (limit/2) * page,
      limit: (limit/2),
      attributes: { include: attributes },
    });
    let i = 0;
    while(i<males.length||i<females.length){
      if(males[i]!=null){
        result.push(males[i]);
      }
      if(females[i]!=null){
        result.push(females[i]);
      }
      if(i==(males.length<=females.length?males.length:females.length)) break;
      i = i + 1;
    }
    let genderDiff = Math.abs(females.length-males.length)
    if(1<genderDiff){
      result = [];
    }
  }else{
    result = await User.findAll({
      where,
      include,
      order,
      offset: limit * page,
      limit,
      attributes: { include: attributes },
    });
  }
  if (mode === 'admin') {
    result = await Promise.all(result.map(async(user) =>{
      let data = user.get({ plain: true });
      data.followersCount = await user.countFollowers({
        through: { where: { denied: false } },
        where: {
          isEnabled: true,
        },
      });
      return data;
    }));
  } else if (mode === 'newbie') {
    // Validate each user
    let newResult = [];
    let viewedQuery = [];
    for (let user of result) {
      let data = user.toJSON();
      data.photos = user.getPhotos(req.user);
      data.photoThumbs = user.getPhotoThumbs(req.user);
      newResult.push(data);
      viewedQuery.push({ vieweeId: user.id, viewerId: req.user.id });
    }
    await UserView.bulkCreate(viewedQuery);
    result = newResult;
  } else if (mode === 'lounge') {
    // Validate each user, and strip profile
    let newResult = [];
    for (let user of result) {
      let hasPaid = true;
      if(!user.isLoungeApproved){
        let oldProfile = await user.getProfileHistories({
          limit: 1,
        });
        if(0<oldProfile.length){
          oldProfile = oldProfile[0];
          user.applyOldProfile(oldProfile);
        }
      }
      if (hasPaid) {
        let data = user.toJSON();
        data.photos = user.getPhotos(req.user);
        data.photoThumbs = user.getPhotoThumbs(req.user);
        let votedReport = await UserRating.findOne({ where: {
          voterId: req.user.id,
          voteeId: user.id,
          updatedAt: {
            $gt: user.newbieStartedAt,
          },
        } });
        let where = { voteeId: user.id };
        if(user.newbieStartedAt!=null){
          where.updatedAt = { $gt: user.newbieStartedAt, }
        }
        let votedCount = await UserRating.count({ where: where,});
        data.hasVoted = votedReport != null;
        data.votedCount = votedCount;
        data.votedScore = votedReport && votedReport.score;
        newResult.push(data);
      } else {
        newResult.push({
          id: user.id,
          gender: user.gender,
          nickname: user.nickname,
          photo: user.photo,
          photos: user.getPhotos(req.user),
          photoThumb: user.photoThumb,
          photoThumbs: user.getPhotoThumbs(req.user),
        });
      }
    }
    result = newResult;
  }
  if (req.query.count != null) {
    let count = await User.count({ where });
    res.json({ count, result });
  } else {
    res.json(result);
  }
}));

router.get('/userLogs',adminRequired, async(async (req,res) => {
  let { limit = 20, page = 0 } = req.query;
  limit = parseInt(limit) || 0;
  page = parseInt(page) || 0;
  if (limit > 50) limit = 50;
  let logs = await UserLog.findAll({
    offset: page * limit,
    limit,
  });
  res.json(logs);
}));
router.get('/lounge',loungeRequired, async(async (req,res) => {
  let result = await getLounge(req,res);
  res.json(result);
}));

router.get('/loungeCount', async(async (req,res) => {
  let count = await User.count({
    where : {
      isLoungeApproved: true,
      isLoungeUsed: true,
      isEnabled: true,
    },
  });
  res.json(count);
}));

router.get('/randomLounge',loungeRequired,async(async (req,res) => {
  let result = await User.findAll({
    where : {
      isLoungeApproved: true,
      isLoungeUsed: true,
      isEnabled: true,
      gender: 1,
    },
    order : [Sequelize.fn('RAND'),],
    limit: 5,
  });
  res.json(result);
}));

router.delete('/lounge',loungeRequired, async(async(req,res) =>{
  await doPayment(req.user, 'loungeRefresh');
  let nowHour = new Date(Date.now());
  nowHour = nowHour.setMinutes(0);
  let ids = await UserView.findAll({
   where: {
    viewerId: req.user.id,
    ignore: false,
  } 
});
  for(let id of ids){
    await id.update({ignore:true});
  }
  let result = await getLounge(req,res);
  res.json(result);
}));

async function getLounge(req,res){
  let result = [];
  let nowDate = new Date(Date.now() - 3 * 60 * 60 * 1000);
  nowDate = new Date(nowDate.setHours(0));
  nowDate = new Date(nowDate.setMinutes(0));
  nowDate = new Date(nowDate.setSeconds(0));
  let ids = await UserView.findAll({
    where: {
      viewerId: req.user.id,
      $or: [
      {ignore: true},
      {createdAt: { $lt: nowDate } }
      ],
    } 
  });
  ids = ids.map(v => v.vieweeId);
  ids.push(req.user.id);
  // A simple workaround to avoid NULL
  if (ids.length === 0) ids = [-65535];
  let where = {
    hasProfile: true,
    approvedAt: { $not: null},
    isLoungeUsed: true,
    isEnabled: true,
    ignoreLounge: false,
    gender: 1 - req.user.gender,
    id: {
      // This is actually bad, however, there is no workaround for
      // this - Sequelize doesn't permit using both LEFT OUTER JOIN and
      // LIMIT query. Until that's fixed, we'll stick to this -
      // besides, the ID table won't be that large anyway.
      $notIn: ids,
    },
  };
  let order = [[ Sequelize.fn('RAND',nowDate.getMonth()+''+req.user.id+''+nowDate.getDate()),],['loggedAt', 'DESC']];
  if(req.user.gender==0) where.isSuggestion = true;
  let users = await User.findAll({
    where,
    order,
    limit: 6,
  });
  for(let user of users){
    ids.push(user.id);
    result.push(user);
  }
  where.id.$notIn = ids;
  where.isSuggestion = false;
  users = await User.findAll({
    where,
    order,
    limit: 3,
  });
  for(let user of users){
    ids.push(user.id);
    result.push(user);
  }
  let nowHour = new Date();
  if(!(3<=nowHour.getHours()&&nowHour.getHours()<10)){
    if(req.user.gender==0) where.isSuggestion = true;
    users = await User.findAll({
      where,
      order,
      limit: 6,
    });
    for(let user of users){
      ids.push(user.id);
      result.push(user);
    }
    where.id.$notIn = ids;
    where.isSuggestion = false;
    users = await User.findAll({
      where,
      order,
      limit: 3,
    });
    for(let user of users){
      ids.push(user.id);
      result.push(user);
    }
  }
  let newResult = [];
  for (let user of result) {
    let hasPaid = await checkPayment(req.user, 'profileView', user.id);
    if(!user.isLoungeApproved){
      let oldProfile = await user.getProfileHistories({
        limit: 1,
      });
      if(0<oldProfile.length){
        oldProfile = oldProfile[0];
        user.applyOldProfile(oldProfile);
      }
    }
    let data = user.toJSON();
    data.photos = user.getPhotos(req.user);
    data.photoThumbs = user.getPhotoThumbs(req.user);
    let votedReport = await UserRating.findOne({ where: {
      voterId: req.user.id,
      voteeId: user.id,
      updatedAt: {
        $gt: user.newbieStartedAt,
      },
    } });
    let where = { voteeId: user.id };
    if(user.newbieStartedAt!=null){
      where.updatedAt = { $gt: user.newbieStartedAt, }
    }
    let votedCount = await UserRating.count({ where: where,});
    data.hasVoted = votedReport != null;
    data.votedCount = votedCount;
    data.votedScore = votedReport && votedReport.score;
    data.hasPaid = hasPaid;
    newResult.push(data);
  }
  result = newResult;
  let viewedQuery = [];
  let viewIds = await UserView.findAll({
    where: {
      viewerId: req.user.id,
    },
  });
  viewIds = viewIds.map(v=>v.vieweeId);
  for (let user of result) {
    if(viewIds.indexOf(user.id)!=-1){
      continue;  
    }
    viewedQuery.push({ vieweeId: user.id, viewerId: req.user.id });
  }
  if(0<viewedQuery.length){
    await UserView.bulkCreate(viewedQuery);
  }
  return result;
}
