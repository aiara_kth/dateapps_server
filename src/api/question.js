import { Router } from 'express';

import async from './util/async';
import loginRequired from './util/loginRequired';
import adminRequired from './util/adminRequired';
import photoUpload from './util/photoUpload';
import sendNotification from './util/notification';
import { validateSingle, sanitize } from '../validation/validate';
import * as validationSchema from '../validation/schema';

import { Question, User } from '../db';

export const router = new Router();
export default router;

function checkModifiable(req, res, next) {
  if (req.user == null) return next('LoginRequired');
  if (req.user.isAdmin) return next();
  if (req.user.id !== req.selectedQuestion.userId) return next('AdminRequired');
  return next();
}

/**
 * @apiDefine ReturnQuestion
 * @apiSuccess {Number} _.id 고유 번호
 * @apiSuccess {Date} _.createdAt 생성 시각
 * @apiSuccess {Date} _.updatedAt 업데이트 시각
 * @apiSuccess {String} _.category 카테고리
 * @apiSuccess {String} _.title 제목
 * @apiSuccess {String} _.body 내용
 * @apiSuccess {User} _.user 질문자
 * @apiSuccess {String[]} _.photos 사진
 * @apiSuccess {String[]} _.photoThumbs 사진 썸네일
 * @apiSuccess {Date} _.answeredAt 답변 시각
 * @apiSuccess {String} _.answerTitle 답변 제목
 * @apiSuccess {String} _.answerBody 답변 내용
 * @apiSuccess {String[]} _.answerPhotos 답변 사진
 * @apiSuccess {String[]} _.answerPhotoThumbs 답변 사진 썸네일
 */

/**
 * @apiDefine GetQuestion
 * @apiParam (Parameter) {Number} questionId 질문 번호
 * @apiError (Error 404) NotFound 지정된 객체를 찾을 수 없습니다.
 */

export const questionRouter = new Router();

/**
 * @api {get} /questions/:questionId/ 질문 가져오기
 * @apiGroup Question
 * @apiName GetQuestion
 * @apiDescription 질문을 가져옵니다.
 * @apiUse GetQuestion
 * @apiSuccess {Question} _ 질문
 * @apiUse ReturnQuestion
 * @apiPermission Modifiable
 * @apiUse Modifiable
 */
questionRouter.get('/', async(async (req, res) => {
  res.json(req.selectedQuestion);
}));

/**
 * @api {patch} /questions/:questionId/ 질문 수정하기
 * @apiGroup Question
 * @apiName PatchQuestion
 * @apiDescription 질문을 수정합니다.
 *
 *   사진 업로드를 하기 위해서는 `multipart/form-data`를 사용해 주세요. 사진이 여기를
 *   사용해서 업로드된 경우 이미 있던 사진들이 전부 삭제됩니다. 이를 원하지 않는다면
 *   `/questions/:questionId/photos` 엔드포인트를 사용해 주세요.
 *
 * @apiUse GetQuestion
 * @apiParam (Body) {String} title 제목
 * @apiParam (Body) {String} category 카테고리
 * @apiParam (Body) {String} body 내용
 * @apiParam (Body) {File[]} photos 사진
 * @apiSuccess {Question} _ 질문
 * @apiUse ReturnQuestion
 * @apiPermission Modifiable
 * @apiUse Modifiable
 */
questionRouter.patch('/', checkModifiable, photoUpload,
async(async (req, res) => {
  // Run validation
  let validationError = validateSingle(
    Object.assign({}, req.selectedQuestion.toJSON(), req.body),
    validationSchema.Question);
  if (validationError != null) {
    throw { name: 'ValidationError', data: validationError };
  }
  let sanitizedData = sanitize(req.body, validationSchema.Question);
  if (req.photos != null && req.photos.length > 0) {
    sanitizedData.photos = req.photos || [];
    if (sanitizedData.photos.length > 8) throw 'PhotoThreshold';
  }
  // Update
  await req.selectedQuestion.update(sanitizedData);
  res.json(req.selectedQuestion);
}));

/**
 * @api {delete} /questions/:questionId/ 질문 삭제하기
 * @apiGroup Question
 * @apiName DeleteQuestion
 * @apiDescription 질문을 삭제합니다.
 * @apiUse GetQuestion
 * @apiPermission Modifiable
 * @apiUse Modifiable
 */
questionRouter.delete('/', checkModifiable, async(async (req, res) => {
  await req.selectedQuestion.destroy();
  res.json({});
}));

/**
 * @api {get} /questions/:questionId/photos 질문 사진 가져오기
 * @apiGroup Question
 * @apiName GetQuestionPhotos
 * @apiDescription 질문의 사진 목록을 가져옵니다.
 * @apiUse GetQuestion
 * @apiSuccess {String[]} photos 사진 배열
 * @apiSuccess {String[]} photoThumbs 사진 썸네일 배열
 * @apiPermission Modifiable
 * @apiUse Modifiable
 */
questionRouter.get('/photos', async(async (req, res) => {
  const { photos, photoThumbs } = req.selectedQuestion;
  res.json({ photos, photoThumbs });
}));

/**
 * @api {post} /questions/:questionId/photos 질문 사진 올리기
 * @apiGroup Question
 * @apiName PostQuestionPhotos
 * @apiDescription 질문의 사진을 추가로 업로드합니다.
 * @apiUse GetQuestion
 * @apiParam (Body) {File[]} photos 사진
 * @apiSuccess {String[]} photos 사진 배열
 * @apiSuccess {String[]} photoThumbs 사진 썸네일 배열
 * @apiError (Error 400) PhotoInvalid 올바른 사진 파일이 아닙니다.
 * @apiError (Error 403) PhotoThreshold 사진의 최대 개수를 초과했습니다.
 * @apiPermission Modifiable
 * @apiUse Modifiable
 */
questionRouter.post('/photos', checkModifiable, photoUpload,
async(async (req, res) => {
  let question = req.selectedQuestion;
  let photos = question.photos.concat(req.photos || []);
  if (photos.length > 8) throw 'PhotoThreshold';
  await question.update({ photos });
  const { photoThumbs } = question;
  res.json({ photos, photoThumbs });
}));

/**
 * @api {delete} /questions/:questionId/photos 질문 사진 전부 삭제하기
 * @apiGroup Question
 * @apiName DeleteQuestionPhotos
 * @apiDescription 질문의 사진을 전부 삭제합니다.
 * @apiUse GetQuestion
 * @apiSuccess {String[]} photos 사진 배열
 * @apiSuccess {String[]} photoThumbs 사진 썸네일 배열
 * @apiPermission Modifiable
 * @apiUse Modifiable
 */
questionRouter.delete('/photos', checkModifiable, async(async (req, res) => {
  let post = req.selectedQuestion;
  await post.update({ photos: [] });
  const { photos, photoThumbs } = post;
  res.json({ photos, photoThumbs });
}));

/**
 * @api {post} /questions/:questionId/photos/:photoId 질문 사진 삭제하기
 * @apiGroup Question
 * @apiName DeleteQuestionPhoto
 * @apiDescription n번째 사진을 삭제합니다.
 * @apiParam (Parameter) {Number} photoId 삭제할 사진 번호
 * @apiSuccess {String[]} photos 사진 배열
 * @apiSuccess {String[]} photoThumbs 사진 썸네일 배열
 * @apiUse GetQuestion
 * @apiPermission Modifiable
 * @apiUse Modifiable
 */
questionRouter.delete('/photos/:photoId', checkModifiable,
async(async (req, res) => {
  let index = parseInt(req.params.photoId);
  if (isNaN(index)) throw 'ValidationError';
  let post = req.selectedQuestion;
  await post.update({
    photos: post.photos.filter((_, i) => i !== index),
  });
  const { photos, photoThumbs } = post;
  res.json({ photos, photoThumbs });
}));


/**
 * @api {patch} /questions/:questionId/answer 답변 달기/수정하기
 * @apiGroup Question
 * @apiName PatchAnswer
 * @apiDescription 답변을 수정합니다.
 *
 *   사진 업로드를 하기 위해서는 `multipart/form-data`를 사용해 주세요. 사진이 여기를
 *   사용해서 업로드된 경우 이미 있던 사진들이 전부 삭제됩니다. 이를 원하지 않는다면
 *   `/questions/:questionId/answer/photos` 엔드포인트를 사용해 주세요.
 *
 * @apiUse GetQuestion
 * @apiParam (Body) {String} answerTitle 제목
 * @apiParam (Body) {String} answerBody 내용
 * @apiParam (Body) {File[]} photos 사진
 * @apiSuccess {Question} _ 질문
 * @apiUse ReturnQuestion
 * @apiPermission AdminRequired
 * @apiUse AdminRequired
 */
questionRouter.patch('/answer', adminRequired, photoUpload,
async(async (req, res) => {
  // Run validation
  let validationError = validateSingle(
    Object.assign({}, req.selectedQuestion.toJSON(), req.body),
    validationSchema.QuestionAnswer);
  if (validationError != null) {
    throw { name: 'ValidationError', data: validationError };
  }
  let sanitizedData = sanitize(req.body, validationSchema.QuestionAnswer);
  if (req.photos != null && req.photos.length > 0) {
    sanitizedData.answerPhotos = req.photos || [];
    if (sanitizedData.answerPhotos.length > 8) throw 'PhotoThreshold';
  }
  sanitizedData.answeredAt = new Date();
  if (req.selectedQuestion.answeredAt == null) {
    // Send notification
    sendNotification(req.selectedQuestion.user, {
      type: 'questionAnswer',
      question: req.selectedQuestion,
    });
  }
  // Update
  await req.selectedQuestion.update(sanitizedData);
  res.json(req.selectedQuestion);
}));

/**
 * @api {post} /questions/:questionId/answer/photos/ 답변 사진 올리기
 * @apiGroup Question
 * @apiName PostAnswerPhotos
 * @apiDescription 답변의 사진을 추가로 업로드합니다.
 * @apiUse GetQuestion
 * @apiParam (Body) {File[]} photos 사진
 * @apiSuccess {String[]} answerPhotos 사진 배열
 * @apiSuccess {String[]} answerPhotoThumbs 사진 썸네일 배열
 * @apiError (Error 400) PhotoInvalid 올바른 사진 파일이 아닙니다.
 * @apiError (Error 403) PhotoThreshold 사진의 최대 개수를 초과했습니다.
 * @apiPermission AdminRequired
 * @apiUse AdminRequired
 */
questionRouter.post('/answer/photos', adminRequired, photoUpload,
async(async (req, res) => {
  let question = req.selectedQuestion;
  let answerPhotos = question.answerPhotos.concat(req.photos || []);
  if (answerPhotos.length > 8) throw 'PhotoThreshold';
  await question.update({ answerPhotos });
  const { answerPhotoThumbs } = question;
  res.json({ answerPhotos, answerPhotoThumbs });
}));

/**
 * @api {delete} /questions/:questionId/answer/photos 답변 사진 전부 삭제하기
 * @apiGroup Question
 * @apiName DeleteAnswerPhotos
 * @apiDescription 답변의 사진을 전부 삭제합니다.
 * @apiUse GetQuestion
 * @apiSuccess {String[]} answerPhotos 사진 배열
 * @apiSuccess {String[]} answerPhotoThumbs 사진 썸네일 배열
 * @apiPermission AdminRequired
 * @apiUse AdminRequired
 */
questionRouter.delete('/answer/photos', adminRequired,
async(async (req, res) => {
  let post = req.selectedQuestion;
  await post.update({ answerPhotos: [] });
  const { answerPhotos, answerPhotoThumbs } = post;
  res.json({ answerPhotos, answerPhotoThumbs });
}));

/**
 * @api {post} /questions/:questionId/answer/photos/:photoId 답변 사진 삭제하기
 * @apiGroup Question
 * @apiName DeleteAnswerPhoto
 * @apiDescription n번째 사진을 삭제합니다.
 * @apiParam (Parameter) {Number} photoId 삭제할 사진 번호
 * @apiSuccess {String[]} answerPhotos 사진 배열
 * @apiSuccess {String[]} answerPhotoThumbs 사진 썸네일 배열
 * @apiUse GetQuestion
 * @apiPermission AdminRequired
 * @apiUse AdminRequired
 */
questionRouter.delete('/answer/photos/:photoId', adminRequired,
async(async (req, res) => {
  let index = parseInt(req.params.photoId);
  if (isNaN(index)) throw 'ValidationError';
  let post = req.selectedQuestion;
  await post.update({
    answerPhotos: post.answerPhotos.filter((_, i) => i !== index),
  });
  const { answerPhotos, answerPhotoThumbs } = post;
  res.json({ answerPhotos, answerPhotoThumbs });
}));

router.use('/questions/:questionId', async(async (req, res, next) => {
  let question = await Question.findById(req.params.questionId, {
    include: [ User ],
  });
  if (question == null) throw 'NotFound';
  req.selectedQuestion = question;
  next();
}), checkModifiable, questionRouter);

/**
 * @api {get} /questions/ 질문 목록 가져오기
 * @apiGroup Question
 * @apiName GetQuestions
 * @apiDescription 질문 목록을 가져옵니다.
 * @apiParam (Query) {String} [title] 제목 필터입니다.
 * @apiParam (Query) {String} [recipient] 질문을 받는 사용자의 ID값
 * @apiParam (Query) {String} [userId] 질문을 한 사용자의 ID값
 * @apiParam (Query) {Boolean} [answered] 답변 여부입니다.
 * @apiParam (Query) {Number} [limit=20] 불러올 개수입니다.
 * @apiParam (Query) {Number} [page=0] 불러올 페이지 번호입니다.
 * @apiSuccess {Question[]} _ 질문 목록
 * @apiUse ReturnQuestion
 * @apiPermission loginRequired
 * @apiUse loginRequired
 */
router.get('/questions', loginRequired, async(async (req, res) => {
  let { limit = 20, page = 0 } = req.query;
  limit = parseInt(limit) || 0;
  page = parseInt(page) || 0;
  if (limit > 50) limit = 50;
  let where = {};
  if (req.query.title != null) {
    where.title = { $like: `%${req.query.title}%` };
  }
  if (req.query.category != null) {
    where.category = req.query.category;
  }
  if (req.query.answered != null) {
    where.answeredAt = req.query.answered === '1' ? { $not: null } : null;
  }
  if (req.query.userId != null) {
    where.userId = req.query.userId;
  }
  if (req.query.recipient != null) {
    where.recipient = req.query.recipient;
  }
  let result = await Question.findAll({
    where,
    order: [['createdAt', 'DESC']],
    offset: limit * page,
    limit,
    include: [{
      model: User,
      attributes: ['id', 'name', 'nickname', 'photos', 'photoPrimary'],
    }],
  });
  if (req.query.count != null) {
    let count = await Question.count({ where });
    res.json({ count, result });
  } else {
    res.json(result);
  }
}));

/**
 * @api {post} /questions/ 질문 올리기
 * @apiGroup Question
 * @apiName PostQuestions
 * @apiDescription 질문을 올립니다.
 *
 *   사진 업로드를 하기 위해서는 `multipart/form-data`를 사용해 주세요.
 *
 * @apiParam (Body) {String} title 제목 (내용만 보내려면 질문을 받는 사용자의 onequestion값)
 * @apiParam (Body) {String} recipient 질문을 받는 사용자의 ID값
 * @apiParam (Body) {String} body 내용
 * @apiParam (Body) {File[]} photos 사진
 * @apiSuccess {Question} _ 질문
 * @apiUse ReturnQuestion
 * @apiPermission LoginRequired
 * @apiUse LoginRequired
 */
router.post('/questions', loginRequired, photoUpload,
async(async (req, res) => {
  // Run validation
  let validationError = validateSingle(req.body, validationSchema.Question);
  if (validationError != null) {
    throw { name: 'ValidationError', data: validationError };
  }
  let sanitizedData = sanitize(req.body, validationSchema.Question);
  if (req.photos != null && req.photos.length > 0) {
    sanitizedData.photos = req.photos || [];
    if (sanitizedData.photos.length > 8) throw 'PhotoThreshold';
  }
  sanitizedData.userId = req.user.id;
  // Create
  let question = await Question.create(sanitizedData);
  let data = question.toJSON();
  data.user = req.user;
  res.json(data);
}));
