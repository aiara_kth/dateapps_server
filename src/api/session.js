import { Router } from 'express';
import randToken from 'rand-token';

import async from './util/async';
import bcrypt from './util/bcrypt';
import loginRequired from './util/loginRequired';
import sendSMS from './util/sendSMS';
import injectUserInfo from './util/injectUserInfo';
import { setUserBearer, setUser } from './middleware/injectUser';

import { validateSingle, sanitize } from '../validation/validate';
import * as validationSchema from '../validation/schema';

import { auth as authConfig } from '../../config';

import { sequelize, User, Passport, OngoingPhoneAuth } from '../db';
import firebase from '../firebase';

import { updateInfo } from './user'

const firebaseDB = firebase.database();

export const router = new Router();
export default router;

/**
 * @apiDefine ReturnUser
 * @apiSuccess {Number} _.id 고유 번호
 * @apiSuccess {String} _.photo 대표 사진
 * @apiSuccess {String} _.photoThumb 대표 사진 썸네일
 * @apiSuccess {NumberNullable} _.photoPrimary 메인 사진 번호
 * @apiSuccess {Number} _.gender 성별
 * @apiSuccess {Boolean} _.isAdmin 관리자 여부
 * @apiSuccess {StringNullable} _.name 이름
 * @apiSuccess {StringNullable} _.nickname 닉네임
 * @apiSuccess {NumberNullable} _.location 위치
 * @apiSuccess {StringNullable} _.birth 생년월일
 * @apiSuccess {StringNullable} _.job 직업
 * @apiSuccess {StringNullable} _.school 학교
 * @apiSuccess {NumberNullable} _.alcohol 음주
 * @apiSuccess {NumberNullable} _.cigarette 흡연
 * @apiSuccess {NumberNullable} _.personality 성격
 * @apiSuccess {NumberNullable} _.bloodtype 혈액형
 * @apiSuccess {NumberNullable} _.bodytype 체형
 * @apiSuccess {NumberNullable} _.height 키
 * @apiSuccess {NumberNullable} _.religion 종교
 * @apiSuccess {NumberNullable} _.interior 가장 살고싶은 인테리어(ID값)
 * @apiSuccess {StringNullable} _.introduct_word1 소개단어1
 * @apiSuccess {StringNullable} _.introduct_word2 소개단어2
 * @apiSuccess {StringNullable} _.introduct_word3 소개단어3
 * @apiSuccess {StringNullable} _.onequestion 핵심 질문
 * @apiSuccess {StringNullable} _.introduction 자기소개
 * @apiSuccess {StringNullable} _.onequestion 한가지 질문
 * @apiSuccess {Boolean} _.isLoungeApproved 라운지 승인 여부
 * @apiSuccess {Boolean} _.isLoungeUsed 라운지 사용 여부
 * @apiSuccess {Boolean} _.updatePush 푸시 알림 사용 여부
 * @apiSuccess {Boolean} _.hasProfile 프로필 보유 여부
 * @apiSuccess {Boolean} _.isApproved 계정 승인 여부
 * @apiSuccess {Boolean} _.isApproveDenied 계정 승인 거절 여부
 * @apiSuccess {Boolean} _.newbiePending 신입 심사 진행 중 여부
 * @apiSuccess {Boolean} _.usePhotoInGroup 그룹에서 사진 사용 여부
 * @apiSuccess {Number} _.diamonds 다이아 개수
 * @apiSuccess {Number} _.rating 별점
 * @apiSuccess {Date} _.createdAt 계정 생성 시각
 * @apiSuccess {Date} _.updatedAt 계정 업데이트 시각
 */

/**
 * @apiDefine ReturnUserSimple
 * @apiSuccess {Number} _.id 고유 번호
 * @apiSuccess {String} _.photo 대표 사진
 * @apiSuccess {String} _.photoThumb 대표 사진 썸네일
 * @apiSuccess {NumberNullable} _.photoPrimary 메인 사진 번호
 * @apiSuccess {StringNullable} _.nickname 닉네임
 */

/**
 * @apiDefine UserNotFound
 * @apiError (Error 404) UserNotFound 입력하신 사용자를 찾을 수 없습니다.
 */

/**
 * @apiDefine LoginRequired 로그인 필요
 *   사용하려면 로그인해야 합니다.
 * @apiError (Error 401) LoginRequired 계속하려면 로그인하셔야 합니다.
 * @apiError (Error 403) UserDisabled 비활성화된 사용자입니다.
 */

/**
 * @apiDefine ProfileRequired 프로필 필요
 *   사용하려면 프로필을 작성해야 합니다.
 * @apiError (Error 401) LoginRequired 계속하려면 로그인하셔야 합니다.
 * @apiError (Error 403) ProfileRequired 프로필을 작성해야 합니다.
 * @apiError (Error 403) UserDisabled 비활성화된 사용자입니다.
 */

/**
 * @apiDefine ApproveRequired 관리자 승인 필요
 *   사용하려면 관리자의 승인이 필요합니다.
 * @apiError (Error 401) LoginRequired 계속하려면 로그인하셔야 합니다.
 * @apiError (Error 403) ApproveRequired 관리자의 승인이 필요합니다.
 * @apiError (Error 403) ProfileRequired 프로필을 작성해야 합니다.
 * @apiError (Error 403) UserDisabled 비활성화된 사용자입니다.
 */

/**
 * @apiDefine LoungeRequired 라운지 승인 필요
 *   사용하려면 라운지 승인이 필요합니다.
 * @apiError (Error 401) LoginRequired 계속하려면 로그인하셔야 합니다.
 * @apiError (Error 403) LoungeRequired 라운지 승인이 필요합니다.
 * @apiError (Error 403) ApproveRequired 관리자의 승인이 필요합니다.
 * @apiError (Error 403) ProfileRequired 프로필을 작성해야 합니다.
 * @apiError (Error 403) UserDisabled 비활성화된 사용자입니다.
 */

/**
 * @api {get} /session/ 사용자 세션 가져오기
 * @apiGroup Session
 * @apiName GetSession
 * @apiDescription 현재 사용자 객체를 가져옵니다.
 *
 *   **가능하면 `/user/`를 사용해 주세요.**
 *   사용자가 로그인하지 않은 경우 401을 반환합니다.
 *
 * @apiPermission LoginRequired
 * @apiUse LoginRequired
 * @apiSuccess {User} _ 사용자 정보
 * @apiUse ReturnUser
 */
router.get('/session', loginRequired, async(async (req, res) => {
  res.json(await injectUserInfo(req.user));
}));

/**
 * @api {post} /session/ 사용자 로그인
 * @apiGroup Session
 * @apiName LoginSession
 * @apiDescription 로그인하고 사용자 객체를 가져옵니다.
 *
 *   로그인에 실패한 경우 401을 반환합니다.
 *
 * @apiParam (Body) {String} email 사용자의 이메일 주소
 * @apiParam (Body) {String} password 사용자의 비밀번호
 * @apiParam (Query) {boolean} [session=false] 토큰 대신 쿠키 사용 여부
 * @apiSuccess {String} access_token 액세스 토큰
 * @apiSuccess {String} firebaseToken Firebase 토큰
 * @apiSuccess {User} _ 사용자 정보
 * @apiUse UserNotFound
 * @apiError (Error 401) PasswordInvalid 비밀번호가 틀렸습니다.
 * @apiUse ReturnUser
 */
router.post('/session', async(async (req, res) => {
  const { email, password, deviceType } = req.body;
  // Get passport with corresponding username
  let passport =
    await Passport.findOne({ where: { type: 'local', identifier: email } });
  if (passport == null) throw 'UserNotFound';
  // Check password
  let passwordMatch = await bcrypt.compare(password, passport.data);
  if (!passwordMatch) throw 'PasswordInvalid';
  // Get user of the passport
  let user = await passport.getUser();
  if (user == null) throw 'UserNotFound';
  // Set user to session
  let output = await injectUserInfo(user);
  if (req.query.session) {
    setUser.call(req, user);
  } else {
    let accessToken = await setUserBearer.call(req, user);
    output.access_token = accessToken;
  }
  await user.update({ loggedAt: new Date() });
	if (deviceType !== null){
	  await user.update({ deviceType });
	}
  let firebaseToken =
    await firebase.auth().createCustomToken(String(req.user.id));
  output.firebaseToken = firebaseToken;
  res.json(output);
}));

/**
 * @api {delete} /session/ 사용자 로그아웃
 * @apiGroup Session
 * @apiName LogoutSession
 * @apiDescription 로그아웃합니다.
 * @apiPermission LoginRequired
 * @apiUse LoginRequired
 */
router.delete('/session', loginRequired, (req, res) => {
  req.setUser(null);
  res.json({});
});

/**
 * @api {post} /session/register 사용자 회원가입
 * @apiGroup Session
 * @apiName RegisterSession
 * @apiDescription 회원가입하고 사용자 객체를 가져옵니다.
 *
 *   로그인에 실패한 경우 401을 반환합니다.
 *
 * @apiParam (Body) {String} email 사용자의 이메일 주소
 * @apiParam (Body) {String} password 사용자의 비밀번호
 * @apiParam (Body) {number=0,1} gender 사용자의 성별
 * @apiParam (Body) {String="010-0000-0000"} phone 사용자의 휴대전화 번호
 * @apiParam (Body) {String="000000"} phoneCode 사용자의 휴대전화 인증 키
 * @apiParam (Body) {String} deviceType 장치 타입 (android, iphone)
 * @apiParam (Body) {String} year 나이(20xx년)
 * @apiParam (Body) {String=facebook,instalgram,kakao,normal} loginfrom SNS로그인타입
 * @apiParam (Body) {number=0,1} phoneAuthCheck 휴대폰인증여부 (값이 1이면 인증은 생략)
 * @apiParam (Body) {number=0,1} nicknameCheck 닉네임 중복확인여부 (값이 1이면 중복확인 생략)
 * @apiParam (Body) {number} interior 인테리어 ID값
 * @apiParam (Body) {String} introduction 소개
 * @apiParam (Body) {String} introduct_word1 소개단어1
 * @apiParam (Body) {String} introduct_word2 소개단어2
 * @apiParam (Body) {String} introduct_word3 소개단어3
 * @apiParam (Body) {String} onequestion 핵심 질문
 * @apiParam (Query) {boolean} [session=false] 토큰 대신 쿠키 사용 여부
 * @apiSuccess {String} access_token 액세스 토큰
 * @apiSuccess {String} firebaseToken Firebase 토큰
 * @apiSuccess {User} _ 사용자 정보
 * @apiError (Error 401) PhoneInvalid 휴대전화 인증에 실패했습니다.
 * @apiError (Error 403) PhoneTimeout 휴대전화 인증 시간이 지났습니다.
 * @apiError (Error 409) EmailConflict 이미 해당 이메일 주소를 가진 사용자가 있습니다.
 * @apiError (Error 409) PhoneConflict 이미 해당 휴대전화 번호를 가진 사용자가 있습니다.
 * @apiError (Error 400) ValidationError 필드의 형식이 잘못되었습니다.
 * @apiUse ReturnUser
 */
router.post('/session/register', async(async (req, res) => {
  const { email, password, gender, phone, phoneCode, deviceType, loginfrom, name, nickname, year, birth, location, introduction, introduct_word1, introduct_word2, introduct_word3, onequestion, interior, phoneAuthCheck, nicknameCheck} = req.body;
  const userData = { email,gender, phone, phoneCode, deviceType, loginfrom, name, nickname, year, birth, location, introduction, introduct_word1, introduct_word2, introduct_word3, onequestion, interior};

    // Do field validation first
    let validationError = validateSingle(userData, validationSchema.User);
  if (validationError != null) {
    throw { name: 'ValidationError', data: validationError };
  }
	let santizedUser = sanitize(userData, validationSchema.User);

    if(nicknameCheck == null || nicknameCheck != '1' || nicknameCheck != 1){
        let nicknameDuplicate = await User.findOne({ where: { nickname } });
        if(nicknameDuplicate != null) throw 'NicknameConflict';
    }

  // TODO Do password validation
  let user = await sequelize.transaction(async t => {
    // Phone verification
      if(phoneAuthCheck == null || phoneAuthCheck != '1' || phoneAuthCheck != 1){
          let phoneAuth = await OngoingPhoneAuth.findOne({
              where: { phone, code: phoneCode } }, { transaction: t });
          if (phoneAuth == null) throw 'PhoneInvalid';
          if ((Date.now() - phoneAuth.updatedAt.getTime()) >= 3 * 60 * 1000) {
              throw 'PhoneTimeout';
          }
          if (phoneAuth.code == null) throw 'PhoneInvalid';
          await phoneAuth.destroy({ transaction: t });

      }

    // Create user
    let user = await User.create(santizedUser, { transaction: t });
    // Create passport
    let salt = await bcrypt.genSalt(authConfig.bcrypt);
    let hash = await bcrypt.hash(password, salt);
    await Passport.create({
      userId: user.id,
      type: 'local',
      identifier: email,
      data: hash,
    }, { transaction: t });
    // All done.
    return user;
  });
  // Set session's user to current user and send result.
  let output = injectUserInfo(user);
  if (req.query.session) {
    setUser.call(req, user);
  } else {
    let accessToken = await setUserBearer.call(req, user);
    output.access_token = accessToken;
  }
  let firebaseToken =
    await firebase.auth().createCustomToken(String(user.id));
  output.firebaseToken = firebaseToken;
  res.json(output);
}));

/**
 * @api {post} /session/email 이메일 중복 검사
 * @apiGroup Session
 * @apiName EmailVerify
 * @apiDescription 이메일이 중복되지 않았는지 확인합니다.
 * @apiParam (Body) {String} email 사용자의 이메일 주소
 * @apiError (Error 400) ValidationError 필드의 형식이 잘못되었습니다.
 * @apiError (Error 409) EmailConflict 이미 해당 이메일 주소를 가진 사용자가 있습니다.
 */
router.post('/session/email', async(async (req, res) => {
  const { email } = req.body;
  // Check if email address already belongs to another user
  let belongUser = await User.findOne({ where: { email } });
  if (belongUser != null) throw 'EmailConflict';
  // Validate phone number
  let validationError = validateSingle({ email },
    { email: validationSchema.User.email });
  if (validationError != null) {
    throw { name: 'ValidationError', data: validationError };
  }
  // Done
  res.json({});
}));

/**
 * @api {post}  닉네임 중복 검사
 * @apiGroup Session
 * @apiName NicknameVerify
 * @apiDescription 닉네임  중복되지 않았는지 확인합니다.
 * @apiParam (Body) {String} email 사용자의 이메일 주소
 * @apiError (Error 400) ValidationError 필드의 형식이 잘못되었습니다.
 * @apiError (Error 409) NicknameConflict 이미 해당 닉네임를 가진 사용자가 있습니다.
 */
router.post('/session/nickname', async(async (req, res) => {
    const { nickname } = req.body;
    // Check if email address already belongs to another user
    let belongUser = await User.findOne({ where: { nickname } });
    if (belongUser != null) throw 'NicknameConflict';
    // Validate phone number
    let validationError = validateSingle({ nickname },
        { email: validationSchema.User.nickname});
    if (validationError != null) {
        throw { name: 'ValidationError', data: validationError };
    }
    // Done
    res.json({});
}));


/**
 * @api {post} /session/phone 휴대폰 인증
 * @apiGroup Session
 * @apiName PhoneVerify
 * @apiDescription 휴대폰 인증 메시지를 보냅니다.
 * @apiParam (Body) {String="010-0000-0000"} phone 사용자의 휴대전화 번호
 * @apiError (Error 403) PhoneThrottle 잠시 후 다시 시도하세요.
 * @apiError (Error 400) ValidationError 필드의 형식이 잘못되었습니다.
 * @apiError (Error 409) PhoneConflict 이미 해당 휴대전화 번호를 가진 사용자가 있습니다.
 */
router.post('/session/phone', async(async (req, res) => {
  const { phone } = req.body;
  // Check if phone number already belongs to another user
  let belongUser = await User.findOne({ where: { phone } });
  if (belongUser != null) throw 'PhoneConflict';
  // Validate phone number
  let validationError = validateSingle({ phone },
    { phone: validationSchema.User.phone });
  if (validationError != null) {
    throw { name: 'ValidationError', data: validationError };
  }
  // Check if phone auth exists
  let phoneAuth = await OngoingPhoneAuth.findOne({ where: { phone } });
  let shouldReset = phoneAuth == null || !(phoneAuth.resetAt != null &&
    (Date.now() - phoneAuth.resetAt.getTime()) < 60 * 60 * 1000);
  if (phoneAuth != null && !shouldReset && phoneAuth.count >= 10) {
    throw 'PhoneThrottle';
  }
  // Generate code
  let code = randToken.generate(6, '0123456789');
  await sendSMS(phone, `[에스티커] 인증번호는 ${code}입니다.`);
  // Save phone code
  if (phoneAuth != null) {
    if (shouldReset) {
      await phoneAuth.update({ code, count: 0, resetAt: new Date() });
    } else {
      await phoneAuth.update({ code, count: phoneAuth.count + 1 });
    }
  } else await OngoingPhoneAuth.create({ phone, code });
  // Done
  res.json({});
}));

/**
 * @api {post} /session/phoneCode 휴대폰 인증 확인
 * @apiGroup Session
 * @apiName PhoneCodeVerify
 * @apiDescription 휴대폰 인증 코드가 일치하는지 확인합니다.
 * @apiParam (Body) {String="010-0000-0000"} phone 사용자의 휴대전화 번호
 * @apiParam (Body) {String="000000"} phoneCode 사용자의 휴대전화 인증 키
 * @apiError (Error 401) PhoneInvalid 휴대전화 인증에 실패했습니다.
 * @apiError (Error 403) PhoneTimeout 휴대전화 인증 시간이 지났습니다.
 */
router.post('/session/phoneCode', async(async (req, res) => {
  const { phone, phoneCode } = req.body;
  // Phone verification
  let phoneAuth = await OngoingPhoneAuth.findOne({
    where: { phone, code: phoneCode } });
  if (phoneAuth == null) throw 'PhoneInvalid';
  if ((Date.now() - phoneAuth.updatedAt.getTime()) >= 3 * 60 * 1000) {
    throw 'PhoneTimeout';
  }
  if (phoneAuth.code == null) throw 'PhoneInvalid';
  // Done
  res.json({});
}));

/**
 * @api {patch} /session/password 비밀번호 변경
 * @apiGroup Session
 * @apiName ChangePassword
 * @apiDescription 비밀번호를 변경합니다.
 * @apiParam (Body) {String} password 사용자의 비밀번호
 * @apiParam (Body) {String} newPassword 새로 사용할 비밀번호
 * @apiError (Error 401) PasswordInvalid 비밀번호가 틀렸습니다.
 * @apiUse LoginRequired
 * @apiPermission LoginRequired
 */
router.patch('/session/password', loginRequired, async(async (req, res) => {
  const { password, newPassword } = req.body;
  // Get passport with corresponding username
  let [passport] = await req.user.getPassports({ where: { type: 'local' } });
  if (passport == null) throw new Error('User passport does not exist');
  // Check password
  let passwordMatch = await bcrypt.compare(password, passport.data);
  if (!passwordMatch) throw 'PasswordInvalid';
  // Update passport
  let salt = await bcrypt.genSalt(authConfig.bcrypt);
  let hash = await bcrypt.hash(newPassword, salt);
  await passport.update({ data: hash });
  res.json({});
}));

/**
 * @api {post} /session/find/password 비밀번호 찾기
 * @apiGroup Session
 * @apiName FindPassword
 * @apiDescription 휴대전화로 임시 비밀번호를 보냅니다.
 * @apiParam (Body) {String="010-0000-0000"} phone 사용자의 휴대전화 번호
 * @apiParam (Body) {String} email 사용자의 이메일 주소
 * @apiError (Error 403) PhoneThrottle 잠시 후 다시 시도하세요.
 * @apiUse UserNotFound
 */
router.post('/session/find/password', async(async (req, res) => {
  const { email, phone } = req.body;
  // Check throttling
  let phoneAuth = await OngoingPhoneAuth.findOne({ where: { phone } });
  if (phoneAuth != null && phoneAuth.passwordAt != null &&
    (Date.now() - phoneAuth.passwordAt.getTime()) < 60 * 60 * 1000
  ) {
    throw 'PhoneThrottle';
  }
  // Check if phone number already belongs to another user
  let belongUser = await User.findOne({ where: { email, phone } });
  if (belongUser == null) throw 'UserNotFound';
  // Find passport
  let passports = await belongUser.getPassports({ where: { type: 'local' } });
  if (passports.length === 0) throw new Error('User passport does not exist');
  // Generate password
  // I've removed L from here since it can be confusing
  let newPassword = randToken.generate(8, '0123456789abcdefghijkmnopqrstuvwxyz');
  let salt = await bcrypt.genSalt(authConfig.bcrypt);
  let hash = await bcrypt.hash(newPassword, salt);
  await passports[0].update({ data: hash });
  // Send SMS
  await sendSMS(phone, `[에스티커] 임시 비밀번호는 ${newPassword}입니다.`);
  // Update throttling
  if (phoneAuth != null) await phoneAuth.update({ passwordAt: new Date() });
  else await OngoingPhoneAuth.create({ phone, passwordAt: new Date() });
  res.json({});
}));

/**
 * @api {post} /session/find/email 이메일 찾기
 * @apiGroup Session
 * @apiName FindEmail
 * @apiDescription 휴대전화로 이메일 주소를 보냅니다.
 * @apiParam (Body) {String="010-0000-0000"} phone 사용자의 휴대전화 번호
 * @apiError (Error 403) PhoneThrottle 잠시 후 다시 시도하세요.
 * @apiUse UserNotFound
 */
router.post('/session/find/email', async(async (req, res) => {
  const { phone } = req.body;
  // Check throttling
  let phoneAuth = await OngoingPhoneAuth.findOne({ where: { phone } });
  if (phoneAuth != null && phoneAuth.emailAt != null &&
    (Date.now() - phoneAuth.emailAt.getTime()) < 60 * 60 * 1000
  ) {
    throw 'PhoneThrottle';
  }
  // Check if phone number already belongs to another user
  let belongUser = await User.findOne({ where: { phone } });
  if (belongUser == null) throw 'UserNotFound';
  // Send SMS
  await sendSMS(phone, `[에스티커] 이메일은 ${belongUser.email}입니다.`);
  // Update throttling
  if (phoneAuth != null) await phoneAuth.update({ emailAt: new Date() });
  else await OngoingPhoneAuth.create({ phone, emailAt: new Date() });
  res.json({});
}));

/**
 * @api {get} /session/firebase Firebase 맞춤 토큰 받기
 * @apiGroup Session
 * @apiName GetFirebaseToken
 * @apiDescription Firebase 맞춤 토큰을 받습니다.
 * @apiSuccess {String} _ Firebase 토큰
 * @apiUse LoginRequired
 * @apiPermission LoginRequired
 */
router.get('/session/firebase', loginRequired, async(async (req, res) => {
  let result = await firebase.auth().createCustomToken(String(req.user.id));
  res.json(result);
}));

/**
 * @api {post} /session/fcm FCM 토큰 올리기
 * @apiGroup Session
 * @apiName PostFCMToken
 * @apiDescription Firebase Cloud Messaging 토큰을 올립니다.
 *
 *   **Firebase 맞춤 토큰과는 다릅니다. 자세한 사항은
 *   https://firebase.google.com/docs/cloud-messaging/android/client 를
 *   참조하세요.**
 *
 * @apiParam (Body) {String} fcm FCM 토큰
 * @apiUse LoginRequired
 * @apiPermission LoginRequired
 */
router.post('/session/fcm', loginRequired, async(async (req, res) => {
  let { fcm } = req.body;
  firebaseDB.ref('/fcmKeys').child(req.user.id).set(fcm);
  await req.user.update({ fcmKey: fcm });
  res.json({});
}));
