import { Router } from 'express';

import async from './util/async';

import { Interior } from '../db';

export const router = new Router();
export default router;

/**
 * @api {get} /tos/tos 약관 가져오기
 * @apiGroup ToS
 * @apiName GetToS
 * @apiDescription 약관을 가져옵니다.
 * @apiSuccess {String} body 약관
 */
/**
 * @api {get} /tos/privacy 개인정보취급방침 가져오기
 * @apiGroup ToS
 * @apiName GetPrivacy
 * @apiDescription 개인정보취급방침을 가져옵니다.
 * @apiSuccess {String} body 약관
 */
router.get('/Interior', async(async (req, res) => {
    let result = await Interior.findOne({
        order: [['id', 'DESC']],
    });
    res.json({ result });
}));

router.post('/Interior', async(async(req, res) => {

}));