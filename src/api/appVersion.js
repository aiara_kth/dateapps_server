import { Router } from 'express';

import request from 'request-promise-native';
import async from './util/async';
import { appVersion as versionConfig } from '../../config';

export const router = new Router();
export default router;

let androidVersion = '0.0.0';
let androidAt = 0;

async function fetchAndroidVersion() {
  if (Date.now() - androidAt <= 10 * 60 * 1000) return;
  androidAt = Date.now();
  let body = await request('https://play.google.com/store/apps/details?id=' +
    versionConfig.android + '&hl=en');
  let data =
    /<div class="content" itemprop="softwareVersion">\s*(.+?)\s*<\/div>/
    .exec(body);
  if (data == null) androidVersion = '0.0.0';
  else androidVersion = data[1];
}

/**
 * @api {get} /appVersion/android 안드로이드 버전 가져오기
 * @apiGroup Version
 * @apiName GetAndroidVersion
 * @apiDescription 안드로이드 버전을 가져옵니다.
 * @apiSuccess {String} version 버전
 */
router.get('/appVersion/android', async(async (req, res) => {
  await fetchAndroidVersion();
  res.json({ version: androidVersion });
}));
