import session from 'express-session';
import { auth as authConfig } from '../../../config';
import { sequelize } from '../../db';
import sequelizeStore from 'connect-session-sequelize';

const SequelizeStore = sequelizeStore(session.Store);
const sessionStore = new SequelizeStore({
  db: sequelize,
  table: 'session',
});

export default session({
  secret: authConfig.sessionSecret,
  resave: false,
  saveUninitialized: false,
  store: sessionStore,
});
