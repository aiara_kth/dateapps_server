import async from '../util/async';
import randToken from 'rand-token';
import { User, Passport } from '../../db';

export function setUser(user) {
  this.user = user;
  this.session.userId = user && user.id;
}

export async function setUserBearer(user) {
  this.user = user;
  if (user == null) {
    // Invalidate current bearer entry
    await Passport.destroy({
      where: { type: 'bearer', identifier: this.token },
    });
  } else {
    let accessToken = randToken.generate(32);
    // Create bearer entry
    await Passport.create({
      type: 'bearer', identifier: accessToken, userId: user.id,
    });
    this.token = accessToken;
    return accessToken;
  }
}

async function injectUser(req, res, next) {
  if (req.token != null) {
    req.setUser = setUserBearer;
    let passport = await Passport.findOne({
      where: { type: 'bearer', identifier: req.token },
    });
    if (passport != null) req.user = await passport.getUser();
  } else {
    req.setUser = setUser;
    if (req.session.userId != null) {
      let user = await User.findById(req.session.userId);
      req.user = user;
    }
  }
  if (req.user != null) {
    await req.user.update({ loggedAt: new Date() });
  }
  next();
}

export default async(injectUser);
