import { Router } from 'express';
import bodyParser from 'body-parser';
import bearerToken from 'express-bearer-token';

import { User } from '../../db';

import session from './session';
import injectUser from './injectUser';
import async from '../util/async';

const router = new Router();
export default router;

router.use(bearerToken());
router.use(bodyParser.urlencoded({ extended: false, limit: '10mb' }));
router.use(session);
router.use(injectUser);
router.use(async(async (req, res, next) => {
  if (req.user && req.user.isAdmin && req.query.sudo != null) {
    // Run 'sudo', fetch the user.
    let id = parseInt(req.query.sudo);
    let user = await User.findById(id);
    if (user == null || user.isAdmin) throw 'Forbidden';
    req.user = user;
    return next();
  }
  return next();
}));
