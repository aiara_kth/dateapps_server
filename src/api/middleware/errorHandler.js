export const ERROR_TABLE = {
  // code, string, log
  // If log is present, it logs user data into separate log file
  LoginRequired: [401, '계속하려면 로그인하셔야 합니다.', false],
  AdminRequired: [403, '수정할 수 있는 권한이 없습니다.', false],
  ApproveRequired: [403, '관리자의 승인이 필요합니다.', false],
  ProfileRequired: [403, '프로필을 작성해야 합니다.', false],
  PurchaseRequired: [403, '구매해야 합니다.', false],
  LoungeRequired: [403, '라운지 승인이 필요합니다.', false],
  UserDisabled: [403, '비활성화된 사용자입니다.', false],
  UserNotFound: [404, '입력하신 사용자를 찾을 수 없습니다.', false],
  GroupNotFound: [404, '입력하신 스티커를 찾을 수 없습니다.', false],
  GroupMemberRequired: [403, '스티커의 회원이어야 합니다.', false],
  GroupAdminRequired: [403, '해당 자원을 수정할 스티커 권한이 없습니다.', false],
  GroupSubAdminRequired: [403, '스티커의 부시삽이어야 합니다.', false],
  GroupStaffRequired: [403, '스티커의 운영진이어야 합니다.', false],
  MeetingMemberRequired: [403, '모임의 참가자여야 합니다.', false],
  MeetingNotFound: [404, '입력하신 모임을 찾을 수 없습니다.', false],
  NotFound: [404, '지정된 객체를 찾을 수 없습니다.', false],
  PasswordInvalid: [401, '비밀번호가 틀렸습니다.', false],
  PhoneInvalid: [401, '휴대전화 인증에 실패했습니다.', false],
  PhoneTimeout: [403, '휴대전화 인증 시간이 지났습니다.', false],
  PhoneConflict: [409, '이미 해당 휴대전화 번호를 가진 사용자가 있습니다.', false],
  EmailConflict: [409, '이미 해당 이메일 주소를 가진 사용자가 있습니다.', false],
  NicknameConflict: [409, '이미 해당 닉네임을 가진 사용자가 있습니다.', false],
  Conflict: [409, '이미 처리되었습니다.', false],
  DateConflict: [409, '다른 모임의 8시간 범위 밖의 모임만 참가할 수 있습니다.', false],
  ValidationError: [400, '필드의 형식이 잘못되었습니다.', true],
  PhotoIndexInvalid: [400, '대표 ​﻿​​﻿﻿​​﻿​﻿​﻿​﻿​사진 인덱스가 잘못되었습니다.', false],
  PhotoInvalid: [400, '올바른 사진 파일이 아닙니다.', false],
  PhotoThreshold: [403, '사진의 최대 개수를 ​﻿​​﻿﻿﻿﻿​﻿​﻿​﻿﻿﻿초과했습니다.', false],
  GroupThreshold: [403, '최대 가입 가능한 그룹 수를 초과했습니다.', false],
  MemberThreshold: [403, '정원을 초과했습니다.', false],
  PhoneThrottle: [403, '잠시 후 다시 시도하세요.', true],
  Throttle: [403, '잠시 후 다시 시도하세요.', true],
  NoDiamonds: [403, '잔여 다이아가 부족합니다.', false],
  Forbidden: [403, '권한이 없습니다.', true],
  ServerError: [500, '서버에 내부 오류가 발생했습니다.', true],
  NotImplemented: [501, '서버에서 아직 구현되지 않았습니다.', true],
};

function truncateStripData(data) {
  let output = [];
  for (let key in data) {
    if (typeof data[key] === 'string') {
      if (key === 'password' || key === 'code') {
        output.push(key + ': <REDACTED>');
        continue;
      }
      if (data[key].length > 200) {
        output.push(key + ': ' + data[key].slice(0, 200) + '... total ' +
          data[key].length + 'chars');
      } else {
        output.push(key + ': ' + data[key]);
      }
    }
  }
  return output.map(v => '  ' + v).join('\n');
}

export default function errorHandler(error, req, res, next) {
  let errorName, errorData;
  if (typeof error === 'string') {
    errorName = error;
  } else if (error.name != null) {
    errorName = error.name;
  }
  if (errorName === 'SequelizeUniqueConstraintError') {
    // TODO Remove hard coding
    if (error.fields[0] === 'email' || error.fields.email != null) {
      errorName = 'EmailConflict';
    } else if (error.fields[0] === 'phone' || error.fields.phone != null) {
      errorName = 'PhoneConflict';
    } else if (error.fields[0] === 'nickname' ||
      error.fields.nickname != null
    ) {
      errorName = 'NicknameConflict';
    }
  }
  console.log(error);
  if (ERROR_TABLE[errorName]) {
    errorData = ERROR_TABLE[errorName];
  } else {
    errorName = 'ServerError';
    errorData = ERROR_TABLE.ServerError;
  }
  if (errorName === 'ValidationError' && error.data) {
    errorData[1] = error.data.field + ' 필드가 ' + error.data.type +
      ' 형식이 아닙니다.';
  }
  if (errorData[2]) {
    console.error(
`==== Error at ${new Date().toString()} ====
${req.method} ${req.originalUrl}
IP: ${req.ip}
Query:
${truncateStripData(req.query)}
Cookies:
${truncateStripData(req.cookies)}
Headers:
${truncateStripData(req.headers)}
Body:
${truncateStripData(req.body)}
`);
  }
  console.error(error);
  res.status(errorData[0]);
  res.json({ name: errorName, data: errorData[1] });
}
