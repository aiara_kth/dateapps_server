import validator from 'validator';
import { fieldDefault } from './schema';

export function normalizeValidation(key, validation) {
  if (validation.value) return validation;
  return { key: fieldDefault[key], value: validation };
}

const BOOLEAN_TRUE_TABLE = ['Y', 'true', 'yes', '1', true, 1];
const BOOLEAN_FALSE_TABLE = ['N', 'false', 'no', '0', false, 0];
const BOOLEAN_TABLE = BOOLEAN_TRUE_TABLE.concat(BOOLEAN_FALSE_TABLE);

function runValidation(key, args, value) {
  // currently only supports:
  // is, isURL, isEmail, len, notEmpty, notIn
  switch (key) {
    case 'is':
      if (value != null && !validator.matches(value, args.value)) {
        return args.key;
      }
      break;
    case 'isURL':
      if (value != null && !validator.isURL(value)) return args.key;
      break;
    case 'isEmail':
      if (value != null && !validator.isEmail(value)) {
        return args.key;
      }
      break;
    case 'len': {
      const [min, max] = args.value;
      if (value != null && !validator.isLength(value, min, max)) {
        return args.key;
      }
      break;
    }
    case 'notEmpty':
      if (value == null || validator.isEmpty(value)) return args.key;
      break;
    case 'notIn':
      if (value != null && validator.isIn(value, args.value)) return args.key;
      if (typeof value === 'string' &&
        validator.isIn(value.toLowerCase(), args.value)) return args.key;
      break;
    case 'isIn':
      if (value != null && !validator.isIn(value, args.value)) return args.key;
      break;
    case 'isInt':
      if (value != null && !validator.isInt(value)) return args.key;
      break;
    case 'isDate':
      if (value != null && new Date(clearValue(value)) == 'Invalid Date') return args.key;
      break;
    case 'isBoolean':
      if (value != null && BOOLEAN_TABLE.indexOf(value) === -1) return args.key;
      break;
    case 'min': {
      const number = parseFloat(value);
      if (!isNaN(number) && number < args.value) return args.key;
      break;
    }
    case 'max': {
      const number = parseFloat(value);
      if (!isNaN(number) && number > args.value) return args.key;
      break;
    }
  }
  return null;
}

// Run validations from the data
export default function validate(data, schema, detailed = false) {
  if (schema == null) return {};
  let errors = {};
  for (let key in schema) {
    let validation = schema[key];
    let value = data[key];
    for (let check in validation) {
      if (validation[check] == null) continue;
      let args = normalizeValidation(check, validation[check]);
      const result = runValidation(check, args, value == null ? value
        : String(value));
      if (result !== null) {
        if (detailed) {
          errors[key] = { name: result, value: args.value };
        } else {
          errors[key] = result;
        }
      }
    }
  }
  return errors;
}

// Run validation, return only one error.
export function validateSingle(data, schema) {
  let result = validate(data, schema);
  for (let key in result) {
    if (result[key] !== false) {
      return { field: key, type: result[key] };
    }
  }
  return null;
}

export function sanitize(data, schema) {
  // It converts strings to numbers if possible, that's all.
  let newData = {};
  for (let key in schema) {
    let validation = schema[key];
    if (data[key] == null) continue;
    if (validation.isInt != null) {
      newData[key] = parseInt(data[key]);
    } else if (validation.isBoolean != null) {
      newData[key] = BOOLEAN_TRUE_TABLE.indexOf(data[key]) !== -1;
    } else if (validation.isDate != null) {
      newData[key] = new Date(clearValue(data[key]));
    } else {
      newData[key] = data[key];
    }
  }
  return newData;
}

function clearValue(value){
  return value.replace(/\"/g,"");
}
