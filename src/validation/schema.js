export const fieldDefault = {
  isEmail: 'email',
  isBoolean: 'boolean',
  notEmpty: 'required',
  is: 'format',
  isIn: 'enum',
  len: 'maxLength',
  isInt: 'number',
  isDate: 'date',
  min: 'min',
  max: 'max',
};

export const User = {
  email: {
    isEmail: true,
    notEmpty: true,
  },
  gender: {
    notEmpty: true,
    isInt: true,
    min: 0,
    max: 1,
  },
  phone: {
    is: { key: 'ValidationPhone', value: /^[0-9]{2,3}-[0-9]{3,4}-[0-9]{4,4}$/ },
    notEmpty: true,
  },
  deviceType: {},
	birth: { isDate: true },
	location: { isInt: true },
	name: { len: [1, 32] },
	nickname: { len: [1, 32] },
    introduction : {},
    loginfrom : {},
    introduct_word1: {},
    introduct_word2: {},
    introduct_word3: {},
    onequestion : {},
    interior: {},
};


export const ProfileHistory = {
  email: {
    isEmail: true,
    notEmpty: true,
  },
  gender: {
    notEmpty: true,
    isInt: true,
    min: 0,
    max: 1,
  },
  birth: { isDate: true },
  location: { isInt: true },
  name: { len: [1, 32] },
  nickname: { len: [1, 32] },
};

export const UserProfile = {
  photoPrimary: { isInt: true },
  name: { len: [1, 32] },
  nickname: { len: [1, 32] },
  location: { isInt: true },
  job: { len: [1, 64] },
  school: { len: [1, 64] },
  alcohol: { isInt: true },
  cigarette: { isInt: true },
  personality: { isInt: true },
  bloodtype: { isInt: true },
  bodytype: { isInt: true },
  height: { isInt: true },
  religion: { isInt: true },
  birth: { isDate: true },
  introduction: {},
  deviceType: {},
  updatePush: { isBoolean: true },
  isLoungeUsed: { isBoolean: true },
  usePhotoInGroup: { isBoolean: true },
  isWeeklySet: { isBoolean: true },
};

export const Group = {
  name: { notEmpty: true, len: [1, 32] },
  category: { notEmpty: true },
  location: { notEmpty: true, isInt: true },
  needApproval: { notEmpty: true, isBoolean: true },
  mainPhoto: {},
  profilePhoto: {},
  description: {},
};

export const GroupUser = {
  type: { isIn: ['banned', 'invited', 'pending', 'member', 'staff',
    'subAdmin', 'admin'] },
  permission: { isInt: true },
  updatePush: { isBoolean: true },
  chatPush: { isBoolean: true },
  joinMessage: { len: [0, 40] },
};

export const GroupPost = {
  category: {
    notEmpty: true,
    isIn: ['notice', 'free', 'greeting', 'gallery', 'review'],
  },
  title: { notEmpty: true },
	body: { notEmpty: true },
};

export const GalleryPost = {
  category: {
	  notEmpty: true,
		isIn: ['notice', 'free', 'greeting', 'gallery', 'review'],
	},
	body: { notEmpty: true },
};

export const GroupPostComment = {
  body: { notEmpty: true },
};

export const MeetingComment = {
  body: { notEmpty: true },
};

export const Meeting = {
  category: {},
  title: { len: [1, 128] },
  body: {},
  location: { isInt: true },
  place: {},
  mapx: { isInt: true },
  mapy: { isInt: true },
  price: { isInt: true, min: 0, max: 5000000 },
  date: { isDate: true },
  minMembers: { notEmpty: true, isInt: true },
  maxMembers: { notEmpty: true, isInt: true },
};

export const MeetingLightning = {
  title: { notEmpty: true, len: [1, 128] },
  place: { notEmpty: true },
  mapx: { isInt: true },
  mapy: { isInt: true },
  price: { notEmpty: true, isInt: true, min: 0, max: 5000000 },
  date: { notEmpty: true, isDate: true },
  minMembers: { notEmpty: true, isInt: true },
  maxMembers: { notEmpty: true, isInt: true },
};

export const Notice = {
  category: { notEmpty: true },
  title: { notEmpty: true, len: [1, 128] },
  body: { notEmpty: true },
};

export const Question = {
  category: {},
  title: { len: [1, 128] },
  body: { notEmpty: true },
  recipient : { notEmpty: true },
};

export const QuestionAnswer = {
  answerTitle: { notEmpty: true, len: [1, 128] },
  answerBody: { notEmpty: true },
};

export const Password = {
  len: { key: 'ValidationMinLength', value: [6] },
  notEmpty: true,
};
