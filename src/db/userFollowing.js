import Sequelize from 'sequelize';
import sequelize from './init';

export default sequelize.define('userFollowing', {
  denied: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
  reply: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
  feedback: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
}, {
  classMethods: {
    associate(models) {
      models.UserFollowing.belongsTo(models.User, {
        as: 'following',
        foreignKey: 'followingId',
        onUpdate: 'cascade',
        onDelete: 'cascade',
      });
      models.UserFollowing.belongsTo(models.User, {
        as: 'follower',
        foreignKey: 'followerId',
        onUpdate: 'cascade',
        onDelete: 'cascade',
      });
    },
  },
});
