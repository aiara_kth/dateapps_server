import Sequelize from 'sequelize';
import sequelize from './init';

export default sequelize.define('paymentTable', {
  data: {
    type: Sequelize.TEXT,
    get: function() {
      let rawValue = (this.getDataValue('data') || '{}');
      return JSON.parse(rawValue);
    },
    set: function(v) {
      this.setDataValue('data', JSON.stringify(v));
    },
  },
});
