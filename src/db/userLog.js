import Sequelize from 'sequelize';
import sequelize from './init';

export default sequelize.define('userLog', {
  email: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  gender: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },
  phone: {
    type: Sequelize.STRING(24),
    allowNull: false,
  },
  photos: {
    type: Sequelize.TEXT,
    get: function() {
      let rawValue = (this.getDataValue('photos') || '');
      if (rawValue === '') return [];
      return rawValue.split('|');
    },
    set: function(v = []) {
      this.setDataValue('photos', v.join('|'));
    },
  },
  photoThumbs: {
    type: Sequelize.VIRTUAL,
    get: function() {
      return this.photos.map(v => v.replace(/\.([a-z]+)$/i, '.thumb.$1'));
    },
  },
  photo: {
    type: Sequelize.VIRTUAL,
    get: function() {
      return this.photos[this.photoPrimary] || null;
    },
  },
  photoThumb: {
    type: Sequelize.VIRTUAL,
    get: function() {
      return this.photoThumbs[this.photoPrimary] || null;
    },
  },
  photoPrimary: Sequelize.INTEGER,
  name: Sequelize.STRING,
  nickname: {
    type: Sequelize.STRING,
  },
});
