import { db as dbConfig } from '../../config';
import Sequelize from 'sequelize';

export default new Sequelize(dbConfig.uri, Object.assign({}, dbConfig, {
  logging: process.env.NODE_ENV !== 'production' ? console.log : false,
}));
