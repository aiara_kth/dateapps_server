import Sequelize from 'sequelize';
import sequelize from './init';

export default sequelize.define('eventMessage', {
  type: Sequelize.STRING,
}, {
  classMethods: {
    associate(models) {
      models.EventMessage.belongsTo(models.User, {
        as: 'user',
        foreignKey: 'userId',
        onUpdate: 'cascade',
        onDelete: 'cascade',
      });
    },
  },
});
