import Sequelize from 'sequelize';
import sequelize from './init';

export default sequelize.define('question', {
  category: {
    type: Sequelize.STRING(48),
    allowNull: true,
      defaultValue: '핵심질문',
  },
  title: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  body: Sequelize.TEXT,
  recipient : Sequelize.INTEGER,
  photos: {
    type: Sequelize.TEXT,
    get: function() {
      let rawValue = (this.getDataValue('photos') || '');
      if (rawValue === '') return [];
      return rawValue.split('|');
    },
    set: function(v = []) {
      this.setDataValue('photos', v.join('|'));
    },
  },
  photoThumbs: {
    type: Sequelize.VIRTUAL,
    get: function() {
      return this.photos.map(v => v.replace(/\.([a-z]+)$/i, '.thumb.$1'));
    },
  },
  answeredAt: Sequelize.DATE,
  answerTitle: Sequelize.STRING,
  answerBody: Sequelize.TEXT,
  answerPhotos: {
    type: Sequelize.TEXT,
    get: function() {
      let rawValue = (this.getDataValue('answerPhotos') || '');
      if (rawValue === '') return [];
      return rawValue.split('|');
    },
    set: function(v = []) {
      this.setDataValue('answerPhotos', v.join('|'));
    },
  },
  answerPhotoThumbs: {
    type: Sequelize.VIRTUAL,
    get: function() {
      return this.answerPhotos.map(v => v.replace(/\.([a-z]+)$/i, '.thumb.$1'));
    },
  },
}, {
  classMethods: {
    associate(models) {
      models.Question.belongsTo(models.User);
    },
  },
});
