import Sequelize from 'sequelize';
import sequelize from './init';

export default sequelize.define('groupPost', {
  category: {
    type: Sequelize.STRING(48),
    allowNull: false,
  },
  isNotice: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
    allowNull: false,
  },
  title: {
    type: Sequelize.STRING,
    allowNull: true,
  },
  body: Sequelize.TEXT,
  photos: {
    type: Sequelize.TEXT,
    get: function() {
      let rawValue = (this.getDataValue('photos') || '');
      if (rawValue === '') return [];
      return rawValue.split('|');
    },
    set: function(v = []) {
      this.setDataValue('photos', v.join('|'));
    },
  },
  photoThumbs: {
    type: Sequelize.VIRTUAL,
    get: function() {
      return this.photos.map(v => v.replace(/\.([a-z]+)$/i, '.thumb.$1'));
    },
  },
  likesCount: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
  commentsCount: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
  viewCount: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
}, {
  indexes: [
    { fields: [{ attribute: 'category', length: 16 }] },
    { fields: ['title'] },
    { fields: ['userId'] },
    { fields: ['groupId'] },
    { fields: ['viewCount'] },
  ],
  instanceMethods: {
    async updateLikes() {
      let likesCount = await this.countLikeUsers();
      await this.update({ likesCount });
    },
    async updateComments() {
      let commentsCount = await this.countComments();
      await this.update({ commentsCount });
    },
  },
  classMethods: {
    associate(models) {
      models.GroupPost.belongsTo(models.User, {
        onUpdate: 'cascade',
        onDelete: 'cascade',
      });
      models.GroupPost.belongsTo(models.Group, {
        onUpdate: 'cascade',
        onDelete: 'cascade',
      });

      models.GroupPost.belongsToMany(models.User, {
        as: 'likeUsers',
        foreignKey: 'postId',
        through: 'groupPostUserLike',
      });

      models.GroupPost.hasMany(models.GroupPostComment, {
        as: 'comments',
        foreignKey: 'postId',
        onUpdate: 'cascade',
        onDelete: 'cascade',
      });
    },
  },
});
