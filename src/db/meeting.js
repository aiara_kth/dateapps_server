import Sequelize from 'sequelize';
import sequelize from './init';

export default sequelize.define('meeting', {
  lightning: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
  category: {
    type: Sequelize.STRING(48),
    allowNull: true,
    defaultValue: '데이트채팅',
  },
  title: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  body: Sequelize.TEXT,
  location: {
    type: Sequelize.INTEGER,
    allowNull: true,
      defaultValue: 1
  },
  place: {
    type: Sequelize.STRING,
    allowNull: true,
      defaultValue: '없음',
  },
  mapx: Sequelize.INTEGER,
  mapy: Sequelize.INTEGER,
  price: {
    type: Sequelize.INTEGER,
    allowNull: true,
      defaultValue: 0
  },
  date: {
    type: Sequelize.DATE,
    allowNull: true,
    defaultValue: Sequelize.NOW,
  },
  minMembers: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },
  maxMembers: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },
  membersCount: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
  likesCount: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
  commentsCount: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
  expired: {
    type: Sequelize.VIRTUAL,
    get: function() {
      return this.date.getTime() < Date.now();
    },
  },
  photo: Sequelize.STRING,
  photoThumb: {
    type: Sequelize.VIRTUAL,
    get: function() {
      if (this.photo == null) return null;
      return this.photo.replace(/\.([a-z]+)$/i, '.thumb.$1');
    },
  },
  paid: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
  failed: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
  failVerified: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
}, {
  indexes: [
    { fields: [{ attribute: 'category', length: 16 }] },
    { fields: ['lightning'] },
    { fields: ['date'] },
    { fields: ['maxMembers'] },
    { fields: ['minMembers'] },
    { fields: ['membersCount'] },
    { fields: ['likesCount'] },
    { fields: ['commentsCount'] },
    { fields: ['location'] },
    { fields: ['groupId'] },
    { fields: ['userId'] },
  ],
  instanceMethods: {
    async updateMembers() {
      let membersCount = await this.countMembers({
        where: sequelize.where(sequelize.literal('meetingUser.enabled'), true),
      });
      await this.update({ membersCount });
    },
    async updateLikes() {
      let likesCount = await this.countLikeUsers();
      await this.update({ likesCount });
    },
    async updateComments() {
      let commentsCount = await this.countComments();
      await this.update({ commentsCount });
    },
  },
  classMethods: {
    associate(models) {
      models.Meeting.belongsTo(models.Group);
      models.Meeting.belongsTo(models.User);
      models.Meeting.hasMany(models.MeetingComment, {
        as: 'comments',
      });
      models.Meeting.belongsToMany(models.User, {
        as: 'likeUsers',
        foreignKey: 'meetingId',
        through: 'meetingUserLike',
      });
      models.Meeting.belongsToMany(models.User, {
        as: 'members',
        foreignKey: 'meetingId',
        through: 'meetingUser',
      });
    },
  },
});
