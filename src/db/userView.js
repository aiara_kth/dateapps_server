import sequelize from './init';
import Sequelize from 'sequelize';

export default sequelize.define('userView', {
    ignore: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
    },
}, {
});
