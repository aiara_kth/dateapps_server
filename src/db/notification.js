import Sequelize from 'sequelize';
import sequelize from './init';

export default sequelize.define('notification', {
  data: {
    type: Sequelize.TEXT,
    allowNull: false,
    get: function() {
      let rawValue = (this.getDataValue('data') || 'null');
      try {
        return JSON.parse(rawValue);
      } catch (e) {
        return null;
      }
    },
    set: function(v) {
      try {
        let rawValue = JSON.stringify(v);
        this.setDataValue('data', rawValue);
      } catch (e) {
      }
    },
  },
  type: Sequelize.STRING,
}, {
  indexes: [
    { fields: ['userId'] },
    { fields: ['type'] },
  ],
  classMethods: {
    associate(models) {
      models.Notification.belongsTo(models.User);
    },
  },
});
