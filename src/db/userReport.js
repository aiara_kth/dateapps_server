import Sequelize from 'sequelize';
import sequelize from './init';

export default sequelize.define('userReport', {
  reason: Sequelize.STRING,
}, {
  classMethods: {
    associate(models) {
      models.UserReport.belongsTo(models.User, {
        as: 'reporter',
        foreignKey: 'reporterId',
        onUpdate: 'cascade',
        onDelete: 'cascade',
      });
      models.UserReport.belongsTo(models.User, {
        as: 'reportee',
        foreignKey: 'reporteeId',
        onUpdate: 'cascade',
        onDelete: 'cascade',
      });
    },
  },
});
