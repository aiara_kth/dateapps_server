import Sequelize from 'sequelize';
import sequelize from './init';

export default sequelize.define('paymentLog', {
  type: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  cost: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },
  diamondsAfter: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },
  targetId: Sequelize.INTEGER,
  nickname: Sequelize.STRING,
  name: Sequelize.STRING,
  phone: Sequelize.STRING,
  data: Sequelize.TEXT,
}, {
  indexes: [
    { fields: [{ attribute: 'type', length: 16 }] },
    { fields: ['type', 'targetId'] },
    { fields: ['createdAt'] },
    { fields: ['cost'] },
  ],
  classMethods: {
    associate(models) {
      models.PaymentLog.belongsTo(models.User);
    },
  },
});
