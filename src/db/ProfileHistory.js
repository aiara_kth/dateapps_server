import Sequelize from 'sequelize';
import sequelize from './init';
import * as validations from '../validation/schema';
import inject from '../validation/sequelize';

export default sequelize.define('profileHistory',inject({
  photos: {
    type: Sequelize.TEXT,
    get: function() {
      let rawValue = (this.getDataValue('photos') || '');
      if (rawValue === '') return [];
      return rawValue.split('|');
    },
    set: function(v = []) {
      this.setDataValue('photos',v.join('|'));
    },
  },
  photoThumbs: {
    type: Sequelize.VIRTUAL,
    get: function() {
      return this.photos.map(v => v.replace(/\.([a-z]+)$/i, '.thumb.$1'));
    },
  },
  photo: {
    type: Sequelize.VIRTUAL,
    get: function() {
      return this.photos[this.photoPrimary] || null;
    },
  },
  photoThumb: {
    type: Sequelize.VIRTUAL,
    get: function(){
      return this.photoThumbs[this.photoPrimary] || null;
    },
  },
  photoPrimary: Sequelize.INTEGER,
  name: Sequelize.STRING,
  nickname: {
    type: Sequelize.STRING,
  },
  location: Sequelize.INTEGER,
  birth: Sequelize.DATEONLY,
  job: Sequelize.STRING,
  school: Sequelize.STRING,
  alcohol: Sequelize.INTEGER,
  cigarette: Sequelize.INTEGER,
  personality: Sequelize.INTEGER,
  bloodtype: Sequelize.INTEGER,
  bodytype: Sequelize.INTEGER,
  height: Sequelize.INTEGER,
  religion: Sequelize.INTEGER,
  introduction: Sequelize.TEXT,
}, validations.ProfileHistory), {
  indexes: [
    { fields: ['nickname'] },
    { fields: ['location'] },
  ],
  instanceMethods: {
    toJSON: function() {
      let obj = this.get({ plain: true });
      obj.photo = obj.photos[obj.photoPrimary] || null;
      obj.photoThumb = obj.photoThumbs[obj.photoPrimary] || null;
      return obj;
    },
    getPhotos: function(user) {
      let size = user != null ? user.photos.length : 1;
      if (user != null && (user.isAdmin || user.id === this.id)) {
        return this.photos;
      }
      let merged = [this.photos[this.photoPrimary]];
      merged = merged.concat(
        this.photos.filter((v, i) => this.photoPrimary !== i));
      return merged.map((v, i) => i >= size ? null : v);
    },
    getPhotoThumbs: function(user) {
      let size = user != null ? user.photos.length : 1;
      if (user != null && (user.isAdmin || user.id === this.id)) {
        return this.photoThumbs;
      }
      let merged = [this.photoThumbs[this.photoPrimary]];
      merged = merged.concat(
        this.photoThumbs.filter((v, i) => this.photoPrimary !== i));
      return merged.map((v, i) => i >= size ? null : v);
    },
  },
  classMethods: {
    associate(models) {
      models.ProfileHistory.belongsTo(models.User);
    },
  },
});