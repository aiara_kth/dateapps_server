import Sequelize from 'sequelize';
import sequelize from './init';
import * as validations from '../validation/schema';
import inject from '../validation/sequelize';

export default sequelize.define('user', inject({
  email: {
    type: Sequelize.STRING,
    unique: true,
    allowNull: false,
  },
  gender: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },
  phone: {
    type: Sequelize.STRING(24),
    unique: true,
    allowNull: false,
  },
  isAdmin: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
  isAllAccessible: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
  photos: {
    type: Sequelize.TEXT,
    get: function() {
      let rawValue = (this.getDataValue('photos') || '');
      if (rawValue === '') return [];
      return rawValue.split('|');
    },
    set: function(v = []) {
      this.setDataValue('photos', v.join('|'));
    },
  },
  photoThumbs: {
    type: Sequelize.VIRTUAL,
    get: function() {
      return this.photos.map(v => v.replace(/\.([a-z]+)$/i, '.thumb.$1'));
    },
  },
  photo: {
    type: Sequelize.VIRTUAL,
    get: function() {
      return this.photos[this.photoPrimary] || null;
    },
  },
  photoThumb: {
    type: Sequelize.VIRTUAL,
    get: function() {
      return this.photoThumbs[this.photoPrimary] || null;
    },
  },
  photoPrimary: Sequelize.INTEGER,
  name: Sequelize.STRING,
  nickname: {
    type: Sequelize.STRING,
    unique: true,
  },
  location: Sequelize.INTEGER,
  birth: Sequelize.DATEONLY,
  job: Sequelize.STRING,
  school: Sequelize.STRING,
  alcohol: Sequelize.INTEGER,
  cigarette: Sequelize.INTEGER,
  personality: Sequelize.INTEGER,
  bloodtype: Sequelize.INTEGER,
  bodytype: Sequelize.INTEGER,
  height: Sequelize.INTEGER,
  religion: Sequelize.INTEGER,
  year : Sequelize.TEXT,
  introduction: Sequelize.TEXT,
  introduct_word1: Sequelize.TEXT,
  introduct_word2: Sequelize.TEXT,
  introduct_word3: Sequelize.TEXT,
  onequestion: Sequelize.TEXT,
  interior: Sequelize.INTEGER,
  loginfrom: Sequelize.TEXT,
  isLoungeApproved: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
  approvedAt: Sequelize.DATE,
  isLoungeUsed: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: true,
  },
  usePhotoInGroup: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: true,
  },
  hasProfile: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
  isApproved: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
  isApproveDenied: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
  hasApproveDeniedBefore: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
  isEnabled: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: true,
  },
  isDailySet: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
  rating: {
    type: Sequelize.FLOAT,
    allowNull: false,
    defaultValue: 5.0,
  },
  updatePush: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: true,
  },
  fcmKey: Sequelize.STRING,
  loggedAt: {
    type: Sequelize.DATE,
    defaultValue: Sequelize.NOW,
  },
  newbieStartedAt: Sequelize.DATE,
  newbiePending: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
  deviceType: Sequelize.STRING,
  diamonds: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
  reportCount: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
  votedCount: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
  chargedCountToday: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
  profileCountToday: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
  isLoungeBest: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
  isSuggestion: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
  ignoreLounge: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
  deletedAt: Sequelize.DATE,
	profileUpdatedAt: Sequelize.DATE,
	photoUpdatedAt: Sequelize.DATE,
}, validations.User), {
    indexes: [
        { fields: ['gender'] },
        { fields: ['nickname'] },
        { fields: ['location'] },
        { fields: ['isEnabled'] },
        { fields: ['rating'] },
        { fields: ['newbieStartedAt'] },
        { fields: ['profileUpdatedAt','photoUpdatedAt'] },
        { fields: ['loggedAt'] },
        { fields: ['isLoungeApproved', 'isLoungeUsed'] },
        { fields: ['hasProfile', 'isApproved'] },
    ],
    instanceMethods: {
        toJSON: function() {
            let obj = this.get({ plain: true });
            delete obj.name;
            delete obj.email;
            delete obj.fcmKey;
            delete obj.userAgent;
            // Get main photo
            obj.photo = obj.photos[obj.photoPrimary] || null;
            obj.photoThumb = obj.photoThumbs[obj.photoPrimary] || null;
            delete obj.photos;
            delete obj.photoThumbs;
            delete obj.diamonds;
            delete obj.votedCount;
            return obj;
        },
        getPhotos: function(user) {
            return this.photos;
            let size = user != null ? user.photos.length : 1;
            if (user != null && (user.isAdmin || user.id === this.id)) {
                return this.photos;
            }
            let merged = [this.photos[this.photoPrimary]];
            merged = merged.concat(
                this.photos.filter((v, i) => this.photoPrimary !== i));
            return merged.map((v, i) => i >= size ? null : v);
        },
        getPhotoThumbs: function(user) {
            return this.photoThumbs;
            let size = user != null ? user.photos.length : 1;
            if (user != null && (user.isAdmin || user.id === this.id)) {
                return this.photoThumbs;
            }
            let merged = [this.photoThumbs[this.photoPrimary]];
            merged = merged.concat(
                this.photoThumbs.filter((v, i) => this.photoPrimary !== i));
            return merged.map((v, i) => i >= size ? null : v);
        },
        applyOldProfile: function(profile){
            ["photos","photoPrimary","name","nickname","location"
                ,"birth","job","school","alcohol","cigarette","personality"
                ,"bloodtype","bodytype","height","religion","introduction",
                "introduct_word1","introduct_word2","introduct_word3",
                "onequestion","loginfrom","year"].forEach(field => {
                this[field] = profile[field];
            });
        },
        updateRating: async function() {
            const models = require('./index');
            await sequelize.transaction(async t => {
                // Retrieve all votes for the user.
                let where = {
                    voteeId: this.id,
                };
                if (this.newbieStartedAt != null) {
                    where.updatedAt = {
                        $gt: this.newbieStartedAt,
                    };
                }
                let query = {
                    where,
                    transaction: t,
                };
                let count = await models.UserRating.count(query);
                let sum = await models.UserRating.sum('score', query);
                // Calculate current score.
                let average = count > 0 ? sum / count : 5.0;
                let diff = { rating: average };
                // Disabled since the newbie selection algorithm has been changed.
                /*
                // Check if the lounge usage should be approved.
                if (votee.isLoungeApproved === false && count >= 10 && average >= 5) {
                  diff.isLoungeApproved = true;
                  // TODO Send push notification to the user saying that (s)he is
                  // allowed to use the lounge.
                  sendNotification(votee, { type: 'loungeAccept' });
                }
                */
                // Update the user.
                await this.update(diff, { transaction: t });
            });
        },
    },
    classMethods: {
        associate(models) {
            models.User.hasMany(models.Passport);
            models.User.hasMany(models.UserRatingHistory, {
                as: {
                    plural: 'ratingHistories',
                    singular: 'ratingHistory',
                },
            });

            models.User.hasMany(models.Notification);

            models.User.belongsToMany(models.User, {
                as: 'voters',
                foreignKey: 'voteeId',
                through: 'userRating',
            });
            models.User.belongsToMany(models.User, {
                as: 'votees',
                foreignKey: 'voterId',
                through: 'userRating',
            });
            models.User.hasMany(models.UserRating, {
                as: 'voterRatings',
                foreignKey: 'voteeId',
            });

            models.User.belongsToMany(models.User, {
                as: 'followers',
                foreignKey: 'followingId',
                through: 'userFollowing',
            });
            models.User.belongsToMany(models.User, {
                as: 'followings',
                foreignKey: 'followerId',
                through: 'userFollowing',
            });
            models.User.hasMany(models.UserFollowing, {
                as: 'followerFollowings',
                foreignKey: 'followingId',
            });
            models.User.hasMany(models.UserFollowing, {
                as: 'followingFollowings',
                foreignKey: 'followerId',
            });
            models.User.hasMany(models.Notification);

            models.User.belongsToMany(models.Group, {
                as: 'groups',
                foreignKey: 'userId',
                through: 'groupUser',
            });
            models.User.hasMany(models.GroupUser, { foreignKey: 'userId' });

            models.User.hasMany(models.GroupPost);
            models.User.belongsToMany(models.GroupPost, {
                as: 'likeGroupPosts',
                foreignKey: 'userId',
                through: 'groupPostUserLike',
            });

            models.User.hasMany(models.GroupPostComment);

            models.User.hasMany(models.Meeting, { as: 'hostedMeetings' });
            models.User.hasMany(models.ProfileHistory, {
                as: 'profileHistories'
            });
            models.User.belongsToMany(models.Meeting, {
                as: 'meetings',
                foreignKey: 'userId',
                through: 'meetingUser',
            });
            models.User.belongsToMany(models.Meeting, {
                as: 'likeMeetings',
                foreignKey: 'userId',
                through: 'meetingUserLike',
            });

            models.User.hasMany(models.UserChat, {
                as: 'senderChats',
                foreignKey: 'senderId',
            });
            models.User.hasMany(models.UserChat, {
                as: 'receiverChats',
                foreignKey: 'receiverId',
            });

            models.User.hasMany(models.Question);

            models.User.hasMany(models.PaymentLog);

            models.User.belongsToMany(models.User, {
                as: 'reportees',
                foreignKey: 'reporterId',
                through: 'userReport',
            });
            models.User.belongsToMany(models.User, {
                as: 'reporters',
                foreignKey: 'reporteeId',
                through: 'userReport',
            });

            models.User.belongsToMany(models.Group, {
                as: 'reporteeGroups',
                foreignKey: 'reporterId',
                through: 'groupReport',
            });

            models.User.belongsToMany(models.User, {
                as: 'viewees',
                foreignKey: 'viewerId',
                through: 'userView',
            });
            models.User.belongsToMany(models.User, {
                as: 'viewers',
                foreignKey: 'vieweeId',
                through: 'userView',
            });
        },
    },
});
