import Sequelize from 'sequelize';
import sequelize from './init';

export default sequelize.define('meetingComment', {
  body: Sequelize.TEXT,
}, {
  indexes: [
    { fields: ['userId'] },
    { fields: ['meetingId'] },
  ],
  classMethods: {
    associate(models) {
      models.MeetingComment.belongsTo(models.User);
      models.MeetingComment.belongsTo(models.Meeting);
    },
  },
});
