import fs from 'fs';
import path from 'path';

import Sequelize from 'sequelize';
import sequelize from './init';

function capitalize(str) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

// NOTE This can't be converted by webpack.
let db = {};

fs.readdirSync(__dirname)
  .filter(file => file.indexOf('.') !== 0 && file !== 'index.js' &&
    file !== 'init.js')
  .forEach(file => {
    var model = require(path.join(__dirname, file)).default;
    db[capitalize(model.name)] = model;
  });

db.sequelize = sequelize;
db.Sequelize = Sequelize;
module.exports = db;

Object.keys(db).forEach(function(modelName) {
  if ('associate' in db[modelName]) {
    db[modelName].associate(db);
  }
});

sequelize.sync();
