import Sequelize from 'sequelize';
import sequelize from './init';

export default sequelize.define('passport', {
  type: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: 'typeIdentifier',
  },
  identifier: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: 'typeIdentifier',
  },
  data: Sequelize.TEXT,
}, {
  classMethods: {
    associate(models) {
      models.Passport.belongsTo(models.User);
    },
  },
});
