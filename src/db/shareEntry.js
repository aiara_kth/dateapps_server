import Sequelize from 'sequelize';
import sequelize from './init';

export default sequelize.define('shareEntry', {
  code: {
    type: Sequelize.STRING,
    primaryKey: true,
  },
  title: Sequelize.STRING,
  description: Sequelize.TEXT,
  image: Sequelize.STRING,
  url: Sequelize.STRING,
  userId: Sequelize.INTEGER,
  viewCount: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
});
