import Sequelize from 'sequelize';
import sequelize from './init';

export default sequelize.define('userRating', {
  score: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },
}, {
  indexes: [
    { fields: ['updatedAt'] },
  ],
});
