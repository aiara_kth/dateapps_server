import Sequelize from 'sequelize';
import sequelize from './init';

export default sequelize.define('userChat', {
  chatKey: Sequelize.STRING,
  expired: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
    allowNull: false,
  },
}, {
  classMethods: {
    associate(models) {
      models.UserChat.belongsTo(models.User, {
        as: 'sender',
        foreignKey: 'senderId',
        onUpdate: 'cascade',
        onDelete: 'cascade',
      });
      models.UserChat.belongsTo(models.User, {
        as: 'receiver',
        foreignKey: 'receiverId',
        onUpdate: 'cascade',
        onDelete: 'cascade',
      });
    },
  },
});
