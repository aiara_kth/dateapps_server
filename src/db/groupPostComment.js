import Sequelize from 'sequelize';
import sequelize from './init';

export default sequelize.define('groupPostComment', {
  body: Sequelize.TEXT,
}, {
  indexes: [
    { fields: ['userId'] },
    { fields: ['postId'] },
  ],
  classMethods: {
    associate(models) {
      models.GroupPostComment.belongsTo(models.User);
      models.GroupPostComment.belongsTo(models.GroupPost, {
        as: 'post',
        foreignKey: 'postId',
      });
    },
  },
});
