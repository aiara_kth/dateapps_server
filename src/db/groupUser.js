import Sequelize from 'sequelize';
import sequelize from './init';

export default sequelize.define('groupUser', {
  type: {
    type: Sequelize.ENUM('banned', 'invited', 'pending', 'member', 'staff',
      'subAdmin', 'admin'),
    allowNull: false,
  },
  permission: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
  updatePush: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: true,
  },
  chatPush: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: true,
  },
  joinMessage: {
    type: Sequelize.STRING,
  },
}, {
  indexes: [
    { fields: ['type'] },
    { fields: ['userId'] },
    { fields: ['groupId'] },
  ],
});
