import Sequelize from 'sequelize';
import sequelize from './init';

export default sequelize.define('userRatingHistory', {
  average: {
    type: Sequelize.FLOAT,
    allowNull: false,
  },
  votes: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },
  scores: {
    type: Sequelize.TEXT,
    allowNull: false,
    get: function() {
      let rawValue = (this.getDataValue('scores') || '');
      if (rawValue === '') return [];
      return rawValue.split(',').map(a => parseFloat(a));
    },
    set: function(v = []) {
      this.setDataValue('scores', v.join(','));
    },
  },
  photo: Sequelize.STRING,
  photoThumb: Sequelize.STRING,
}, {
  classMethods: {
    associate(models) {
      models.UserRatingHistory.belongsTo(models.User);
    },
  },
});
