import Sequelize from 'sequelize';
import sequelize from './init';

export default sequelize.define('notice', {
  category: {
    type: Sequelize.STRING(48),
    allowNull: false,
  },
  title: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  body: Sequelize.TEXT,
  photos: {
    type: Sequelize.TEXT,
    get: function() {
      let rawValue = (this.getDataValue('photos') || '');
      if (rawValue === '') return [];
      return rawValue.split('|');
    },
    set: function(v = []) {
      this.setDataValue('photos', v.join('|'));
    },
  },
  photoThumbs: {
    type: Sequelize.VIRTUAL,
    get: function() {
      return this.photos.map(v => v.replace(/\.([a-z]+)$/i, '.thumb.$1'));
    },
  },
  viewCount: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
});
