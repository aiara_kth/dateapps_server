import Sequelize from 'sequelize';
import sequelize from './init';

export default sequelize.define('group', {
  category: {
    type: Sequelize.STRING(48),
    allowNull: false,
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  description: Sequelize.TEXT,
  location: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },
  needApproval: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: true,
  },
  invitationKey: Sequelize.STRING(16),
  profilePhoto: Sequelize.STRING,
  mainPhoto: Sequelize.STRING,
  profilePhotoThumb: {
    type: Sequelize.VIRTUAL,
    get: function() {
      if (this.profilePhoto == null) return null;
      return this.profilePhoto.replace(/\.([a-z]+)$/i, '.thumb.$1');
    },
  },
  mainPhotoThumb: {
    type: Sequelize.VIRTUAL,
    get: function() {
      if (this.mainPhoto == null) return null;
      return this.mainPhoto.replace(/\.([a-z]+)$/i, '.thumb.$1');
    },
  },
  membersCount: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
  postsCount: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
  score: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
  forceRanking: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
  reportCount: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
  chatKey: Sequelize.STRING,
  deletedAt: Sequelize.DATE,
  notices: {
    type: Sequelize.STRING,
    get: function() {
      let rawValue = (this.getDataValue('notices') || '');
      if (rawValue === '') return [];
      return rawValue.split('|').map(v => parseInt(v));
    },
    set: function(v = []) {
      this.setDataValue('notices', v.map(v => v.toFixed()).join('|'));
    },
  },
  // Billing cycle stuff
  subscriptionData: Sequelize.TEXT,
  subscriptionSignature: Sequelize.TEXT,
  subscriptionProvider: Sequelize.STRING,
  subscriptionTransactionId: Sequelize.STRING,
  subscriptionValidUntil: Sequelize.DATE,
  cancelPending: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
  isValid: {
    type: Sequelize.VIRTUAL,
    get: function() {
      if (this.subscriptionValidUntil == null) return false;
      return this.subscriptionValidUntil.getTime() > Date.now();
    },
  },
}, {
  indexes: [
    { fields: [{ attribute: 'category', length: 16 }] },
    { fields: ['score'] },
    { fields: ['forceRanking'] },
    { fields: ['location'] },
    { fields: ['needApproval'] },
    { fields: ['reportCount'] },
    { fields: ['subscriptionTransactionId'] },
    { fields: ['subscriptionValidUntil'] },
  ],
  instanceMethods: {
    toJSON() {
      let obj = this.get({ plain: true });
      delete obj.chatKey;
      delete obj.subscriptionData;
      delete obj.subscriptionSignature;
      delete obj.subscriptionProvider;
      delete obj.subscriptionTransactionId;
      delete obj.subscriptionValidUntil;
      delete obj.notices;
      return obj;
    },
    async updateMembers() {
      let membersCount = await this.countMembers({
        where: [sequelize.where(sequelize.literal('groupUser.type'), {
          $notIn: ['invited', 'banned', 'pending'],
        }), {
          isEnabled: true,
        }],
      });
      let postsCount = this.postsCount;
      let score = membersCount * 10 + postsCount;
      await this.update({ membersCount, score });
    },
    async updatePosts() {
      let postsCount = await this.countPosts();
      let membersCount = this.membersCount;
      let score = membersCount * 10 + postsCount;
      await this.update({ postsCount, score });
    },
    async updateNotices(id, order) {
      // Run parseInt here?
      let orderId = parseInt(order);
      if (orderId === -1 && this.notices.indexOf(id) !== -1) {
        await this.update({ notices: this.notices.filter(v => v !== id) });
        return;
      }
      if (isNaN(orderId) || orderId < 0 || orderId > 7) return;
      let noticesOrig = this.notices;
      noticesOrig = noticesOrig.filter(v => v !== id);
      let notices = noticesOrig.slice(0, orderId).concat(
        id,
        noticesOrig.slice(orderId, 7),
      );
      await this.update({ notices });
    },
  },
  classMethods: {
    associate(models) {
      models.Group.hasMany(models.GroupPost, {
        as: 'posts',
        onUpdate: 'cascade',
        onDelete: 'cascade',
      });
      models.Group.belongsTo(models.User, { as: 'admin' });
      models.Group.belongsToMany(models.User, {
        as: 'members',
        foreignKey: 'groupId',
        through: 'groupUser',
      });
      models.Group.hasMany(models.Meeting, {
        onUpdate: 'cascade',
        onDelete: 'cascade',
      });
      models.Group.belongsToMany(models.User, {
        as: 'reporters',
        foreignKey: 'reporteeId',
        through: 'groupReport',
      });
    },
  },
});
