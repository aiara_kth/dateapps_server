import Sequelize from 'sequelize';
import sequelize from './init';

export default sequelize.define('ongoingPhoneAuth', {
  phone: {
    type: Sequelize.STRING,
    primaryKey: true,
  },
  code: Sequelize.STRING,
  // When did we send email / password SMS?
  emailAt: Sequelize.DATE,
  passwordAt: Sequelize.DATE,
  resetAt: {
    type: Sequelize.DATE,
    defaultValue: Sequelize.NOW,
  },
  count: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
});
