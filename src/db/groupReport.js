import Sequelize from 'sequelize';
import sequelize from './init';

export default sequelize.define('groupReport', {
  reason: Sequelize.STRING,
}, {
  classMethods: {
    associate(models) {
      models.GroupReport.belongsTo(models.User, {
        as: 'reporter',
        foreignKey: 'reporterId',
        onUpdate: 'cascade',
        onDelete: 'cascade',
      });
      models.GroupReport.belongsTo(models.Group, {
        as: 'reportee',
        foreignKey: 'reporteeId',
        onUpdate: 'cascade',
        onDelete: 'cascade',
      });
    },
  },
});
