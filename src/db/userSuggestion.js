import Sequelize from 'sequelize';
import sequelize from './init';

export default sequelize.define('userSuggestion', {
  
}, {
  classMethods: {
    associate(models) {
      models.UserSuggestion.belongsTo(models.User, {
        as: 'user',
        foreignKey: 'userId',
        onUpdate: 'cascade',
        onDelete: 'cascade',
      });
      models.UserSuggestion.belongsTo(models.User, {
        as: 'target',
        foreignKey: 'targetId',
        onUpdate: 'cascade',
        onDelete: 'cascade',
      });
    },
  },
});