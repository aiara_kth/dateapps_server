import Sequelize from 'sequelize';
import sequelize from './init';

export default sequelize.define('meetingUser', {
  enabled: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: true,
  },
  leaveCount: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
}, {
  indexes: [
    { fields: ['enabled'] },
    { fields: ['userId'] },
    { fields: ['meetingId'] },
  ],
});
