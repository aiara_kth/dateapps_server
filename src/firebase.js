import firebase from 'firebase-admin';
import { firebase as firebaseConfig } from '../config';

export default firebase;

firebase.initializeApp({
  credential: firebase.credential.cert(firebaseConfig.credential),
  databaseURL: firebaseConfig.databaseURL,
});
