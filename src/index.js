import express from 'express';
import serveStatic from 'serve-static';
import path from 'path';
import cors from 'cors';
import cluster from 'cluster';

import logger from './util/logger';
import listen from './util/listen';
import api from './api';

import worker from './worker';

import * as photoUpload from './api/util/photoUpload';

const app = express();
app.set('view engine', 'pug');
app.set('views', path.resolve(__dirname, 'templates'));
app.set('query parser', 'simple');
app.set('trust proxy', ['loopback', 'linklocal', 'uniquelocal']);

app.use(logger);
if (process.env.NODE_ENV !== 'production') {
  // Use CORS in development mode
  app.use(cors({
    origin: (_, callback) => callback(null, true),
    credentials: true,
  }));
} else {
  // Use CORS in production mode
  app.use(cors({
    origin: ['http://admin.estickerapp.com', 'http://localhost:8080'],
    credentials: true,
  }));
}
app.use(api);
app.use(photoUpload.publicPath, serveStatic(photoUpload.uploadPath));
// Serve docs if in development mode
if (process.env.NODE_ENV !== 'production') {
  app.use('/docs', serveStatic(path.resolve(__dirname, '../apidoc')));
}

// Start listening from specified host / port.
listen(app);

// Check if the process is host if multiprocessing is enabled
if (cluster.isMaster) worker();
