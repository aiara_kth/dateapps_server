'use strict';
// Entry point of the application

if (process.env.NODE_ENV === 'production') {
  // In production, use transpiled source code
  // Run migration
  require('./lib/migrations').default().then(() => {
    require('./lib');
  }).catch((e) => {
    console.error(e.stack);
    process.exit(1);
  });
} else {
  // In development, use babel-register and jump to source code
  require('babel-register');
  require('./src/migrations').default().then(() => {
    require('./src');
  }).catch((e) => {
    console.error(e.stack);
    process.exit(1);
  });
}
