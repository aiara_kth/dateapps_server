# esticker-api
esticker API server

## 서버 구동하기
1. `npm install`로 의존성 패키지들을 설치합니다.
2. `npm run build`로 소스코드를 트랜스파일합니다.
3. `config/` 디렉토리의 설정을 변경합니다.
4. `NODE_ENV` 환경변수를 **꼭** `production`으로 변경하고 `npm start` 혹은
   `node index.js`를 실행합니다.

## 인증
대다수의 명령어는 로그인된 사용자로 인증하여 사용해야 합니다. 인증은 다음의 방법중 한 가지를
선택하여 사용할 수 있습니다.

- 쿠키를 사용합니다. 로그인이나 회원가입할 때 `?session=1`을 붙이면 쿠키 (세션) 기반
   인증을 사용하게 됩니다.
- 액세스 토큰을 사용합니다. 로그인이나 회원가입할 때 출력되는 `access_token` 값을
  사용하면 됩니다.
  - 매 요청마다 Body에 `access_token`을 삽입합니다.
  - 매 요청마다 Query에 `access_token`을 삽입합니다.
  - 매 요청마다 헤더에 `Authorization: Bearer <토큰>`을 삽입합니다.

아무 방법을 사용하든 상관 없으나, 한 가지 방법만 사용해 주세요.

## 에러 처리
모든 API는 에러가 발생하면 다음과 같이 JSON이 출력됩니다. HTTP 코드가 200번대가 아니면
해당 에러 코드와 메시지를 토대로 처리하면 됩니다.
```js
{ name: 'LoginRequired', data: '계속하려면 로그인하셔야 합니다.' }
```

## 스키마
API 문서에는 타입만 기재되어 있을 뿐 스키마가 누락되어 있습니다. 이 섹션에서는 스키마에
대해서 설명합니다.

### User
```js
{
  id: 0, // 고유 번호
  photos: [
    '/uploads/test.png'
  ],
  photoThumbs: [
    '/uploads/test.thumb.png'
  ],
  photoPrimary: 0, // 대표 사진 번호. null일 수 있음
  gender: 0, // 성별. 0 = 남자, 1 = 여자
  isAdmin: false, // 관리자 여부
  nickname: '닉네임', // 닉네임. null일 수 있음
  location: 1, // 위치. null일 수 있음
  birth: '2017-01-01', // 생년월일. null일 수 있음
  job: '직업', // 직업. null일 수 있음
  school: '학교', // 학교. null일 수 있음
  alcohol: 0, // 음주. null일 수 있음
  cigarette: 0, // 흡연. null일 수 있음
  personality: 1023, // 성격, null일 수 있음. BitSet을 사용하라는 의도로 이렇게
  // 만들었으나 추후에 변경할 수도 있습니다.
  bloodtype: 1, // 혈액형. null일 수 있음
  bodytype: 3, // 체형. null일 수 있음
  height: 102, // 키(cm). null일 수 있음
  religion: 1, // 종교. null일 수 있음
  introduction: '자기소개', // 자기소개. null일 수 있음.
  isLoungeApproved: false, // 라운지 승인 여부 (관리자가 정함)
  isLoungeUsed: false, // 라운지 사용 여부 (사용자가 설정에서 변경 가능)
  hasProfile: true, // 프로필 보유 여부 (회원가입 직후엔 false)
  rating: 10, // 별점. 0~10 사이의 실수
  createdAt: '2017-01-01T00:00:00.000Z', // 계정 생성 시각
  updatedAt: '2017-01-01T00:00:00.000Z', // 계정 업데이트 시각
}
```

### Notification
```js
{
  data: {
    type: 'groupInvite', // 알림 타입
    title: '알림 제목', // 알림 제목
    message: '알림 내용', // 알림 내용
    user: { // 해당 행동을 한 사용자. null일 수 있음.
      id: 1, // 고유 번호
      nickname: '닉네임', // 닉네임
    },
    group: { // 해당 행동이 속한 스티커. null일 수 있음.
      id: 1, // 고유 번호
      name: '스티커', // 이름
    },
    post: { // 해당 행동이 속한 게시글. null일 수 있음.
      id: 1, // 고유 번호
      title: '게시글', // 제목
    },
  },
  createdAt: '2017-01-01T00:00:00.000Z', // 알림 생성 시각
  updatedAt: '2017-01-01T00:00:00.000Z', // 알림 업데이트 시각
}
```

### Group
```js
{
  id: 1, // 고유 번호
  category: 'diy', // 카테고리
  name: 'name', // 이름
  description: 'description', // 설명
  location: 32, // 위치
  needApproval: false, // 가입시 허용 필요 여부
  invitationKey: 'abcd', // 초대 키. 현재는 null
  profilePhoto: '/uploads/aaaa.jpg', // 프로필 사진
  profilePhotoThumb: '/uploads/aaaa.thumb.jpg', // 프로필 사진 썸네일
  mainPhoto: '/uploads/aaaa.jpg', // 메인 사진
  mainPhotoThumb: '/uploads/aaaa.thumb.jpg', // 메인 사진 썸네일
  membersCount: 1, // 멤버 수
  postsCount: 2, // 게시글 수
  score: 12, // 점수
  createdAt: '2017-01-01T00:00:00.000Z', // 생성 시각
  updatedAt: '2017-01-01T00:00:00.000Z', // 업데이트 시각
}
```

### Post
```js
{
  id: 1, // 고유 번호
  category: 'diy', // 카테고리
  title: 'title', // 제목
  body: 'body', // 내용
  likesCount: 0, // 좋아요 수
  commentsCount: 0, // 댓글 수
  photos: [
    '/uploads/test.png'
  ],
  photoThumbs: [
    '/uploads/test.thumb.png'
  ],
  user: { // 작성자
    id: 1,
    nickname: 'nickname',
    photos: [],
    photoThumbs: [],
    photoPrimary: 0,
  },
  createdAt: '2017-01-01T00:00:00.000Z', // 생성 시각
  updatedAt: '2017-01-01T00:00:00.000Z', // 업데이트 시각
}
```

### Comment
```js
{
  id: 1, // 고유 번호
  body: 'body', // 내용
  user: { // 작성자
    id: 1,
    nickname: 'nickname',
    photos: [],
    photoThumbs: [],
    photoPrimary: 0,
  },
  createdAt: '2017-01-01T00:00:00.000Z', // 생성 시각
  updatedAt: '2017-01-01T00:00:00.000Z', // 업데이트 시각
}
```
